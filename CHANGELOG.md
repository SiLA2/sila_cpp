# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres
to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

<!--
Types of changes

    `Added` for new features.
    `Changed` for changes in existing functionality.
    `Deprecated` for soon-to-be removed features.
    `Removed` for now removed features.
    `Fixed` for any bug fixes.
    `Security` in case of vulnerabilities.
-->

## [Unreleased]

See the fragment files in the [changelog.d directory](changelog.d/).

<!-- scriv-insert-here -->

<a id='changelog-v0.3.10'></a>

## [v0.3.10] &ndash; 2023-12-07

### Fixed

- Revert conversion of ranged-`for` loop to `for_each` in SiLAClient.cpp because this changed the behavior of the loop

<a id='changelog-0.3.9'></a>

## [v0.3.9] &ndash; 2023-12-05

### Added

- Update codegen lib to be compatible with Qt6 (mostly replace `QMap` with `QMultiMap`)

- Add support for building with Qt6 in addition to Qt5

- `QDebug operator<<` overload for `CDynamicSiLAClient`

- `ICommandManager::setExecutor` can now also be called with a `const` member
  function pointer

### Changed

- Make default c'tors of all `CFullyQualified<...>Identifier` classes `public`

- All usages of Server UUIDs consistently use `QUuid` now instead of `QString` (Qt6 removed the implicit conversion from `QString` to `QUuid`)

- All `enum`s are now `enum class`es
- Naming of `enum`s is now consistent and doesn't use an 'e' as a prefix anymore
  (for backwards compatibility the old name is kept but marked `[[deprecated]]`)
- Naming of `enum` values is now consistently CamelCase (C++ Core Guidelines
  Enum.5; for backwards compatibility the old names are still available but
  marked `[[deprecated]]`)
- The underlying type of most `enum`s is now `uint8_t` since this saves space
- Some member functions have been made `const` if possible
- Add missing `explicit` and `noexcept` to c'tors
- A whole lot of code quality/readability/correctness/consistency improvements
  internally

### Fixed

- Fix potential crash in `CDynamicCommand::callCommand` for Observable Commands

- Fix memory leak in `CDynamicCommand::start`

- Data types test cases to work with the updated library for Qt6

- Detection whether the tests need a server (excluded test cases or tags using catch's '~' syntax is now correctly taken into account and no server is started when the *showHelp* option is specified)

- On Windows `.dll.a` files are now correctly installed into `<INSTALL_PREFIX>/lib` again

- The CMake option `BUILD_TESTING` now works as it should and all references to the previous option `SILA_CPP_BUILD_TESTS` have been removed

- Add missing implementations for `CDynamicFeatureStub::metadata` overloads

- `CDynamicValue`'s move c'tor and move assignment operator (which caused crashes when using `CDynamicValue`s in Qt6 `QList`s which now uses move operations instead of copies in Qt5)

<a id='changelog-v0.3.8'></a>

## [v0.3.8] &ndash; 2023-07-13

### Fixed

- Fix git tag and revision parsing in CMakeLists.txt for tagged commits

<a id='changelog-v0.3.7'></a>

## [v0.3.7] &ndash; 2023-07-13

### !!! BREAKING CHANGE !!!

- `CTimezone::fromQTimeZone` is now `CTimezone::fromQDateTime` to fix the issue that the DST offset was not taken into account properly (because the previous implementation was wrong I decided to not keep it for backwards compatibility - please update any existing code to use the new correct implementation)

### Removed

- #54: `CSiLAServer` and `CSiLAClient` don't force users to use `QCommandLineParser` any more (CLI parsing must be done in the applications that use sila_cpp)

### Added

- SiLA Server Discovery errors are now passed on to the user by `CServerManager`
- Comparison operators `operator==` and `operator!=` for FDL classes to make the internal FDL representation comparable
- `CServerManager::contains` to check if the Manager contains an entry for a Server with a given UUID
- `CFullyQualifiedIdentifierRegex` class as a helper for dealing with Fully Qualified Identifier constraints
- `serverConnected` signal for `CServerManager`

### Changed

- SiLA Server Discovery TXT records for the server's root CA certificate are now separated line by line (as per the most recent update to Part B)
- The `serviceUpdated` signal from QtZeroConf is now properly used
- Change SiLA Service implementation functions in `CSiLAClient` to be `const`
- `CDynamicSiLAClient::setAcceptUntrustedServerCertificateHandler` now returns the previous handler when setting a new handler
- The `sila_cpp_common` library is now a normal library instead of an object library to fix build errors in the tests
- `CDynamicValue::dataType` is now `public` to allow consumers in-depth introspection access to the Data Type
- Starting Subscriptions can now cause a timeout if the Server takes too long to before sending the first Property value - this validates the standard and is thus considered an error
- `CServerManager::connect` will now produce better error messages in case of a UUID mismatch
- Add more fields to the self-signed X509 certificate to improve interoperability (thanks @nmrkr (Delf Neumärker))

### Fixed

- #55: `CBinary` + `CString` truncates data due to incorrect use of `std::string_view` in the constructor (thanks for reporting @nmrkr (Delf Neumärker))
- Potential crash due to uncaught exceptions when destroying an Observable `CDynamicProperty` and its Subscriptions
- The automatic cancellation of a Subscription to an Observable Property when using `CDynamicProperty::get` in a thread that has no event loop now works as well
- Fix deserialization of Data Types with a Set constraint
- Fix `CDynamicValue::setValue` for `CDynamicStructure` in case the Elements of the Structure are Constrained or Lists
- Fix self-signed X.509 certificate version field (thanks @nmrkr (Delf Neumärker))

<a id='changelog-v0.3.6'></a>

## [v0.3.6] &ndash; 2022-08-23

### Added

- Using scriv for changelog management

### Changed

- #52/!23: Server-side API for controlling if the server should be started with/without encryption
  - Removed CLI option '--allow-insecure-communication' and c'tor parameter `AllowInsecureCommunication`
  - Added CLI option '--force-insecure' and new `CSiLAServer::runInsecure` function
  - Providing server credentials is not done via c'tor parameter any more but via a parameter to `run`
  - There are two overloads of `run`: the first without credentials will create a self-signed certificate on the fly, the second accepts credentials as a parameter
  - If self-signed credentials cannot be created and there are no user-provided credentials, `run` will throw an exception instead of falling back to insecure communication

- #52/!23: Client-side API for controlling if the client should use encryption
  - Removed CLI option '--allow-insecure-communication' and c'tor parameter `AllowInsecureCommunication`
  - Added CLI option '--force-insecure' and new `CSiLAClient::connectInsecure` function
  - Providing credentials is not done via c'tor parameter any more but via a parameter to `connect` and there is a new method called `connectInsecure`
    - Calling `connect` without credentials will try to use the certificate that the server provides in the TXT records of its discovery announcement
    - Calling `connect` with credentials will use those to connect to the server
    - Calling `connectInsecure` will not use credentials and connect to the server using unencrypted communication
  - `CDynamicSiLAClient` behaves the same way as `CSiLAClient`
  - `CServerManager` has learned similar methods, namely `connect`/`addAndConnect` (accept an optional certificate and use encryption), `connectInsecure`/`addAndConnectInsecure` (don't use a certificate and don't use encryption)
  - The methods that allowed the Server Manager to fall back to insecure communication have been removed in favor of the new methods
  - Empty `CSSLCredentials` will not result in `grpc::InsecureChannelCredentials` as before but instead use the OS's default trust store

- Tests use insecure communication since there seems to be a problem with the CI container when encryption is used

- `CSiLAServer::shutdown` now only uses `QCoreApplication::quit` if the server was `run` with `block == true`

- Add project_options (<https://github.com/aminya/project_options>) and use it for enabling coverage, project warnings and conan
- CodeCoverage.cmake (from <https://github.com/bilke/cmake-modules>) is now downloaded if necessary instead of having to keep a copy of the file in our repo

- Change how users tell sila_cpp that Parameter Validation was successful:
  call the new `setValidationSuccessful()` function of the `CObservableCommandWrapper` that is passed to every
  Observable Command Executor function after you've validated all parameters (the implicit way by sending a Execution
  Info will still work but it discouraged as the new way is more explicit)

### Fixed

- #51: Compilation error with GCC12

- #53/!22: Fix conversions from `grpc::string_ref` (thanks @nmrkr (Delf Neumärker))

## [v0.3.5]

### Added

- `CServerManager`'s `connect` and `addAndConnect` methods now accept an optional
  parameter for the certificate that should be used when connecting to a Server.

### Changed

- `polymorphic_value` now comes from conan
- Update build process
  - the CMake option `SILA_CPP_BUILD_TESTS` is now called `BUILD_TESTING` which
    is the more standard way to enable building the tests

### Fixed

- `sila_cpp_tests` will now also start the Test Server if no specific tests or
  tags were specified (as this means to run all tests, i.e. the test requiring
  a server will also be run)
- Custom Data Type definitions that use other Custom Data Types can be correctly
  resolved now
- `CDynamicValue` conversion to an unconstrained value and to a list work
  properly now

## [v0.3.4]

### Added

- `CommandStatus` and `CExecutionInfo` have overloads for `std::ostream operator<<`
  now
- `CServerManager` now emits `started` and `stopped` signals and has a function
  to check if the Manager has been started
- The SSL certificate of a Server can now be retrieved from the Server Manager
- `CSiLAClient` also gained a new function that returns the credentials of the
  client that were (or will be) used to connect to a Server

### Fixed

- Potential crash in consumer applications due to logging messages using the
  `qDebug` macros when the Log Manager has already been destroyed. To mitigate
  this the Log Manager will now re-install the previous message handler upon its
  destruction.

## [v0.3.3]

### Changed

- The `sila_cpp_tests` application will now only start a SiLA Server if the test
  case or scenario requires this. The requirement for a Server is indicated with
  the `[needs-server]` tag in its name *and* its tags.
- GitLab CI/CD can now report coverage values again
- GitLab CI/CD doesn't need to generate a version badge for the README.md anymore
  as this is now directly supported by GitLab

### Fixed

- Ignoring of SSL errors in the SiLA Client now works as intended

## [v0.3.2]

### Added

- #13: SiLA Client Metadata can now be used conveniently on the Server side, as
  well
- The `CDiscovery` class now supports SiLA 2 v1.1 TXT records that contain the
  self-signed certificate of the Server
- New logging category for the Dynamic Client heartbeat:
  `sila_cpp.client.heartbeat`, configurable via the
  `SILA_CPP_CLIENT_HEARTBEAT_LOGGING_LEVEL` environment variable

### Changed

- `CServerManager::connect()` will now refuse a connection when the UUID reported
  by Discovery differs from what the Server's SiLA Service Feature returns
- The self-signed certificate generation now conforms to SiLA 2 v1.1 (i.e. the
  generated certificate has the *Common Name* set to *SiLA2* and it contains the
  UUID of the Server so that Clients can verify the identity of a discovered
  Server)
- The heartbeat for Dynamic Clients has been changed to better detect when a
  Server has been shut down
- QtZeroConf has been updated to the latest upstream version **(you need to run
  `git submodule update` after pulling the new version of sila_cpp)**

### Fixed

- Fully Qualified Defined Execution Error Identifiers wrongly expected the
  Identifier to contain the string *"DefinedError"* instead of
  *"DefinedExecutionError"* (which is actually correct)

## [v0.3.1]

### Changed

- `CServerManager` does not remove Servers that disappeared from the network
  (either indicated by the discovery or the heartbeat mechanism), they have to be
  removed manually now

### Fixed

- Potential crash in `CSubscription` d'tor
- Re-add `emit serverDisappeared()` in `CServerManager`
- Fix maximum number of threads in the Subscription Thread Pool
- The timeout is now set on a per Call basis in `CDynamicCall` and not for all
  Calls as it was before
- Fix the order of Fully Qualified Identifiers returned by `CDynamicParameterList`,
  `CDynamicResponseList` and `CClientMetadataList`'s `identifiers()` function

## [v0.3.0]

### Added

- #42: Qt 5.10 is the minimum required Qt version; this version of sila_cpp adds
  a CMake configure-time check
- #20: Implementation of SiLA Any Type (this does not include conversion from/to
  a SiLA Structure Type but only SiLA Basic and SiLA List Types since there is
  no uniform representation of SiLA Structures in sila_cpp yet)
- `CSiLAError` and subclasses now provide a dedicated method to get the pure
  error message in addition to the standard `what()` inherited from
  `std::exception`
- All Data Type classes can now be constructed from a `google::protobuf::Message`
- The Date, Time, Timestamp and Timezone types can now be converted to and
  constructed from `QDateTime`/`QTimezone`
- #43/!19: Implementation of Dynamic Client
  - Using the `CSiLAServerManager` class you can automatically discover all
      Servers on your network and connect to them through a Dynamic Client
  - Using the `CDynamicSiLAClient` class you can connect to an unknown SiLA
      Server and use its Features without having to know them at compile time
  - Every SiLA Data Type (Basic Types as well as Constrained/List/Structure/
      Custom Types) can be used by the Dynamic Client
- !19: Implementation of a code generator library that converts FDL to protobuf
  (for the creation of dynamic stubs for the Dynamic Client)
- Dynamic Clients obtained from the Server Manager are monitored by a heartbeat
  to more reliably determine whether a Server is still available on the network
  or not

### Changed

- #40: You can now install your own QtMessageHandler to format your log messages;
  sila_cpp will only format its own messages from now on
- Setting message handlers and logging messages is now done through the new
  `CLogManager` class instead of plain free functions (You can also install a
  different message handler to handle all log messages from sila_cpp now)
- There are more logging categories for finer control over what to log and at
  which severity level (see [the wiki] for more info)
- QtZeroConf submodule now uses the upstream version **(you need to run
  `git submodule update --remote && git submodule update` after pulling the new
  version of sila_cpp)**
- SiLA Servers now announce themselves using additional attributes as specified
  by SiLA 2 v1.1
- sila_cpp uses correct Fully Qualified Identifiers everywhere (we don't try to
  deduce the Fully Qualified Identifiers from the gRPC service name anymore)
- "SiLA2" as Common Name (CN) of self-signed certificates:
  - When a Server is started without a custom SSL certificate and the Server
      generates a self-signed certificate on the fly it will have the Common
      Name (CN) set to "SiLA2" as specified by SiLA 2 v1.1 Part B
  - When a Client tries to connect to a Server it tries to set the SSL target
      name override to "SiLA2" so that gRPC won't fail with the dreaded "no
      match found for server name" error
- !20: Added explicit dependency to abseil by adding `find_package(absl)`
- `CSiLAClient`'s methods to call Commands or request Properties of the SiLA
  Service Feature now throw an exception (`CSiLAError`) if the operation couldn't
  be completed successfully

### Deprecated

- `CExecutionError`/`CDefinedExecutionError` and `CValidationError` gained new
  constructors that take a Fully Qualified Identifier instead of a plain string;
  the corresponding `string` versions have been deprecated and will be removed
  in the first stable version.

### Fixed

- #41: Crash on exit in SiLA Server
- !15: Crash when the Lifetime of Execution of an Observable Command is 0
- Fully Qualified Identifiers are now case-insensitive as specified by the
  standard ([*Uniqueness of Identifiers* in Part A])

## [v0.2.1]

### Fixed

- !6: Support for all gRPC versions (especially the most recent v1.32.0)
- !7/#37: Crash in TemperatureController (related to a timing issue when a
  streaming RPC got cancelled)
- #38: Resolve dependency cycle that prevented ninja to build sila_cpp
- #39: QtZeroConf installation works properly now **(you need to run
  `git submodule update` after pulling the new version of sila_cpp)**

## [v0.2.0]

### Added

- !3: Basic interoperability tests following the current proposal in
  sila_interoperability!1
- `SiLA2::CServerAddress` can now be constructed from `std::string`s in addition
  to the existing `QString` c'tor
- `SiLA2::CServer` will now automatically shut the internal gRPC server down on
  destruction
- The logging level can be restricted even more by setting it to "FATAL"
- #26: Any SiLA server application can now be shut down properly with Ctrl-C
  (resp. `SIGINT` on Unix or `CTRL_C_EVENT` on Windows) from the command line
- #33: The Lifetime of Execution of an Observable Command is now properly taken
  into account
- #12: SiLA Servers now advertise themselves in the network using ZeroConf
  (SiLA Server Discovery)
- !5/#14: All communication between server and client is now encrypted (we only
  fall back to insecure communication as the very last resort)
- #35: You can force the server to only use encrypted communication (it'll refuse
  to start when this can't be achieved)

### Changed

- `HelloSiLA2` now stores the FDL files in a .qrc file
- Naming in the `HelloSiLA2` example is now more C++-like (i.e. uses CamelCase
  for the application names rather than suffixes with underscores)
- Use gRPC version 1.31.0 (makes building gRPC easier for MinGW)
- Use Protobuf version 3.12.2 (this breaks the Data Types implementation; see
  next point)
- #32: Change implementation of Data Types (don't use inheritance from the
  Protobuf generated classes)
- Update `sila_base` (you need to run `git submodule update` after pulling the
  new version of sila_cpp)
- `SiLA2::CSiLAClient` now has a much easier to work with interface as it uses
  SiLA convenience Data Types as parameters and return values instead of the
  ugly Protobuf Message types
- Time-/Date-related data types now check if they're given a valid time/date
- SiLA servers and clients now have a command line parser with some default
  options that can be extended by subclasses

### Removed

- `CCommandConfirmation` data type class since it shouldn't be used by users (in
  the context of #32)

### Fixed

- #29: CMake tag version string parsing
- Almost all data types now have a default constructor
- AsyncRPCHandler: There won't be segmentation fault any more when an RPC gets
  cancelled while it's running in another thread.
- #30: Server crash when port is not available
- #34: An Observable Command that finished immediately after it was started
  won't cause the client hang anymore

## [v0.1.0]

### Added

- Basic library structure (`src`, `examples`, `test`) and CMake project setup
- Implementation of the following parts of the SiLA 2 standard
  - SiLA Server and Client creation through dedicated base classes
  - Implementation of mandatory `SiLAService` Feature as part of the Server
  - SiLA Feature implementation through dedicated base class
  - Convenient creation of Unobservable Commands and Properties
  - Convenient creation of Observable Commands and Properties
  - Mapping of SiLA Error Handling on C++ errors
  - Convenience classes for SiLA Data Types
- Simple `HelloSiLA2` example to showcase the usage of the library
  - Implementation of `GreetingProvider` Feature showing how to use
      Unobservable Commands/Properties
  - Implementation of `TemperatureController` Feature showing how to use
      Observable Commands/Properties

[Unreleased]: https://gitlab.com/SiLA2/sila_cpp/-/tree/master
[v0.3.10]: https://gitlab.com/SiLA2/sila_cpp/-/compare/v0.3.9...v0.3.10
[v0.3.9]: https://gitlab.com/SiLA2/sila_cpp/-/compare/v0.3.8...v0.3.9
[v0.3.8]: https://gitlab.com/SiLA2/sila_cpp/-/compare/v0.3.7...v0.3.8
[v0.3.7]: https://gitlab.com/SiLA2/sila_cpp/-/compare/v0.3.6...v0.3.7
[v0.3.6]: https://gitlab.com/SiLA2/sila_cpp/-/compare/v0.3.5...v0.3.6
[v0.3.5]: https://gitlab.com/SiLA2/sila_cpp/-/compare/v0.3.4...v0.3.5
[v0.3.4]: https://gitlab.com/SiLA2/sila_cpp/-/compare/v0.3.3...v0.3.4
[v0.3.3]: https://gitlab.com/SiLA2/sila_cpp/-/compare/v0.3.2...v0.3.3
[v0.3.2]: https://gitlab.com/SiLA2/sila_cpp/-/compare/v0.3.1...v0.3.2
[v0.3.1]: https://gitlab.com/SiLA2/sila_cpp/-/compare/v0.3.0...v0.3.1
[v0.3.0]: https://gitlab.com/SiLA2/sila_cpp/-/compare/v0.2.1...v0.3.0
[v0.2.1]: https://gitlab.com/SiLA2/sila_cpp/-/compare/v0.2.0...v0.2.1
[v0.2.0]: https://gitlab.com/SiLA2/sila_cpp/-/compare/v0.1.0...v0.2.0
[v0.1.0]: https://gitlab.com/SiLA2/sila_cpp/-/releases/v0.1.0

[the wiki]: https://gitlab.com/SiLA2/sila_cpp/-/wikis/home
[*Uniqueness of Identifiers* in Part A]: https://docs.google.com/document/d/1nGGEwbx45ZpKeKYH18VnNysREbr1EXH6FqlCo03yASM/edit#heading=h.gq7yg5m5tagq
