/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SiLAFeature.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   26.01.2021
/// \brief  Implementation of the ISiLAFeature class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/server/SiLAFeature.h>
#include <sila_cpp/server/SiLAServer.h>

#include <QCoreApplication>
#include <QFile>

static const auto QRC_PREFIX = QStringLiteral(":");
static const auto FDL_FILE_DIR_PREFIX = QStringLiteral("/meta/");
static const auto FDL_FILE_EXTENSION = QStringLiteral(".sila.xml");

namespace SiLA2
{
//=============================================================================
ISiLAFeature::ISiLAFeature(CSiLAServer* parent) : m_Server{parent}
{}

//============================================================================
ISiLAFeature::~ISiLAFeature() = default;

//=============================================================================
CSiLAServer* ISiLAFeature::server() const
{
    return m_Server;
}

//=============================================================================
QByteArray ISiLAFeature::featureDefinition() const
{
    const auto FilePath = FDL_FILE_DIR_PREFIX
                          + fullyQualifiedIdentifier().identifier()
                          + FDL_FILE_EXTENSION;
    auto File = QFile{QRC_PREFIX + FilePath};
    if (!File.open(QFile::ReadOnly | QFile::Text))
    {
        // try from normal file name if qrc didn't work
        File.setFileName(QCoreApplication::applicationDirPath().append(FilePath));
        if (!File.open(QFile::ReadOnly | QFile::Text))
        {
            qCWarning(sila_cpp_server)
                << "Could not open FDL file (" << File.fileName()
                << ") for reading of feature definition!";
            return {};
        }
    }

    return File.readAll();
}
}  // namespace SiLA2
