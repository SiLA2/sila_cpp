/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SSLCredentials.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   21.09.2020
/// \brief  Implementation of the CSSLCredentials class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/SSLCredentials.h>
#include <sila_cpp/common/logging.h>
#include <sila_cpp/internal/HostInfo.h>

#include "SelfSignedCertificateHelper.h"

#include <QFile>

#include <grpcpp/security/credentials.h>
#include <grpcpp/security/server_credentials.h>

using namespace std;

namespace SiLA2
{
/**
 * @brief Helper to read the contents of the file given by @a Filename
 *
 * @param Filename The name of the file to read
 *
 * @return The contents of the file or an empty string if the file could not be
 * opened
 */
string readFile(QStringView Filename)
{
    if (Filename.empty())
    {
        return ""s;
    }

    auto File = QFile{Filename.toString()};
    if (!File.open(QFile::ReadOnly))
    {
        qCWarning(sila_cpp_common) << "Cannot open file" << File.fileName();
        return ""s;
    }
    return File.readAll().toStdString();
}

//============================================================================
CSSLCredentials::CSSLCredentials(std::string RootCA) : m_RootCA{std::move(RootCA)}
{}

//============================================================================
CSSLCredentials::CSSLCredentials(string Key, string Certificate,
                                 std::string RootCA)
    : m_RootCA{std::move(RootCA)},
      m_KeyCertPairs{{std::move(Key), std::move(Certificate)}}
{}

//============================================================================
void CSSLCredentials::setRootCA(const std::string& RootCA)
{
    m_RootCA = RootCA;
}

//============================================================================
void CSSLCredentials::setRootCAFromFile(QStringView FileName)
{
    if (const auto RootCA = readFile(FileName); !RootCA.empty())
    {
        setRootCA(RootCA);
    }
}

//============================================================================
string CSSLCredentials::rootCA() const
{
    return m_RootCA;
}

//============================================================================
void CSSLCredentials::setCertificate(const std::string& Certificate)
{
    m_KeyCertPairs[0].Certificate = Certificate;
}

//============================================================================
void CSSLCredentials::setCertificateFromFile(QStringView FileName)
{
    if (const auto Certificate = readFile(FileName); !Certificate.empty())
    {
        setCertificate(Certificate);
    }
}

//============================================================================
string CSSLCredentials::certificate() const
{
    return m_KeyCertPairs.at(0).Certificate;
}

//============================================================================
void CSSLCredentials::setKey(const std::string& Key)
{
    m_KeyCertPairs[0].Key = Key;
}

//============================================================================
void CSSLCredentials::setKeyFromFile(QStringView FileName)
{
    if (const auto Key = readFile(FileName); !Key.empty())
    {
        setKey(Key);
    }
}

//============================================================================
string CSSLCredentials::key() const
{
    return m_KeyCertPairs.at(0).Key;
}

//============================================================================
bool CSSLCredentials::isEmpty() const
{
    return m_RootCA.empty() && certificate().empty() && key().empty();
}

//============================================================================
bool CSSLCredentials::isSelfSigned() const
{
    return m_RootCA == certificate();
}

//============================================================================
shared_ptr<grpc::ServerCredentials> CSSLCredentials::toServerCredentials() const
{
    if (certificate().empty() || key().empty())
    {
        qCInfo(sila_cpp_common) << "Using insecure communication";
        return grpc::InsecureServerCredentials();
    }
    qCInfo(sila_cpp_common) << "Using secure communication";

    using PemKeyCertPair = grpc::SslServerCredentialsOptions::PemKeyCertPair;
    auto Opts = grpc::SslServerCredentialsOptions{
        GRPC_SSL_DONT_REQUEST_CLIENT_CERTIFICATE};
    Opts.pem_root_certs = m_RootCA;
    for_each(begin(m_KeyCertPairs), end(m_KeyCertPairs),
             [&Opts](const auto Pair) {
                 Opts.pem_key_cert_pairs.emplace_back(
                     PemKeyCertPair{Pair.Key, Pair.Certificate});
             });
    return grpc::SslServerCredentials(Opts);
}

//============================================================================
shared_ptr<grpc::ChannelCredentials> CSSLCredentials::toChannelCredentials() const
{
    return m_RootCA.empty() ?
               grpc::SslCredentials({}) :  ///< uses system default trust store
               grpc::SslCredentials({m_RootCA, key(), certificate()});
}

//============================================================================
CSSLCredentials CSSLCredentials::fromFile(QStringView CertificateFileName,
                                          QStringView KeyFileName,
                                          QStringView RootCAFileName)
{
    return CSSLCredentials{readFile(KeyFileName), readFile(CertificateFileName),
                           readFile(RootCAFileName)};
}

//============================================================================
CSSLCredentials CSSLCredentials::fromFile(QStringView RootCAFileName)
{
    return CSSLCredentials{readFile(RootCAFileName)};
}

//============================================================================
CSSLCredentials CSSLCredentials::fromSelfSignedCertificate(
    const QUuid& ServerUUID, const QString& IP)
{
    qCInfo(sila_cpp_common).noquote()
        << "Creating self-signed certificate for server with UUID" << ServerUUID
        << "and IP" << IP;
    const auto Key = generateKey();
    const auto CertString =
        certificateToString(generateCertificate(Key, "SiLA2"s, IP, ServerUUID));
    return CSSLCredentials{keyToString(Key), CertString, CertString};
}

//============================================================================
CSSLCredentials::CSSLCredentials(std::vector<KeyCertPair> KeyCertPairs,
                                 std::string RootCA)
    : m_RootCA{std::move(RootCA)}, m_KeyCertPairs{std::move(KeyCertPairs)}
{}
}  // namespace SiLA2
