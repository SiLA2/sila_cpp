/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   FullyQualifiedDefinedErrorID.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   06.10.2021
/// \brief  Implementation of the CFullyQualifiedDefinedErrorID class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/FullyQualifiedDefinedErrorID.h>
#include <sila_cpp/common/logging.h>

#include <QStringList>

using namespace std;

namespace SiLA2
{
//============================================================================
CFullyQualifiedDefinedErrorID::CFullyQualifiedDefinedErrorID(
    const CFullyQualifiedFeatureID& FeatureID, string_view Identifier)
    : CFullyQualifiedFeatureID{FeatureID}, m_Identifier{Identifier.data()}
{}

//============================================================================
CFullyQualifiedDefinedErrorID::CFullyQualifiedDefinedErrorID(
    const CFullyQualifiedFeatureID& FeatureID, QStringView Identifier)
    : CFullyQualifiedFeatureID{FeatureID}, m_Identifier{Identifier.toString()}
{}

//============================================================================
CFullyQualifiedDefinedErrorID CFullyQualifiedDefinedErrorID::fromString(
    const QString& from)
{
    return fromStdString(from.toStdString());
}

//============================================================================
CFullyQualifiedDefinedErrorID CFullyQualifiedDefinedErrorID::fromStdString(
    const string& from)
{
    using PartIndex = FullyQualifiedIdentifierPartIndex;
    const auto List = QString::fromStdString(from).split('/');
    if (List.size()
            != PartIndex::FullyQualifiedDefinedExecutionErrorIdentifierLength
        || List.at(+PartIndex::DefinedExecutionError).toLower()
               != "definedexecutionerror")
    {
        qCWarning(sila_cpp_common).nospace()
            << "The given string \"" << from
            << "\" is not in the correct format of a Fully Qualified Defined "
               "Execution Error Identifier";
        return CFullyQualifiedDefinedErrorID{};
    }
    return CFullyQualifiedDefinedErrorID{
        {List.at(+PartIndex::Originator), List.at(+PartIndex::Category),
         List.at(+PartIndex::FeatureIdentifier), List.at(+PartIndex::Version)},
        List.at(+PartIndex::DefinedExecutionErrorIdentifier)};
}

//============================================================================
bool CFullyQualifiedDefinedErrorID::operator==(
    const CFullyQualifiedDefinedErrorID& rhs) const
{
    return featureIdentifier() == rhs.featureIdentifier()
           && m_Identifier.toLower() == rhs.m_Identifier.toLower();
}

//============================================================================
CFullyQualifiedDefinedErrorID
CFullyQualifiedDefinedErrorID::definedErrorIdentifier() const
{
    return *this;
}

//============================================================================
QString CFullyQualifiedDefinedErrorID::identifier() const
{
    return m_Identifier;
}

//============================================================================
bool CFullyQualifiedDefinedErrorID::isValid() const
{
    return CFullyQualifiedFeatureID::isValid() && !m_Identifier.isEmpty();
}

//============================================================================
QString CFullyQualifiedDefinedErrorID::toString() const
{
    return CFullyQualifiedFeatureID::toString() + "/DefinedExecutionError/"
           + m_Identifier;
}

//============================================================================
string CFullyQualifiedDefinedErrorID::toStdString() const
{
    return toString().toStdString();
}
}  // namespace SiLA2
