/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   ConstraintContentType.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   01.02.2021
/// \brief  Implementation of the CConstraintContentType class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/fdl/ConstraintContentType.h>
#include <sila_cpp/common/logging.h>

#include "utils.h"

using namespace std;

namespace SiLA2::codegen::fdl
{
//============================================================================
QVariant CContentTypeParameter::toVariant() const
{
    return variantMultiMap({{"Attribute", m_Attribute}, {"Value", m_Value}});
}

//============================================================================
void CContentTypeParameter::fromVariant(const QVariant& from)
{
    const auto Map = from.value<QVariantMultiMap>();
    if (Q_UNLIKELY(Map.empty()))
    {
        throw runtime_error{"Failed to parse Feature Description! <ContentType> "
                            "constraint is empty."};
    }
    m_Attribute = Map.value("Attribute").toString();
    m_Value = Map.value("Value").toString();
}

//============================================================================
bool CContentTypeParameter::isEqual(const ISerializable& r) const
{
    const auto rhs = dynamic_cast<const CContentTypeParameter&>(r);
    return m_Attribute == rhs.m_Attribute && m_Value == rhs.m_Value;
}

//============================================================================
QVariant CConstraintContentType::toVariant() const
{
    auto Map = QVariantMultiMap{{"Type", m_ContentType}, {"Subtype", m_SubType}};
    insertIfNotEmpty(Map, "Parameters",
                     toReverseVariantMaps("Parameter", m_Parameters));
    return QVariant::fromValue(Map);
}

//============================================================================
void CConstraintContentType::fromVariant(const QVariant& from)
{
    const auto Map = from.value<QVariantMultiMap>();
    m_ContentType = Map.value("Type").toString();
    m_SubType = Map.value("Subtype").toString();
    fromVariantList(Map.value("Parameters").toList(), m_Parameters);
}

//============================================================================
bool CConstraintContentType::isEqual(const ISerializable& r) const
{
    const auto rhs = dynamic_cast<const CConstraintContentType&>(r);
    return m_ContentType == rhs.m_ContentType && m_SubType == rhs.m_SubType
           && m_Parameters == rhs.m_Parameters;
}

//============================================================================
QDebug operator<<(QDebug dbg, const CContentTypeParameter& rhs)
{
    return dbg << rhs.toString();
}

//============================================================================
std::ostream& operator<<(std::ostream& os, const CContentTypeParameter& rhs)
{
    return os << rhs.toString();
}

//============================================================================
QDebug operator<<(QDebug dbg, const CConstraintContentType& rhs)
{
    QDebugStateSaver s{dbg};
    return dbg << rhs.contentType() + "/" + rhs.subType()
               << ", Parameters: " << rhs.parameters();
}

//============================================================================
std::ostream& operator<<(std::ostream& os, const CConstraintContentType& rhs)
{
    return os << rhs.contentType() + "/" + rhs.subType()
              << ", Parameters: " << rhs.parameters();
}
}  // namespace SiLA2::codegen::fdl
