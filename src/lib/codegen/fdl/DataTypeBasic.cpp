/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   DataTypeBasic.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   27.01.2021
/// \brief  Implementation of the CDataTypeBasic class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/fdl/DataTypeBasic.h>
#include <sila_cpp/common/logging.h>

namespace SiLA2::codegen::fdl
{
//============================================================================
QVariant CDataTypeBasic::toVariant() const
{
    return m_Identifier;
}

//============================================================================
void CDataTypeBasic::fromVariant(const QVariant& from)
{
    const auto Identifier = from.toString();
    if (Q_UNLIKELY(Identifier.isEmpty()))
    {
        throw std::runtime_error{"Failed to parse Feature Description! <Basic> "
                                 "data type is empty."};
    }
    m_Identifier = Identifier;
}

//============================================================================
bool CDataTypeBasic::isEqual(const ISerializable& r) const
{
    const auto rhs = dynamic_cast<const CDataTypeBasic&>(r);
    return m_Identifier == rhs.m_Identifier;
}

//============================================================================
QDebug operator<<(QDebug dbg, const CDataTypeBasic& rhs)
{
    return dbg << rhs.identifier();
}

//============================================================================
std::ostream& operator<<(std::ostream& os, const CDataTypeBasic& rhs)
{
    return os << rhs.identifier();
}
}  // namespace SiLA2::codegen::fdl
