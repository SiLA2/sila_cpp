/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   FDLSerializer.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   26.01.2021
/// \brief  Implementation of the CFDLSerializer class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/fdl/FDLSerializer.h>
#include <sila_cpp/common/FullyQualifiedFeatureID.h>
#include <sila_cpp/common/logging.h>

#include "FDLErrorHandler.h"
#include "utils.h"

#include <QXmlStreamReader>

#include <xercesc/framework/MemBufInputSource.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>

using namespace std;

namespace SiLA2::codegen::fdl
{
/**
 * @brief This enum defines the used `QMetaType::Type`s used by `CFDLSerializer`
 */
enum class MetaTypeType : uint8_t
{
    QVariantMap = QMetaType::Type::QVariantMap,
    QVariantList = QMetaType::Type::QVariantList,
};

///===========================================================================
///                               SERIALIZATION
///===========================================================================
/**
 * @brief Write the given @c QVariant object @a Variant to the given XML stream
 * @a Stream using the @a NodeName as the XML tag for the element
 *
 * @param NodeName The XML tag for the given @a Variant
 * @param Variant The object to write to the stream
 * @param Stream The XML stream to write to
 */
void writeVariantToStream(const QString& NodeName, const QVariant& Variant,
                          QXmlStreamWriter& Stream);

/**
 * @brief Write the given @c QVariantMap object @a Variant to the given XML stream
 * @a Stream
 *
 * @param Variant The map to write to the stream
 * @param Stream The XML stream to write to
 */
void writeVariantMapToStream(const QVariant& Variant, QXmlStreamWriter& Stream);

/**
 * @brief Write the given @c QVariantList object @a Variant to the given XML
 * stream @a Stream
 *
 * @param Variant The list to write to the stream
 * @param Stream The XML stream to write to
 */
void writeVariantListToStream(const QVariant& Variant, QXmlStreamWriter& Stream);

/**
 * @brief Write the given text @a Variant to the given XML stream @a Stream
 *
 * @param Variant The text to write to the stream
 * @param Stream The XML stream to write to
 */
void writeTextToStream(const QVariant& Variant, QXmlStreamWriter& Stream);

//============================================================================
void writeVariantToStream(const QString& NodeName, const QVariant& Variant,
                          QXmlStreamWriter& Stream)
{
    qCDebug(sila_cpp_codegen) << NodeName << Variant;

    if (!NodeName.isEmpty())
    {
        Stream.writeStartElement(NodeName);
    }

#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    const auto TypeId = static_cast<int>(Variant.type());
#else
    const auto TypeId = Variant.typeId();
#endif

    if (TypeId == static_cast<int>(MetaTypeType::QVariantMap)
        || Variant.canConvert<QVariantMultiMap>())
    {
        writeVariantMapToStream(Variant, Stream);
    }
    else if (TypeId == static_cast<int>(MetaTypeType::QVariantList))
    {
        writeVariantListToStream(Variant, Stream);
    }
    else
    {
        writeTextToStream(Variant, Stream);
    }

    if (!NodeName.isEmpty())
    {
        Stream.writeEndElement();
    }
}

//============================================================================
void writeVariantMapToStream(const QVariant& Variant, QXmlStreamWriter& Stream)
{
    auto Map = Variant.value<QVariantMultiMap>();
    const auto Attributes = Map.values("attribute");
    for_each(cbegin(Attributes), cend(Attributes),
             [&Stream, &Map](const auto& Attribute) {
                 const auto AttributeMap =
                     Attribute.template value<QVariantMultiMap>();
                 Stream.writeAttribute(AttributeMap.firstKey(),
                                       AttributeMap.first().toString());
                 Map.remove(Map.key(Attribute));
             });

    for (auto it = Map.begin(); it != Map.end(); ++it)
    {
        writeVariantToStream(it.key(), it.value(), Stream);
    }
}

//============================================================================
void writeVariantListToStream(const QVariant& Variant, QXmlStreamWriter& Stream)
{
    const auto List = Variant.toList();
    for_each(cbegin(List), cend(List), [&Stream](const auto& el) {
        return writeVariantToStream("", el, Stream);
    });
}

//============================================================================
void writeTextToStream(const QVariant& Variant, QXmlStreamWriter& Stream)
{
    Stream.writeCharacters(Variant.toString());
}

//============================================================================
QString CFDLSerializer::serialize(const ISerializable& Element)
{
    return serialize(Element.name(), Element.toVariant());
}

//============================================================================
QString CFDLSerializer::serialize(const QString& ElementName,
                                  const QVariant& ElementVariant)
{
    QByteArray FDL;
    QXmlStreamWriter Stream{&FDL};
    Stream.setAutoFormatting(true);
    Stream.writeStartDocument();
    writeVariantToStream(ElementName, ElementVariant, Stream);
    Stream.writeEndDocument();
    qCDebug(sila_cpp_codegen()) << FDL.toStdString();
    return FDL;
}

///===========================================================================
///                              DESERIALIZATION
///===========================================================================

/**
 * @brief Maps strings to the @c Type they represent
 */
const QMap<QString, MetaTypeType> StringToType{
    {"Feature", MetaTypeType::QVariantMap},
    {"DataType", MetaTypeType::QVariantMap},
    {"List", MetaTypeType::QVariantMap},
    {"Structure", MetaTypeType::QVariantMap},
    {"Element", MetaTypeType::QVariantMap},
    {"Command", MetaTypeType::QVariantMap},
    {"Parameter", MetaTypeType::QVariantMap},
    {"Response", MetaTypeType::QVariantMap},
    {"IntermediateResponse", MetaTypeType::QVariantMap},
    {"Property", MetaTypeType::QVariantMap},
    {"DefinedExecutionError", MetaTypeType::QVariantMap},
    {"Metadata", MetaTypeType::QVariantMap},
    {"DefinedExecutionErrors", MetaTypeType::QVariantMap},
    {"DataTypeDefinition", MetaTypeType::QVariantMap},
    {"Constrained", MetaTypeType::QVariantMap},
    {"Constraints", MetaTypeType::QVariantMap},
    {"Set", MetaTypeType::QVariantMap},
    {"Unit", MetaTypeType::QVariantMap},
    {"UnitComponent", MetaTypeType::QVariantMap},
    {"ContentType", MetaTypeType::QVariantMap},
    {"Parameters", MetaTypeType::QVariantList},  ///< belongs to ContentType
    {"Schema", MetaTypeType::QVariantMap},
    {"AllowedTypes", MetaTypeType::QVariantList},
};

/**
 * @brief Read a @c QVariant object from the given XML stream @a Stream
 *
 * @param Stream The XML stream to read from
 * @return The read @c QVariant object
 */
QVariant readVariantFromStream(QXmlStreamReader& Stream);

/**
 * @brief Read a @c QVariantMap from the given stream @a Stream. The returned Map
 * will also contain all attributes of the current Stream Element.
 *
 * @param Stream The XML stream to read from
 * @return The read @c QVariantMap
 */
QVariant readVariantMapFromStream(QXmlStreamReader& Stream);

/**
 * @brief Read a @c QVariantList from the given stream @a Stream
 *
 * @param Stream The XML stream to read from
 * @return The read @c QVariantList
 */
QVariant readVariantListFromStream(QXmlStreamReader& Stream);

/**
 * @brief Read the text content of the current XML element from the given stream
 * @a Stream
 *
 * @param Stream The XML stream to read from
 * @return The text content of the current element
 */
QVariant readTextFromStream(QXmlStreamReader& Stream);

//============================================================================
QVariant readVariantFromStream(QXmlStreamReader& Stream)
{
    if (Stream.hasError())
    {
        throw std::runtime_error{"Could not parse Feature Definition! "
                                 "The following error occurred: "
                                 + Stream.errorString().toStdString()};
    }

    const auto TypeString = Stream.name().toString();
    qCDebug(sila_cpp_codegen) << TypeString;
    switch (StringToType.value(TypeString))
    {
    case MetaTypeType::QVariantMap:
        return readVariantMapFromStream(Stream);
    case MetaTypeType::QVariantList:
        return readVariantListFromStream(Stream);
    default:
        return readTextFromStream(Stream);
    }
}

//============================================================================
QVariant readVariantMapFromStream(QXmlStreamReader& Stream)
{
    qCDebug(sila_cpp_codegen) << "---" << Stream.name();
    QVariantMultiMap Map;

    const auto Attributes = Stream.attributes();
    for_each(cbegin(Attributes), cend(Attributes), [&Map](const auto& Attribute) {
        Map.insert(Attribute.name().toString(), Attribute.value().toString());
    });

    while (Stream.readNextStartElement())
    {
        Map.insert(Stream.name().toString(), readVariantFromStream(Stream));
    }
    //    qCDebug(sila_cpp_codegen) << "Map" << Map;
    return QVariant::fromValue(Map);
}

//============================================================================
QVariant readVariantListFromStream(QXmlStreamReader& Stream)
{
    qCDebug(sila_cpp_codegen) << "---" << Stream.name();
    QVariantList List;
    while (Stream.readNextStartElement())
    {
        List.append(readVariantFromStream(Stream));
    }
    //    qCDebug(sila_cpp_codegen) << "List" << List;
    return List;
}

//============================================================================
QVariant readTextFromStream(QXmlStreamReader& Stream)
{
    return Stream.readElementText();
}

//============================================================================
bool CFDLSerializer::deserialize(ISerializable& Element, const QString& FDL)
{
    QXmlStreamReader Stream{FDL};
    Stream.readNextStartElement();
    try
    {
        Element.fromVariant(readVariantFromStream(Stream));
    }
    catch (const runtime_error& err)
    {
        qCWarning(sila_cpp_codegen)
            << "Failed to deserialize Feature! Reason:" << err.what();
        return false;
    }
    return true;
}

//============================================================================
bool CFDLSerializer::validate(const CFullyQualifiedFeatureID& FeatureID,
                              const QString& FDL)
{
#ifdef Q_OS_WIN
    // currently, xerces-c does not support https:// URLs even with the winsock
    // network accessor and thus all of the following would always fail with the
    // error "unsupported protocol in URL"
    // (see https://issues.apache.org/jira/browse/XERCESC-2220)
    return true;
#else
    xercesc::XMLPlatformUtils::Initialize();
    xercesc::XercesDOMParser Parser;
    CFDLErrorHandler ErrorHandler{FDL};
    Parser.setErrorHandler(&ErrorHandler);
    Parser.setValidationScheme(xercesc::XercesDOMParser::Val_Always);
    Parser.setDoNamespaces(true);
    Parser.setDoSchema(true);
    Parser.setLoadSchema(true);
    Parser.setValidationSchemaFullChecking(true);

    const auto FDLString = FDL.toStdString();
    xercesc::MemBufInputSource FDLInputSource{
        reinterpret_cast<const XMLByte* const>(FDLString.c_str()),
        sizeof(XMLByte) * static_cast<ulong>(FDLString.size()),
        FeatureID.toStdString().c_str()};
    Parser.parse(FDLInputSource);
    if (Parser.getErrorCount() > 0)
    {
        qCWarning(sila_cpp_codegen)
            << "Validation of Feature Description for Feature" << FeatureID
            << "failed";
        return false;
    }
    qCDebug(sila_cpp_codegen) << "Validation of Feature Description for Feature"
                              << FeatureID << "successful";
    return true;
#endif
}
}  // namespace SiLA2::codegen::fdl
