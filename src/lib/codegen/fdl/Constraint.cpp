/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   Constraint.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   29.01.2021
/// \brief  Implementation of the CConstraint class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/fdl/Constraint.h>
#include <sila_cpp/codegen/fdl/ConstraintAllowedTypes.h>
#include <sila_cpp/codegen/fdl/ConstraintContentType.h>
#include <sila_cpp/codegen/fdl/ConstraintElementCount.h>
#include <sila_cpp/codegen/fdl/ConstraintExclusive.h>
#include <sila_cpp/codegen/fdl/ConstraintFullyQualifiedID.h>
#include <sila_cpp/codegen/fdl/ConstraintInclusive.h>
#include <sila_cpp/codegen/fdl/ConstraintLength.h>
#include <sila_cpp/codegen/fdl/ConstraintPattern.h>
#include <sila_cpp/codegen/fdl/ConstraintSchema.h>
#include <sila_cpp/codegen/fdl/ConstraintSet.h>
#include <sila_cpp/codegen/fdl/ConstraintUnit.h>
#include <sila_cpp/common/logging.h>

#include "utils.h"

namespace SiLA2::codegen::fdl
{
//============================================================================
QVariant CConstraint::toVariant() const
{
    if (!m_Constraint)
    {
        return {};
    }

    switch (m_Type)
    {
    case Type::Invalid:
        qCCritical(sila_cpp_codegen) << "Invalid Constraint Type!";
        break;
    case Type::Length:
        return variantMultiMap(
            {{typeToString(m_Type),
              dynamic_cast<CConstraintLength*>(m_Constraint)->toVariant()}});
    case Type::MinimalLength:
        return variantMultiMap(
            {{typeToString(m_Type),
              dynamic_cast<CConstraintMinimalLength*>(m_Constraint)
                  ->toVariant()}});
    case Type::MaximalLength:
        return variantMultiMap(
            {{typeToString(m_Type),
              dynamic_cast<CConstraintMaximalLength*>(m_Constraint)
                  ->toVariant()}});
    case Type::Set:
        return variantMultiMap(
            {{typeToString(m_Type),
              dynamic_cast<CConstraintSet*>(m_Constraint)->toVariant()}});
    case Type::Pattern:
        return variantMultiMap(
            {{typeToString(m_Type),
              dynamic_cast<CConstraintPattern*>(m_Constraint)->toVariant()}});
    case Type::MaximalExclusive:
        return variantMultiMap(
            {{typeToString(m_Type),
              dynamic_cast<CConstraintMaximalExclusive*>(m_Constraint)
                  ->toVariant()}});
    case Type::MaximalInclusive:
        return variantMultiMap(
            {{typeToString(m_Type),
              dynamic_cast<CConstraintMaximalInclusive*>(m_Constraint)
                  ->toVariant()}});
    case Type::MinimalExclusive:
        return variantMultiMap(
            {{typeToString(m_Type),
              dynamic_cast<CConstraintMinimalExclusive*>(m_Constraint)
                  ->toVariant()}});
    case Type::MinimalInclusive:
        return variantMultiMap(
            {{typeToString(m_Type),
              dynamic_cast<CConstraintMinimalInclusive*>(m_Constraint)
                  ->toVariant()}});
    case Type::Unit:
        return variantMultiMap(
            {{typeToString(m_Type),
              dynamic_cast<CConstraintUnit*>(m_Constraint)->toVariant()}});
    case Type::ContentType:
        return variantMultiMap(
            {{typeToString(m_Type),
              dynamic_cast<CConstraintContentType*>(m_Constraint)->toVariant()}});
    case Type::ElementCount:
        return variantMultiMap(
            {{typeToString(m_Type),
              dynamic_cast<CConstraintElementCount*>(m_Constraint)->toVariant()}});
    case Type::MinimalElementCount:
        return variantMultiMap(
            {{typeToString(m_Type),
              dynamic_cast<CConstraintMinimalElementCount*>(m_Constraint)
                  ->toVariant()}});
    case Type::MaximalElementCount:
        return variantMultiMap(
            {{typeToString(m_Type),
              dynamic_cast<CConstraintMaximalElementCount*>(m_Constraint)
                  ->toVariant()}});
    case Type::FullyQualifiedIdentifier:
        return variantMultiMap(
            {{typeToString(m_Type),
              dynamic_cast<CConstraintFullyQualifiedID*>(m_Constraint)
                  ->toVariant()}});
    case Type::Schema:
        return variantMultiMap(
            {{typeToString(m_Type),
              dynamic_cast<CConstraintSchema*>(m_Constraint)->toVariant()}});
    case Type::AllowedTypes:
        return variantMultiMap(
            {{typeToString(m_Type),
              dynamic_cast<CConstraintAllowedTypes*>(m_Constraint)->toVariant()}});
    }
    qCCritical(sila_cpp_codegen) << "Shouldn't get here!";
    return {};
}

//============================================================================
void CConstraint::fromVariant(const QVariant& from)
{
    const auto Map = from.value<QVariantMultiMap>();
    if (Q_UNLIKELY(Map.empty()))
    {
        throw std::runtime_error{"Failed to parse Feature Description! "
                                 "<Constraints> is empty."};
    }
    const auto& Key = Map.firstKey();
    m_Type = stringToType(Key);

    switch (m_Type)
    {
    case Type::Invalid:
        qCWarning(sila_cpp_codegen) << "Invalid Constraint Type!";
        break;
    case Type::Length:
        m_Constraint = new CConstraintLength;
        dynamic_cast<CConstraintLength*>(m_Constraint)
            ->fromVariant(Map.value(Key));
        break;
    case Type::MinimalLength:
        m_Constraint = new CConstraintMinimalLength;
        dynamic_cast<CConstraintMinimalLength*>(m_Constraint)
            ->fromVariant(Map.value(Key));
        break;
    case Type::MaximalLength:
        m_Constraint = new CConstraintMaximalLength;
        dynamic_cast<CConstraintMaximalLength*>(m_Constraint)
            ->fromVariant(Map.value(Key));
        break;
    case Type::Set:
        m_Constraint = new CConstraintSet;
        dynamic_cast<CConstraintSet*>(m_Constraint)->fromVariant(Map.value(Key));
        break;
    case Type::Pattern:
        m_Constraint = new CConstraintPattern;
        dynamic_cast<CConstraintPattern*>(m_Constraint)
            ->fromVariant(Map.value(Key));
        break;
    case Type::MaximalExclusive:
        m_Constraint = new CConstraintMaximalExclusive;
        dynamic_cast<CConstraintMaximalExclusive*>(m_Constraint)
            ->fromVariant(Map.value(Key));
        break;
    case Type::MaximalInclusive:
        m_Constraint = new CConstraintMaximalInclusive;
        dynamic_cast<CConstraintMaximalInclusive*>(m_Constraint)
            ->fromVariant(Map.value(Key));
        break;
    case Type::MinimalExclusive:
        m_Constraint = new CConstraintMinimalExclusive;
        dynamic_cast<CConstraintMinimalExclusive*>(m_Constraint)
            ->fromVariant(Map.value(Key));
        break;
    case Type::MinimalInclusive:
        m_Constraint = new CConstraintMinimalInclusive;
        dynamic_cast<CConstraintMinimalInclusive*>(m_Constraint)
            ->fromVariant(Map.value(Key));
        break;
    case Type::Unit:
        m_Constraint = new CConstraintUnit;
        dynamic_cast<CConstraintUnit*>(m_Constraint)->fromVariant(Map.value(Key));
        break;
    case Type::ContentType:
        m_Constraint = new CConstraintContentType;
        dynamic_cast<CConstraintContentType*>(m_Constraint)
            ->fromVariant(Map.value(Key));
        break;
    case Type::ElementCount:
        m_Constraint = new CConstraintElementCount;
        dynamic_cast<CConstraintElementCount*>(m_Constraint)
            ->fromVariant(Map.value(Key));
        break;
    case Type::MinimalElementCount:
        m_Constraint = new CConstraintMinimalElementCount;
        dynamic_cast<CConstraintMinimalElementCount*>(m_Constraint)
            ->fromVariant(Map.value(Key));
        break;
    case Type::MaximalElementCount:
        m_Constraint = new CConstraintMaximalElementCount;
        dynamic_cast<CConstraintMaximalElementCount*>(m_Constraint)
            ->fromVariant(Map.value(Key));
        break;
    case Type::FullyQualifiedIdentifier:
        m_Constraint = new CConstraintFullyQualifiedID;
        dynamic_cast<CConstraintFullyQualifiedID*>(m_Constraint)
            ->fromVariant(Map.value(Key));
        break;
    case Type::Schema:
        m_Constraint = new CConstraintSchema;
        dynamic_cast<CConstraintSchema*>(m_Constraint)
            ->fromVariant(Map.value(Key));
        break;
    case Type::AllowedTypes:
        m_Constraint = new CConstraintAllowedTypes;
        dynamic_cast<CConstraintAllowedTypes*>(m_Constraint)
            ->fromVariant(Map.value(Key));
        break;
    }
}

//============================================================================
bool CConstraint::isEqual(const ISerializable& r) const
{
    const auto rhs = dynamic_cast<const CConstraint&>(r);
    if (Q_UNLIKELY(!m_Constraint || !rhs.m_Constraint))
    {
        // shouldn't happen
        throw std::runtime_error{"m_Constraint is nullptr!"};
    }
    return IConstraint::isEqual(rhs) && *m_Constraint == *rhs.m_Constraint;
}

//============================================================================
CConstraintLength CConstraint::length() const
{
    if (!m_Constraint || m_Type != Type::Length)
    {
        throw CInvalidConstraintType{Type::Length};
    }
    return *(dynamic_cast<CConstraintLength*>(m_Constraint));
}

//============================================================================
CConstraintMinimalLength CConstraint::minimalLength() const
{
    if (!m_Constraint || m_Type != Type::MinimalLength)
    {
        throw CInvalidConstraintType{Type::MinimalLength};
    }
    return *(dynamic_cast<CConstraintMinimalLength*>(m_Constraint));
}

//============================================================================
CConstraintMaximalLength CConstraint::maximalLength() const
{
    if (!m_Constraint || m_Type != Type::MaximalLength)
    {
        throw CInvalidConstraintType{Type::MaximalLength};
    }
    return *(dynamic_cast<CConstraintMaximalLength*>(m_Constraint));
}

//============================================================================
CConstraintElementCount CConstraint::elementCount() const
{
    if (!m_Constraint || m_Type != Type::ElementCount)
    {
        throw CInvalidConstraintType{Type::ElementCount};
    }
    return *(dynamic_cast<CConstraintElementCount*>(m_Constraint));
}

//============================================================================
CConstraintMinimalElementCount CConstraint::minimalElementCount() const
{
    if (!m_Constraint || m_Type != Type::MinimalElementCount)
    {
        throw CInvalidConstraintType{Type::MinimalElementCount};
    }
    return *(dynamic_cast<CConstraintMinimalElementCount*>(m_Constraint));
}

//============================================================================
CConstraintMaximalElementCount CConstraint::maximalElementCount() const
{
    if (!m_Constraint || m_Type != Type::MaximalElementCount)
    {
        throw CInvalidConstraintType{Type::MaximalElementCount};
    }
    return *(dynamic_cast<CConstraintMaximalElementCount*>(m_Constraint));
}

//============================================================================
CConstraintMinimalExclusive CConstraint::minimalExclusive() const
{
    if (!m_Constraint || m_Type != Type::MinimalExclusive)
    {
        throw CInvalidConstraintType{Type::MinimalExclusive};
    }
    return *(dynamic_cast<CConstraintMinimalExclusive*>(m_Constraint));
}

//============================================================================
CConstraintMaximalExclusive CConstraint::maximalExclusive() const
{
    if (!m_Constraint || m_Type != Type::MaximalExclusive)
    {
        throw CInvalidConstraintType{Type::MaximalExclusive};
    }
    return *(dynamic_cast<CConstraintMaximalExclusive*>(m_Constraint));
}

//============================================================================
CConstraintMinimalInclusive CConstraint::minimalInclusive() const
{
    if (!m_Constraint || m_Type != Type::MinimalInclusive)
    {
        throw CInvalidConstraintType{Type::MinimalInclusive};
    }
    return *(dynamic_cast<CConstraintMinimalInclusive*>(m_Constraint));
}

//============================================================================
CConstraintMaximalInclusive CConstraint::maximalInclusive() const
{
    if (!m_Constraint || m_Type != Type::MaximalInclusive)
    {
        throw CInvalidConstraintType{Type::MaximalInclusive};
    }
    return *(dynamic_cast<CConstraintMaximalInclusive*>(m_Constraint));
}

//============================================================================
CConstraintFullyQualifiedID CConstraint::fullyQualifiedIdentifier() const
{
    if (!m_Constraint || m_Type != Type::FullyQualifiedIdentifier)
    {
        throw CInvalidConstraintType{Type::FullyQualifiedIdentifier};
    }
    return *(dynamic_cast<CConstraintFullyQualifiedID*>(m_Constraint));
}

//============================================================================
CConstraintPattern CConstraint::pattern() const
{
    if (!m_Constraint || m_Type != Type::Pattern)
    {
        throw CInvalidConstraintType{Type::Pattern};
    }
    return *(dynamic_cast<CConstraintPattern*>(m_Constraint));
}

//============================================================================
CConstraintSet CConstraint::set() const
{
    if (!m_Constraint || m_Type != Type::Set)
    {
        throw CInvalidConstraintType{Type::Set};
    }
    return *(dynamic_cast<CConstraintSet*>(m_Constraint));
}

//============================================================================
CConstraintAllowedTypes CConstraint::allowedTypes() const
{
    if (!m_Constraint || m_Type != Type::AllowedTypes)
    {
        throw CInvalidConstraintType{Type::AllowedTypes};
    }
    return *(dynamic_cast<CConstraintAllowedTypes*>(m_Constraint));
}

//============================================================================
CConstraintContentType CConstraint::contentType() const
{
    if (!m_Constraint || m_Type != Type::ContentType)
    {
        throw CInvalidConstraintType{Type::ContentType};
    }
    return *(dynamic_cast<CConstraintContentType*>(m_Constraint));
}

//============================================================================
CConstraintSchema CConstraint::schema() const
{
    if (!m_Constraint || m_Type != Type::Schema)
    {
        throw CInvalidConstraintType{Type::Schema};
    }
    return *(dynamic_cast<CConstraintSchema*>(m_Constraint));
}

//============================================================================
CConstraintUnit CConstraint::unit() const
{
    if (!m_Constraint || m_Type != Type::Unit)
    {
        throw CInvalidConstraintType{Type::Unit};
    }
    return *(dynamic_cast<CConstraintUnit*>(m_Constraint));
}

//============================================================================
QDebug operator<<(QDebug dbg, const CConstraint& rhs)
{
    using namespace SiLA2::codegen::fdl;
    QDebugStateSaver s{dbg};
    dbg.nospace() << "CConstraint(" << rhs.type() << ", ";

    if (auto* const Constraint = rhs.constraint(); !Constraint)
    {
        dbg << "no constraint";
    }
    else
    {
        switch (rhs.type())
        {
        case CConstraint::Type::Invalid:
            dbg << "invalid constraint";
            break;
        case CConstraint::Type::Length:
            dbg << *(dynamic_cast<CConstraintLength*>(Constraint));
            break;
        case CConstraint::Type::MinimalLength:
            dbg << *(dynamic_cast<CConstraintMinimalLength*>(Constraint));
            break;
        case CConstraint::Type::MaximalLength:
            dbg << *(dynamic_cast<CConstraintMaximalLength*>(Constraint));
            break;
        case CConstraint::Type::Set:
            dbg << *(dynamic_cast<CConstraintSet*>(Constraint));
            break;
        case CConstraint::Type::Pattern:
            dbg << *(dynamic_cast<CConstraintPattern*>(Constraint));
            break;
        case CConstraint::Type::MaximalExclusive:
            dbg << *(dynamic_cast<CConstraintMaximalExclusive*>(Constraint));
            break;
        case CConstraint::Type::MaximalInclusive:
            dbg << *(dynamic_cast<CConstraintMaximalInclusive*>(Constraint));
            break;
        case CConstraint::Type::MinimalExclusive:
            dbg << *(dynamic_cast<CConstraintMinimalExclusive*>(Constraint));
            break;
        case CConstraint::Type::MinimalInclusive:
            dbg << *(dynamic_cast<CConstraintMinimalInclusive*>(Constraint));
            break;
        case CConstraint::Type::Unit:
            dbg << *(dynamic_cast<CConstraintUnit*>(Constraint));
            break;
        case CConstraint::Type::ContentType:
            dbg << *(dynamic_cast<CConstraintContentType*>(Constraint));
            break;
        case CConstraint::Type::ElementCount:
            dbg << *(dynamic_cast<CConstraintElementCount*>(Constraint));
            break;
        case CConstraint::Type::MinimalElementCount:
            dbg << *(dynamic_cast<CConstraintMinimalElementCount*>(Constraint));
            break;
        case CConstraint::Type::MaximalElementCount:
            dbg << *(dynamic_cast<CConstraintMaximalElementCount*>(Constraint));
            break;
        case CConstraint::Type::FullyQualifiedIdentifier:
            dbg << *(dynamic_cast<CConstraintFullyQualifiedID*>(Constraint));
            break;
        case CConstraint::Type::Schema:
            dbg << *(dynamic_cast<CConstraintSchema*>(Constraint));
            break;
        case CConstraint::Type::AllowedTypes:
            dbg << *(dynamic_cast<CConstraintAllowedTypes*>(Constraint));
            break;
        }
    }
    return dbg << ')';
}

//============================================================================
std::ostream& operator<<(std::ostream& os, const CConstraint& rhs)
{
    using namespace SiLA2::codegen::fdl;
    os << "CConstraint(" << rhs.type() << ", ";

    if (auto* const Constraint = rhs.constraint(); !Constraint)
    {
        os << "no constraint";
    }
    else
    {
        switch (rhs.type())
        {
        case CConstraint::Type::Invalid:
            os << "invalid constraint";
            break;
        case CConstraint::Type::Length:
            os << *(dynamic_cast<CConstraintLength*>(Constraint));
            break;
        case CConstraint::Type::MinimalLength:
            os << *(dynamic_cast<CConstraintMinimalLength*>(Constraint));
            break;
        case CConstraint::Type::MaximalLength:
            os << *(dynamic_cast<CConstraintMaximalLength*>(Constraint));
            break;
        case CConstraint::Type::Set:
            os << *(dynamic_cast<CConstraintSet*>(Constraint));
            break;
        case CConstraint::Type::Pattern:
            os << *(dynamic_cast<CConstraintPattern*>(Constraint));
            break;
        case CConstraint::Type::MaximalExclusive:
            os << *(dynamic_cast<CConstraintMaximalExclusive*>(Constraint));
            break;
        case CConstraint::Type::MaximalInclusive:
            os << *(dynamic_cast<CConstraintMaximalInclusive*>(Constraint));
            break;
        case CConstraint::Type::MinimalExclusive:
            os << *(dynamic_cast<CConstraintMinimalExclusive*>(Constraint));
            break;
        case CConstraint::Type::MinimalInclusive:
            os << *(dynamic_cast<CConstraintMinimalInclusive*>(Constraint));
            break;
        case CConstraint::Type::Unit:
            os << *(dynamic_cast<CConstraintUnit*>(Constraint));
            break;
        case CConstraint::Type::ContentType:
            os << *(dynamic_cast<CConstraintContentType*>(Constraint));
            break;
        case CConstraint::Type::ElementCount:
            os << *(dynamic_cast<CConstraintElementCount*>(Constraint));
            break;
        case CConstraint::Type::MinimalElementCount:
            os << *(dynamic_cast<CConstraintMinimalElementCount*>(Constraint));
            break;
        case CConstraint::Type::MaximalElementCount:
            os << *(dynamic_cast<CConstraintMaximalElementCount*>(Constraint));
            break;
        case CConstraint::Type::FullyQualifiedIdentifier:
            os << *(dynamic_cast<CConstraintFullyQualifiedID*>(Constraint));
            break;
        case CConstraint::Type::Schema:
            os << *(dynamic_cast<CConstraintSchema*>(Constraint));
            break;
        case CConstraint::Type::AllowedTypes:
            os << *(dynamic_cast<CConstraintAllowedTypes*>(Constraint));
            break;
        }
    }

    return os << ')';
}
}  // namespace SiLA2::codegen::fdl
