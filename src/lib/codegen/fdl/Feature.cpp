/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   Feature.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   26.01.2021
/// \brief  Implementation of the CFeature class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/fdl/Feature.h>
#include <sila_cpp/common/logging.h>

#include "utils.h"

using namespace std;

namespace SiLA2::codegen::fdl
{
//============================================================================
QVariant CFeature::toVariant() const
{
    auto Map = CSiLAElement::toVariant().value<QVariantMultiMap>();

    Map.insert("attribute", variantMultiMap({{"Category", m_Category}}));
    Map.insert("attribute", variantMultiMap({{"Originator", m_Originator}}));
    Map.insert("attribute",
               variantMultiMap({{"FeatureVersion", m_FeatureVersion}}));
    Map.insert("attribute", variantMultiMap({{"SiLA2Version", m_SiLA2Version}}));

    Map.unite(toReverseVariantMaps("Command", m_Commands));
    Map.unite(toReverseVariantMaps("Property", m_Properties));
    Map.unite(toReverseVariantMaps("DefinedExecutionError", m_Errors));
    Map.unite(toReverseVariantMaps("Metadata", m_Metadata));
    Map.unite(toReverseVariantMaps("DataTypeDefinition", m_DataTypeDefinitions));

    return QVariant::fromValue(Map);
}

//============================================================================
void CFeature::fromVariant(const QVariant& from)
{
    CSiLAElement::fromVariant(from);
    //    qCDebug(sila_cpp_codegen) << from;
    const auto Map = from.value<QVariantMultiMap>();
    m_Originator = Map.value("Originator").toString();
    m_Category = Map.value("Category", "none").toString();
    if (Q_UNLIKELY(m_Category.isEmpty()))
    {
        m_Category = "none";
    }
    m_FeatureVersion = Map.value("FeatureVersion").toString();
    m_SiLA2Version = Map.value("SiLA2Version").toString();

    fromReverseVariantList(Map.values("Command"), m_Commands);
    fromReverseVariantList(Map.values("Property"), m_Properties);
    fromReverseVariantList(Map.values("DefinedExecutionError"), m_Errors);
    fromReverseVariantList(Map.values("Metadata"), m_Metadata);
    fromReverseVariantList(Map.values("DataTypeDefinition"),
                           m_DataTypeDefinitions);
    qCDebug(sila_cpp_codegen) << *this;
}

//============================================================================
bool CFeature::isEqual(const ISerializable& r) const
{
    const auto rhs = dynamic_cast<const CFeature&>(r);
    return CSiLAElement::isEqual(rhs) && m_Originator == rhs.m_Originator
           && m_Category == rhs.m_Category
           && m_FeatureVersion == rhs.m_FeatureVersion
           && m_SiLA2Version == rhs.m_SiLA2Version && m_Commands == rhs.m_Commands
           && m_Properties == rhs.m_Properties && m_Errors == rhs.m_Errors
           && m_Metadata == rhs.m_Metadata
           && m_DataTypeDefinitions == rhs.m_DataTypeDefinitions;
}

//============================================================================
QDebug operator<<(QDebug dbg, const CFeature& rhs)
{
    QDebugStateSaver s{dbg};
    return dbg.nospace()
           << "CFeature("
           << rhs.originator() + '.' + rhs.category() + '.' + rhs.identifier()
           << " ("
           << "v" + rhs.featureVersion() << "):"
           << "\n\tCommands: " << rhs.commands()
           << ",\n\tProperties: " << rhs.properties()
           << ",\n\tErrors: " << rhs.errors()
           << ",\n\tMetadata: " << rhs.metadata()
           << ",\n\tData Type Definitions: " << rhs.dataTypeDefinitions() << ')';
}

//============================================================================
ostream& operator<<(ostream& os, const CFeature& rhs)
{
    return os << "CFeature("
              << rhs.originator() + '.' + rhs.category() + '.' + rhs.identifier()
              << " ("
              << "v" + rhs.featureVersion() << "):"
              << "\n\tCommands: " << rhs.commands()
              << ",\n\tProperties: " << rhs.properties()
              << ",\n\tErrors: " << rhs.errors()
              << ",\n\tMetadata: " << rhs.metadata()
              << ",\n\tData Type Definitions: " << rhs.dataTypeDefinitions()
              << ')';
}
}  // namespace SiLA2::codegen::fdl
