/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   ConstraintSet.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   29.01.2021
/// \brief  Implementation of the CConstraintSet class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/fdl/ConstraintSet.h>
#include <sila_cpp/common/logging.h>

#include "utils.h"

using namespace std;

namespace SiLA2::codegen::fdl
{
//============================================================================
QVariant CConstraintSet::toVariant() const
{
    QVariantMultiMap Map;
    for_each(crbegin(m_Values), crend(m_Values),
             [&Map](const auto& el) { Map.insert("Value", el); });
    return QVariant::fromValue(Map);
}

//============================================================================
void CConstraintSet::fromVariant(const QVariant& from)
{
    const auto Map = from.value<QVariantMultiMap>();
    if (Q_UNLIKELY(Map.empty()))
    {
        throw std::runtime_error{"Failed to parse Feature Description! <Set> "
                                 "constraint is empty."};
    }
    m_Values.reserve(Map.size());
    // Elements in the Map are ordered from the most recently inserted (i.e. the
    // actual last) item to the least recently inserted (i.e. the actual first)
    // item
    transform(begin(Map), end(Map), front_inserter(m_Values),
              [](const auto& Variant) { return Variant.toString(); });
}

//============================================================================
bool CConstraintSet::isEqual(const ISerializable& r) const
{
    const auto rhs = dynamic_cast<const CConstraintSet&>(r);
    return m_Values == rhs.m_Values;
}

//============================================================================
QDebug operator<<(QDebug dbg, const CConstraintSet& rhs)
{
    return dbg << rhs.values();
}

//============================================================================
std::ostream& operator<<(std::ostream& os, const CConstraintSet& rhs)
{
    return os << rhs.values();
}
}  // namespace SiLA2::codegen::fdl
