/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   ErrorCollector.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   13.01.2021
/// \brief  Implementation of the CErrorCollector class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include "ErrorCollector.h"

#include <sila_cpp/common/logging.h>

using namespace std;

namespace SiLA2::codegen::proto
{
//============================================================================
void CErrorCollector::AddError(const string& /*Filename*/, int Line, int Column,
                               const string& Message)
{
    qCCritical(sila_cpp_codegen).nospace()
        << "Dynamic proto file parsing: " << Line << ':' << Column << ": "
        << Message;
}

//============================================================================
void CErrorCollector::AddWarning(const string& /*Filename*/, int Line, int Column,
                                 const string& Message)
{
    qCWarning(sila_cpp_codegen).nospace()
        << "Dynamic proto file parsing: " << Line << ':' << Column << ": "
        << Message;
}
}  // namespace SiLA2::codegen::proto
