/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SiLATimezone.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   17.01.2020
/// \brief  Implementation of the CTimezone class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/logging.h>
#include <sila_cpp/framework/data_types/SiLATimezone.h>
#include <sila_cpp/framework/grpc/SiLAFramework.pb.h>

#include <QDateTime>

using sila2::org::silastandard::Timezone;
using TypePair = std::pair<int32_t, uint32_t>;

namespace SiLA2
{
/**
 * @brief Check if the timezone given by @a Hours and @a Minutes is valid
 *
 * @return true, if the timezone is valid, false otherwise
 */
static bool isValid(int32_t Hours, uint32_t Minutes)
{
    return CTimezone::toQTimeZone(Hours, Minutes).isValid()
           && abs(Hours) <= 12  // Qt accepts timezones with -14 <= hours <= +14
           && static_cast<int32_t>(Minutes) >= 0;  // SiLA requires positive
                                                   // minutes
}

//============================================================================
CTimezone::CTimezone(int32_t Hours, uint32_t Minutes)
    : CDataType<TypePair>{{Hours, Minutes}}
{
    if (!isValid(Hours, Minutes))
    {
        throw CInvalidTimezone{Hours, Minutes};
    }
}

//============================================================================
CTimezone::CTimezone(const Timezone& rhs)
    : CDataType<TypePair>{{rhs.hours(), rhs.minutes()}}
{}

//============================================================================
CTimezone CTimezone::fromProtoMessage(const google::protobuf::Message& from)
{
    Timezone Temp;
    Temp.CopyFrom(from);
    return Temp;
}

//============================================================================
CTimezone CTimezone::fromQDateTime(const QDateTime& dateTime)
{
    if (!dateTime.isValid())
    {
        qCWarning(sila_cpp_data_types) << "SiLA2::CTimezone::fromQDateTime "
                                          "called with an invalid QDateTime";
        return {};
    }

    static constexpr auto OneMinuteInSeconds = 60;
    static constexpr auto OneHourInSeconds =
        OneMinuteInSeconds * OneMinuteInSeconds;

    const auto Timezone = dateTime.timeZone();
    const auto Offset = Timezone.standardTimeOffset(dateTime)
                        + Timezone.daylightTimeOffset(dateTime);
    const auto AbsOffset = abs(Offset);
    const auto Sign = AbsOffset == 0 ? 1 : AbsOffset / Offset;
    return {Sign * (AbsOffset / OneHourInSeconds),
            static_cast<uint32_t>((AbsOffset % OneHourInSeconds)
                                  / OneMinuteInSeconds)};
}

//============================================================================
Timezone CTimezone::toProtoMessage() const
{
    auto Result = Timezone{};
    Result.set_hours(hours());
    Result.set_minutes(minutes());
    return Result;
}

//============================================================================
Timezone* CTimezone::toProtoMessagePtr() const
{
    auto* Result = new Timezone{};
    Result->set_hours(hours());
    Result->set_minutes(minutes());
    return Result;
}

//============================================================================
int32_t CTimezone::hours() const
{
    return value().first;
}

//============================================================================
void CTimezone::setHours(int32_t Hours)
{
    if (!isValid(Hours, minutes()))
    {
        throw CInvalidTimezone{Hours, CInvalidTimezone::TimezonePart::Hours};
    }

    auto tmp = value();
    tmp.first = Hours;
    setValue(tmp);
}

//============================================================================
uint32_t CTimezone::minutes() const
{
    return std::get<1>(value());
}

//============================================================================
void CTimezone::setMinutes(uint32_t Minutes)
{
    if (!isValid(hours(), Minutes))
    {
        throw CInvalidTimezone{static_cast<int32_t>(Minutes),
                               CInvalidTimezone::TimezonePart::Minutes};
    }

    auto tmp = value();
    tmp.second = Minutes;
    setValue(tmp);
}

//============================================================================
QTimeZone CTimezone::toQTimeZone() const
{
    return toQTimeZone(hours(), minutes());
}

//============================================================================
QTimeZone CTimezone::toQTimeZone(int32_t Hours, uint32_t Minutes)
{
    static constexpr auto OneMinuteInSeconds = 60;
    static constexpr auto OneHourInSeconds =
        OneMinuteInSeconds * OneMinuteInSeconds;

    const auto AbsHours = abs(Hours);
    const auto Sign = AbsHours == 0 ? 1 : AbsHours / Hours;
    return QTimeZone{Sign
                     * (AbsHours * OneHourInSeconds
                        + static_cast<int>(Minutes) * OneMinuteInSeconds)};
}

///===========================================================================
///                        CInvalidTimezone implementation
///===========================================================================
CInvalidTimezone::CInvalidTimezone(int32_t Hours, uint32_t Minutes)
{
    auto Stream = QTextStream{&m_Message};
    Stream << "Tried to construct an invalid SiLA2::CTimezone with hours = "
           << Hours << ", minutes = " << Minutes;
}

//============================================================================
CInvalidTimezone::CInvalidTimezone(int32_t Value, TimezonePart Part)
{
    const char* PartString = "unhandled timezone part";
    switch (Part)
    {
    case TimezonePart::Hours:
        PartString = "hours";
        break;
    case TimezonePart::Minutes:
        PartString = "minutes";
        break;
    }
    auto Stream = QTextStream{&m_Message};
    Stream << "Invalid " << PartString << " value " << Value
           << " for SiLA2::CTimezone";
}

//============================================================================
const char* CInvalidTimezone::what() const noexcept
{
    return qPrintable(m_Message);
}

//============================================================================
QDebug operator<<(QDebug dbg, const CTimezone& rhs)
{
    QDebugStateSaver s{dbg};
    return dbg.nospace() << "SiLA2::CTimezone(" << rhs.hours() << ':'
                         << rhs.minutes() << ')';
}

//============================================================================
std::ostream& operator<<(std::ostream& os, const CTimezone& rhs)
{
    return os << "SiLA2::CTimezone(" << rhs.hours() << ':' << rhs.minutes()
              << ')';
}
}  // namespace SiLA2
