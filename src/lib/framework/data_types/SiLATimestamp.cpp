/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SiLATimestamp.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   17.01.2020
/// \brief  Implementation of the CTimestampstamp class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/logging.h>
#include <sila_cpp/framework/data_types/SiLADate.h>
#include <sila_cpp/framework/data_types/SiLATime.h>
#include <sila_cpp/framework/data_types/SiLATimestamp.h>
#include <sila_cpp/framework/grpc/SiLAFramework.pb.h>

using sila2::org::silastandard::Timestamp;

namespace SiLA2
{
using TypeTuple = std::tuple<uint32_t, uint32_t, uint32_t, uint32_t, uint32_t,
                             uint32_t, CTimezone>;

//============================================================================
CTimestamp::CTimestamp(uint32_t Hour, uint32_t Minute, uint32_t Second,
                       uint32_t Day, uint32_t Month, uint32_t Year,
                       const CTimezone& Timezone)
    : CDataType<TypeTuple>{{Hour, Minute, Second, Day, Month, Year, Timezone}}
{
    if (!isValid(Hour, Minute, Second, Day, Month, Year))
    {
        throw CInvalidTimestamp{Hour, Minute, Second, Day, Month, Year};
    }
}

//============================================================================
CTimestamp::CTimestamp(const Timestamp& rhs)
    : CDataType<TypeTuple>{{rhs.hour(), rhs.minute(), rhs.second(), rhs.day(),
                            rhs.month(), rhs.year(), rhs.timezone()}}
{}

//============================================================================
CTimestamp CTimestamp::fromProtoMessage(const google::protobuf::Message& from)
{
    Timestamp Temp;
    Temp.CopyFrom(from);
    return Temp;
}

//============================================================================
CTimestamp CTimestamp::fromQDateTime(const QDateTime& from)
{
    if (!from.isValid())
    {
        qCWarning(sila_cpp_data_types) << "SiLA2::CTimestamp::fromQDateTime "
                                          "called with an invalid QDateTime";
        return {};
    }

    const auto Date = from.date();
    const auto Time = from.time();
    return {static_cast<uint32_t>(Time.hour()),
            static_cast<uint32_t>(Time.minute()),
            static_cast<uint32_t>(Time.second()),
            static_cast<uint32_t>(Date.day()),
            static_cast<uint32_t>(Date.month()),
            static_cast<uint32_t>(Date.year()),
            CTimezone::fromQDateTime(from)};
}

//============================================================================
Timestamp CTimestamp::toProtoMessage() const
{
    const auto [Hour, Minute, Second, Day, Month, Year, Timezone] = value();
    auto Result = Timestamp{};
    Result.set_hour(Hour);
    Result.set_minute(Minute);
    Result.set_second(Second);
    Result.set_day(Day);
    Result.set_month(Month);
    Result.set_year(Year);
    Result.set_allocated_timezone(Timezone.toProtoMessagePtr());
    return Result;
}

//============================================================================
Timestamp* CTimestamp::toProtoMessagePtr() const
{
    const auto [Hour, Minute, Second, Day, Month, Year, Timezone] = value();
    auto* Result = new Timestamp{};
    Result->set_hour(Hour);
    Result->set_minute(Minute);
    Result->set_second(Second);
    Result->set_day(Day);
    Result->set_month(Month);
    Result->set_year(Year);
    Result->set_allocated_timezone(Timezone.toProtoMessagePtr());
    return Result;
}

//============================================================================
uint32_t CTimestamp::hour() const
{
    return std::get<0>(value());
}

//============================================================================
void CTimestamp::setHour(uint32_t Hour)
{
    if (!isValid(Hour, minute(), second(), day(), month(), year()))
    {
        throw CInvalidTimestamp{Hour, CInvalidTimestamp::TimestampPart::Hour};
    }

    auto tmp = value();
    std::get<0>(tmp) = Hour;
    setValue(tmp);
}

//============================================================================
uint32_t CTimestamp::minute() const
{
    return std::get<1>(value());
}

//============================================================================
void CTimestamp::setMinute(uint32_t Minute)
{
    if (!isValid(hour(), Minute, second(), day(), month(), year()))
    {
        throw CInvalidTimestamp{Minute, CInvalidTimestamp::TimestampPart::Minute};
    }

    auto tmp = value();
    std::get<1>(tmp) = Minute;
    setValue(tmp);
}

//============================================================================
uint32_t CTimestamp::second() const
{
    return std::get<2>(value());
}

//============================================================================
void CTimestamp::setSecond(uint32_t Second)
{
    if (!isValid(hour(), minute(), Second, day(), month(), year()))
    {
        throw CInvalidTimestamp{Second, CInvalidTimestamp::TimestampPart::Second};
    }

    auto tmp = value();
    std::get<2>(tmp) = Second;
    setValue(tmp);
}

//============================================================================
uint32_t CTimestamp::day() const
{
    return std::get<3>(value());
}

//============================================================================
void CTimestamp::setDay(uint32_t Day)
{
    if (!isValid(hour(), minute(), second(), Day, month(), year()))
    {
        throw CInvalidTimestamp{Day, CInvalidTimestamp::TimestampPart::Day};
    }

    auto tmp = value();
    std::get<3>(tmp) = Day;
    setValue(tmp);
}

//============================================================================
uint32_t CTimestamp::month() const
{
    return std::get<4>(value());
}

//============================================================================
void CTimestamp::setMonth(uint32_t Month)
{
    if (!isValid(hour(), minute(), second(), day(), Month, year()))
    {
        throw CInvalidTimestamp{Month, CInvalidTimestamp::TimestampPart::Month};
    }

    auto tmp = value();
    std::get<4>(tmp) = Month;
    setValue(tmp);
}

//============================================================================
uint32_t CTimestamp::year() const
{
    return std::get<5>(value());
}

//============================================================================
void CTimestamp::setYear(uint32_t Year)
{
    if (!isValid(hour(), minute(), second(), day(), month(), Year))
    {
        throw CInvalidTimestamp{Year, CInvalidTimestamp::TimestampPart::Year};
    }

    auto tmp = value();
    std::get<5>(tmp) = Year;
    setValue(tmp);
}

//============================================================================
CTimezone CTimestamp::timezone() const
{
    return std::get<6>(value());
}

//============================================================================
void CTimestamp::setTimezone(const CTimezone& Timezone)
{
    auto tmp = value();
    std::get<6>(tmp) = Timezone;
    setValue(tmp);
}

//============================================================================
bool CTimestamp::isValid(uint32_t Hour, uint32_t Minute, uint32_t Second,
                         uint32_t Day, uint32_t Month, uint32_t Year)
{
    return CTime::isValid(Hour, Minute, Second)
           && CDate::isValid(Day, Month, Year);
}

//============================================================================
QDateTime CTimestamp::toQDateTime() const
{
    return {{static_cast<int>(year()), static_cast<int>(month()),
             static_cast<int>(day())},
            {static_cast<int>(hour()), static_cast<int>(minute()),
             static_cast<int>(second())},
            timezone().toQTimeZone()};
}

///===========================================================================
///                        CInvalidTimestamp implementation
///===========================================================================
CInvalidTimestamp::CInvalidTimestamp(uint32_t Hour, uint32_t Minute,
                                     uint32_t Second, uint32_t Day,
                                     uint32_t Month, uint32_t Year)
{
    auto Stream = QTextStream{&m_Message};
    Stream << "Tried to construct an invalid SiLA2::CTimestamp with  hour = "
           << Hour << ", minute = " << Minute << ", second = " << Second
           << ", day = " << Day << ", month = " << Month << ", year = " << Year;
}

//============================================================================
CInvalidTimestamp::CInvalidTimestamp(uint32_t Value, TimestampPart Part)
{
    const char* PartString = "unhandled timestamp part";
    switch (Part)
    {
    case TimestampPart::Hour:
        PartString = "hour";
        break;
    case TimestampPart::Minute:
        PartString = "minute";
        break;
    case TimestampPart::Second:
        PartString = "second";
        break;
    case TimestampPart::Day:
        PartString = "day";
        break;
    case TimestampPart::Month:
        PartString = "month";
        break;
    case TimestampPart::Year:
        PartString = "year";
        break;
    }
    auto Stream = QTextStream{&m_Message};
    Stream << "Invalid " << PartString << " value " << Value
           << " for SiLA2::CTimestamp";
}

//============================================================================
const char* CInvalidTimestamp::what() const noexcept
{
    return qPrintable(m_Message);
}

//============================================================================
QDebug operator<<(QDebug dbg, const CTimestamp& rhs)
{
    QDebugStateSaver s{dbg};
    return dbg.nospace() << "SiLA2::CTimestamp(" << rhs.year() << '-'
                         << rhs.month() << '-' << rhs.day() << ' ' << rhs.hour()
                         << ':' << rhs.minute() << ':' << rhs.second() << ", "
                         << rhs.timezone() << ')';
}

//============================================================================
std::ostream& operator<<(std::ostream& os, const CTimestamp& rhs)
{
    return os << "SiLA2::CTimestamp(" << rhs.year() << '-' << rhs.month() << '-'
              << rhs.day() << ' ' << rhs.hour() << ':' << rhs.minute() << ':'
              << rhs.second() << ", " << rhs.timezone() << ')';
}
}  // namespace SiLA2
