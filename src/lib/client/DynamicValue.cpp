/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   DynamicValue.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   01.03.2021
/// \brief  Implementation of the CDynamicValue class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/client/CustomDataType.h>
#include <sila_cpp/client/DynamicFeatureStub.h>
#include <sila_cpp/client/DynamicParameter.h>
#include <sila_cpp/client/DynamicValue.h>
#include <sila_cpp/codegen/fdl/DataTypeConstrained.h>
#include <sila_cpp/codegen/fdl/DataTypeIdentifier.h>

#include "DynamicValue_p.h"
#include "codegen/fdl/utils.h"

#include <utility>

using namespace std;
using namespace isocpp_p0201;
using namespace SiLA2::codegen;

namespace SiLA2
{
///============================================================================
const QHash<QString, CDynamicValue::BasicTypeType>
    CDynamicValue::PrivateImpl::StringToBasicTypeType{
        {"Any", BasicTypeType::Any},
        {"Binary", BasicTypeType::Binary},
        {"Boolean", BasicTypeType::Boolean},
        {"Date", BasicTypeType::Date},
        {"Duration", BasicTypeType::Duration},
        {"Integer", BasicTypeType::Integer},
        {"Real", BasicTypeType::Real},
        {"String", BasicTypeType::String},
        {"Time", BasicTypeType::Time},
        {"Timestamp", BasicTypeType::Timestamp},
        {"Timezone", BasicTypeType::Timezone},
    };

//===========================================================================
CDynamicValue::PrivateImpl::PrivateImpl(
    CDynamicValue* parent, fdl::CDataType DataTypeFDL,
    shared_ptr<proto::CDynamicMessageFactory> DMF)
    : DataType{std::move(DataTypeFDL)},
      DataTypeType_{static_cast<enum DataTypeType>(DataType.type())},
      DynamicMessageFactory{std::move(DMF)},
      q_ptr{parent}
{}

//============================================================================
CDynamicValue::PrivateImpl::PrivateImpl(const PrivateImpl& rhs)
{
    // NOTE: Normally, we would not need this explicit copy c'tor when using
    // polymorphic_value since it provides value semantics. However, because
    // `Value` is a  unique_ptr` and has a `delete`d copy c'tor, we need to
    // provide our own copy c'tor and take care of properly copying the
    // `unique_ptr`.
    copyFrom(rhs);
}

//============================================================================
CDynamicValue::PrivateImpl& CDynamicValue::PrivateImpl::operator=(
    const PrivateImpl& rhs)
{
    if (std::addressof(rhs) == this)
    {
        return *this;
    }

    // NOTE: Normally, we would not need this explicit copy assignment operator
    // when using polymorphic_value since it provides value semantics. However,
    // because `Value` is a  unique_ptr` and has a `delete`d copy assignment
    // operator, we need to provide our own copy assignment operator and take care
    // of properly copying the `unique_ptr`.
    copyFrom(rhs);
    return *this;
}

//============================================================================
bool CDynamicValue::PrivateImpl::isBasic(const fdl::CDataType& DataType,
                                         const QString& Identifier)
{
    return DataType.type() == fdl::CDataType::Type::Basic
           && (Identifier.isEmpty()
               || DataType.basic().identifier() == Identifier);
}

//============================================================================
bool CDynamicValue::PrivateImpl::isConstrained(const fdl::CDataType& DataType)
{
    return DataType.type() == fdl::CDataType::Type::Constrained;
}

//============================================================================
bool CDynamicValue::PrivateImpl::isList(const fdl::CDataType& DataType)
{
    return DataType.type() == fdl::CDataType::Type::List;
}

//============================================================================
bool CDynamicValue::PrivateImpl::isUnderlyingBasic(const fdl::CDataType& DataType,
                                                   const QString& Identifier)
{
    return isBasic(getUnderlyingBasicType(DataType), Identifier);
}

//============================================================================
fdl::CDataType CDynamicValue::PrivateImpl::getUnderlyingBasicType(
    const fdl::CDataType& DataType)
{
    switch (DataType.type())
    {
    case fdl::IDataType::Type::Basic:
        return DataType;
    case fdl::IDataType::Type::Constrained:
        // might be a Constrained List
        return getUnderlyingBasicType(DataType.constrained().dataType());
    case fdl::IDataType::Type::List:
        // might be a List of Constrained
        return getUnderlyingBasicType(DataType.list().dataType());
    case fdl::IDataType::Type::Identifier:
    case fdl::IDataType::Type::Structure:
        return {};
    case fdl::IDataType::Type::Invalid:
        qCWarning(sila_cpp_client) << "Invalid Data Type has no underlying type";
        return {};
    }

    qCCritical(sila_cpp_client)
        << "Unhandled Data Type Type" << static_cast<int>(DataType.type());
    return {};
}

//============================================================================
fdl::CDataType CDynamicValue::PrivateImpl::getUnderlyingDataType() const
{
    switch (DataTypeType_)
    {
    case DataTypeType::Basic:
    case DataTypeType::DataTypeDefinition:
    case DataTypeType::Structure:
        return DataType;
    case DataTypeType::Constrained:
        return DataType.constrained().dataType();
    case DataTypeType::List:
        return DataType.list().dataType();
    case DataTypeType::Unknown:
        qCWarning(sila_cpp_client) << "Invalid Data Type has no underlying type";
        return {};
    }

    qCCritical(sila_cpp_client)
        << "Unhandled Data Type Type" << static_cast<int>(DataType.type());
    return {};
}

//============================================================================
void CDynamicValue::PrivateImpl::copyFrom(const PrivateImpl& from)
{
    if (from.Value)
    {
        Value.reset(from.Value->New());
        Value->CopyFrom(*from.Value);
    }
    else
    {
        Values.clear();
        Values.reserve(from.Values.size());
        transform(begin(from.Values), end(from.Values), back_inserter(Values),
                  [](const auto& Val) {
                      auto tmp = Val->New();
                      tmp->CopyFrom(*Val);
                      return unique_ptr<google::protobuf::Message>{tmp};
                  });
    }
    DataType = from.DataType;
    DataTypeType_ = from.DataTypeType_;
    DynamicMessageFactory = from.DynamicMessageFactory;
}

//============================================================================
void CDynamicValue::PrivateImpl::setEmptyNestedValue(
    const CDynamicFeatureStub* const Feature, CDynamicValue& Value)
{
    switch (Value.dataTypeType())
    {
    case DataTypeType::Basic:
        switch (Value.basicTypeType())
        {
        case BasicTypeType::Any:
            Value.setValue(CAnyType{});
            return;
        case BasicTypeType::Binary:
            Value.setValue(CBinary{});
            return;
        case BasicTypeType::Boolean:
            Value.setValue(CBoolean{});
            return;
        case BasicTypeType::Date:
            Value.setValue(CDate{});
            return;
        case BasicTypeType::Duration:
            Value.setValue(CDuration{});
            return;
        case BasicTypeType::Integer:
            Value.setValue(CInteger{});
            return;
        case BasicTypeType::Real:
            Value.setValue(CReal{});
            return;
        case BasicTypeType::String:
            Value.setValue(CString{});
            return;
        case BasicTypeType::Time:
            Value.setValue(CTime{});
            return;
        case BasicTypeType::Timestamp:
            Value.setValue(CTimestamp{});
            return;
        case BasicTypeType::Timezone:
            Value.setValue(CTimezone{});
            return;
        case BasicTypeType::Invalid:
            qCWarning(sila_cpp_client)
                << "Invalid Basic Type. Leaving this Field empty!";
            return;
        }
        qCCritical(sila_cpp_client)
            << "Unhandled Basic Type" << static_cast<int>(Value.basicTypeType());
        return;
    case DataTypeType::DataTypeDefinition:
    {
        auto CustomDataType =
            Feature->dataType(Value.dataType().identifier().identifier());
        setEmptyNestedValue(Feature, CustomDataType);
        Value.setValue(CustomDataType);
    }
        return;
    case DataTypeType::Structure:
        // have an empty structure that the user only needs to fill with values
        Value.setValue(Value.structure());
        return;
    case DataTypeType::Constrained:
    case DataTypeType::List:
        return;
    case DataTypeType::Unknown:
        qCWarning(sila_cpp_client)
            << "Invalid Data Type does not have nested values";
        return;
    }

    qCCritical(sila_cpp_client)
        << "Unhandled Data Type Type" << static_cast<int>(Value.dataTypeType());
}

//==========================================================================
bool CDynamicValue::PrivateImpl::valueHasFieldType(
    const unique_ptr<google::protobuf::Message>& Value,
    const google::protobuf::FieldDescriptor* Field, const string& FieldName)
{
    if (Value->GetDescriptor() != Field->message_type())
    {
        qCWarning(sila_cpp_client)
            << "Mismatched data types for Field" << FieldName
            << "detected: Expected type" << Field->message_type()->name()
            << "but got type" << Value->GetDescriptor()->name()
            << "\nLeaving this Field empty!";
        return false;
    }
    return true;
}

//============================================================================
google::protobuf::Message* CDynamicValue::PrivateImpl::toProtoMessagePtr(
    const string& MessageName, const string& FieldName) const
{
    PIMPL_Q(const CDynamicValue);

    if (!DynamicMessageFactory)
    {
        qCWarning(sila_cpp_client)
            << "Cannot convert Dynamic Value to a protobuf Message!";
        return nullptr;
    }

    auto Message = DynamicMessageFactory->getPrototype(MessageName);

    if (!Value && Values.empty())
    {
        qCDebug(sila_cpp_client) << "Empty Dynamic Value";
        return Message.release();
    }

    const auto* Reflection = Message->GetReflection();
    const auto* Field = Message->GetDescriptor()->FindFieldByName(FieldName);
    qCDebug(sila_cpp_client) << "FieldByName" << FieldName << Field
                             << (Field ? Field->DebugString() : "");
    if (!Field)
    {
        Field = Message->GetDescriptor()->field(0);
        qCDebug(sila_cpp_client)
            << "Field 0" << Field << (Field ? Field->DebugString() : "");
    }

    if (!Field)
    {
        qCWarning(sila_cpp_client)
            << "No Field named" << FieldName << "for Dynamic Value" << *q;
    }
    else if (q->isList())
    {
        auto ValueCopies = q->getCopies();
        for_each(begin(ValueCopies), end(ValueCopies), [&](auto& ValueCopy) {
            if (valueHasFieldType(ValueCopy, Field, FieldName))
            {
                Reflection->AddAllocatedMessage(Message.get(), Field,
                                                ValueCopy.release());
            }
        });
    }
    else if (auto ValueCopy = q->getCopy(); ValueCopy)
    {
        if (valueHasFieldType(ValueCopy, Field, FieldName))
        {
            Reflection->SetAllocatedMessage(Message.get(), ValueCopy.release(),
                                            Field);
        }
    }
    return Message.release();
}

//============================================================================
fdl::CDataType CDynamicValue::PrivateImpl::makeStructDataTypeFDL(
    QMultiMap<QString, QVariant> ElementsMap, bool MightHaveNestedStruct)
{
    fdl::CDataType DataTypeFDL;
    if (MightHaveNestedStruct)
    {
        // We cannot simply use CDataTypeFDL::fromVariant here because the nested
        // QVariantMaps have their keys ordered for serialization to XML.
        // Therefore, we have to first create the XML by serializing it and then
        // deserialize it again in order to keep the correct order of nested
        // Elements.
        reverse(begin(ElementsMap), end(ElementsMap));
        const auto TempFDLString = fdl::CFDLSerializer::serialize(
            "DataType", QVariant::fromValue(QVariantMultiMap{
                            {"Structure", QVariant::fromValue(ElementsMap)}}));
        fdl::CFDLSerializer::deserialize(DataTypeFDL, TempFDLString);
    }
    else
    {
        DataTypeFDL.fromVariant(QVariant::fromValue(
            QVariantMultiMap{{"Structure", QVariant::fromValue(ElementsMap)}}));
    }
    return DataTypeFDL;
}

//============================================================================
string CDynamicValue::PrivateImpl::prettyString(google::protobuf::Message* Val)
{
    namespace gp = google::protobuf;
    namespace gps = gp::strings;

    static const auto prettyDateString = [](uint32_t Day, uint32_t Month,
                                            uint32_t Year) {
        return gp::SimpleItoa(Year) + (Month < 10 ? "-0" : "-")
               + gp::SimpleItoa(Month) + (Day < 10 ? "-0" : "-")
               + gp::SimpleItoa(Day);
    };

    static const auto prettyTimeString = [](uint32_t Hour, uint32_t Minute,
                                            uint32_t Second) {
        return (Hour < 10 ? "0" : "") + gp::SimpleItoa(Hour)
               + (Minute < 10 ? ":0" : ":") + gp::SimpleItoa(Minute)
               + (Second < 10 ? ":0" : ":") + gp::SimpleItoa(Second);
    };

    string Result;
    const auto* const Descriptor = Val->GetDescriptor();
    const auto* const Reflection = Val->GetReflection();
    const auto& TypeName = Descriptor->full_name();

    if (TypeName == "sila2.org.silastandard.String"
        || TypeName == "sila2.org.silastandard.Binary")
    {
        Result = '"' + Reflection->GetString(*Val, Descriptor->field(0)) + '"';
    }
    else if (TypeName == "sila2.org.silastandard.Integer")
    {
        Result = gp::SimpleItoa(Reflection->GetInt64(*Val, Descriptor->field(0)));
    }
    else if (TypeName == "sila2.org.silastandard.Real")
    {
        Result = QString::number(
                     Reflection->GetDouble(*Val, Descriptor->field(0)), 'g', 4)
                     .toStdString();
    }
    else if (TypeName == "sila2.org.silastandard.Boolean")
    {
        Result = Reflection->GetBool(*Val, Descriptor->field(0)) ? "true" :
                                                                   "false";
    }
    else if (TypeName == "sila2.org.silastandard.Date")
    {
        const auto Day = Reflection->GetUInt32(*Val, Descriptor->field(0));
        const auto Month = Reflection->GetUInt32(*Val, Descriptor->field(1));
        const auto Year = Reflection->GetUInt32(*Val, Descriptor->field(2));
        auto* Timezone = Reflection->MutableMessage(Val, Descriptor->field(3));
        Result = prettyDateString(Day, Month, Year) + prettyString(Timezone);
    }
    else if (TypeName == "sila2.org.silastandard.Time")
    {
        const auto Second = Reflection->GetUInt32(*Val, Descriptor->field(0));
        const auto Minute = Reflection->GetUInt32(*Val, Descriptor->field(1));
        const auto Hour = Reflection->GetUInt32(*Val, Descriptor->field(2));
        auto* Timezone = Reflection->MutableMessage(Val, Descriptor->field(3));
        Result = prettyTimeString(Hour, Minute, Second) + prettyString(Timezone);
    }
    else if (TypeName == "sila2.org.silastandard.Timestamp")
    {
        const auto Second = Reflection->GetUInt32(*Val, Descriptor->field(0));
        const auto Minute = Reflection->GetUInt32(*Val, Descriptor->field(1));
        const auto Hour = Reflection->GetUInt32(*Val, Descriptor->field(2));
        const auto Day = Reflection->GetUInt32(*Val, Descriptor->field(3));
        const auto Month = Reflection->GetUInt32(*Val, Descriptor->field(4));
        const auto Year = Reflection->GetUInt32(*Val, Descriptor->field(5));
        auto* Timezone = Reflection->MutableMessage(Val, Descriptor->field(6));
        Result = prettyDateString(Day, Month, Year) + 'T'
                 + prettyTimeString(Hour, Minute, Second)
                 + prettyString(Timezone);
    }
    else if (TypeName == "sila2.org.silastandard.Timezone")
    {
        const auto Hours = Reflection->GetInt32(*Val, Descriptor->field(0));
        const auto Minutes = Reflection->GetUInt32(*Val, Descriptor->field(1));
        Result = (Hours < 0 ? '-' : '+') + (abs(Hours) < 10 ? "0"s : ""s)
                 + gp::SimpleItoa(Hours) + (Minutes < 10 ? ":0" : ":")
                 + gp::SimpleItoa(Minutes);
    }
    else
    {
        // Custom Data Type
        const auto FieldCount = Descriptor->field_count();
        for (int i = 0; i < FieldCount; ++i)
        {
            const auto* const Field = Descriptor->field(i);
            const auto PrettyNestedString =
                prettyString(Reflection->MutableMessage(Val, Field));
            if (gps::EndsWith(Field->message_type()->full_name(), "_Struct"))
            {
                Result += '(' + PrettyNestedString + ')';
            }
            else if (gps::EndsWith(TypeName, "DataType_" + Field->name()))
            {
                Result += PrettyNestedString;
            }
            else
            {
                Result += Field->name() + '=' + PrettyNestedString + ',' + ' ';
                if (i + 1 == FieldCount)
                {
                    // strip off last ", "
                    Result.erase(Result.size() - 2);
                }
            }
        }
    }

    return Result;
}

///===========================================================================
CDynamicValue::CDynamicValue() : d_ptr{make_polymorphic_value<PrivateImpl>(this)}
{}

//============================================================================
CDynamicValue::CDynamicValue(google::protobuf::Message* Value,
                             fdl::CDataType DataType,
                             shared_ptr<proto::CDynamicMessageFactory> DMF)
    : d_ptr{make_polymorphic_value<PrivateImpl>(this, std::move(DataType),
                                                std::move(DMF))}
{
    PIMPL_D(CDynamicValue);
    if (Value)
    {
        d->Value.reset(Value->New());
        d->Value->CopyFrom(*Value);
    }
}

//============================================================================
CDynamicValue::CDynamicValue(vector<google::protobuf::Message*> Values,
                             fdl::CDataType DataType,
                             shared_ptr<proto::CDynamicMessageFactory> DMF)
    : d_ptr{make_polymorphic_value<PrivateImpl>(this, std::move(DataType),
                                                std::move(DMF))}
{
    PIMPL_D(CDynamicValue);
    d->Values.reserve(Values.size());
    transform(begin(Values), end(Values), back_inserter(d->Values),
              [](const auto& Val) {
                  auto tmp = unique_ptr<google::protobuf::Message>{Val->New()};
                  tmp->CopyFrom(*Val);
                  return tmp;
              });
}

//============================================================================
CDynamicValue::CDynamicValue(PrivateImplPtr priv) : d_ptr{std::move(priv)}
{}

//============================================================================
CDynamicValue::CDynamicValue(const CDynamicValue& rhs) : d_ptr{rhs.d_ptr}
{
    d_ptr->q_ptr = this;
}

//============================================================================
CDynamicValue::CDynamicValue(CDynamicValue&& rhs) noexcept
    : d_ptr{std::move(rhs.d_ptr)}
{
    d_ptr->q_ptr = this;
}

//============================================================================
CDynamicValue& CDynamicValue::operator=(const CDynamicValue& rhs)
{
    if (std::addressof(rhs) == this)
    {
        return *this;
    }

    d_ptr = rhs.d_ptr;
    d_ptr->q_ptr = this;
    return *this;
}

//============================================================================
CDynamicValue& CDynamicValue::operator=(CDynamicValue&& rhs) noexcept
{
    if (std::addressof(rhs) == this)
    {
        return *this;
    }

    d_ptr = std::move(rhs.d_ptr);
    d_ptr->q_ptr = this;
    return *this;
}

//===========================================================================
//                                Single value
//===========================================================================
google::protobuf::Message* CDynamicValue::value() const
{
    PIMPL_D(const CDynamicValue);
    return d->Value.get();
}

//============================================================================
unique_ptr<google::protobuf::Message> CDynamicValue::getCopy() const
{
    PIMPL_D(const CDynamicValue);
    if (!d->Value)
    {
        return nullptr;
    }
    auto* tmp = d->Value->New();
    tmp->CopyFrom(*d->Value);
    return unique_ptr<google::protobuf::Message>{tmp};
}

//============================================================================
void CDynamicValue::setValue(const CDynamicStructure& Struct)
{
    PIMPL_D(CDynamicValue);

    bool MightHaveNestedStruct = false;
    QMultiMap<QString, QVariant> ElementsMap;
    for_each(cbegin(Struct), cend(Struct),
             [&MightHaveNestedStruct, &ElementsMap](const auto& Element) {
                 const auto ElementFDL = Element.fdl();
                 ElementsMap.insert("Element", ElementFDL.toVariant());
                 const auto ElementDataTypeType = ElementFDL.dataType().type();
                 MightHaveNestedStruct |=
                     ElementDataTypeType == fdl::IDataType::Type::Structure
                     || ElementDataTypeType == fdl::IDataType::Type::Constrained
                     || ElementDataTypeType == fdl::IDataType::Type::List;
             });
    setValue(Struct.toProtoMessagePtr(),
             d->makeStructDataTypeFDL(ElementsMap, MightHaveNestedStruct));
    if (!d->DynamicMessageFactory && !Struct.empty())
    {
        d->DynamicMessageFactory = Struct.at(0).dmf();
    }
}

//============================================================================
void CDynamicValue::setValue(const CCustomDataType& DataType)
{
    PIMPL_D(CDynamicValue);

    fdl::CDataType DataTypeFDL;
    DataTypeFDL.fromVariant(variantMultiMap(
        {{"DataTypeIdentifier", DataType.identifier().identifier()}}));

    setValue(DataType.toProtoMessagePtr(), DataTypeFDL);
    if (!d->DynamicMessageFactory)
    {
        d->DynamicMessageFactory = DataType.d_ptr->DynamicMessageFactory;
    }
}

//============================================================================
void CDynamicValue::setValue(google::protobuf::Message* Value,
                             const fdl::CDataType& DataType)
{
    PIMPL_D(CDynamicValue);
    d->Value.reset(Value);
    d->DataType = DataType;
    d->DataTypeType_ = static_cast<DataTypeType>(DataType.type());
}

//===========================================================================
//                                List value
//===========================================================================
vector<google::protobuf::Message*> CDynamicValue::values() const
{
    PIMPL_D(const CDynamicValue);
    vector<google::protobuf::Message*> Res;
    Res.reserve(d->Values.size());
    transform(begin(d->Values), end(d->Values), back_inserter(Res),
              [](const auto& Val) { return Val.get(); });
    return Res;
}

//============================================================================
vector<unique_ptr<google::protobuf::Message>> CDynamicValue::getCopies() const
{
    PIMPL_D(const CDynamicValue);
    vector<unique_ptr<google::protobuf::Message>> Res;
    Res.reserve(d->Values.size());
    transform(begin(d->Values), end(d->Values), back_inserter(Res),
              [](const auto& Val) {
                  auto tmp = Val->New();
                  tmp->CopyFrom(*Val);
                  return unique_ptr<google::protobuf::Message>{tmp};
              });
    return Res;
}

//============================================================================
void CDynamicValue::setValues(const vector<google::protobuf::Message*>& Values,
                              const fdl::CDataType& DataType)
{
    PIMPL_D(CDynamicValue);
    d->Values.clear();
    d->Values.reserve(Values.size());
    transform(begin(Values), end(Values), back_inserter(d->Values),
              [](const auto& Val) {
                  return unique_ptr<google::protobuf::Message>{Val};
              });
    d->DataType = DataType;
    d->DataTypeType_ = static_cast<DataTypeType>(DataType.type());
}

//============================================================================
void CDynamicValue::addValue(google::protobuf::Message* Value,
                             const fdl::CDataType& DataType)
{
    PIMPL_D(CDynamicValue);

    if (DataType.type() == fdl::IDataType::Type::Basic
        && !d->isUnderlyingBasic(d->DataType, DataType.basic().identifier()))
    {
        qCWarning(sila_cpp_client)
            << "Trying to add value with type" << DataType.basic().identifier()
            << "to Dynamic Value list that has type" << basicTypeTypeName();
    }

    auto* tmp = Value->New();
    tmp->CopyFrom(*Value);
    d->Values.emplace_back(tmp);
}

//============================================================================
void CDynamicValue::addValue(const CDynamicStructure& Struct)
{
    PIMPL_D(CDynamicValue);

    if (underlyingDataTypeType() != DataTypeType::Structure)
    {
        qCWarning(sila_cpp_client)
            << "Won't add a Structure value to Dynamic Value "
               "list that has type"
            << underlyingDataTypeTypeName();
        return;
    }

    qCWarning(sila_cpp_client)
        << "Adding struct" << Struct << "to dynamic value list" << *this;

    bool HasNestedStruct = false;
    QMultiMap<QString, QVariant> ElementsMap;
    for_each(
        begin(Struct), end(Struct),
        [&HasNestedStruct, &ElementsMap](const auto& Element) {
            ElementsMap.insert(
                "Element",
                variantMultiMap(
                    {{"Identifier", QString::fromStdString(Element.identifier())},
                     {"DisplayName", Element.displayName()},
                     {"Description", Element.description()},
                     {"DataType", Element.dataType().toVariant()}}));

            HasNestedStruct |= Element.fdl().dataType().type()
                               == fdl::IDataType::Type::Structure;
        });
    addValue(Struct.toProtoMessagePtr(),
             d->makeStructDataTypeFDL(ElementsMap, HasNestedStruct));
    if (!d->DynamicMessageFactory && !Struct.empty())
    {
        d->DynamicMessageFactory = Struct.at(0).dmf();
    }
}

//============================================================================
void CDynamicValue::addValue(const CCustomDataType& DataType)
{
    PIMPL_D(CDynamicValue);

    if (underlyingDataTypeType() != DataTypeType::DataTypeDefinition)
    {
        qCWarning(sila_cpp_client) << "Won't add a Custom Data Type value to "
                                      "Dynamic Value list that has type"
                                   << underlyingDataTypeTypeName();
    }

    fdl::CDataType DataTypeFDL;
    DataTypeFDL.fromVariant(variantMultiMap(
        {{"DataTypeDefinition", DataType.dataType().toVariant()}}));

    addValue(DataType.toProtoMessagePtr(), DataTypeFDL);
    if (!d->DynamicMessageFactory)
    {
        d->DynamicMessageFactory = DataType.d_ptr->DynamicMessageFactory;
    }
}

//============================================================================
void CDynamicValue::setValue(google::protobuf::Message* Value,
                             const fdl::CDataType& DataType, size_t Index)
{
    PIMPL_D(CDynamicValue);

    if (DataType.type() == fdl::IDataType::Type::Basic
        && !d->isUnderlyingBasic(d->DataType, DataType.basic().identifier()))
    {
        qCWarning(sila_cpp_client)
            << "Won't set value with type" << DataType.basic().identifier()
            << "at" << Index << "where type" << basicTypeTypeName()
            << "is expected";
        return;
    }

    auto* tmp = Value->New();
    tmp->CopyFrom(*Value);
    d->Values[Index].reset(tmp);
}

//============================================================================
void CDynamicValue::setValue(const CDynamicStructure& Struct, size_t Index)
{
    PIMPL_D(CDynamicValue);

    if (underlyingDataTypeType() != DataTypeType::Structure)
    {
        qCWarning(sila_cpp_client)
            << "Won't set a Structure value at" << Index << "where type"
            << underlyingDataTypeTypeName() << "is expected";
        return;
    }

    bool HasNestedStruct = false;
    QMultiMap<QString, QVariant> ElementsMap;
    for_each(begin(Struct), end(Struct),
             [&HasNestedStruct, &ElementsMap](const auto& Element) {
                 const auto ElementFDL = Element.fdl();
                 ElementsMap.insert("Element", ElementFDL.toVariant());
                 HasNestedStruct |= ElementFDL.dataType().type()
                                    == fdl::IDataType::Type::Structure;
             });
    setValue(Struct.toProtoMessagePtr(),
             d->makeStructDataTypeFDL(ElementsMap, HasNestedStruct), Index);
    if (!d->DynamicMessageFactory && !Struct.empty())
    {
        d->DynamicMessageFactory = Struct.at(0).dmf();
    }
}

//============================================================================
void CDynamicValue::setValue(const CCustomDataType& DataType, size_t Index)
{
    PIMPL_D(CDynamicValue);

    if (underlyingDataTypeType() != DataTypeType::DataTypeDefinition)
    {
        qCWarning(sila_cpp_client)
            << "Won't set a Custom Data Type value at" << Index << "where type"
            << underlyingDataTypeTypeName() << "is expected";
        return;
    }

    //  TODO refactor into function / maybe it's unnecessary -> needs testing
    fdl::CDataType DataTypeFDL;
    DataTypeFDL.fromVariant(variantMultiMap(
        {{"DataTypeDefinition", DataType.dataType().toVariant()}}));
    // end refactor

    setValue(DataType.toProtoMessagePtr(), DataTypeFDL, Index);
    if (!d->DynamicMessageFactory)
    {
        d->DynamicMessageFactory = DataType.d_ptr->DynamicMessageFactory;
    }
}

//===========================================================================
//                                  Common
//===========================================================================
void CDynamicValue::clear()
{
    PIMPL_D(CDynamicValue);
    d->Value.reset(nullptr);
    d->Values.clear();
}

//============================================================================
QString CDynamicValue::prettyString() const
{
    PIMPL_D(const CDynamicValue);

    if (isStructure())
    {
        return toStructure(nullptr).prettyString();
    }

    QString Result;
    if (d->Value)
    {
        Result = PrivateImpl::prettyString(d->Value);
    }
    else
    {
        Result += '(';
        auto it = cbegin(d->Values);
        const auto end = cend(d->Values);
        if (it != end)
        {
            Result += PrivateImpl::prettyString(*it);
            ++it;
        }
        while (it != end)
        {
            Result += ',';
            Result += ' ' + PrivateImpl::prettyString(*it);
            ++it;
        }
        Result += ')';
    }
    return Result;
}

//============================================================================
google::protobuf::Message* CDynamicValue::toProtoMessagePtr() const
{
    qCWarning(sila_cpp_client)
        << "Subclass of CDynamicValue does not implement toProtoMessagePtr()!";
    return nullptr;
}

//===========================================================================
//                             Type information
//============================================================================
fdl::CDataType CDynamicValue::dataType() const
{
    PIMPL_D(const CDynamicValue);
    return d->DataType;
}

//===========================================================================
CDynamicValue::DataTypeType CDynamicValue::dataTypeType() const
{
    PIMPL_D(const CDynamicValue);
    return d->DataTypeType_;
}

//============================================================================
string CDynamicValue::dataTypeTypeName() const
{
    PIMPL_D(const CDynamicValue);
    return dataTypeTypeToString(d->DataTypeType_);
}

//===========================================================================
CDynamicValue::DataTypeType CDynamicValue::underlyingDataTypeType() const
{
    PIMPL_D(const CDynamicValue);
    switch (d->DataTypeType_)
    {
    case DataTypeType::Basic:
    case DataTypeType::DataTypeDefinition:
    case DataTypeType::Structure:
        return d->DataTypeType_;
    case DataTypeType::List:
        return static_cast<DataTypeType>(d->DataType.list().dataType().type());
    case DataTypeType::Constrained:
        return static_cast<DataTypeType>(
            d->DataType.constrained().dataType().type());
    case DataTypeType::Unknown:
        qCWarning(sila_cpp_client) << "Invalid Data Type has no underlying type";
        return d->DataTypeType_;
    }

    qCCritical(sila_cpp_client)
        << "Unhandled Data Type Type" << static_cast<int>(d->DataTypeType_);
    return d->DataTypeType_;
}

//============================================================================
string CDynamicValue::underlyingDataTypeTypeName() const
{
    return dataTypeTypeToString(underlyingDataTypeType());
}

//============================================================================
string CDynamicValue::dataTypeTypeToString(DataTypeType Type)
{
    switch (Type)
    {
    case DataTypeType::Unknown:
        return "Unknown";
    case DataTypeType::Basic:
        return "Basic";
    case DataTypeType::Constrained:
        return "Constrained";
    case DataTypeType::DataTypeDefinition:
        return "Data Type Definition";
    case DataTypeType::List:
        return "List";
    case DataTypeType::Structure:
        return "Structure";
    }
    qCCritical(sila_cpp_client)
        << "Unhandled DataTypeType" << static_cast<int>(Type);
    return "";
}

//============================================================================
CDynamicValue::BasicTypeType CDynamicValue::basicTypeType() const
{
    PIMPL_D(const CDynamicValue);

    if (const auto UnderlyingType = d->getUnderlyingBasicType(d->DataType);
        PrivateImpl::isBasic(UnderlyingType))
    {
        return PrivateImpl::StringToBasicTypeType.value(
            UnderlyingType.basic().identifier());
    }
    return BasicTypeType::Invalid;
}

//============================================================================
string CDynamicValue::basicTypeTypeName() const
{
    return basicTypeTypeToString(basicTypeType());
}

//============================================================================
string CDynamicValue::basicTypeTypeToString(BasicTypeType BasicType)
{
    switch (BasicType)
    {
    case BasicTypeType::Invalid:
        return "Invalid";
    case BasicTypeType::Any:
        return "Any";
    case BasicTypeType::Binary:
        return "Binary";
    case BasicTypeType::Boolean:
        return "Boolean";
    case BasicTypeType::Date:
        return "Date";
    case BasicTypeType::Duration:
        return "Duration";
    case BasicTypeType::Integer:
        return "Integer";
    case BasicTypeType::Real:
        return "Real";
    case BasicTypeType::String:
        return "String";
    case BasicTypeType::Time:
        return "Time";
    case BasicTypeType::Timestamp:
        return "Timestamp";
    case BasicTypeType::Timezone:
        return "Timezone";
    }
    qCCritical(sila_cpp_client)
        << "Unknown BasicTypeType" << static_cast<int>(BasicType);
    return "";
}

//============================================================================
CFullyQualifiedDataTypeID CDynamicValue::customDataTypeIdentifier(
    const CDynamicFeatureStub* const Feature) const
{
    PIMPL_D(const CDynamicValue);

    if (const auto DataTypeFDL = d->getUnderlyingDataType();
        DataTypeFDL.type() == fdl::IDataType::Type::Identifier)
    {
        return {Feature->identifier(), DataTypeFDL.identifier().identifier()};
    }
    return {Feature->identifier(), ""};
}

//============================================================================
CDynamicStructure CDynamicValue::structure() const
{
    PIMPL_D(const CDynamicValue);

    qCCritical(sila_cpp_client) << "Subclass of CDynamicValue does not implement "
                                   "structure()! Expecting trouble!";

    if (const auto DataTypeFDL = d->getUnderlyingDataType();
        DataTypeFDL.type() == fdl::IDataType::Type::Structure)
    {
        return CDynamicStructure{DataTypeFDL.structure(), "", "",
                                 d->DynamicMessageFactory, nullptr};
    }
    return {};
}

//============================================================================
QList<fdl::CConstraint> CDynamicValue::constraints() const
{
    PIMPL_D(const CDynamicValue);
    return d->isConstrained(d->DataType) ?
               d->DataType.constrained().constraints() :
               QList<fdl::CConstraint>{};
}

//===========================================================================
//                              Convenient access
//===========================================================================
CCustomDataType CDynamicValue::toCustomDataType(
    const CDynamicFeatureStub* const Feature) const
{
    PIMPL_D(const CDynamicValue);

    qCDebug(sila_cpp_client) << d->DataType << d->DataType.identifier();
    auto Result = Feature->dataType(d->DataType.identifier().identifier());
    qCDebug(sila_cpp_client) << d->DataType.identifier().identifier() << Result;
    if (isCustomDataType() && d->Value)
    {
        const auto* const Reflection = d->Value->GetReflection();
        const auto* const Descriptor = d->Value->GetDescriptor();
        auto* const NestedMessage =
            Reflection->MutableMessage(d->Value.get(), Descriptor->field(0));
        auto* Value = NestedMessage->New();
        Value->CopyFrom(*NestedMessage);
        Result.setValue(Value, Result.dataType());
    }
    qCDebug(sila_cpp_client) << Result;
    return Result;
}

//============================================================================
CDynamicStructure CDynamicValue::toStructure(
    CDynamicFeatureStub* const Feature) const
{
    PIMPL_D(const CDynamicValue);
    if (!isStructure() || !d->Value || !d->DynamicMessageFactory)
    {
        qCDebug(sila_cpp_client)
            << "Cannot convert Dynamic Value to Structure because at least one "
               "of the prerequisites is not met\nisStructure()"
            << isStructure() << "\nValue" << d->Value.get()
            << "\nDynamicMessageFactory" << d->DynamicMessageFactory.get();
        return {};
    }

    const auto* const Descriptor = d->Value->GetDescriptor();
    const auto* const Reflection = d->Value->GetReflection();
    const auto& StructMsgName = Descriptor->full_name();
    const auto Sep = StructMsgName.find_last_of('.');
    const auto SurroundingMsgName = StructMsgName.substr(0, Sep);
    const auto StructFieldName = StructMsgName.substr(
        Sep + 1, StructMsgName.find("_Struct", Sep) - Sep - 1);
    CDynamicStructure Result{d->DataType.structure(), SurroundingMsgName,
                             StructFieldName, d->DynamicMessageFactory, Feature};

    for (int i = 0; i < Result.size(); ++i)
    {
        const auto* const Field = Descriptor->field(i);
        qCDebug(sila_cpp_client) << "Field" << i << Field->DebugString();
        //        auto& Element = Result[i];
        auto& Element = Result[Field->name()];
        qCDebug(sila_cpp_client)
            << "Element" << Field->name() << Element << Element.dataType();

        namespace gp = google::protobuf;
        if (Field->is_repeated())
        {
            vector<gp::Message*> Values;
            // can't directly iterate over `Ref` because that would require to
            // allocate an object of abstract `Message`
            const auto Ref =
                Reflection->GetRepeatedFieldRef<gp::Message>(*d->Value, Field);
            for (int j = 0; j < Ref.size(); ++j)
            {
                const auto& Value =
                    Reflection->MutableRepeatedMessage(d->Value.get(), Field, j);
                auto* tmp = Value->New();
                tmp->CopyFrom(*Value);
                Values.push_back(tmp);
            }
            Element.setValues(Values, Element.dataType());
        }
        else
        {
            auto* ElementMessage =
                Reflection->MutableMessage(d->Value.get(), Field);

            auto* Value = ElementMessage->New();
            Value->CopyFrom(*ElementMessage);
            Element.setValue(Value, Element.dataType());
        }
    }
    return Result;
}

//============================================================================
CDynamicValue CDynamicValue::toUnconstrained() const
{
    PIMPL_D(const CDynamicValue);
    if (isConstrained())
    {
        const auto DataType = d->DataType.constrained().dataType();
        switch (DataType.type())
        {
        case fdl::IDataType::Type::Basic:
        case fdl::IDataType::Type::Constrained:
        case fdl::IDataType::Type::Identifier:
        case fdl::IDataType::Type::Structure:
            return {d->Value.get(), DataType, d->DynamicMessageFactory};
        case fdl::IDataType::Type::List:
            return {values(), DataType, d->DynamicMessageFactory};
        case fdl::IDataType::Type::Invalid:
            qCWarning(sila_cpp_client) << "Invalid Data Type Type";
        }
    }

    return *this;
}

//============================================================================
vector<CDynamicValue> CDynamicValue::toList() const
{
    PIMPL_D(const CDynamicValue);

    vector<CDynamicValue> Result;
    if (isList())
    {
        Result.reserve(d->Values.size());
        transform(begin(d->Values), end(d->Values), back_inserter(Result),
                  [DataType = d->DataType.list().dataType(),
                   DMF = d->DynamicMessageFactory](const auto& Val) {
                      return CDynamicValue{Val.get(), DataType, DMF};
                  });
    }

    return Result;
}

//============================================================================
CAnyType CDynamicValue::toAnyType() const
{
    PIMPL_D(const CDynamicValue);
    if (isAnyType() && d->Value)
    {
        return CAnyType::fromProtoMessage(*d->Value);
    }

    return {};
}

//============================================================================
vector<CAnyType> CDynamicValue::toAnyTypeList() const
{
    PIMPL_D(const CDynamicValue);
    if (isAnyType() && !d->Values.empty())
    {
        return d->toList<CAnyType>();
    }

    return {};
}

//============================================================================
CBinary CDynamicValue::toBinary() const
{
    PIMPL_D(const CDynamicValue);
    if (isBinary() && d->Value)
    {
        return CBinary::fromProtoMessage(*d->Value);
    }

    return {};
}

//============================================================================
vector<CBinary> CDynamicValue::toBinaryList() const
{
    PIMPL_D(const CDynamicValue);
    if (isBinary() && !d->Values.empty())
    {
        return d->toList<CBinary>();
    }

    return {};
}

//============================================================================
CBoolean CDynamicValue::toBoolean() const
{
    PIMPL_D(const CDynamicValue);
    if (isBoolean() && d->Value)
    {
        return CBoolean::fromProtoMessage(*d->Value);
    }

    return {};
}

//============================================================================
vector<CBoolean> CDynamicValue::toBooleanList() const
{
    PIMPL_D(const CDynamicValue);
    if (isBoolean() && !d->Values.empty())
    {
        return d->toList<CBoolean>();
    }

    return {};
}

//============================================================================
CDate CDynamicValue::toDate() const
{
    PIMPL_D(const CDynamicValue);
    if (isDate() && d->Value)
    {
        return CDate::fromProtoMessage(*d->Value);
    }

    return {};
}

//============================================================================
vector<CDate> CDynamicValue::toDateList() const
{
    PIMPL_D(const CDynamicValue);
    if (isDate() && !d->Values.empty())
    {
        return d->toList<CDate>();
    }

    return {};
}

//============================================================================
CDuration CDynamicValue::toDuration() const
{
    PIMPL_D(const CDynamicValue);
    if (isDuration() && d->Value)
    {
        return CDuration::fromProtoMessage(*d->Value);
    }

    return {};
}

//============================================================================
vector<CDuration> CDynamicValue::toDurationList() const
{
    PIMPL_D(const CDynamicValue);
    if (isDuration() && !d->Values.empty())
    {
        return d->toList<CDuration>();
    }

    return {};
}

//============================================================================
CInteger CDynamicValue::toInteger() const
{
    PIMPL_D(const CDynamicValue);
    if (isInteger() && d->Value)
    {
        return CInteger::fromProtoMessage(*d->Value);
    }

    return {};
}

//============================================================================
vector<CInteger> CDynamicValue::toIntegerList() const
{
    PIMPL_D(const CDynamicValue);
    if (isInteger() && !d->Values.empty())
    {
        return d->toList<CInteger>();
    }

    return {};
}

//============================================================================
CReal CDynamicValue::toReal() const
{
    PIMPL_D(const CDynamicValue);
    if (isReal() && d->Value)
    {
        return CReal::fromProtoMessage(*d->Value);
    }

    return {};
}

//============================================================================
vector<CReal> CDynamicValue::toRealList() const
{
    PIMPL_D(const CDynamicValue);
    if (isReal() && !d->Values.empty())
    {
        return d->toList<CReal>();
    }

    return {};
}

//============================================================================
CString CDynamicValue::toString() const
{
    PIMPL_D(const CDynamicValue);
    if (isString() && d->Value)
    {
        return CString::fromProtoMessage(*d->Value);
    }

    return {};
}

//============================================================================
vector<CString> CDynamicValue::toStringList() const
{
    PIMPL_D(const CDynamicValue);
    if (isString() && !d->Values.empty())
    {
        return d->toList<CString>();
    }

    return {};
}

//============================================================================
CTime CDynamicValue::toTime() const
{
    PIMPL_D(const CDynamicValue);
    if (isTime() && d->Value)
    {
        return CTime::fromProtoMessage(*d->Value);
    }

    return {};
}

//============================================================================
vector<CTime> CDynamicValue::toTimeList() const
{
    PIMPL_D(const CDynamicValue);
    if (isTime() && !d->Values.empty())
    {
        return d->toList<CTime>();
    }

    return {};
}

//============================================================================
CTimestamp CDynamicValue::toTimestamp() const
{
    PIMPL_D(const CDynamicValue);
    if (isTimestamp() && d->Value)
    {
        return CTimestamp::fromProtoMessage(*d->Value);
    }

    return {};
}

//============================================================================
vector<CTimestamp> CDynamicValue::toTimestampList() const
{
    PIMPL_D(const CDynamicValue);
    if (isTimestamp() && !d->Values.empty())
    {
        return d->toList<CTimestamp>();
    }

    return {};
}

//============================================================================
CTimezone CDynamicValue::toTimezone() const
{
    PIMPL_D(const CDynamicValue);
    if (isTimezone() && d->Value)
    {
        return CTimezone::fromProtoMessage(*d->Value);
    }

    return {};
}

//============================================================================
vector<CTimezone> CDynamicValue::toTimezoneList() const
{
    PIMPL_D(const CDynamicValue);
    if (isTimezone() && !d->Values.empty())
    {
        return d->toList<CTimezone>();
    }

    return {};
}

//============================================================================
QDebug operator<<(QDebug dbg, const CDynamicValue& rhs)
{
    if (auto* const Value = rhs.value(); Value)
    {
        return dbg << *Value;
    }
    if (const auto Values = rhs.values(); !Values.empty())
    {
        return dbg << Values;
    }
    return dbg << "{}";
}

//============================================================================
ostream& operator<<(ostream& os, const CDynamicValue& rhs)
{
    using ::operator<<;
    if (auto* const Value = rhs.value(); Value)
    {
        return os << *Value;
    }
    if (const auto Values = rhs.values(); !Values.empty())
    {
        return os << Values;
    }
    return os << "{}";
}
}  // namespace SiLA2
