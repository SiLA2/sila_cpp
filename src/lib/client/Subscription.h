/*******************************************************************************
 ** This file is part of the sila_cpp project.
 ** Copyright 2021 SiLA 2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 *****************************************************************************/

//============================================================================
/// \file   Subscription.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   16.02.2021
/// \brief  Declaration of the
///         CThreadPoolMaxThreadCountReachedError, CSubscriptionBase and
///         CSubscription classes
//============================================================================
#ifndef SUBSCRIPTION_H
#define SUBSCRIPTION_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/client/ClientMetadata.h>
#include <sila_cpp/config.h>
#include <sila_cpp/framework/error_handling/ClientError.h>
#include <sila_cpp/framework/error_handling/SiLAError.h>
#include <sila_cpp/global.h>
#include <sila_cpp/qt5compat_global.h>

#include "DynamicCall.h"

#include <QFuture>
#include <QFutureWatcher>
#include <QHash>
#include <QtConcurrentRun>

#include <functional>

//============================================================================
//                            FORWARD DECLARATIONS
//============================================================================
#if GRPC_VERSION >= GRPC_VERSION_CHECK(1, 32, 0)
namespace grpc
{
class Channel;
}  // namespace grpc
#else
namespace grpc_impl
{
class Channel;
}  // namespace grpc_impl
namespace grpc
{
typedef grpc_impl::Channel Channel;
}  // namespace grpc
#endif

namespace google::protobuf
{
class Message;
}  // namespace google::protobuf

namespace SiLA2
{
/**
 * @brief The CThreadPoolMaxThreadCountReachedError class indicates that the
 * maximum number of threads is busy and that there are no more free threads to do
 * any work.
 */
class CThreadPoolMaxThreadCountReachedError : public std::runtime_error
{
public:
    /**
     * @brief C'tor
     */
    CThreadPoolMaxThreadCountReachedError();

    /**
     * @brief Returns the maximum thread count that was reached
     */
    static int maxThreadCount();
};

/**
 * @brief The CSubscriptionThreadPool class provides the thread pool to use for
 * all subscription threads
 */
class CSubscriptionThreadPool final : public QThreadPool
{
public:
    /**
     * @brief Get a shared_ptr to the @c CThreadPool instance
     *
     * @return A shared_ptr to the @c CThreadPool instance
     */
    static std::shared_ptr<CSubscriptionThreadPool> instance() noexcept;

    /**
     * @brief Returns whether this thread pool has any free threads available or
     * not
     *
     * @returns true, if there are no free threads any more, false otherwise
     */
    bool maxThreadCountReached() const;

private:
    /**
     * @brief C'tor
     */
    CSubscriptionThreadPool() = default;

    static std::weak_ptr<CSubscriptionThreadPool> m_Instance;
};

/**
 * @brief The CSubscription class template wraps the process of subscribing to any
 * value that might be subscribed to on a SiLA 2 Server
 */
template<typename T>
class CSubscription
{
    using ConverterFunc =
        std::function<T(const std::unique_ptr<google::protobuf::Message>&)>;
    template<typename C>
    using ConverterMemFunc =
        T (C::*)(const std::unique_ptr<google::protobuf::Message>&);
    template<typename C>
    using ConstConverterMemFunc =
        T (C::*)(const std::unique_ptr<google::protobuf::Message>&) const;

public:
    /**
     * @brief D'tor
     * Cancels the subscription
     */
    ~CSubscription();

    /**
     * @brief Start the subscription using the optional @a Metadata
     *
     * @param ParameterMessage The @c Message for the Parameter (has to be already
     * of the correct type)
     * @param Metadata (optional) The Metadata to add to the subscription
     * @return A @c QFuture for the response(s) received from the Server.
     *
     * @throws CThreadPoolMaxThreadCountReachedError if the Subscription cannot be
     * started because there are no free threads available in the Thread Pool
     */
    [[nodiscard]] QFuture<T> start(
        std::unique_ptr<google::protobuf::Message> ParameterMessage,
        const CClientMetadataList& Metadata = {});

private:
    friend class CDynamicCommand;
    friend class CDynamicProperty;

    /**
     * @internal Only to be used by @c CDynamicCommand and @c CDynamicProperty
     * @brief C'tor
     *
     * @param Channel The channel to the Server
     * @param MethodName The name of the gRPC Method to use for the subscription
     * @param ResponseMessage The @c Message for the Response (has to be already
     * of the correct type)
     * @param Converter A function pointer to a function that converts the raw
     * @c google::protobuf::Message to the actual type @a T
     */
    CSubscription(std::shared_ptr<grpc::Channel> Channel, std::string MethodName,
                  std::unique_ptr<google::protobuf::Message> ResponseMessage,
                  ConverterFunc Converter);

    /**
     * @internal Only to be used by @c CDynamicCommand and @c CDynamicProperty
     * @brief C'tor
     *
     * @param Channel The channel to the Server
     * @param MethodName The name of the gRPC Method to use for the subscription
     * @param ResponseMessage The @c Message for the Response (has to be already
     * of the correct type)
     * @param Class A pointer to the class to use with the @a Converter member
     * function pointer
     * @param Converter A member function pointer to a function that converts the
     * raw @c google::protobuf::Message to the actual type @a T
     */
    template<typename C>
    CSubscription(std::shared_ptr<grpc::Channel> Channel, std::string MethodName,
                  std::unique_ptr<google::protobuf::Message> ResponseMessage,
                  C* Class, ConverterMemFunc<C> Converter);

    /**
     * @internal Only to be used by @c CDynamicCommand and @c CDynamicProperty
     * @brief C'tor
     *
     * @param Channel The channel to the Server
     * @param MethodName The name of the gRPC Method to use for the subscription
     * @param ResponseMessage The @c Message for the Response (has to be already
     * of the correct type)
     * @param Class A const pointer to the class to use with the @a Converter
     * member function pointer
     * @param Converter A const  member function pointer to a function that
     * converts the raw @c google::protobuf::Message to the actual type @a T
     */
    template<typename C>
    CSubscription(std::shared_ptr<grpc::Channel> Channel, std::string MethodName,
                  std::unique_ptr<google::protobuf::Message> ResponseMessage,
                  const C* Class, ConstConverterMemFunc<C> Converter);

    /**
     * @brief Read from the given @c CDynamicCall @a Call and post the results to
     * the @a FutureInterface
     *
     * @param Call The Dynamic Call to read from
     * @param FutureInterface The Future Interface to post the results and report
     * any errors to
     */
    void readFromCall(const std::shared_ptr<CDynamicCall>& Call,
                      QFutureInterface<T> FutureInterface) const;

    std::shared_ptr<grpc::Channel> m_Channel;
    std::string m_MethodName;
    std::unique_ptr<google::protobuf::Message> m_ResponseMessage;
    ConverterFunc m_Converter;

    QList<std::pair<std::shared_ptr<CDynamicCall>, QFutureWatcher<T>*>>
        m_CallWatchers;
    std::shared_ptr<CSubscriptionThreadPool> m_ThreadPool;
};

//============================================================================
template<typename T>
CSubscription<T>::CSubscription(
    std::shared_ptr<grpc::Channel> Channel, std::string MethodName,
    std::unique_ptr<google::protobuf::Message> ResponseMessage,
    ConverterFunc Converter)
    : m_Channel{std::move(Channel)},
      m_MethodName{std::move(MethodName)},
      m_ResponseMessage{std::move(ResponseMessage)},
      m_Converter{Converter},
      m_ThreadPool{CSubscriptionThreadPool::instance()}
{}

//============================================================================
template<typename T>
template<typename C>
CSubscription<T>::CSubscription(
    std::shared_ptr<grpc::Channel> Channel, std::string MethodName,
    std::unique_ptr<google::protobuf::Message> ResponseMessage, C* Class,
    ConverterMemFunc<C> Converter)
    : CSubscription{std::move(Channel), std::move(MethodName),
                    std::move(ResponseMessage),
                    static_cast<ConverterFunc>(
                        std::bind(Converter, Class, std::placeholders::_1))}
{}

//============================================================================
template<typename T>
template<typename C>
CSubscription<T>::CSubscription(
    std::shared_ptr<grpc::Channel> Channel, std::string MethodName,
    std::unique_ptr<google::protobuf::Message> ResponseMessage, const C* Class,
    ConstConverterMemFunc<C> Converter)
    : CSubscription{std::move(Channel), std::move(MethodName),
                    std::move(ResponseMessage),
                    static_cast<ConverterFunc>(
                        std::bind(Converter, Class, std::placeholders::_1))}
{}

//============================================================================
template<typename T>
CSubscription<T>::~CSubscription()
{
    qCDebug(sila_cpp_client)
        << "Cancelling all" << m_MethodName << "Subscriptions";
    std::for_each(std::begin(m_CallWatchers), std::end(m_CallWatchers),
                  [](auto CallWatcher) {
                      auto [Call, Watcher] = CallWatcher;
                      Call->cancel();
                      try
                      {
                          Watcher->waitForFinished();
                      }
                      catch (const CSiLAError&)
                      {}
                      delete Watcher;
                  });
    qCDebug(sila_cpp_client)
        << "remaining threads" << m_ThreadPool->activeThreadCount();
}

//============================================================================
template<typename T>
QFuture<T> CSubscription<T>::start(
    std::unique_ptr<google::protobuf::Message> ParameterMessage,
    const CClientMetadataList& Metadata)
{
    if (m_ThreadPool->maxThreadCountReached())
    {
        throw CThreadPoolMaxThreadCountReachedError{};
    }

    QFutureInterface<T> FutureInterface;
    FutureInterface.reportStarted();
    auto Future = FutureInterface.future();

    auto Call = std::make_shared<CDynamicCall>(m_Channel, m_MethodName,
                                               Metadata.toMultiMap());
    Call->write(ParameterMessage);
    Call->writesDone();

    std::ignore = QtConcurrent::run(
        static_cast<QThreadPool*>(m_ThreadPool.get()),
        Qt5Compat_ConcurrentMemberFunc(&CSubscription<T>::readFromCall, this),
        Call, FutureInterface);

    auto Watcher = new QFutureWatcher<T>;
    QObject::connect(Watcher, &QFutureWatcher<T>::canceled, Watcher,
                     [this, Call, Watcher]() {
                         Call->cancel();
                         m_CallWatchers.removeOne({Call, Watcher});
                         delete Watcher;
                     });
    Watcher->setFuture(Future);
    m_CallWatchers.append({Call, Watcher});

    return Future;
}

//============================================================================
template<typename T>
void CSubscription<T>::readFromCall(const std::shared_ptr<CDynamicCall>& Call,
                                    QFutureInterface<T> FutureInterface) const
{
    std::unique_ptr<google::protobuf::Message> Response{m_ResponseMessage->New()};
    try
    {
        Call->setTimeoutSeconds(-1);  ///< don't time out reading streaming RPCs
        while (Call->read(Response))
        {
            FutureInterface.reportResult(m_Converter(Response));
            Response.reset(m_ResponseMessage->New());
        }
        Call->setTimeoutSeconds(5);  ///< time out for `finish`
        throwOnError(Call->finish());
    }
    catch (const QException& err)
    {
        // If there is an error before the server sent any value users can't catch
        // this exception. Therefore, we have to report a dummy result.
        FutureInterface.reportResult(m_Converter(Response));
        FutureInterface.reportException(err);
    }
    FutureInterface.reportFinished();
    qCDebug(sila_cpp_client)
        << "Subscription for" << m_MethodName << "is finished";
}
}  // namespace SiLA2

#endif  // SUBSCRIPTION_H
