/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   DynamicCommand.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   17.02.2021
/// \brief  Implementation of the CDynamicCommand class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/client/DynamicCommand.h>
#include <sila_cpp/codegen/fdl/Command.h>
#include <sila_cpp/codegen/proto/DynamicMessageFactory.h>
#include <sila_cpp/framework/error_handling/ClientError.h>
#include <sila_cpp/framework/error_handling/SiLAError.h>

#include "Subscription.h"

#include <QEventLoop>
#include <QFutureWatcher>

#include <grpcpp/grpcpp.h>

#include <utility>

using namespace std;
using namespace isocpp_p0201;
using namespace SiLA2::codegen;
using namespace sila2::org::silastandard;

namespace SiLA2
{
using RPCType = CFullyQualifiedCommandID::RPCType;

/**
 * @brief Private data of the CDynamicCommand class - pimpl
 */
class CDynamicCommand::PrivateImpl
{
public:
    /**
     * @brief C'tor
     */
    PrivateImpl(CFullyQualifiedCommandID CommandID,
                const fdl::CCommand& CommandFDL,
                shared_ptr<grpc::Channel> SharedChannel,
                shared_ptr<proto::CDynamicMessageFactory> DMF,
                CDynamicFeatureStub* FeatureStub);

    /**
     * @brief Construct a list of @c CDynamicParameters or Response Identifiers
     * (@c QString) from the given list of FDL objects
     *
     * @tparam T Either @c CDynamicParameter or @c QString
     * @tparam U The type of FDL object (either @c fdl::CParameter or
     * @c fdl::CResponse)
     * @param FDLList The list of FDL objects
     * @return The converted list
     */
    template<typename T, typename U>
    [[nodiscard]] QList<T> makeList(const QList<U>& FDLList);

    /**
     * @brief Throws a @c std::logic_error if the Command is Unobservable
     */
    void throwIfUnobservable() const;

    /**
     * @brief Get the default (prototype) protobuf @c Message for the Parameters
     * of this Command
     *
     * @return An empty protobuf @c Message of the Command's Parameters type
     */
    [[nodiscard]] unique_ptr<google::protobuf::Message> getParameterPrototype()
        const;

    /**
     * @brief Get the default (prototype) protobuf @c Message for the
     * IntermediateResponses of this Command
     *
     * @param Type The type of the RPC method to construct the name for
     * @return An empty protobuf @c Message of the Command's Responses type
     */
    [[nodiscard]] unique_ptr<google::protobuf::Message> getResponsePrototype(
        RPCType Type = RPCType::Invalid) const;

    /**
     * @brief Convert the given list of @c CDynamicParameters to the protobuf
     * @c Message for the Parameters of this Command
     *
     * @param List The list of Parameters to convert
     * @return A protobuf @c Message of the Command's Parameters filled with the
     * values from the given @a List
     */
    [[nodiscard]] unique_ptr<google::protobuf::Message> parameterListToMessage(
        const CDynamicParameterList& List) const;

    /**
     * @brief Convert the given protobuf @c Message for the IntermediateResponses
     * of this Command to a list of @c CDynamicResponses
     *
     * @param Message The protobuf @c Message of the Command's
     * IntermediateResponses
     * @return A list of Dynamic IntermediateResponses with the values of the
     * given @a Message's fields
     */
    [[nodiscard]] CDynamicResponseList messageToResponseList(
        const unique_ptr<google::protobuf::Message>& Message) const;

    const CFullyQualifiedCommandID Identifier;
    const fdl::CCommand Command;
    const shared_ptr<grpc::Channel> Channel;
    const shared_ptr<proto::CDynamicMessageFactory> DynamicMessageFactory;
    CDynamicFeatureStub* Feature;
    const CDynamicParameterList ParameterList;
    const CDynamicResponseList ResponseList;
    const QStringList ResponseNamesList;
    CClientMetadataList Metadata;
    shared_ptr<CSubscription<CExecutionInfo>>
        ExecInfoSubscription;  ///< non-copyable
    shared_ptr<CSubscription<CDynamicResponseList>>
        IntermediateRespSubscription;  ///< non-copyable
};

//============================================================================
CDynamicCommand::PrivateImpl::PrivateImpl(
    CFullyQualifiedCommandID CommandID, const fdl::CCommand& CommandFDL,
    shared_ptr<grpc::Channel> SharedChannel,
    shared_ptr<proto::CDynamicMessageFactory> DMF,
    CDynamicFeatureStub* FeatureStub)
    : Identifier{std::move(CommandID)},
      Command{CommandFDL},
      Channel{std::move(SharedChannel)},
      DynamicMessageFactory{std::move(DMF)},
      Feature{FeatureStub},
      ParameterList{makeList<CDynamicParameter>(Command.parameters())},
      ResponseList{makeList<CDynamicResponse>(Command.responses())},
      ResponseNamesList{makeList<QString>(Command.responses())}
{
    if (Command.isObservable())
    {
        ExecInfoSubscription.reset(new CSubscription<CExecutionInfo>{
            Channel, Identifier.toMethodName(RPCType::Info),
            getResponsePrototype(RPCType::Info), [](const auto& Response) {
                return CExecutionInfo::fromProtoMessage(*Response);
            }});
        if (!Command.intermediateResponses().empty())
        {
            IntermediateRespSubscription.reset(
                new CSubscription<CDynamicResponseList>{
                    Channel, Identifier.toMethodName(RPCType::IntermediateResult),
                    getResponsePrototype(RPCType::IntermediateResult), this,
                    &PrivateImpl::messageToResponseList});
        }
    }
}

//============================================================================
template<typename T, typename U>
QList<T> CDynamicCommand::PrivateImpl::makeList(const QList<U>& FDLList)
{
    QList<T> Result;
    Result.reserve(FDLList.size());
    transform(begin(FDLList), end(FDLList), back_inserter(Result), [this](const auto& FDL) {
        if constexpr (is_same_v<T, QString>)
        {
            return FDL.identifier();
        }
        else if constexpr (std::is_same_v<T, CDynamicResponse>)
        {
            return T{nullptr, FDL, Identifier, DynamicMessageFactory, Feature};
        }
        else
        {
            return T{FDL, Identifier, DynamicMessageFactory, Feature};
        }
    });
    return Result;
}

//============================================================================
void CDynamicCommand::PrivateImpl::throwIfUnobservable() const
{
    if (!Command.isObservable())
    {
        throw logic_error{"This Command ("s + Identifier.toStdString() + ") "
                          + "is not Observable! Use `CDynamicCommand::call()` "
                            "instead."};
    }
}

//============================================================================
unique_ptr<google::protobuf::Message>
CDynamicCommand::PrivateImpl::getParameterPrototype() const
{
    return DynamicMessageFactory->getParameterPrototype(Identifier);
}

//============================================================================
unique_ptr<google::protobuf::Message>
CDynamicCommand::PrivateImpl::getResponsePrototype(RPCType Type) const
{
    return DynamicMessageFactory->getResponsePrototype(Identifier, Type);
}

//============================================================================
unique_ptr<google::protobuf::Message>
CDynamicCommand::PrivateImpl::parameterListToMessage(
    const CDynamicParameterList& List) const
{
    auto ParametersMessage = getParameterPrototype();
    for_each(begin(List), end(List), [&ParametersMessage](const auto& Param) {
        if (auto ParamMessage = Param.toProtoMessagePtr(); ParamMessage)
        {
            ParametersMessage->MergeFrom(*ParamMessage);
            delete ParamMessage;
        }
    });
    return ParametersMessage;
}

//============================================================================
CDynamicResponseList CDynamicCommand::PrivateImpl::messageToResponseList(
    const unique_ptr<google::protobuf::Message>& Message) const
{
    const auto* const Descriptor = Message->GetDescriptor();
    CDynamicResponseList List;
    for (int i = 0; i < Descriptor->field_count(); ++i)
    {
        const auto* const Field = Message->GetDescriptor()->field(i);
        auto* Msg = Message.get();
        if (!Field->is_repeated())
        {
            Msg = Message->GetReflection()->MutableMessage(Msg, Field);
        }
        List.push_back({Msg, Command.responses().at(i), Identifier,
                        DynamicMessageFactory, Feature});
    }
    return List;
}

///===========================================================================
CDynamicCommand::CDynamicCommand(const CFullyQualifiedCommandID& Identifier,
                                 const fdl::CCommand& CommandFDL,
                                 shared_ptr<grpc::Channel> Channel,
                                 shared_ptr<proto::CDynamicMessageFactory> DMF,
                                 CDynamicFeatureStub* Feature)
    : d_ptr{make_polymorphic_value<PrivateImpl>(
        Identifier, CommandFDL, std::move(Channel), std::move(DMF), Feature)}
{}

//============================================================================
CFullyQualifiedCommandID CDynamicCommand::identifier() const
{
    PIMPL_D(const CDynamicCommand);
    return d->Identifier;
}

//============================================================================
QString CDynamicCommand::displayName() const
{
    PIMPL_D(const CDynamicCommand);
    return d->Command.displayName();
}

//============================================================================
QString CDynamicCommand::description() const
{
    PIMPL_D(const CDynamicCommand);
    return d->Command.description().simplified();
}

//============================================================================
bool CDynamicCommand::isObservable() const
{
    PIMPL_D(const CDynamicCommand);
    return d->Command.isObservable();
}

//============================================================================
CDynamicParameterList CDynamicCommand::parameters() const
{
    PIMPL_D(const CDynamicCommand);
    return d->ParameterList;
}

//============================================================================
CDynamicResponseList CDynamicCommand::responses() const
{
    PIMPL_D(const CDynamicCommand);
    return d->ResponseList;
}

//============================================================================
QStringList CDynamicCommand::responseNames() const
{
    PIMPL_D(const CDynamicCommand);
    return d->ResponseNamesList;
}

//============================================================================
QString CDynamicCommand::responseDescription(const QString& ResponseName) const
{
    PIMPL_D(const CDynamicCommand);
    return d->Command.responses()
        .at(d->ResponseNamesList.indexOf(ResponseName))
        .description();
}

//============================================================================
CClientMetadataList CDynamicCommand::metadata() const
{
    PIMPL_D(const CDynamicCommand);
    return d->Metadata;
}

//============================================================================
void CDynamicCommand::addAffectingMetadata(const CClientMetadata& Metadata)
{
    PIMPL_D(CDynamicCommand);
    d->Metadata.append(Metadata);
}

//============================================================================
CDynamicFeatureStub* CDynamicCommand::feature() const
{
    PIMPL_D(const CDynamicCommand);
    return d->Feature;
}

//============================================================================
QString CDynamicCommand::commandFDLString() const
{
    PIMPL_D(const CDynamicCommand);
    return fdl::CFDLSerializer::serialize(d->Command);
}

//============================================================================
fdl::CCommand CDynamicCommand::commandFDL() const
{
    PIMPL_D(const CDynamicCommand);
    return d->Command;
}

//============================================================================
CDynamicResponseList CDynamicCommand::call(
    unique_ptr<google::protobuf::Message> Parameters,
    const CClientMetadataList& Metadata)
{
    PIMPL_D(CDynamicCommand);

    if (!isObservable())
    {
        qCInfo(sila_cpp_client)
            << "--- Calling Unobservable Command" << d->Identifier;
        qCDebug(sila_cpp_client) << d->Identifier << "Parameters:" << *Parameters;
        qCDebug(sila_cpp_client) << d->Identifier << "Metadata:" << Metadata;

        auto Responses = d->getResponsePrototype();
        const auto Status =
            CDynamicCall::call(d->Channel, d->Identifier.toMethodName(),
                               Parameters, Responses, Metadata.toMultiMap());

        throwOnError(Status);
        qCDebug(sila_cpp_client) << d->Identifier << "Responses:" << *Responses;

        emit called();

        return d->messageToResponseList(Responses);
    }

    // initiation
    const auto UUID = start(std::move(Parameters), Metadata);
    // info
    auto Future = subscribeExecutionInfo(UUID);
    QFutureWatcher<CExecutionInfo> Watcher;
    QEventLoop EventLoop;
    connect(&Watcher, &QFutureWatcher<CExecutionInfo>::resultReadyAt, this,
            [Future, &EventLoop](int index) {
                const auto ExecInfo = Future.resultAt(index);
                qCDebug(sila_cpp_client)
                    << "Received ExecutionInfo value" << ExecInfo;
                switch (ExecInfo.commandStatus())
                {
                case CommandStatus::Waiting:
                case CommandStatus::Running:
                    break;
                case CommandStatus::FinishedSuccessfully:
                case CommandStatus::FinishedWithError:
                    qDebug() << "Future finished" << Future.isFinished()
                             << "running" << Future.isRunning() << "canceled"
                             << Future.isCanceled();
                    if (!Future.isFinished())
                    {
                        EventLoop.quit();
                    }
                    break;
                }
            });
    connect(&Watcher, &QFutureWatcher<CExecutionInfo>::finished, &EventLoop,
            &QEventLoop::quit);
    Watcher.setFuture(Future);
    EventLoop.exec();
    disconnect(&Watcher, &QFutureWatcher<CExecutionInfo>::resultReadyAt, this,
               nullptr);
    if (const auto ResultCount = Future.resultCount(); ResultCount > 0)
    {
        // re-throw any error that might have occurred
        std::ignore = Future.resultAt(ResultCount - 1);
    }
    Future.cancel();

    emit called();

    // result
    return result(UUID);
}

//============================================================================
CDynamicResponseList CDynamicCommand::call(
    const CDynamicParameterList& Parameters, const CClientMetadataList& Metadata)
{
    PIMPL_D(CDynamicCommand);
    return call(d->parameterListToMessage(Parameters), Metadata);
}

//============================================================================
CDynamicResponseList CDynamicCommand::call(const CClientMetadataList& Metadata)
{
    PIMPL_D(CDynamicCommand);
    return call(d->getParameterPrototype(), Metadata);
}

//============================================================================
CCommandExecutionUUID CDynamicCommand::start(
    unique_ptr<google::protobuf::Message> Parameters,
    const CClientMetadataList& Metadata)
{
    PIMPL_D(CDynamicCommand);
    d->throwIfUnobservable();

    unique_ptr<google::protobuf::Message> Confirmation{
        CommandConfirmation::default_instance().New()};
    const auto Status =
        CDynamicCall::call(d->Channel, d->Identifier.toMethodName(), Parameters,
                           Confirmation, Metadata.toMultiMap());

    throwOnError(Status);

    CCommandExecutionUUID UUID{
        (dynamic_cast<CommandConfirmation*>(Confirmation.get()))
            ->commandexecutionuuid()};
    qCInfo(sila_cpp_client) << "--- Started Execution of Observable Command"
                            << d->Identifier << "with UUID" << UUID.toString();

    qCDebug(sila_cpp_client) << d->Identifier << "Parameters:" << *Parameters;
    qCDebug(sila_cpp_client) << d->Identifier << "Metadata:" << Metadata;

    return UUID;
}

//============================================================================
CCommandExecutionUUID CDynamicCommand::start(
    const CDynamicParameterList& Parameters, const CClientMetadataList& Metadata)
{
    PIMPL_D(CDynamicCommand);
    return start(d->parameterListToMessage(Parameters), Metadata);
}

//============================================================================
CCommandExecutionUUID CDynamicCommand::start(const CClientMetadataList& Metadata)
{
    PIMPL_D(CDynamicCommand);
    return start(d->getParameterPrototype(), Metadata);
}

//============================================================================
CExecutionInfo CDynamicCommand::getExecutionInfo(const CCommandExecutionUUID& UUID)
{
    PIMPL_D(CDynamicCommand);
    d->throwIfUnobservable();

    const auto Future = subscribeExecutionInfo(UUID);
    QFutureWatcher<CExecutionInfo> Watcher{this};
    Watcher.setFuture(Future);
    const auto EventLoop = QEventLoop{this};
    connect(&Watcher, &QFutureWatcher<CExecutionInfo>::resultReadyAt, &EventLoop,
            &QEventLoop::quit);
    return Future.resultAt(Future.resultCount());
}

//============================================================================
QFuture<CExecutionInfo> CDynamicCommand::subscribeExecutionInfo(
    const CCommandExecutionUUID& UUID)
{
    PIMPL_D(CDynamicCommand);
    d->throwIfUnobservable();

    qCDebug(sila_cpp_client)
        << "Starting Subscription for Execution Info of" << d->Identifier;
    try
    {
        return d->ExecInfoSubscription->start(
            unique_ptr<google::protobuf::Message>{UUID.toProtoMessagePtr()});
    }
    catch (CThreadPoolMaxThreadCountReachedError& err)
    {
        throw runtime_error{
            QStringLiteral("Cannot start Subscription to Execution Info of %1 "
                           "because there are too may Subscriptions (%2) running "
                           "at the same time.")
                .arg(d->Identifier.toString())
                .arg(err.maxThreadCount())
                .toStdString()};
    }
}

//============================================================================
CDynamicResponseList CDynamicCommand::getIntermediateResponses(
    const CCommandExecutionUUID& UUID)
{
    PIMPL_D(CDynamicCommand);
    d->throwIfUnobservable();

    const auto Future = subscribeIntermediateResponses(UUID);
    QFutureWatcher<CDynamicResponseList> Watcher{this};
    Watcher.setFuture(Future);
    const auto EventLoop = QEventLoop{this};
    connect(&Watcher, &QFutureWatcher<CDynamicResponseList>::resultReadyAt,
            &EventLoop, &QEventLoop::quit);
    return Future.resultAt(Future.resultCount());
}

//============================================================================
QFuture<CDynamicResponseList> CDynamicCommand::subscribeIntermediateResponses(
    const CCommandExecutionUUID& UUID)
{
    PIMPL_D(CDynamicCommand);
    d->throwIfUnobservable();

    qCDebug(sila_cpp_client)
        << "Starting Subscription for Intermediate Responses of" << d->Identifier;
    try
    {
        return d->IntermediateRespSubscription->start(
            unique_ptr<google::protobuf::Message>{UUID.toProtoMessagePtr()});
    }
    catch (CThreadPoolMaxThreadCountReachedError& err)
    {
        throw runtime_error{
            QStringLiteral("Cannot start Subscription to Intermediate Responses "
                           "of %1 because there are too may Subscriptions (%2) "
                           "running at the same time.")
                .arg(d->Identifier.toString())
                .arg(err.maxThreadCount())
                .toStdString()};
    }
}

//============================================================================
CDynamicResponseList CDynamicCommand::result(const CCommandExecutionUUID& UUID)
{
    PIMPL_D(CDynamicCommand);
    d->throwIfUnobservable();

    qCInfo(sila_cpp_client)
        << "--- Requesting Command Execution Result of Observable Command"
        << d->Identifier << "with UUID" << UUID.toString();

    auto Responses = d->getResponsePrototype(RPCType::Result);

    const auto Status = CDynamicCall::call(
        d->Channel, d->Identifier.toMethodName(RPCType::Result),
        unique_ptr<CommandExecutionUUID>{UUID.toProtoMessagePtr()}, Responses);

    throwOnError(Status);
    qCDebug(sila_cpp_client) << d->Identifier << "Responses:" << *Responses;

    return d->messageToResponseList(Responses);
}

//============================================================================
QDebug operator<<(QDebug dbg, const CDynamicCommand& rhs)
{
    QDebugStateSaver s{dbg};
    return dbg.nospace() << "CDynamicCommand(" << rhs.identifier()
                         << "\n\tParameters: " << rhs.parameters()
                         << "\n\tMetadata: " << rhs.metadata() << ')';
}

//============================================================================
ostream& operator<<(ostream& os, const CDynamicCommand& rhs)
{
    return os << "CDynamicCommand(" << rhs.identifier().toStdString()
              << "\n\tParameters: " << rhs.parameters()
              << "\n\tMetadata: " << rhs.metadata() << ')';
}
}  // namespace SiLA2
