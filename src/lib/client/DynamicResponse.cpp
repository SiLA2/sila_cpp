/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   DynamicResponse.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   16.03.2021
/// \brief  Implementation of the CDynamicResponse class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/client/DynamicResponse.h>
#include <sila_cpp/client/DynamicStructure.h>
#include <sila_cpp/codegen/fdl/DataTypeIdentifier.h>
#include <sila_cpp/common/FullyQualifiedCommandID.h>

#include "DynamicValue_p.h"

#include <utility>

using namespace std;
using namespace isocpp_p0201;
using namespace SiLA2::codegen;

namespace SiLA2
{
/**
 * @brief Private data of the CCustomDataType class - pimpl
 */
class CDynamicResponse::PrivateImpl : public CDynamicValue::PrivateImpl
{
public:
    /**
     * @brief C'tor
     */
    explicit PrivateImpl(CDynamicResponse* parent,
                         const fdl::CResponse& ResponseFDL,
                         CFullyQualifiedCommandID CommandIdentifier,
                         shared_ptr<proto::CDynamicMessageFactory> DMF,
                         CDynamicFeatureStub* FeatureStub);

    const fdl::CResponse Response{};
    const string Identifier;
    const CFullyQualifiedCommandID CommandID;
    CDynamicFeatureStub* Feature;
};

//============================================================================
CDynamicResponse::PrivateImpl::PrivateImpl(
    CDynamicResponse* parent, const fdl::CResponse& ResponseFDL,
    CFullyQualifiedCommandID CommandIdentifier,
    shared_ptr<proto::CDynamicMessageFactory> DMF,
    CDynamicFeatureStub* FeatureStub)
    : CDynamicValue::PrivateImpl{parent, ResponseFDL.dataType(), std::move(DMF)},
      Response{ResponseFDL},
      Identifier{Response.identifier().toStdString()},
      CommandID{std::move(CommandIdentifier)},
      Feature{FeatureStub}
{}

///===========================================================================
CDynamicResponse::CDynamicResponse(google::protobuf::Message* Value,
                                   const fdl::CResponse& ResponseFDL,
                                   CFullyQualifiedCommandID CommandID,
                                   shared_ptr<proto::CDynamicMessageFactory> DMF,
                                   CDynamicFeatureStub* Feature)
    : CDynamicValue{
        make_polymorphic_value<CDynamicValue::PrivateImpl, PrivateImpl>(
            this, ResponseFDL, std::move(CommandID), std::move(DMF), Feature)}
{
    PIMPL_D(CDynamicResponse);

    if (!Value)
    {
        PrivateImpl::setEmptyNestedValue(Feature, *this);
        return;
    }

    namespace gp = google::protobuf;
    if (d->DataTypeType_ == DataTypeType::List)
    {
        const auto* const Reflection = Value->GetReflection();
        const auto* const Field =
            Value->GetDescriptor()->FindFieldByName(d->Identifier);
        // can't directly iterate over `Ref` because that would require to
        // allocate an object of abstract `Message`
        const auto Ref =
            Reflection->GetRepeatedFieldRef<gp::Message>(*Value, Field);
        for (int i = 0; i < Ref.size(); ++i)
        {
            const auto& Val = Reflection->MutableRepeatedMessage(Value, Field, i);
            auto* tmp = Val->New();
            tmp->CopyFrom(*Val);
            d->Values.emplace_back(tmp);
        }
    }
    else
    {
        d->Value.reset(Value->New());
        d->Value->CopyFrom(*Value);
    }
}

//============================================================================
CFullyQualifiedCommandResponseID CDynamicResponse::identifier() const
{
    PIMPL_D(const CDynamicResponse);
    return {d->CommandID, d->Identifier};
}

//============================================================================
QString CDynamicResponse::displayName() const
{
    PIMPL_D(const CDynamicResponse);
    return d->Response.displayName();
}

//============================================================================
QString CDynamicResponse::description() const
{
    PIMPL_D(const CDynamicResponse);
    return d->Response.description().simplified();
}

//============================================================================
CDynamicFeatureStub* CDynamicResponse::feature() const
{
    PIMPL_D(const CDynamicResponse);
    return d->Feature;
}

//============================================================================
CDynamicStructure CDynamicResponse::structure() const
{
    PIMPL_D(const CDynamicResponse);

    if (const auto DataTypeFDL = d->getUnderlyingDataType();
        DataTypeFDL.type() == fdl::IDataType::Type::Structure)
    {
        using RPCType = CFullyQualifiedCommandID::RPCType;
        return CDynamicStructure{
            DataTypeFDL.structure(),
            d->CommandID.responseMessageName(RPCType::Result), d->Identifier,
            d->DynamicMessageFactory, d->Feature};
    }
    qCDebug(sila_cpp_client) << "Underlying Data Type is not a Structure!";
    return {};
}

//============================================================================
QString CDynamicResponse::prettyString() const
{
    PIMPL_D(const CDynamicResponse);
    return QString::fromStdString(d->Identifier) + '='
           + CDynamicValue::prettyString();
}

///===========================================================================
CDynamicResponseList::CDynamicResponseList(const QList<CDynamicResponse>& rhs)
    : QList{rhs}
{}

//============================================================================
QList<CFullyQualifiedCommandResponseID> CDynamicResponseList::identifiers() const
{
    QList<CFullyQualifiedCommandResponseID> List;
    List.reserve(size());
    transform(begin(), end(), back_inserter(List),
              [](const auto& Resp) { return Resp.identifier(); });
    return List;
}

//============================================================================
template<typename Self>
auto& at(Self& self, const CFullyQualifiedCommandResponseID& Identifier)
{
    const auto it =
        find_if(begin(self), end(self), [&Identifier](const auto& Resp) {
            return Resp.identifier() == Identifier;
        });
    if (it != end(self))
    {
        return *it;
    }
    throw std::out_of_range{"No Dynamic Response named \""s
                            + Identifier.toStdString() + "\" in Response List!"};
}

//============================================================================
template<typename Self>
auto& at(Self& self, string_view Identifier)
{
    const auto it =
        find_if(begin(self), end(self), [&Identifier](const auto& Resp) {
            return Resp.identifier().identifier().toStdString() == Identifier;
        });
    if (it != end(self))
    {
        return *it;
    }
    throw std::out_of_range{"No Dynamic Response named \""s + Identifier.data()
                            + "\" in Response List!"};
}

//============================================================================
const CDynamicResponse& CDynamicResponseList::at(
    const CFullyQualifiedCommandResponseID& Identifier) const
{
    return SiLA2::at(*this, Identifier);
}

//============================================================================
const CDynamicResponse& CDynamicResponseList::at(string_view Identifier) const
{
    return SiLA2::at(*this, Identifier);
}

//============================================================================
CDynamicResponse& CDynamicResponseList::operator[](string_view Identifier)
{
    return SiLA2::at(*this, Identifier);
}

//============================================================================
CDynamicResponse& CDynamicResponseList::operator[](
    const CFullyQualifiedCommandResponseID& Identifier)
{
    return SiLA2::at(*this, Identifier);
}

//============================================================================
QDebug operator<<(QDebug dbg, const CDynamicResponse& rhs)
{
    QDebugStateSaver s{dbg};
    dbg.nospace() << "CDynamicResponse(" << rhs.identifier() << '=';
    dbg << dynamic_cast<const CDynamicValue&>(rhs);
    return dbg << ')';
}

//============================================================================
ostream& operator<<(ostream& os, const CDynamicResponse& rhs)
{
    os << "CDynamicResponse(" << rhs.identifier() << '=';
    os << dynamic_cast<const CDynamicValue&>(rhs);
    return os << ')';
}
}  // namespace SiLA2
