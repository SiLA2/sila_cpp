/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SiLAReal.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   17.01.2020
/// \brief  Declaration of the CReal class
//============================================================================
#ifndef SILAREAL_H
#define SILAREAL_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/global.h>

#include "DataType.h"
#include "utils.h"

//============================================================================
//                            FORWARD DECLARATIONS
//============================================================================
namespace sila2::org::silastandard
{
class Real;
}

namespace SiLA2
{
/**
 * @brief The CReal class provides a convenience wrapper around SiLAFramework's
 * Real class.
 *
 * @details The <b>SiLA Real Type</b> represents a real number as defined per
 * IEEE 754.
 */
class SILA_CPP_EXPORT CReal : public CDataType<double>
{
    using Base = CDataType<double>;

public:
    /**
     * @brief C'tor
     */
    // NOLINTNEXTLINE(google-explicit-constructor)
    CReal(double Value = 0.0);

    /**
     * @brief Converting copy c'tor from sila2::org::silastandard::Real
     */
    // NOLINTNEXTLINE(google-explicit-constructor)
    CReal(const sila2::org::silastandard::Real& rhs);

    SILA_CPP_CREATE_SPECIAL_MEMBER_FUNCTIONS(CReal, Base)

    /**
     * @brief Construct a new @c CReal from the given protobuf @c Message @a from
     *
     * This is just for convenience if you happen to have a pointer or reference
     * to @c google::protobuf::Message and want to convert that to a @c CReal
     * without having to cast it to a @c sila2::org::silastandard::Real before.
     * The conversion will be performed for you here but might fail if you pass a
     * @c Message that is not a @c Real.
     */
    [[nodiscard]] static CReal fromProtoMessage(
        const google::protobuf::Message& from);

    /**
     * @brief Convert this convenience type to a SiLAFramework type, i.e. the
     * Protobuf Message
     *
     * @return The SiLAFramework equivalent of this type as a Protobuf Message
     */
    [[nodiscard]] sila2::org::silastandard::Real toProtoMessage() const;

    /**
     * @brief Convert this convenience type to a SiLAFramework type, i.e. the
     * Protobuf Message
     *
     * @return The SiLAFramework equivalent of this type as a Protobuf Message
     * pointer
     */
    [[nodiscard]] sila2::org::silastandard::Real* toProtoMessagePtr() const;
};

/**
 * @brief Overload for debugging CReals
 */
SILA_CPP_EXPORT QDebug operator<<(QDebug dbg, const CReal& rhs);
SILA_CPP_EXPORT std::ostream& operator<<(std::ostream& os, const CReal& rhs);
}  // namespace SiLA2

#endif  // SILAREAL_H
