/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   DataTypeSerializer.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   13.12.2020
/// \brief  Declaration of the CDataTypeSerializer class
//============================================================================
#ifndef DATATYPESERIALIZER_H
#define DATATYPESERIALIZER_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/fdl/DataType.h>
#include <sila_cpp/codegen/fdl/DataTypeBasic.h>
#include <sila_cpp/codegen/fdl/DataTypeList.h>
#include <sila_cpp/codegen/fdl/FDLSerializer.h>
#include <sila_cpp/codegen/fdl/utils.h>
#include <sila_cpp/common/logging.h>
#include <sila_cpp/global.h>
#include <sila_cpp/internal/type_traits.h>

#include <google/protobuf/message.h>

#include <map>

namespace SiLA2
{
//============================================================================
//                            FORWARD DECLARATIONS
//============================================================================
class CString;
class CInteger;
class CReal;
class CBoolean;
class CBinary;
class CDate;
class CTime;
class CTimestamp;
class CTimezone;
class CDuration;
class CAnyType;

using codegen::fdl::variantMultiMap;

/**
 * @brief The CDataTypeSerializer class provides methods to serialize and
 * deserialize SiLA Data Types to and from an XML string.
 */
class SILA_CPP_EXPORT CDataTypeSerializer
{
    using DataTypeVariant =
        std::variant<CString, CInteger, CReal, CBoolean, CBinary, CDate, CTime,
                     CTimestamp, CTimezone, CDuration, CAnyType>;

public:
    /**
     * @brief Serialize the given Payload into an XML string
     *
     * @tparam DataTypeT A wrapped SiLA Type or a SiLAFramework type or a List of
     * the before
     * @param Payload The payload to serialize
     * @return QString The payload serialized as an XML string
     */
    template<typename DataTypeT,
             typename = std::enable_if_t<
                 (internal::is_sila_type_v<DataTypeT>)
                 || (internal::is_sila_framework_type_v<DataTypeT>)
                 || internal::is_container_v<DataTypeT>>>
    static QString serialize(const DataTypeT& Payload);

    /**
     * @brief Deserialize the given XML string
     *
     * @param XMLString The XML string to deserialize
     * @param Payload The payload containing the serialized Protobuf message
     * @return A pair indicating the type of the Data Type and containing a
     * default constructed wrapped SiLA Data Type that was deserialized from the
     * @a XMLString
     */
    static std::pair<codegen::fdl::IDataType::Type, DataTypeVariant> deserialize(
        const QString& XMLString);

private:
    using DataTypeMap = std::map<QString, DataTypeVariant>;
    static const DataTypeMap
        m_DataTypeMap;  ///< maps the data type name to the type

    /**
     * @brief Helper function to get the protobuf @c Descriptor for the given
     * @a Payload
     *
     * @tparam DataTypeT Either a wrapped SiLA Data Type, a SiLA Framework Type
     * (i.e. already a protobuf @a Message) or a list of the before
     * @param Payload The Data Type object to get the @c Descriptor for
     * @return The protobuf @c Descriptor of the given @a Payload
     */
    template<typename DataTypeT,
             typename =
                 std::enable_if_t<internal::is_sila_type_v<DataTypeT>
                                  || internal::is_sila_framework_type_v<DataTypeT>
                                  || internal::is_container_v<DataTypeT>>>
    static const google::protobuf::Descriptor* getDescriptor(
        const DataTypeT& Payload);
};

//=============================================================================
template<typename DataTypeT, typename>
QString CDataTypeSerializer::serialize(const DataTypeT& Payload)
{
    QVariantMultiMap Map;
    const auto BasicMap = QVariantMultiMap{
        {"Basic", QString::fromStdString(getDescriptor(Payload)->name())}};
    // TODO: Structure Type (see CDynamicValue::setValue(CDynamicStructure)!!)
    if constexpr (internal::is_container_v<DataTypeT>)
    {
        Map = {{"List",
                variantMultiMap({{"DataType", QVariant::fromValue(BasicMap)}})}};
    }
    else
    {
        Map = BasicMap;
    }
    codegen::fdl::CDataType DataType;
    DataType.fromVariant(QVariant::fromValue(Map));
    qCDebug(sila_cpp_data_types) << DataType;

    return codegen::fdl::CFDLSerializer::serialize(DataType).chopped(1);
}

//============================================================================
template<typename DataTypeT, typename>
const google::protobuf::Descriptor* CDataTypeSerializer::getDescriptor(
    const DataTypeT& Payload)
{
    if constexpr (internal::is_container_v<DataTypeT>)
    {
        return getDescriptor(Payload.front());
    }
    else if constexpr (internal::is_sila_type_v<DataTypeT>)
    {
        return Payload.toProtoMessagePtr()->GetDescriptor();
    }
    else
    {
        return Payload.GetDescriptor();
    }
}
}  // namespace SiLA2

#endif  // DATATYPESERIALIZER_H
