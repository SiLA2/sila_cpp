/*******************************************************************************
 ** This file is part of the sila_cpp project.
 ** Copyright 2022 SiLA 2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 *****************************************************************************/

//============================================================================
/// \file   utils.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   20.10.2022
/// \brief  Declaration of constraint helpers
//============================================================================
#ifndef FRAMEWORK_CONSTRAINTS_UTILS_H
#define FRAMEWORK_CONSTRAINTS_UTILS_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/global.h>

#include <QString>

namespace SiLA2
{
/**
 * @brief The CFullyQualifiedIdentifierRegex class is a helper class containing
 * the regular expressions for validating SiLA 2 Fully Qualified Identifiers
 */
class SILA_CPP_EXPORT CFullyQualifiedIdentifierRegex
{
public:
    /**
     * @brief Returns the regular expression to validate a Fully Qualified Feature
     * Identifier
     *
     * @return The regex for a Fully Qualified Feature Identifier
     */
    [[nodiscard]] static QString featureIdentifier();

    /**
     * @brief Returns the regular expression to validate a Fully Qualified Command
     * Identifier
     *
     * @return The regex for a Fully Qualified Command Identifier
     */
    [[nodiscard]] static QString commandIdentifier();

    /**
     * @brief Returns the regular expression to validate a Fully Qualified Command
     * Parameter Identifier
     *
     * @return The regex for a Fully Qualified Command Parameter Identifier
     */
    [[nodiscard]] static QString commandParameterIdentifier();

    /**
     * @brief Returns the regular expression to validate a Fully Qualified Command
     * Response Identifier
     *
     * @return The regex for a Fully Qualified Command Response Identifier
     */
    [[nodiscard]] static QString commandResponseIdentifier();

    /**
     * @brief Returns the regular expression to validate a Fully Qualified
     * Intermediate Command Response Identifier
     *
     * @return The regex for a Fully Qualified Intermediate Command Response
     * Identifier
     */
    [[nodiscard]] static QString intermediateCommandResponseIdentifier();

    /**
     * @brief Returns the regular expression to validate a Fully Qualified Defined
     * Execution Error Identifier
     *
     * @return The regex for a Fully Qualified Defined Execution Error Identifier
     */
    [[nodiscard]] static QString definedExecutionErrorIdentifier();

    /**
     * @brief Returns the regular expression to validate a Fully Qualified
     * Property Identifier
     *
     * @return The regex for a Fully Qualified Property Identifier
     */
    [[nodiscard]] static QString propertyIdentifier();

    /**
     * @brief Returns the regular expression to validate a Fully Qualified Data
     * Type Identifier
     *
     * @return The regex for a Fully Qualified Data Type Identifier
     */
    [[nodiscard]] static QString dataTypeIdentifier();

    /**
     * @brief Returns the regular expression to validate a Fully Qualified
     * Metadata Identifier
     *
     * @return The regex for a Fully Qualified Metadata Identifier
     */
    [[nodiscard]] static QString metadataIdentifier();

    /**
     * @brief Returns the regular expression for the given @a Identifier (e.g.
     * "FeatureIdentifier" or "CommandResponseIdentifier")
     *
     * @return The regex for the given @a Identifier
     */
    [[nodiscard]] static QString get(const QString& Id);
};
}  // namespace SiLA2

#endif  // FRAMEWORK_CONSTRAINTS_UTILS_H
