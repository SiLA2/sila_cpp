/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   FrameworkError.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   29.01.2020
/// \brief  Declaration of the CFrameworkError class
//============================================================================
#ifndef FRAMEWORKERROR_H
#define FRAMEWORKERROR_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/framework/grpc/SiLAFramework.pb.h>
#include <sila_cpp/global.h>

#include "SiLAError.h"

#include <grpcpp/impl/codegen/status.h>

//============================================================================
//                            FORWARD DECLARATIONS
//============================================================================
namespace sila2::org::silastandard
{
class SiLAError;
}  // namespace sila2::org::silastandard

namespace SiLA2
{
/**
 * @brief The CFrameworkError class represents a SiLA 2 Framework Error.
 *
 * A Framework Error is an error which occurs when a SiLA Client accesses a
 * SiLA Server in a way that violates the SiLA 2 specification.
 */
class SILA_CPP_EXPORT CFrameworkError : public CSiLAError
{
    class PrivateImpl;

public:
    /**
     * @brief The FrameworkErrorType enum specifies the different types of SiLA 2
     * Framework Errors
     */
    enum class FrameworkErrorType : uint8_t
    {
        CommandExecutionNotAccepted = sila2::org::silastandard::
            FrameworkError_ErrorType_COMMAND_EXECUTION_NOT_ACCEPTED,
        InvalidCommandExecutionUuid = sila2::org::silastandard::
            FrameworkError_ErrorType_INVALID_COMMAND_EXECUTION_UUID,
        CommandExecutionNotFinished = sila2::org::silastandard::
            FrameworkError_ErrorType_COMMAND_EXECUTION_NOT_FINISHED,
        InvalidMetadata =
            sila2::org::silastandard::FrameworkError_ErrorType_INVALID_METADATA,
        NoMetadataAllowed =
            sila2::org::silastandard::FrameworkError_ErrorType_NO_METADATA_ALLOWED,
        Invalid,

        COMMAND_EXECUTION_NOT_ACCEPTED
        [[deprecated("Use the CamelCase version instead")]] =
            CommandExecutionNotAccepted,
        INVALID_COMMAND_EXECUTION_UUID
        [[deprecated("Use the CamelCase version instead")]] =
            InvalidCommandExecutionUuid,
        COMMAND_EXECUTION_NOT_FINISHED
        [[deprecated("Use the CamelCase version instead")]] =
            CommandExecutionNotFinished,
        INVALID_METADATA
        [[deprecated("Use the CamelCase version instead")]] = InvalidMetadata,
        NO_METADATA_ALLOWED
        [[deprecated("Use the CamelCase version instead")]] = NoMetadataAllowed,
        INVALID [[deprecated("Use the CamelCase version instead")]] = Invalid,
    };
    using eFrameworkErrorType
        [[deprecated("Use FrameworkErrorType (without the 'e' prefix) "
                     "instead")]] = FrameworkErrorType;

    /**
     * @brief C'tor
     *
     * @param Type The error type of the Framework Error as defined in the
     * SiLA standard
     * @param Message The error message providing details about the occurred
     * error. If left empty, an extremely generic error message will be used.
     */
    explicit CFrameworkError(FrameworkErrorType Type, std::string Message = "");

    /**
     * @brief Get the framework error type of this error
     *
     * @return This framework error's type
     */
    [[nodiscard]] FrameworkErrorType frameworkErrorType() const;

    /**
     * @brief Get a human-readable representation of the framework error type of
     * this error
     *
     * This is a convenience method and essentially the same as
     * @code
     * CFrameworkError::frameworkErrorTypeToString(SomeFrameworkError.frameworkErrorType());
     * @endcode
     *
     * @return This framework error's type's human-readable string representation
     */
    [[nodiscard]] std::string frameworkErrorTypeName() const;

    /**
     * @brief Get a human-readable name of the given Framework Error Type @a Type
     *
     * @param Type The Framework Error Type to convert into a string
     * @return const std::string& The human-readable name of the Error Type
     */
    [[nodiscard]] static std::string frameworkErrorTypeToString(
        FrameworkErrorType Type);

    /**
     * @brief Create a Framework Error from the given gRPC error message @a Error
     *
     * @param Error The gRPC error message that is serialized into a
     * @c grpc::Status object in case an error occurred during a RPC
     * @return A Framework Error where its fields have been deserialized from
     * @a Error
     */
    [[nodiscard]] static CFrameworkError fromErrorMessage(
        const sila2::org::silastandard::SiLAError& Error);

    /**
     * @override
     * @brief Get a string with explanatory information about the error.
     */
    [[nodiscard]] const char* what() const noexcept override;

    /**
     * @override
     *
     * @brief Throws this error
     */
    void raise() const override { throw *this; }

    /**
     * @override
     *
     * @brief Clones this error
     *
     * @return A copy of this error
     */
    [[nodiscard]] CFrameworkError* clone() const override
    {
        return new CFrameworkError{*this};
    }

private:
    PIMPL_DECLARE_PRIVATE(CFrameworkError)
};
}  // namespace SiLA2

#endif  // FRAMEWORKERROR_H
