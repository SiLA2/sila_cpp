/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   ConnectionError.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   26.04.2021
/// \brief  Declaration of the CConnectionError class
//============================================================================
#ifndef CONNECTIONERROR_H
#define CONNECTIONERROR_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/global.h>

#include "SiLAError.h"

#include <grpcpp/impl/codegen/status.h>

//============================================================================
//                            FORWARD DECLARATIONS
//============================================================================
namespace sila2::org::silastandard
{
class SiLAError;
}  // namespace sila2::org::silastandard

namespace SiLA2
{
/**
 * @brief The CConnectionError class represents a SiLA 2 Connection Error.
 *
 * SiLA 2 treats any error with the Connection between a SiLA Client and a SiLA
 * Server as a Connection Error. In contrast to the other error types, Connection
 * Errors are not issued by the SiLA Server nor the SiLA Client, but by the
 * underlying infrastructure.
 */
class SILA_CPP_EXPORT CConnectionError : public CSiLAError
{
    class PrivateImpl;

public:
    /**
     * @brief C'tor
     *
     * @param Status The @c grpc::Status that indicates a Connection Error
     */
    explicit CConnectionError(grpc::Status Status);

    /**
     * @brief Get the @c grpc::StatusCode of this Error
     *
     * @return The error's status code
     */
    [[nodiscard]] grpc::StatusCode statusCode() const;

    /**
     * @brief Get a human-readable representation of the status code of this error
     *
     * This is a convenience method and essentially the same as
     * @code
     * CConnectionError::statusCodeToString(ConnectionError.statusCode());
     * @endcode
     *
     * @return This error's status code's human-readable string representation
     */
    [[nodiscard]] std::string statusCodeName() const;

    /**
     * @brief Convert the given @c grpc::StatusCode @a Code to its human-readable
     * string representation
     *
     * @param Code The @c grpc::StatusCode to convert
     * @return The @a Code's human-readable string representation
     */
    [[nodiscard]] static std::string statusCodeToString(grpc::StatusCode Code);

    /**
     * @override
     *
     * @brief Converts this Error to a @c grpc::Status that can be sent along
     * with an RPC.
     *
     * @note Connection Errors are not issued by a SiLA Server nor a SiLA Client
     *   but only by the underlying gRPC framework! Hence, this function should
     *   not need to be called by the average user. You should really know what
     *   you're doing with the @c grpc::Status you're getting back here.
     */
    [[nodiscard]] QT_DEPRECATED_X("Connection Errors are only issued by gRPC and "
                                  "not the SiLA Server/Client!") grpc::Status
        toStatus() const override;

    /**
     * @override
     *
     * @brief Get a string with explanatory information about the error.
     */
    [[nodiscard]] const char* what() const noexcept override;

    /**
     * @override
     *
     * @brief Throws this error
     */
    void raise() const override { throw *this; }

    /**
     * @override
     *
     * @brief Clones this error
     *
     * @return A copy of this error
     */
    [[nodiscard]] CConnectionError* clone() const override
    {
        return new CConnectionError{*this};
    }

private:
    PIMPL_DECLARE_PRIVATE(CConnectionError)
};
}  // namespace SiLA2

#endif  // CONNECTIONERROR_H
