/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   IObservableCommandManager.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   24.06.2020
/// \brief  Definition of the IObservableCommandManager class
//============================================================================
#ifndef IOBSERVABLECOMMANDMANAGER_H
#define IOBSERVABLECOMMANDMANAGER_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/framework/data_types/CommandExecutionUUID.h>
#include <sila_cpp/framework/data_types/SiLADuration.h>

#include "ICommandManager.h"
#include "ObservableCommandWrapper.h"

#include <QCoreApplication>

#include <memory>
#include <string_view>
#include <utility>

namespace SiLA2
{
/**
 * @brief The IObservableCommandManager class is the base class of every SiLA 2
 * Observable Command Manager.
 *
 * @tparam ParametersT The type of the Command Parameters
 * @tparam ResponsesT The type of the Command Responses
 * @tparam IntermediateResponsesT The type of the Intermediate Responses (not
 * actually a pack, but rather an optional type parameter)
 */
template<typename ParametersT, typename ResponsesT,
         typename... IntermediateResponsesT>
class IObservableCommandManager :
    public ICommandManager<CObservableCommandWrapper<ParametersT, ResponsesT,
                                                     IntermediateResponsesT...>>
{
public:
    using Base =
        ICommandManager<CObservableCommandWrapper<ParametersT, ResponsesT,
                                                  IntermediateResponsesT...>>;
    using ObservableCommandWrapper =
        CObservableCommandWrapper<ParametersT, ResponsesT,
                                  IntermediateResponsesT...>;

    /**
     * @brief C'tor
     *
     * @param Feature A pointer to the SiLA Feature that this Command belongs to
     * @param Lifetime The Lifetime of this Command (specifies the Duration for
     * which the UUID of a Command Execution is valid)
     */
    explicit IObservableCommandManager(ISiLAFeature* Feature,
                                       CDuration Lifetime = {});

    /**
     * @brief Add a new Command to the Manager. This creates a new
     * @a CObservableCommandWrapper instance and associates it with a new Command
     * Execution UUID.
     *
     * @return A tuple of the Command Execution UUID, the Command's Lifetime and
     * a pointer to the newly created Command
     */
    std::tuple<const CCommandExecutionUUID, const CDuration,
               std::shared_ptr<ObservableCommandWrapper>>
    addCommand();

    /**
     * @brief Get the Command currently being executed that is identified by @a
     * UUID.
     *
     * @param UUID The Command Execution UUID of the Command to retrieve
     *
     * @return A shared_ptr to the Command identified by @a UUID or a nullptr if
     * there is no such Command.
     */
    [[nodiscard]] std::shared_ptr<ObservableCommandWrapper> getCommandByUUID(
        const CCommandExecutionUUID& UUID) const;

    /**
     * @brief Terminate all currently running Executions of this Command
     */
    void interruptAll() const;

private:
    QHash<CCommandExecutionUUID,
          std::shared_ptr<ObservableCommandWrapper>>
        m_Commands;  ///< Maps UUIDs to their corresponding Command Wrapper
    const CDuration m_Lifetime{};  ///< Initial Lifetime of Execution
};

//============================================================================
template<typename ParametersT, typename ResponsesT,
         typename... IntermediateResponsesT>
IObservableCommandManager<ParametersT, ResponsesT, IntermediateResponsesT...>::
    IObservableCommandManager(ISiLAFeature* Feature, CDuration Lifetime)
    : Base{Feature}, m_Lifetime{std::move(Lifetime)}
{}

//============================================================================
template<typename ParametersT, typename ResponsesT,
         typename... IntermediateResponsesT>
std::tuple<const CCommandExecutionUUID, const CDuration,
           std::shared_ptr<CObservableCommandWrapper<ParametersT, ResponsesT,
                                                     IntermediateResponsesT...>>>
IObservableCommandManager<ParametersT, ResponsesT,
                          IntermediateResponsesT...>::addCommand()
{
    const auto UUID = CCommandExecutionUUID{};
    const auto Command =
        m_Commands
            .insert(UUID, std::make_shared<ObservableCommandWrapper>(
                              UUID, m_Lifetime, this))
            .value();
    Command->moveToThread(qApp->thread());

    // automatically remove the Command when it's lifetime expired
    QObject::connect(Command.get(), &IObservableCommandWrapper::lifetimeExpired,
                     [this](const CCommandExecutionUUID& UUID) {
                         qCInfo(sila_cpp_server).nospace().noquote()
                             << "Lifetime of Execution for Observable Command "
                             << this->identifier()
                             << " (UUID: " << UUID.toString() << ") has expired";
                         auto Command = m_Commands.take(UUID);
                         // There is still a CCommandInfoRPC instance out there
                         // that holds a reference to the Command Wrapper until
                         // the Wrapper's Executor Thread finishes. Then the Info
                         // RPC releases its reference and we can safely return
                         // from this slot which will subsequently deallocate the
                         // Command Wrapper instance we were holding here (i.e.
                         // the one we took from the m_Commands hash above). It's
                         // important that the deallocation happens upon returning
                         // from this slot because it runs in the main application
                         // thread. When we wouldn't wait here then the
                         // deallocation would happen when the Info RPC releases
                         // its reference to the Wrapper (since this would be the
                         // last reference). This however happens in the slot that
                         // is connected to the Wrapper's `executionFinished`
                         // signal and this slot is executed in the signalling
                         // thread (which is the Command Wrapper's Executor
                         // Thread)
                         while (Command.use_count() > 1)
                         {
                             QThread::msleep(250);
                         }
                     });

    qCDebug(sila_cpp_server) << "Created new Command Wrapper" << Command.get()
                             << "for execution with" << UUID;
    return {UUID, m_Lifetime, Command};
}

//=============================================================================
template<typename ParametersT, typename ResponsesT,
         typename... IntermediateResponsesT>
std::shared_ptr<
    CObservableCommandWrapper<ParametersT, ResponsesT, IntermediateResponsesT...>>
IObservableCommandManager<ParametersT, ResponsesT, IntermediateResponsesT...>::
    getCommandByUUID(const CCommandExecutionUUID& UUID) const
{
    return m_Commands.value(UUID, nullptr);
}

//============================================================================
template<typename ParametersT, typename ResponsesT,
         typename... IntermediateResponsesT>
void IObservableCommandManager<ParametersT, ResponsesT,
                               IntermediateResponsesT...>::interruptAll() const
{
    foreach (const auto& Command, m_Commands.values())
    {
        if (Command->started())
        {
            Command->requestInterruption();
        }
    }
}
}  // namespace SiLA2

#endif  // IOBSERVABLECOMMANDMANAGER_H
