/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   UnobservablePropertyWrapper.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   13.07.2020
/// \brief  Declaration of the CUnobservablePropertyWrapper class
//============================================================================
#ifndef UNOBSERVABLEPROPERTYWRAPPER_H
#define UNOBSERVABLEPROPERTYWRAPPER_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/server/SiLAServer.h>
#include <sila_cpp/server/rpc/AsyncRPCHandler.h>
#include <sila_cpp/server/rpc/GetRPC.h>

#include "IPropertyWrapper.h"

#include <QObject>

namespace SiLA2
{
/**
 * @brief The CUnobservablePropertyWrapper class is a class template that allows
 * to implement SiLA 2 Unobservable Properties.
 *
 * @tparam T Underlying type of the actual Property
 * @tparam RequestGetF Member function pointer of the `RequestGet_<Property>`
 * function
 *
 * @note The underlying type must be convertible to the Data Type used in the
 * Feature Description. I.e. if in the FDL file the Property was declared as
 * @code
 *     <Property>
 *         <Identifier>MyUnobservableProperty</Identifier>
 *         <DisplayName>My Unobservable Property</DisplayName>
 *         <Description>...</Description>
 *         <Unobservable>Yes</Unobservable>
 *         <DataType>
 *             <Basic>Real</Basic>
 *         </DataType>
 *     </Property>
 * @endcode
 *
 * then the underlying type should either be @a SiLA2::CReal or @a double.
 *
 * @details As a user you just need to create a CUnobservablePropertyWrapper
 * instance providing the necessary function for the Unobservable Property and the
 * underlying type of the actual Property as template parameters. In your
 * Feature's constructor you need to pass a reference to your Feature and
 * optionally set an initial value. The rest is handled by this class. For example
 * @code
 * class MySiLAFeatureImpl : public SiLA2::CSiLAFeature<...>
 * {
 * public:
 *     MySiLAFeatureImpl()
 *         : m_MyUnobservableProperty{this, SiLA2::CReal{1.0}}
 *     {}
 *
 * private:
 *     SiLA2::CUnobservablePropertyWrapper<
 *         &MySiLAFeatureImpl::RequestGet_MyUnobservableProperty,
 *         SiLA2::CReal>
 *         m_MyUnobservableProperty;
 * };
 * @endcode
 *
 * You can access your property via the getter and setter functions (@a value()
 * and @a setValue()). If the underlying type of the Property supports the
 * assignment operator you can also assign values directly by using this class's
 * @a operator=() overload. E.g. considering the code above you could assign new
 * values to @c m_MyUnobservableProperty like this:
 * @code
 * m_MyUnobservableProperty.setValue(123.45); // using the setter function
 * m_MyUnobservableProperty = 123.45;         // using the `operator=()` overload
 * @endcode
 *
 * See the CGreetingProviderImpl class in the HelloSiLA2 example for an example on
 * how to implement Unobservable Properties.
 */
template<typename T, auto RequestGetF>
class CUnobservablePropertyWrapper;

/**
 * @brief Template specialisation of the CUnobservablePropertyWrapper class
 *
 * @tparam T Underlying type of the actual Property
 * @tparam GetServiceT The gRPC Service that contains the `RequestGet_<Property>`
 * member function
 * @tparam ParametersT The type of the `Get_<Property>` parameters
 * @tparam ResponsesT The type of the `Get_<Property>` responses
 * @tparam RG Signature of the `RequestGet_<Property>` member function pointer
 */
template<typename T, typename GetServiceT, typename ParametersT,
         typename ResponsesT, GetServiceTemplate>
class CUnobservablePropertyWrapper<T, RG> : public IPropertyWrapper<T, ResponsesT>
{
    using Super = IPropertyWrapper<T, ResponsesT>;

public:
    using typename Super::ValueCallbackF;
    template<typename FeatureT>
    using ValueCallbackMemF =
        typename Super::template ValueCallbackMemF<FeatureT>;
    template<typename FeatureT>
    using ValueCallbackConstMemF =
        typename Super::template ValueCallbackConstMemF<FeatureT>;

    /**
     * @brief C'tor
     *
     * @tparam SiLAFeatureT User defined SiLA Feature Implementation
     * @param Feature The SiLA Feature that contains this Unobservable Property
     * @param Value The initial value of the Property
     */
    template<typename SiLAFeatureT,
             typename =
                 std::enable_if_t<std::is_base_of_v<ISiLAFeature, SiLAFeatureT>>>
    explicit CUnobservablePropertyWrapper(SiLAFeatureT* Feature,
                                          const T& Value = T{});

    /**
     * @brief C'tor
     *
     * @tparam SiLAFeatureT User defined SiLA Feature Implementation
     * @param Feature A pointer to the SiLA Feature that this Property belongs to
     * @param Callback A callback function (can be a free function or a lambda, or
     * a const/non-const member function of @a Feature) that should be used to get
     * the current value of the Property
     */
    template<typename SiLAFeatureT,
             typename =
                 std::enable_if_t<std::is_base_of_v<ISiLAFeature, SiLAFeatureT>>>
    CUnobservablePropertyWrapper(SiLAFeatureT* Feature, ValueCallbackF Callback);

    template<typename SiLAFeatureT,
             typename =
                 std::enable_if_t<std::is_base_of_v<ISiLAFeature, SiLAFeatureT>>>
    CUnobservablePropertyWrapper(SiLAFeatureT* Feature,
                                 ValueCallbackMemF<SiLAFeatureT> Callback);

    template<typename SiLAFeatureT,
             typename =
                 std::enable_if_t<std::is_base_of_v<ISiLAFeature, SiLAFeatureT>>>
    CUnobservablePropertyWrapper(SiLAFeatureT* Feature,
                                 ValueCallbackConstMemF<SiLAFeatureT> Callback);

    /**
     * @brief C'tor
     *
     * @tparam SiLAFeatureT User defined SiLA Feature Implementation
     * @param Feature A pointer to the SiLA Feature that this Property belongs to
     * @param Value The initial value of the Property
     * @param Callback A callback function (can be a free function or a lambda, or
     * a const/non-const member function of @a Feature) that should be used to get
     * the current value of the Property
     */
    template<typename SiLAFeatureT,
             typename =
                 std::enable_if_t<std::is_base_of_v<ISiLAFeature, SiLAFeatureT>>>
    CUnobservablePropertyWrapper(SiLAFeatureT* Feature, const T& Value,
                                 ValueCallbackF Callback);

    template<typename SiLAFeatureT,
             typename =
                 std::enable_if_t<std::is_base_of_v<ISiLAFeature, SiLAFeatureT>>>
    CUnobservablePropertyWrapper(SiLAFeatureT* Feature, const T& Value,
                                 ValueCallbackMemF<SiLAFeatureT> Callback);

    template<typename SiLAFeatureT,
             typename =
                 std::enable_if_t<std::is_base_of_v<ISiLAFeature, SiLAFeatureT>>>
    CUnobservablePropertyWrapper(SiLAFeatureT* Feature, const T& Value,
                                 ValueCallbackConstMemF<SiLAFeatureT> Callback);

    /**
     * @brief Assignment operator for a more convenient usage with underlying
     * types that have an assignment operator themselves.
     * @note This function is only available for underlying types that are either
     * POD or implement @c operator=()
     *
     * @tparam U The type of the value to assign
     *
     * @param val The new value to set
     */
    template<typename U>
    typename std::enable_if_t<(std::is_pod_v<T> && std::is_pod_v<U>)
                                  || internal::has_assignment_operator_v<T, U>,
                              CUnobservablePropertyWrapper&>
    operator=(const U& val);

private:
    /**
     * @brief Connects to the SiLA Server's @c started() signal to start the Async
     * RPC handler after the Server has been started
     *
     * @param Feature The SiLA Feature whose server to connect to
     */
    void connectToServerStarted(ISiLAFeature* Feature);

    using GetRPC = CGetRPC<GetServiceT, ParametersT, ResponsesT, T, RG>;
    CAsyncRPCHandler<CUnobservablePropertyWrapper, GetServiceT, GetRPC>
        m_Handler;  ///< RPC handler for the Get RPC
};

//=============================================================================
template<typename T, typename GetServiceT, typename ParametersT,
         typename ResponsesT, GetServiceTemplate>
template<typename SiLAFeatureT, typename>
CUnobservablePropertyWrapper<T, RG>::CUnobservablePropertyWrapper(
    SiLAFeatureT* Feature, const T& Value)
    : Super{Feature, Value},
      m_Handler{this, Feature, Feature->server()->addCompletionQueue()}
{
    connectToServerStarted(Feature);
}

//=============================================================================
template<typename T, typename GetServiceT, typename ParametersT,
         typename ResponsesT, GetServiceTemplate>
template<typename SiLAFeatureT, typename>
CUnobservablePropertyWrapper<T, RG>::CUnobservablePropertyWrapper(
    SiLAFeatureT* Feature, ValueCallbackF Callback)
    : Super{Feature, std::move(Callback)},
      m_Handler{this, Feature, Feature->server()->addCompletionQueue()}
{
    connectToServerStarted(Feature);
}

//=============================================================================
template<typename T, typename GetServiceT, typename ParametersT,
         typename ResponsesT, GetServiceTemplate>
template<typename SiLAFeatureT, typename>
CUnobservablePropertyWrapper<T, RG>::CUnobservablePropertyWrapper(
    SiLAFeatureT* Feature, ValueCallbackMemF<SiLAFeatureT> Callback)
    : Super{Feature, std::move(Callback)},
      m_Handler{this, Feature, Feature->server()->addCompletionQueue()}
{
    connectToServerStarted(Feature);
}

//=============================================================================
template<typename T, typename GetServiceT, typename ParametersT,
         typename ResponsesT, GetServiceTemplate>
template<typename SiLAFeatureT, typename>
CUnobservablePropertyWrapper<T, RG>::CUnobservablePropertyWrapper(
    SiLAFeatureT* Feature, ValueCallbackConstMemF<SiLAFeatureT> Callback)
    : Super{Feature, std::move(Callback)},
      m_Handler{this, Feature, Feature->server()->addCompletionQueue()}
{
    connectToServerStarted(Feature);
}

//=============================================================================
template<typename T, typename GetServiceT, typename ParametersT,
         typename ResponsesT, GetServiceTemplate>
template<typename SiLAFeatureT, typename>
CUnobservablePropertyWrapper<T, RG>::CUnobservablePropertyWrapper(
    SiLAFeatureT* Feature, const T& Value, ValueCallbackF Callback)
    : Super{Feature, Value, std::move(Callback)},
      m_Handler{this, Feature, Feature->server()->addCompletionQueue()}
{
    connectToServerStarted(Feature);
}

//=============================================================================
template<typename T, typename GetServiceT, typename ParametersT,
         typename ResponsesT, GetServiceTemplate>
template<typename SiLAFeatureT, typename>
CUnobservablePropertyWrapper<T, RG>::CUnobservablePropertyWrapper(
    SiLAFeatureT* Feature, const T& Value,
    ValueCallbackMemF<SiLAFeatureT> Callback)
    : Super{Feature, Value, std::move(Callback)},
      m_Handler{this, Feature, Feature->server()->addCompletionQueue()}
{
    connectToServerStarted(Feature);
}

//=============================================================================
template<typename T, typename GetServiceT, typename ParametersT,
         typename ResponsesT, GetServiceTemplate>
template<typename SiLAFeatureT, typename>
CUnobservablePropertyWrapper<T, RG>::CUnobservablePropertyWrapper(
    SiLAFeatureT* Feature, const T& Value,
    ValueCallbackConstMemF<SiLAFeatureT> Callback)
    : Super{Feature, Value, std::move(Callback)},
      m_Handler{this, Feature, Feature->server()->addCompletionQueue()}
{
    connectToServerStarted(Feature);
}

//=============================================================================
template<typename T, typename GetServiceT, typename ParametersT,
         typename ResponsesT, GetServiceTemplate>
template<typename U>
typename std::enable_if_t<(std::is_pod_v<T> && std::is_pod_v<U>)
                              || internal::has_assignment_operator_v<T, U>,
                          CUnobservablePropertyWrapper<T, RG>&>
CUnobservablePropertyWrapper<T, RG>::operator=(const U& val)
{
    this->setValue(static_cast<T>(val));
    return *this;
}

//=============================================================================
template<typename T, typename GetServiceT, typename ParametersT,
         typename ResponsesT, GetServiceTemplate>
void CUnobservablePropertyWrapper<T, RG>::connectToServerStarted(
    ISiLAFeature* Feature)
{
    QObject::connect(Feature->server(), &CSiLAServer::started,
                     [this]() { m_Handler.start(); });
}
}  // namespace SiLA2

#endif  // UNOBSERVABLEPROPERTYWRAPPER_H
