/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   Status.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   08.07.2020
/// \brief  Declaration of the CStatus class
//============================================================================
#ifndef STATUS_H
#define STATUS_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/framework/error_handling/SiLAError.h>
#include <sila_cpp/global.h>

#include <grpcpp/impl/codegen/status.h>

namespace SiLA2
{
/**
 * @brief The CStatus class indicates if a SiLA 2 related operation was successful
 * or not. Construct a CStatus instance from a SiLa Error to indicate a
 * non-successful operation.
 *
 * @details This class wraps grpc::Status and adds some functionality for better
 * integration with SiLA 2 Command Execution
 */
class SILA_CPP_EXPORT CStatus
{
public:
    /**
     * @brief Construct an OK instance
     */
    CStatus() noexcept;

    /**
     * @brief Construct a Status object from a SiLA Error
     *
     * @param Error The Error used to construct the Status
     *
     * @return The constructed Status instance
     */
    static CStatus fromSiLAError(const CSiLAError& Error);

    /**
     * @brief Whether the Status indicates success or not
     *
     * @returns true, for success
     * @returns false, otherwise
     */
    [[nodiscard]] bool ok() const;

    explicit operator grpc::Status() const;

    static const CStatus& OK;  ///< Predefined OK instance

private:
    /**
     * @brief Converting c'tor from a grpc::Status
     */
    explicit CStatus(grpc::Status Status);

    grpc::Status m_Status;

    friend QDebug& operator<<(QDebug dbg, const CStatus& rhs);
};
}  // namespace SiLA2

#endif  // STATUS_H
