/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   FullyQualifiedFeatureID.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   23.01.2020
/// \brief  Declaration of the CFullyQualifiedFeatureID class
//============================================================================
#ifndef FULLYQUALIFIEDFEATUREID_H
#define FULLYQUALIFIEDFEATUREID_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/global.h>

#include <QString>
#include <QStringView>

#include <string_view>

namespace SiLA2
{
/**
 * @brief The FullyQualifiedIdentifierPartIndex enum defines the indices of the
 * different fields used in all Fully Qualified Identifiers
 */
enum class FullyQualifiedIdentifierPartIndex : uint8_t
{
    Originator = 0,
    Category = 1,
    FeatureIdentifier = 2,
    Version = 3,
    FullyQualifiedFeatureIdentifierLength = 4,

    Command = 4,
    CommandIdentifier = 5,
    FullyQualifiedCommandIdentifierLength = 6,

    CommandParameter = 6,
    CommandParameterIdentifier = 7,
    FullyQualifiedCommandParameterIdentifierLength = 8,

    CommandResponse = 6,
    CommandResponseIdentifier = 7,
    FullyQualifiedCommandResponseIdentifierLength = 8,

    CommandIntermediateResponse = 6,
    CommandIntermediateResponseIdentifier = 7,
    FullyQualifiedCommandIntermediateResponseIdentifierLength = 8,

    Property = 4,
    PropertyIdentifier = 5,
    FullyQualifiedPropertyIdentifierLength = 6,

    DataType = 4,
    DataTypeIdentifier = 5,
    FullyQualifiedDataTypeIdentifierLength = 6,

    DefinedExecutionError = 4,
    DefinedExecutionErrorIdentifier = 5,
    FullyQualifiedDefinedExecutionErrorIdentifierLength = 6,

    Metadata = 4,
    MetadataIdentifier = 5,
    FullyQualifiedMetadataIdentifierLength = 6,
};

/**
 * @brief Logical operator overloads for FullyQualifiedIdentifierPartIndex with
 * integer types
 */
template<typename T, typename = std::enable_if_t<std::is_integral_v<T>>>
bool operator==(const FullyQualifiedIdentifierPartIndex lhs, const T rhs)
{
    return static_cast<int>(lhs) == rhs;
}

//============================================================================
template<typename T, typename = std::enable_if_t<std::is_integral_v<T>>>
bool operator==(const T lhs, const FullyQualifiedIdentifierPartIndex rhs)
{
    return rhs == lhs;
}

//============================================================================
template<typename T, typename = std::enable_if_t<std::is_integral_v<T>>>
bool operator!=(const FullyQualifiedIdentifierPartIndex lhs, const T rhs)
{
    return !(lhs == rhs);
}

//============================================================================
template<typename T, typename = std::enable_if_t<std::is_integral_v<T>>>
bool operator!=(const T lhs, const FullyQualifiedIdentifierPartIndex rhs)
{
    return rhs != lhs;
}

/**
 * @brief Convenience operator to convert a FullyQualifiedIdentifierPartIndex to
 * and integer
 */
inline int operator+(const FullyQualifiedIdentifierPartIndex& rhs)
{
    return static_cast<int>(rhs);
}

/**
 * @brief The CFullyQualifiedFeatureID class represents a Fully Qualified
 * Feature Identifier as specified by the SiLA 2 standard
 */
class SILA_CPP_EXPORT CFullyQualifiedFeatureID
{
public:
    /**
     * @brief C'tor
     *
     * @param Originator The Feature's Originator (e.g. 'org.silastandard')
     * @param Category The Feature's Category
     * @param Identifier The Feature's Identifier
     * @param Version The Feature's Major Version (has to be in the format "v1",
     * for example)
     */
    CFullyQualifiedFeatureID(std::string_view Originator,
                             std::string_view Category,
                             std::string_view Identifier,
                             std::string_view Version);
    CFullyQualifiedFeatureID(QStringView Originator, QStringView Category,
                             QStringView Identifier, QStringView Version);

    /**
     * @brief Default c'tor
     *
     * Constructs an empty (invalid) Fully Qualified Feature Identifier
     */
    CFullyQualifiedFeatureID() = default;

    /**
     * @brief D'tor
     */
    virtual ~CFullyQualifiedFeatureID();

    /**
     * @brief Construct a CFullyQualifiedFeatureID from the given string @a from.
     * This assumes that @a from is already in the form of a Fully Qualified
     * Identifier and just needs to be converted to the correct type.
     *
     * @param from A Fully Qualified Feature Identifier as a @c std::string
     * @return The corresponding Fully Qualified Feature Identifier or an empty
     * Identifier if the given string @a from was invalid
     *
     * @sa isValid()
     */
    [[nodiscard]] static CFullyQualifiedFeatureID fromString(const QString& from);
    [[nodiscard]] static CFullyQualifiedFeatureID fromStdString(
        const std::string& from);

    /**
     * @brief Check if this Fully Qualified Identifier is equal to @a rhs
     * according to the <i>Uniqueness of Identifiers</i> defined in the standard
     * (i.e. Identifiers are case-insensitive)
     *
     * @param rhs The Identifier to compare to
     * @returns true, if *this == rhs, false otherwise
     */
    bool operator==(const CFullyQualifiedFeatureID& rhs) const;

    /**
     * @brief Get the Originator of the SiLA Feature without any additional
     * information like the Category, Identifier or the Feature Version.
     *
     * @return QString The Feature's Originator
     */
    [[nodiscard]] QString originator() const;

    /**
     * @brief Get the Category of the SiLA Feature without any additional
     * information like the Originator, Identifier or the Feature Version.
     *
     * @return QString The Feature's Category
     */
    [[nodiscard]] QString category() const;

    /**
     * @brief Get the Major Version of the SiLA Feature without any additional
     * information like the Originator, Category or the Identifier.
     *
     * @return QString The Feature's Major Version
     */
    [[nodiscard]] QString version() const;

    /**
     * @brief Get the Fully Qualified Feature Identifier
     *
     * @return The Fully Qualified Feature Identifier part of this Fully Qualified
     * Identifier
     */
    [[nodiscard]] CFullyQualifiedFeatureID featureIdentifier() const;

    /**
     * @brief Get the Identifier of the SiLA Feature without any additional
     * information like the Originator, Category or the Feature Version.
     * @note Since every other Fully Qualified Identifier inherits from this class
     * this method will return different values depending on the exact subclass.
     * For example, @c CFullyQualifiedCommandID::identifier() returns the Command
     * Identifier whereas the Feature Identifier can be obtained through
     * @c CFullyQualifiedCommandID::featureIdentifier().identifier().
     *
     * @return QString The Feature's Identifier
     */
    [[nodiscard]] virtual QString identifier() const;

    /**
     * @brief Check if this Fully Qualified Feature Identifier is valid. Most of
     * the time this is @c true, but when constructing a Fully Qualified Feature
     * Identifier from a string using @c fromString() or @c fromStdString() it
     * might happen that the returned Identifier is invalid if the given string
     * was invalid.
     *
     * @returns true, if the Identifier is valid, false otherwise
     *
     * @sa fromStdString()
     */
    [[nodiscard]] virtual bool isValid() const;

    /**
     * @brief Convert this Fully Qualified Feature Identifier to a @c QString
     *
     * @return The Fully Qualified Feature Identifier as a @c QString
     */
    [[nodiscard]] virtual QString toString() const;

    /**
     * @brief Convert this Fully Qualified Feature Identifier to a @c std::string
     *
     * @return The Fully Qualified Feature Identifier as a @c std::string
     */
    [[nodiscard]] virtual std::string toStdString() const;

    /**
     * @brief Convert this Fully Qualified Feature Identifier into a fully
     * qualified gRPC service name
     *
     * @return The gRPC service name for this Fully Qualified Feature Identifier
     */
    [[nodiscard]] std::string toServiceName() const;

    /**
     * @brief Less than comparison operator to enable usage of Fully Qualified
     * Feature Identifiers, e.g. in maps
     *
     * @note That that Fully Qualified Identifiers are case-insensitive as
     * specified by the SiLA 2 standard Part A (section <i>Uniqueness of
     * Identifiers</i>)
     *
     * @returns true, if @a lhs < @a rhs, false otherwise
     */
    friend SILA_CPP_EXPORT bool operator<(const CFullyQualifiedFeatureID& lhs,
                                          const CFullyQualifiedFeatureID& rhs);

protected:
    /**
     * @internal To be used by derived classes only
     * @brief Convert the Fully Qualified SiLA 2 Identifier to a gRPC fully
     * qualified message name
     *
     * @param MessageName The non-fully qualified message name
     * @return The gRPC fully qualified message name for the given @a MessageName
     */
    [[nodiscard]] std::string toFullyQualifiedMessageName(
        const QString& MessageName) const;

private:
    QString m_Originator{};
    QString m_Category{};
    QString m_Identifier{};
    QString m_Version{};
};

/**
 * @brief Overload for debugging CFullyQualifiedFeatureID
 */
SILA_CPP_EXPORT QDebug operator<<(QDebug dbg,
                                  const CFullyQualifiedFeatureID& rhs);
SILA_CPP_EXPORT std::ostream& operator<<(std::ostream& os,
                                         const CFullyQualifiedFeatureID& rhs);
}  // namespace SiLA2

#endif  // FULLYQUALIFIEDFEATUREID_H
