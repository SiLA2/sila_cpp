/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   ConstraintContentType.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   01.02.2021
/// \brief  Declaration of the CConstraintContentType class
//============================================================================
#ifndef CODEGEN_CONSTRAINTCONTENTTYPE_H
#define CODEGEN_CONSTRAINTCONTENTTYPE_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/fdl/DataType.h>
#include <sila_cpp/codegen/fdl/IConstraint.h>
#include <sila_cpp/global.h>

namespace SiLA2::codegen::fdl
{
/**
 * @brief The CContentTypeParameter class represents a simple wrapper for
 * Content Type Parameters as defined in chapter 5.1 of RFC 2045
 */
class SILA_CPP_EXPORT CContentTypeParameter final : public ISerializable
{
public:
    /**
     * @brief Get the name of this object
     *
     * @return The object's name
     */
    [[nodiscard]] QString name() const override { return "Parameter"; }

    /**
     * @brief Convert this object to its @c QVariant representation
     *
     * @return A QVariant instance for this object
     */
    [[nodiscard]] QVariant toVariant() const override;

    /**
     * @brief Initializes this object with the values given in the @c QVariant
     * @a from
     *
     * @param from The QVariant instance that represents an object of this class
     *
     * @throws std::runtime_error if the initialization fails
     */
    void fromVariant(const QVariant& from) override;

    /**
     * @brief Get the attribute of the Parameter
     *
     * @return The Parameter's attribute
     */
    [[nodiscard]] QString attribute() const { return m_Attribute; }

    /**
     * @brief Get the value of the Parameter
     *
     * @return The Parameter's value
     */
    [[nodiscard]] QString value() const { return m_Value; }

    /**
     * @brief Get the string representation of the Parameter
     *
     * @return The Parameter as a string
     */
    [[nodiscard]] QString toString() const { return m_Attribute + "=" + m_Value; }

protected:
    /**
     * @brief Helper function for the equality comparison operators
     *
     * @param rhs The Content Type Parameter to compare this Parameter to
     *
     * @return @c true, if this Parameter is semantically equal to @a rhs, i.e. if
     * @b Attribute and @b Value match, @c false otherwise
     */
    [[nodiscard]] bool isEqual(const ISerializable& rhs) const override;

private:
    QString m_Attribute;
    QString m_Value;
};

/**
 * @brief The CConstraintContentType class represents a SiLA 2 ContentType
 * Constraint
 */
class SILA_CPP_EXPORT CConstraintContentType final : public IConstraint
{
public:
    /**
     * @brief Get the name of this object
     *
     * @return The object's name
     */
    [[nodiscard]] QString name() const override { return "ContentType"; }

    /**
     * @brief Convert this ContentType Constraint to its @c QVariant
     * representation
     *
     * @return A QVariant instance for this ContentType Constraint
     */
    [[nodiscard]] QVariant toVariant() const override;

    /**
     * @brief Initializes this ContentType Constraint with the values given in
     * the @c QVariant @a from
     *
     * @param from The QVariant instance that represents a ContentType Constraint
     */
    void fromVariant(const QVariant& from) override;

    /**
     * @brief Get the @a Type element of the Content Type Constraint
     *
     * @return The Content Type Constraint's @a Type element
     */
    [[nodiscard]] QString contentType() const { return m_ContentType; }

    /**
     * @brief Get the @a SubType element of the Content Type Constraint
     *
     * @return The Content Type Constraint's @a SubType element
     */
    [[nodiscard]] QString subType() const { return m_SubType; }

    /**
     * @brief Get the list of (optional) Parameters of the Content Type Constraint
     *
     * @return The Content Type Constraint's list of (optional) Parameters
     */
    [[nodiscard]] QList<CContentTypeParameter> parameters() const
    {
        return m_Parameters;
    }

    /**
     * @brief Get the <i>i</i>th CContentTypeParameter of this Content Type
     * Constraint
     *
     * @param i The index of the value to return
     * @return The Content Type Constraint's <i>i</i>th CContentTypeParameter
     */
    [[nodiscard]] CContentTypeParameter parameter(int i) const
    {
        return m_Parameters.at(i);
    }

protected:
    /**
     * @brief Helper function for the equality comparison operators
     *
     * @param rhs The Constraint to compare this Constraint to
     *
     * @return @c true, if this Constraint is semantically equal to @a rhs, i.e.
     * if @b ContentType, @b SubType, and @b Parameters match, @c false otherwise
     */
    [[nodiscard]] bool isEqual(const ISerializable& rhs) const override;

private:
    // see https://gitlab.com/SiLA2/sila_base/blob/master/CONTENT_TYPES.md for
    // supported content types
    QString m_ContentType;  ///< currently supported: 'application'
    QString m_SubType;      ///< currently supported: 'xml', 'x-animl', 'json'
    QList<CContentTypeParameter> m_Parameters;
};

/**
 * @brief Overload for debugging CContentTypeParameters
 */
SILA_CPP_EXPORT QDebug operator<<(QDebug dbg, const CContentTypeParameter& rhs);

SILA_CPP_EXPORT std::ostream& operator<<(std::ostream& os,
                                         const CContentTypeParameter& rhs);

/**
 * @brief Overload for debugging CConstraintContentTypes
 */
SILA_CPP_EXPORT QDebug operator<<(QDebug dbg, const CConstraintContentType& rhs);

SILA_CPP_EXPORT std::ostream& operator<<(std::ostream& os,
                                         const CConstraintContentType& rhs);
}  // namespace SiLA2::codegen::fdl

#endif  // CODEGEN_CONSTRAINTCONTENTTYPE_H
