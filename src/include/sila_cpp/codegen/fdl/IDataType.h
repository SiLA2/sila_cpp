/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   DataType.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   27.01.2021
/// \brief  Declaration of the IDataType class
//============================================================================
#ifndef CODEGEN_IDATATYPE_H
#define CODEGEN_IDATATYPE_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/fdl/ISerializable.h>
#include <sila_cpp/global.h>

namespace SiLA2::codegen::fdl
{
/**
 * @brief The IDataType class represents an abstract interface for all SiLA 2 Data
 * Types that can be defined in a FDL file
 */
class SILA_CPP_EXPORT IDataType : public ISerializable
{
public:
    /**
     * @brief The Type enum describes the different types of Data Types defined in
     * the SiLA 2 standard
     */
    enum class Type
    {
        Invalid,
        Basic,
        Constrained,
        Identifier,
        List,
        Structure,
    };

    /**
     * @brief Get the Type of this Data Type
     *
     * @return The Data Type's Type
     */
    [[nodiscard]] Type type() const { return m_Type; }

    /**
     * @brief Get the string representation of the given Type @a t
     *
     * @param t The Type to convert to a string
     * @return The string corresponding to the Type @a t
     */
    [[nodiscard]] static QString typeToString(Type t);

    /**
     * @brief Given the string representation @a String of a Type return the
     * corresponding Type
     *
     * @param String The string representation of a @c Type
     * @return The @c Type corresponding to the string @a String
     */
    [[nodiscard]] static Type stringToType(const QString& String);

protected:
    /**
     * @brief Helper function for the equality comparison operators
     *
     * @param r The Data Type to compare this Data Type to
     *
     * @return @c true, if this Data Type's @b Type is equal to the @b Type of
     * @a rhs, @c false otherwise
     */
    [[nodiscard]] bool isEqual(const ISerializable& r) const override
    {
        const auto rhs = dynamic_cast<const IDataType*>(&r);
        return m_Type == rhs->m_Type;
    }

    Type m_Type{Type::Invalid};
};
}  // namespace SiLA2::codegen::fdl

#endif  // CODEGEN_IDATATYPE_H
