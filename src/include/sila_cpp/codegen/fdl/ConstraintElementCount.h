/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   ConstraintElementCount.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   29.01.2021
/// \brief  Declaration of the CConstraintElementCount,
/// CConstraintMinimalElementCount and CConstraintMaximalElementCount classes
//============================================================================
#ifndef CODEGEN_CONSTRAINTELEMENTCOUNT_H
#define CODEGEN_CONSTRAINTELEMENTCOUNT_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/fdl/SingleValueConstraint.h>
#include <sila_cpp/global.h>

namespace SiLA2::codegen::fdl
{
/**
 * @brief The CConstraintElementCount class represents a SiLA 2 ElementCount
 * Constraint
 */
class SILA_CPP_EXPORT CConstraintElementCount :
    public CSingleValueConstraint<uint>
{
public:
    /**
     * @brief Get the name of this object
     *
     * @return The object's name
     */
    [[nodiscard]] QString name() const override { return "ElementCount"; }
};

/**
 * @brief The CConstraintElementCount class represents a SiLA 2
 * MinimalElementCount Constraint
 */
class CConstraintMinimalElementCount final : public CConstraintElementCount
{
public:
    /**
     * @brief Get the name of this object
     *
     * @return The object's name
     */
    [[nodiscard]] QString name() const override { return "MinimalElementCount"; }
};

/**
 * @brief The CConstraintElementCount class represents a SiLA 2
 * MaximalElementCount Constraint
 */
class CConstraintMaximalElementCount final : public CConstraintElementCount
{
public:
    /**
     * @brief Get the name of this object
     *
     * @return The object's name
     */
    [[nodiscard]] QString name() const override { return "MaximalElementCount"; }
};
}  // namespace SiLA2::codegen::fdl

#endif  // CODEGEN_CONSTRAINTELEMENTCOUNT_H
