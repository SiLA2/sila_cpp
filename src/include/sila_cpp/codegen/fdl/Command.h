/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   Command.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   26.01.2021
/// \brief  Declaration of the CCommand class
//============================================================================
#ifndef CODEGEN_COMMAND_H
#define CODEGEN_COMMAND_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/fdl/Parameter.h>
#include <sila_cpp/codegen/fdl/Response.h>
#include <sila_cpp/codegen/fdl/SiLAElement.h>
#include <sila_cpp/global.h>

namespace SiLA2::codegen::fdl
{
/**
 * @brief The CCommand class represents a SiLA 2 Command Definition that was
 * defined in a FDL file
 */
class SILA_CPP_EXPORT CCommand final : public CSiLAElementWithErrors
{
public:
    /**
     * @brief Get the name of this object
     *
     * @return The object's name
     */
    [[nodiscard]] QString name() const override { return "Command"; }

    /**
     * @brief Convert this object to its @c QVariant representation
     *
     * @return A QVariant instance for this object
     */
    [[nodiscard]] QVariant toVariant() const override;

    /**
     * @brief Initializes this object with the values given in the @c QVariant
     * @a from
     *
     * @param from The QVariant instance that represents an object of this class
     */
    void fromVariant(const QVariant& from) override;

    /**
     * @brief Get whether this Command is Observable or not
     *
     * @returns true, if the Command is Observable, false if it's Unobservable
     */
    [[nodiscard]] bool isObservable() const { return m_Observable; }

    /**
     * @brief Get this Command's Parameters
     *
     * @return All Parameters of this Command
     */
    [[nodiscard]] QList<CParameter> parameters() const { return m_Parameters; }

    /**
     * @brief Get this Command's Responses
     *
     * @return All Responses of this Command
     */
    [[nodiscard]] QList<CResponse> responses() const { return m_Responses; }
    /**
     * @brief Get this Command's Intermediate Responses
     *
     * @return All Responses of this Command
     */
    [[nodiscard]] QList<CIntermediateResponse> intermediateResponses() const
    {
        return m_IntermediateResponses;
    }

protected:
    /**
     * @brief Helper function for the equality comparison operators
     *
     * @param rhs The Command to compare this Command to
     *
     * @return @c true, if this Command is semantically equal to @a rhs, i.e. if
     * @b Observable, @b Parameters, @b Responses and @b IntermediateResponses
     * match, @c false otherwise
     *
     * @sa CSiLAElementWithErrors::isEqual(const ISerializable&)
     */
    [[nodiscard]] bool isEqual(const ISerializable& rhs) const override;

private:
    bool m_Observable{};
    QList<CParameter> m_Parameters;
    QList<CResponse> m_Responses;
    QList<CIntermediateResponse> m_IntermediateResponses;
};

/**
 * @brief Overload for debugging CCommands
 */
SILA_CPP_EXPORT QDebug operator<<(QDebug dbg, const CCommand& rhs);
SILA_CPP_EXPORT std::ostream& operator<<(std::ostream& os, const CCommand& rhs);
}  // namespace SiLA2::codegen::fdl

#endif  // CODEGEN_COMMAND_H
