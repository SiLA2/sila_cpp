/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   ConstraintUnit.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   02.02.2021
/// \brief  Declaration of the CUnitComponent and CConstraintUnit classes
//============================================================================
#ifndef CODEGEN_CONSTRAINTUNIT_H
#define CODEGEN_CONSTRAINTUNIT_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/fdl/DataType.h>
#include <sila_cpp/codegen/fdl/IConstraint.h>
#include <sila_cpp/global.h>

namespace SiLA2::codegen::fdl
{
class SILA_CPP_EXPORT CUnitComponent final : public ISerializable
{
public:
    /**
     * @brief The SIUnit enum defines the 7 SI Units
     */
    enum class SIUnit
    {
        Invalid,
        Dimensionless,
        Meter,
        Kilogram,
        Second,
        Ampere,
        Kelvin,
        Mole,
        Candela,
    };

    /**
     * @brief Get the name of this object
     *
     * @return The object's name
     */
    [[nodiscard]] QString name() const override { return "UnitComponent"; }

    /**
     * @brief Convert this object to its @c QVariant representation
     *
     * @return A QVariant instance for this object
     */
    [[nodiscard]] QVariant toVariant() const override;

    /**
     * @brief Initializes this object with the values given in the @c QVariant
     * @a from
     *
     * @param from The QVariant instance that represents an object of this class
     *
     * @throws std::runtime_error if the initialization fails
     */
    void fromVariant(const QVariant& from) override;

    /**
     * @brief Convert the given string @a String into the corresponding SIUnit
     *
     * @return The SIUnit corresponding to the given string
     */
    [[nodiscard]] static SIUnit stringToSIUnit(const QString& String);

    /**
     * @brief Convert the given SIUnit @a Unit into its String representation
     *
     * @return The string representation of the given SIUnit
     */
    [[nodiscard]] static QString siUnitToString(SIUnit Unit);

    /**
     * @brief Get the Unit Name (i.e. the SI Unit without an exponent) of this SI
     * Unit
     *
     * @return The SI Unit's Unit Name
     */
    [[nodiscard]] SIUnit siUnit() const { return m_SIUnit; }

    /**
     * @brief Get the exponent of this SI Unit
     *
     * @return The SI Unit's exponent
     */
    [[nodiscard]] int64_t exponent() const { return m_Exponent; }

protected:
    /**
     * @brief Helper function for the equality comparison operators
     *
     * @param rhs The unit Component to compare this Component to
     *
     * @return @c true, if this Component is semantically equal to @a rhs, i.e. if
     * @b SIUnit and @b Exponent match, @c false otherwise
     */
    [[nodiscard]] bool isEqual(const ISerializable& rhs) const override;

private:
    SIUnit m_SIUnit{SIUnit::Invalid};
    int64_t m_Exponent{};
};

/**
 * @brief The CConstraintUnit class represents a SiLA 2 Unit Constraint
 */
class SILA_CPP_EXPORT CConstraintUnit final : public IConstraint
{
public:
    /**
     * @brief Get the name of this object
     *
     * @return The object's name
     */
    [[nodiscard]] QString name() const override { return "Unit"; }

    /**
     * @brief Convert this Unit Constraint to its @c QVariant representation
     *
     * @return A QVariant instance for this Unit Constraint
     */
    [[nodiscard]] QVariant toVariant() const override;

    /**
     * @brief Initializes this Unit Constraint with the values given in the
     * @c QVariant @a from
     *
     * @param from The QVariant instance that represents a Unit Constraint
     */
    void fromVariant(const QVariant& from) override;

    /**
     * @brief Get the Label of this Unit Constraint
     *
     * @return The Unit Constraint's Label
     */
    [[nodiscard]] QString label() const { return m_Label; }

    /**
     * @brief Get the Conversion Factor of this Unit Constraint
     *
     * @return The Unit Constraint's Conversion Factor
     */
    [[nodiscard]] double conversionFactor() const { return m_ConversionFactor; }

    /**
     * @brief Get the Conversion Offset of this Unit Constraint
     *
     * @return The Unit Constraint's Conversion Offset
     */
    [[nodiscard]] double conversionOffset() const { return m_ConversionOffset; }

    /**
     * @brief Get the SI Base Unit of this Unit Constraint (i.e. basically a List
     * of all Unit Components)
     *
     * @return The Unit Constraint's SI Base Unit
     */
    [[nodiscard]] QList<CUnitComponent> siBaseUnit() const
    {
        return m_SIBaseUnit;
    }

    /**
     * @brief Get the <i>i</i>'th Unit Component of the SI Base Unit of this Unit
     * Constraint
     *
     * @param i The index of the Unit Component to return
     * @return The Unit Constraint's <i>i</i>'th Unit Component of the SI Base
     * Unit
     */
    [[nodiscard]] CUnitComponent unitComponent(int i) const
    {
        return m_SIBaseUnit.value(i);
    }

protected:
    /**
     * @brief Helper function for the equality comparison operators
     *
     * @param rhs The Constraint to compare this Constraint to
     *
     * @return @c true, if this Constraint is semantically equal to @a rhs, i.e
     * if @b Label, @b ConversionFactor, @b ConversionOffset, qnd @b SIBaseUnit
     * match, @c false otherwise
     */
    [[nodiscard]] bool isEqual(const ISerializable& rhs) const override;

private:
    QString m_Label;
    double m_ConversionFactor{};  ///< conversion between Unit Label and SI Unit
    double m_ConversionOffset{};  ///< conversion between Unit Label and SI Unit
    QList<CUnitComponent> m_SIBaseUnit;  ///< Unit Component(s) make up base SI unit
};

/**
 * @brief Overload for debugging CUnitComponent
 */
SILA_CPP_EXPORT QDebug operator<<(QDebug dbg, const CUnitComponent& rhs);

SILA_CPP_EXPORT std::ostream& operator<<(std::ostream& os,
                                         const CUnitComponent& rhs);

/**
 * @brief Overload for debugging CConstraintUnit
 */
SILA_CPP_EXPORT QDebug operator<<(QDebug dbg, const CConstraintUnit& rhs);

SILA_CPP_EXPORT std::ostream& operator<<(std::ostream& os,
                                         const CConstraintUnit& rhs);
}  // namespace SiLA2::codegen::fdl
#endif  // CODEGEN_CONSTRAINTUNIT_H
