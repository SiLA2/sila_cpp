/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   DynamicStructure.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   27.02.2021
/// \brief  Declaration of the CDynamicStructureElement and
/// CDynamicStructure classes
//============================================================================
#ifndef DYNAMICSTRUCTURE_H
#define DYNAMICSTRUCTURE_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/client/DynamicValue.h>
#include <sila_cpp/codegen/fdl/DataTypeStructure.h>
#include <sila_cpp/common/logging.h>

namespace SiLA2
{
/**
 * @brief The CDynamicStructure class represents a single Element of a SiLA 2
 * Structure that is not known at compile time. It simply consists of an
 * Identifier (as it is returned by @c CDynamicStructure::elements(), i.e. not
 * Fully Qualified since the Structure knows its Fully Qualified Identifier an can
 * infer the Element's from it) and a Value
 */
class SILA_CPP_EXPORT CDynamicStructureElement : public CDynamicValue
{
    class PrivateImpl;
    using PrivateImplPtr = isocpp_p0201::polymorphic_value<PrivateImpl>;

public:
    // inherit special member functions
    using CDynamicValue::operator=;

    /**
     * @brief Construct a Dynamic Structure with the given @a Identifier and
     * @a Value
     *
     * @tparam T The type of the value (must be either a wrapped SiLA Type, e.g.
     * @c SiLA2::CString, or a SiLA Framework Type, e.g.
     * @c sila2::org::silastandard::String or a C++ type that is convertible to
     * a wrapped SiLA Type, e.g. a @c std::string for a @c SiLA2::CString)
     * @param Identifier The identifier of the Structure (not Fully Qualified -
     * see class doc)
     * @param Value The value to set
     */
    template<typename T, typename = std::enable_if_t<
                             internal::is_sila_type_v<T>
                             || internal::is_sila_framework_type_v<T>
                             || internal::is_sila_type_convertible_v<T>>>
    CDynamicStructureElement(std::string_view Identifier, const T& Value);

    /**
     * @brief Construct a Dynamic Structure with the given @a Identifier and
     * @a Value
     *
     * @tparam T The type of the value (must be either a wrapped SiLA Type, e.g.
     * @c SiLA2::CString, or a SiLA Framework Type, e.g.
     * @c sila2::org::silastandard::String or a C++ type that is convertible to
     * a wrapped SiLA Type, e.g. a @c std::string for a @c SiLA2::CString)
     * @param Identifier The identifier of the Structure (not Fully Qualified -
     * see class doc)
     * @param Value The value to set
     */
    template<typename T, typename = std::enable_if_t<
                             internal::is_sila_type_v<T>
                             || internal::is_sila_framework_type_v<T>
                             || internal::is_sila_type_convertible_v<T>>>
    CDynamicStructureElement(QStringView Identifier, const T& Value);

    /**
     * @brief Get the name (identifier) of this Structure Element
     *
     * @return The Element's name
     */
    [[nodiscard]] std::string identifier() const;

    /**
     * @brief Get the Display Name of this Structure Element
     *
     * @return The Element's Display Name
     */
    [[nodiscard]] QString displayName() const;

    /**
     * @brief Get the Description of this Structure Element
     *
     * @return The Element's Description
     */
    [[nodiscard]] QString description() const;

    /**
     * @brief Convert this Dynamic Structure Element into its corresponding
     * protobuf @c Message
     *
     * @return A pointer to the protobuf @c Message for this Structure Element
     */
    [[nodiscard]] google::protobuf::Message* toProtoMessagePtr() const override;

    /**
     * @brief Get a default constructed (i.e. empty) @c CDynamicStructure object
     * for this Value
     *
     * This object can be filled with the necessary Structure Element values
     * and then set as this Value by using @a setValue() or added to a List by
     * using @a addValue()
     *
     * @note This function should only be called when
     * <code>dataTypeType() == DataTypeType::STRUCTURE</code> or
     * <code>underlyingDataTypeType() == DataTypeType::STRUCTURE</code>.
     * Otherwise the returned @c CDynamicStructure will have no Elements (since
     * this Value has a different Data Type Type)
     *
     * @return An empty @c CDynamicStructure
     */
    [[nodiscard]] CDynamicStructure structure() const override;

    /**
     * @brief Returns the string representation of this Structure Element that can
     * be displayed in a UI
     *
     * @return The Structure Element's string representation
     */
    [[nodiscard]] QString prettyString() const override;

private:
    friend class CDynamicStructure;
    friend class CDynamicValue;

    /**
     * @internal Only to be used by @c CDynamicStructure
     * @brief C'tor
     *
     * @param ElementFDL The Element's parsed FDL description
     * @param StructMessageName The name of the surrounding Structure message (for
     * creating nested Structs)
     * @param DMF A shared_ptr to the Dynamic Message Factory to use to create the
     * dynamic protobuf Message for this Structure
     * @param Feature The SiLA Feature Stub used to resolve Custom Data Type
     * Definitions
     * @param Value (optional) The initial value of the Structure Element
     */
    CDynamicStructureElement(
        const codegen::fdl::CDataTypeStructureElement& ElementFDL,
        std::string StructMessageName,
        std::shared_ptr<codegen::proto::CDynamicMessageFactory> DMF,
        CDynamicFeatureStub* Feature, google::protobuf::Message* Value = nullptr);

    /**
     * @internal Only to be used by @c CDynamicStructureElement
     * @brief Construct a Dynamic Structure with the given @a Identifier
     *
     * @param Identifier The identifier of the Structure (not Fully Qualified -
     * see class doc)
     */
    explicit CDynamicStructureElement(std::string Identifier);

    /**
     * @internal Only to be used by @c CDynamicStructure
     * @brief Get the fully qualified gRPC message of the Structure that this
     * Element belongs to
     *
     * @return The Element's Structure's gRPC message name
     */
    [[nodiscard]] std::string structMessageName() const;

    /**
     * @internal Only to be used by @c CDynamicStructure
     * @brief Get the Dynamic Message Factory used by this Element
     *
     * @return The Element's Dynamic Message Factory
     */
    [[nodiscard]] std::shared_ptr<codegen::proto::CDynamicMessageFactory> dmf()
        const;

    /**
     * @internal Only to be used by @c CDynamicValue
     * @brief Get the FDL representation of this Structure Element
     *
     * @return The Element's FDL representation
     */
    [[nodiscard]] codegen::fdl::CDataTypeStructureElement fdl() const;

    PIMPL_DECLARE_PRIVATE(CDynamicStructureElement)
};

//============================================================================
template<typename T, typename>
CDynamicStructureElement::CDynamicStructureElement(std::string_view Identifier,
                                                   const T& Value)
    : CDynamicStructureElement{Identifier.data()}
{
    setValue(Value);
}

//============================================================================
template<typename T, typename>
CDynamicStructureElement::CDynamicStructureElement(QStringView Identifier,
                                                   const T& Value)
    : CDynamicStructureElement{Identifier.toString().toStdString()}
{
    setValue(Value);
}

/**
 * @brief The CDynamicStructure class represents a SiLA 2 Structure Data Type that
 * is not known at compile time. It simply is a list of
 * @c CDynamicStructureElements.
 */
class SILA_CPP_EXPORT CDynamicStructure : public QList<CDynamicStructureElement>
{
public:
    // inherit special member functions
    using QList::QList;
    using QList::operator[];

    /**
     * @brief Converting copy c'tor
     */
    explicit CDynamicStructure(const QList<CDynamicStructureElement>& rhs);

    /**
     * @brief Convert this Structure to its corresponding protobuf @c Message
     *
     * @return A pointer to the protobuf @c Message for this Structure
     */
    [[nodiscard]] google::protobuf::Message* toProtoMessagePtr() const;

    /**
     * @brief Get the identifiers of all @c CDynamicStructureElements in this
     * Structure
     *
     * @return A list of all Element Identifiers
     */
    [[nodiscard]] QStringList elementIdentifiers() const;

    /**
     * @brief Copies all @c CDynamicStructureElements from the given Structure
     * @a from. Any Elements that this Structure may have had are overwritten
     */
    void copyElements(const CDynamicStructure& from);

    /**
     * @brief Returns the string representation of this Structure that can be
     * displayed in a UI
     *
     * @return The Structure's string representation
     */
    [[nodiscard]] QString prettyString() const;

    /**
     * @brief Returns the @c CDynamicStructureElement for the given @a Identifier
     *
     * @param Identifier The Identifier of the Element to get
     * @return A reference to the @c CDynamicStructureElement
     *
     * @throws std::out_of_range if there is no @c CDynamicStructureElement with
     * the given name @a Identifier
     */
    [[nodiscard]] CDynamicStructureElement& operator[](
        std::string_view Identifier);

    /**
     * @brief Returns the @c CDynamicStructureElement for the given @a Identifier
     *
     * @param Identifier The Identifier of the Element to get
     * @return A reference to the @c CDynamicStructureElement
     *
     * @throws std::out_of_range if there is no @c CDynamicStructureElement with
     * the given name @a Identifier
     */
    [[nodiscard]] CDynamicStructureElement& operator[](QStringView Identifier)
    {
        return operator[](Identifier.toString().toStdString());
    }

private:
    friend class CDynamicValue;
    friend class CDynamicProperty;
    friend class CDynamicParameter;
    friend class CDynamicResponse;
    friend class CClientMetadata;
    friend class CCustomDataType;
    friend class CDynamicStructureElement;
    /**
     * @internal Only to be used by @c CDynamicValue, @c CDynamicParameter,
     * @c CDynamicResponse, @c CClientMetadata, @c CCustomDataType and
     * @c CDynamicStructureElement
     * @brief C'tor
     *
     * @param StructureFDL The parsed FDL description of the Structure
     * @param SurroundingMessageName The fully qualified gRPC message name of the
     * message in which this Struct is nested
     * @param StructFieldName The name of the field that this Struct is used for
     * @param DMF A shared_ptr to the Dynamic Message Factory to use to create the
     * dynamic protobuf Message for this Structure
     * @param Feature The SiLA Feature Stub used to resolve Custom Data Type
     * Definitions
     */
    explicit CDynamicStructure(
        codegen::fdl::CDataTypeStructure StructureFDL,
        const std::string& SurroundingMessageName,
        const std::string& StructFieldName,
        std::shared_ptr<codegen::proto::CDynamicMessageFactory> DMF,
        CDynamicFeatureStub* Feature);

    codegen::fdl::CDataTypeStructure m_Structure;
    const std::string m_StructMessageName;
    std::shared_ptr<codegen::proto::CDynamicMessageFactory>
        m_DynamicMessageFactory;
};

/**
 * @brief Overload for debugging @c CDynamicStructureElement
 */
SILA_CPP_EXPORT QDebug operator<<(QDebug dbg,
                                  const CDynamicStructureElement& rhs);

/**
 * @brief Overload for printing @c CDynamicStructureElements
 */
SILA_CPP_EXPORT std::ostream& operator<<(std::ostream& os,
                                         const CDynamicStructureElement& rhs);
}  // namespace SiLA2
#endif  // DYNAMICSTRUCTURE_H
