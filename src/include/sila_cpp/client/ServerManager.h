/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   ServerManager.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   20.01.2021
/// \brief  Declaration of the CServerManager class
//============================================================================
#ifndef SERVERMANAGER_H
#define SERVERMANAGER_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/ServerInformation.h>
#include <sila_cpp/discovery/SiLAServerDiscovery.h>
#include <sila_cpp/global.h>

#include <QObject>

#include <polymorphic_value.h>

namespace SiLA2
{
//============================================================================
//                            FORWARD DECLARATIONS
//============================================================================
class CServerAddress;
class CDynamicSiLAClient;

/**
 * @brief The CServerManager class allows to browse for available Servers on the
 * network and keeps track of all found or manually added Servers and the
 * corresponding @c CDynamicClient connections to these Servers.
 */
class SILA_CPP_EXPORT CServerManager : public QObject
{
    Q_OBJECT

    class PrivateImpl;
    using PrivateImplPtr = isocpp_p0201::polymorphic_value<PrivateImpl>;

public:
    /**
     * @brief C'tor
     *
     * This won't start searching for SiLA Servers yet. You need to call
     * @c start() for this. That way you can connect to the Manager's signals
     * first and then start searching for Servers.
     *
     * @sa start()
     */
    explicit CServerManager(QObject* parent = nullptr, PrivateImplPtr priv = {});

    /**
     * @brief D'tor
     *
     * Stops the Manager
     *
     * @sa stop()
     */
    ~CServerManager() override;

    /**
     * @brief Get a list of all Server UUIDs of the Servers that are currently
     * present in the Manager
     *
     * @return A list of UUIDs of all Servers
     */
    [[nodiscard]] QList<QUuid> serverUUIDs() const;

    /**
     * @brief Returns whether the Server Manager contains an entry for the Server
     * with the given @a ServerUUID
     *
     * @param ServerUUID The UUID of the Server to search for
     *
     * @return @c true, if the Manager contains an entry for @a ServerUUID,
     * @c false otherwise
     */
    [[nodiscard]] bool contains(const QUuid& ServerUUID) const;

    /**
     * @brief Get a @c shared_ptr to the @c CDynamicSiLAClient instance that is
     * connected to the Server identified by the given UUID. If there is no Server
     * registered under the given @a ServerUUID then this functions returns a
     * @c nullptr. You might have to call @c connect(ServerUUID) or
     * @c addAndConnect() with the Server's address first.
     *
     * @param ServerUUID The UUID of the Server
     * @return The Client instance connected to the server with @a ServerUUID or
     * @c nullptr if there is no such instance
     */
    [[nodiscard]] std::shared_ptr<CDynamicSiLAClient> connection(
        const QUuid& ServerUUID) const;

    /**
     * @brief Returns the address of the Server with the given @a ServerUUID
     *
     * @param ServerUUID The UUID of the Server to get the address for
     *
     * @return The Server's address
     */
    [[nodiscard]] CServerAddress serverAddress(const QUuid& ServerUUID) const;

    /**
     * @brief Get the Server Information for the Server given through its
     * @a ServerUUID
     *
     * @param ServerUUID The UUID of the Server to get the Server Information for
     * @return The Server's Information read from the DNS TXT record (As of SiLA 2
     * v1.1 a Server adds additional information like its name, version and
     * description as TXT records to its DNS entry. If the Server that was
     * discovered does not implement this behaviour yet, the fields of the
     * @c CServerInformation returned here will be empty and can be discarded. In
     * this case you need to connect to the Server first in order to get this
     * info.)
     * If there is no Server for the given @a ServerUUID present in the Manager
     * all fields of the returned @c CServerInformation will be empty as well.
     */
    [[nodiscard]] CServerInformation serverInformation(
        const QUuid& ServerUUID) const;

    /**
     * @brief Get the SSL certificate of the Server given through its
     * @a ServerUUID
     *
     * @param ServerUUID The UUID of the Server to get the certificate for
     * @return The Server's SSL certificate read from the DNS TXT record (As of
     * SiLA 2 v1.1 a Server adds the contents of its certificate as TXT records to
     * its DNS entry. If the Server that was discovered does not implement this
     * behaviour yet, the  returned string will be empty and can be discarded. In
     * this case you need to connect to the Server first in order to get its
     * certificate if it has one at all.) If there is no Server for the given
     * @a ServerUUID present in the Manager the returned string will be empty as
     * well.
     */
    [[nodiscard]] QString certificate(const QUuid& ServerUUID) const;

    /**
     * @brief Get the last error that occurred while connecting to the Server
     * given through its @a ServerUUID
     *
     * @param ServerUUID The UUID of the Server to get the last error for
     * @return The last error that occurred while connecting to the given Server.
     * If no error occurred and the connection could be established successfully,
     * then this function returns an empty string. Same goes for when there is no
     * Server with the given UUID. Otherwise it returns a description of what wnt
     * wrong while establishing the connection.
     *
     * @note This function is not able to return the last error for Servers that
     * were connected using only the Address and not the UUID.
     */
    [[nodiscard]] QString lastError(const QUuid& ServerUUID) const;

    /**
     * @brief Whether the SiLA Server Discovery is currently active
     *
     * Discovery can be enabled by calling @c start()
     *
     * @returns @c true, if Discovery is active, @c false otherwise
     */
    [[nodiscard]] bool isDiscoveryActive() const;

public slots:
    /**
     * @brief Starts searching for available SiLA Servers on the network
     */
    void start();

    /**
     * @brief Stops searching for SiLA Servers
     */
    void stop();

    /**
     * @brief Same as @c stop() followed by @c start()
     */
    void restart();

    /**
     * @brief Add a Server that has the given @a ServerUUID and runs on the
     * given @a Address to the Manager but don't connect to it yet
     *
     * @param ServerUUID The UUID of the Server to add
     * @param Address The IP address and port on which the Server is running
     *
     * @returns true, if the Server was actually added, false otherwise (i.e
     * if there already is a Server with the given @a UUID present in the
     * Manager)
     */
    bool addServer(const QUuid& ServerUUID, const CServerAddress& Address);

    /**
     * @brief Connect to the Server given through its @a ServerUUID
     *
     * This requires that the Server you want to connect to has already been
     * discovered through SiLA Server Discovery (as of SiLA 2 v1.1) or has been
     * added manually through @c onServerAdded(). If the Server does not support
     * SiLA 2 v1.1's Discovery Specification but only SiLA 2 v1.0's Discovery
     * Specification or has not been discovered at all you have to add the Server
     * using @c addAndConnect(CServerAddress).
     * Calling this function multiple times with the same @a ServerUUID does not
     * result in multiple Clients being connected to the same Server. The already
     * connected Client instance will be returned in this case.
     *
     * @param ServerUUID The UUID of the Server to connect to
     * @param Certificate The certificate that should be used to connect to the
     * Server. If given, this takes precedence over the certificate that may have
     * been received via SiLA Server Discovery. If not given, but a certificate
     * has been read from the SiLA Server Discovery TXT records then the
     * certificate from the Discovery is used. Otherwise, the Client tries to
     * retrieve the Server's certificate and use that to connect to the Server.
     *
     * @return A @c shared_ptr to the @c CDynamicSiLAClient instance connected to
     * the Server if the connection could be established or a @c nullptr if not
     * (in this case @b serverConnectionFailed will be emitted with information on
     * why the connection failed)
     */
    std::shared_ptr<CDynamicSiLAClient> connect(const QUuid& ServerUUID,
                                                const QString& Certificate = "");

    /**
     * @brief Connect to the Server given through its @a ServerUUID using
     * unencrypted communication
     *
     * @note The SiLA 2 standard requires all connections to be encrypted. Only
     * use this function for testing purposes.
     *
     * This requires that the Server you want to connect to has already been
     * discovered through SiLA Server Discovery or has been added manually through
     * @c onServerAdded(). Calling this function multiple times with the same @a
     * ServerUUID does not result in multiple Clients being connected to the same
     * Server. The already connected Client instance will be returned in this
     * case.
     *
     * @param ServerUUID The UUID of the Server to connect to
     *
     * @return A @c shared_ptr to the @c CDynamicSiLAClient instance connected to
     * the Server if the connection could be established or a @c nullptr if not
     * (in this case @b serverConnectionFailed will be emitted with information on
     * why the connection failed)
     */
    std::shared_ptr<CDynamicSiLAClient> connectInsecure(const QUuid& ServerUUID);

    /**
     * @brief Add a Server to the Manager and connect to it (a separate call to
     * @c connect() is not necessary)
     *
     * @param ServerUUID The ServerUUID of the server
     * @param Address The IP address and port on which the Server is running
     * @param Certificate The certificate that should be used to connect to the
     * Server. If given, this takes precedence over the certificate that may have
     * been received via SiLA Server Discovery. If not given, but a certificate
     * has been read from the SiLA Server Discovery TXT records then the
     * certificate from the Discovery is used. Otherwise, the Client tries to
     * retrieve the Server's certificate and use that to connect to the Server.
     *
     * @return A @c shared_ptr to the @c CDynamicSiLAClient instance connected to
     * the Server if the Server could be added, and the connection could be
     * established, or a @c nullptr if not (in this case @b serverConnectionFailed
     * will be emitted with information on why the connection failed)
     *
     * @sa CSiLAClient::CSiLAClient()
     * @sa onServerAdded(QUuid, CServerAddress)
     * @sa connect(QUuid)
     */
    std::shared_ptr<CDynamicSiLAClient> addAndConnect(
        const QUuid& ServerUUID, const CServerAddress& Address,
        const QString& Certificate = "");

    /**
     * @brief Add a Server to the Manager and connect to it using unencrypted
     * communication (a separate call to @c connectInsecure() is not necessary)
     *
     * @note The SiLA 2 standard requires all connections to be encrypted. Only
     * use this function for testing purposes.
     *
     * @param ServerUUID The ServerUUID of the server
     * @param Address The IP address and port on which the Server is running
     *
     * @return A @c shared_ptr to the @c CDynamicSiLAClient instance connected to
     * the Server if the Server could be added, and the connection could be
     * established, or a @c nullptr if not (in this case @b serverConnectionFailed
     * will be emitted with information on why the connection failed)
     *
     * @sa CSiLAClient::CSiLAClient()
     * @sa onServerAdded(QUuid, CServerAddress)
     * @sa connectInsecure(QUuid)
     */
    std::shared_ptr<CDynamicSiLAClient> addAndConnectInsecure(
        const QUuid& ServerUUID, const CServerAddress& Address);

    /**
     * @overload
     * @brief Add a server to the Manager and connect to it (a separate call to
     * @c connect() is not necessary)
     *
     * This is an overloaded method. In case a Server has been discovered but only
     * using SiLA 2 v1.0's Discovery Specification, or if the Server has not been
     * discovered at all by the Manager yet, this method will connect to the
     * Server and retrieve it's UUID in order to save the connection and make it
     * available for calls to @c connection() for example.
     *
     * @param Address The IP address and port on which the Server is running
     * @param Certificate The certificate that should be used to connect to the
     * Server. If given, this takes precedence over the certificate that may have
     * been received via SiLA Server Discovery. If not given, but a certificate
     * has been read from the SiLA Server Discovery TXT records then the
     * certificate from the Discovery is used. Otherwise, the Client tries to
     * retrieve the Server's certificate and use that to connect to the Server.
     *
     * @return A @c shared_ptr to the @c CDynamicSiLAClient instance connected to
     * the Server if the Server could be added, and the connection could be
     * established, or a @c nullptr if not (in this case @b serverConnectionFailed
     * will be emitted with information on why the connection failed)
     *
     * @sa CSiLAClient::CSiLAClient()
     * @sa onServerAdded(QUuid, CServerAddress)
     * @sa connect(QUuid)
     */
    std::shared_ptr<CDynamicSiLAClient> addAndConnect(
        const CServerAddress& Address, const QString& Certificate = "");

    /**
     * @overload
     * @brief Add a server to the Manager and connect to it using unencrypted
     * communication (a separate call to
     * @c connectInsecure() is not necessary)
     *
     * @note The SiLA 2 standard requires all connections to be encrypted. Only
     * use this function for testing purposes.
     *
     * This is an overloaded method. In case a Server has been discovered but only
     * using SiLA 2 v1.0's Discovery Specification, or if the Server has not been
     * discovered at all by the Manager yet, this method will connect to the
     * Server and retrieve it's UUID in order to save the connection and make it
     * available for calls to @c connection() for example.
     *
     * @param Address The IP address and port on which the Server is running
     *
     * @return A @c shared_ptr to the @c CDynamicSiLAClient instance connected to
     * the Server if the Server could be added, and the connection could be
     * established, or a @c nullptr if not (in this case @b serverConnectionFailed
     * will be emitted with information on why the connection failed)
     *
     * @sa CSiLAClient::CSiLAClient()
     * @sa onServerAdded(QUuid, CServerAddress)
     * @sa connectInsecure(QUuid)
     */
    std::shared_ptr<CDynamicSiLAClient> addAndConnectInsecure(
        const CServerAddress& Address);

    /**
     * @brief Remove the given Server from the Manager effectively
     * disconnecting from it
     *
     * @note The Manager will release its @c shared_ptr to the corresponding
     * @c CDynamicSiLAClient. If you're still holding a @c shared_ptr to this
     * Client there will be no disconnect (since the @c shared_ptr will only
     * deallocate the instance when all references are gone). However, from the
     * point of the Manager this looks like a disconnect since it releases all
     * resources associated with this Server. I.e. if you want to add the same
     * Server to the Manager again the Manager will create a completely new Client
     * instance since at this point it has no way of knowing if there might still
     * be a Client instance around that is already connected to this Server.
     *
     * @param ServerUUID The ServerUUID of the Server
     *
     * @returns true, if the Server was actually removed, false otherwise (i.e
     * if there was no Server with the given @a ServerUUID present in the Manager)
     */
    bool removeServer(const QUuid& ServerUUID);

    /**
     * @brief Disconnect from the Server
     *
     * @param ServerUUID The ServerUUID of the Server
     */
    void disconnect(const QUuid& ServerUUID);

signals:
    /**
     * @brief This signal is emitted when the SiLA Server Discovery has been
     * started
     */
    void started() const;

    /**
     * @brief This signal is emitted when the SiLA Server Discovery has been
     * stopped
     */
    void stopped() const;

    /**
     * @brief This signal will be emitted when a Server was discovered
     * automatically through SiLA Server Discovery. The Server hasn't been
     * connected to yet. To connect to it call @c connect(ServerUUID).
     *
     * @param UUID The UUID of the discovered Server
     * @param Address The IP address and port on which the Server is running
     */
    void serverDiscovered(const QUuid& UUID, const CServerAddress& Address) const;

    /**
     * @brief This signal will be emitted when a Server's info changed in the
     * network (as indicated by the SiLA Server Discovery mechanism)
     *
     * @param UUID The UUID of the Server that changed
     */
    void serverChanged(const QUuid& UUID) const;

    /**
     * @brief This signal will be emitted when a Server disappeared from the
     * network (as indicated by the SiLA Server Discovery mechanism)
     *
     * The Manager will not remove this Server from its internal list. You need to
     * call @c onServerRemoved(ServerUUID) yourself.
     *
     * @param UUID The UUID of the Server that disappeared
     * @param ServerInformation The information of the Server that disappeared
     */
    void serverDisappeared(const QUuid& UUID,
                           const CServerInformation& ServerInformation) const;

    /**
     * @brief This signal will be emitted when the connection to the Server with
     * the given @a ServerUUID was established successfully
     *
     * @param ServerUUID The UUID of the Server that has been connected
     */
    void serverConnected(const QUuid& ServerUUID) const;

    /**
     * @brief This signal will be emitted when a Server was discovered but the
     * Manager could not establish a connection to it. @a ErrorMsg holds a
     * description why the connection failed
     *
     * @param UUID The UUID of the Server that was discovered but couldn't be
     * connected to
     * @param ErrorMsg A description of the error that occurred while connecting
     * to the Server
     */
    void serverConnectionFailed(const QUuid& UUID, const QString& ErrorMsg) const;

    /**
     * @brief This signal will be emitted when a Server was added manually through
     * @c addAndConnect using only its Address but the Manager could not establish
     * a connection to it. @a ErrorMsg holds a description why the connection
     * failed
     *
     * @param Address The Address of the Server that was added but couldn't be
     * connected to
     * @param ErrorMsg A description of the error that occurred while connecting
     * to the Server
     */
    void serverConnectionFailed(const CServerAddress& Address,
                                const QString& ErrorMsg) const;

    /**
     * @brief This signal will be emitted when the Manager could not establish
     * a connection to a Server regardless of whether the Server was added
     * manually or automatically through Discovery. This is just here for
     * convenience if you're only interested in the @a ErrorMsg but not in the
     * UUID or Address of the Server. @a ErrorMsg holds a description why the
     * connection failed
     *
     * @param ErrorMsg A description of the error that occurred while connecting
     * to the Server
     */
    void serverConnectionFailed(const QString& ErrorMsg) const;

    /**
     * @brief This signal will be emitted when there is an error with the SiLA
     * Server Discovery
     *
     * @param Message The error message
     */
    void discoveryError(const QString& Message);

protected:
    PrivateImplPtr d_ptr;

private:
    PIMPL_DECLARE_PRIVATE(CServerManager)
};
}  // namespace SiLA2
#endif  // SERVERMANAGER_H
