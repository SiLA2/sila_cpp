/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   DynamicParameter.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   22.02.2021
/// \brief  Declaration of the CDynamicParameter and CDynamicParameterList classes
//============================================================================
#ifndef DYNAMICPARAMETER_H
#define DYNAMICPARAMETER_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/client/DynamicStructure.h>
#include <sila_cpp/client/DynamicValue.h>
#include <sila_cpp/codegen/fdl/Parameter.h>
#include <sila_cpp/common/logging.h>

#include <polymorphic_value.h>

//============================================================================
//                            FORWARD DECLARATIONS
//============================================================================
namespace SiLA2
{
class CDynamicCommand;
}  // namespace SiLA2

namespace SiLA2
{
/**
 * @brief The CDynamicParameter class represents a parameter for calling a
 * @c CDynamicCommand that is not known at compile time. It simply consists of an
 * Identifier (as it is returned by @c CDynamicCommand::parameters(), i.e. not
 * Fully Qualified since the Command knows its Fully Qualified Identifier an can
 * infer the Parameter's from it) and a Value.
 */
class SILA_CPP_EXPORT CDynamicParameter : public CDynamicValue
{
    class PrivateImpl;
    using PrivateImplPtr = isocpp_p0201::polymorphic_value<PrivateImpl>;

public:
    // inherit special member functions
    using CDynamicValue::operator=;

    /**
     * @brief Get the Fully Qualified Identifier of this Parameter
     *
     * @return The Parameter's Fully Qualified Identifier
     */
    [[nodiscard]] CFullyQualifiedCommandParameterID identifier() const;

    /**
     * @brief Get the Display Name of this Parameter
     *
     * @return The Parameter's Display Name
     */
    [[nodiscard]] QString displayName() const;

    /**
     * @brief Get the Description of this Parameter
     *
     * @return The Parameter's Description
     */
    [[nodiscard]] QString description() const;

    /**
     * @brief Get a pointer to the Feature that this Parameter belongs to
     *
     * @note No ownership is transferred here!
     *
     * @return A pointer to the Feature of this Parameter
     */
    [[nodiscard]] CDynamicFeatureStub* feature() const;

    /**
     * @brief Convert this Dynamic Parameter to its corresponding protobuf @c
     * Message
     *
     * @return A pointer to the protobuf @c Message for this Parameter
     */
    [[nodiscard]] google::protobuf::Message* toProtoMessagePtr() const override;

    /**
     * @brief Get a default constructed (i.e. empty) @c CDynamicStructure object
     * for this Value
     *
     * This object can be filled with the necessary Structure Element values
     * and then set as this Value by using @a setValue() or added to a List by
     * using @a addValue()
     *
     * @note This function should only be called when
     * <code>dataTypeType() == DataTypeType::STRUCTURE</code> or
     * <code>underlyingDataTypeType() == DataTypeType::STRUCTURE</code>.
     * Otherwise the returned @c CDynamicStructure will have no Elements (since
     * this Value has a different Data Type Type)
     *
     * @return An empty @c CDynamicStructure
     */
    [[nodiscard]] CDynamicStructure structure() const override;

    /**
     * @brief Returns the string representation of this Parameter that can be
     * displayed in a UI
     *
     * @return The Parameter's string representation
     */
    [[nodiscard]] QString prettyString() const override;

private:
    friend class CDynamicCommand;
    /**
     * @internal Only to be used by @c CDynamicCommand
     * @brief C'tor
     *
     * @param ParameterFDL The parsed FDL description of the Parameter
     * @param CommandID The Fully Qualified Command Identifier of the Command that
     * this Parameter belongs to
     * @param DMF The Dynamic Message Factory to use to create the protobuf
     * Messages for any Custom Data Types or Structures that this Parameter might
     * hold
     * @param Feature The SiLA Feature Stub that this Parameter belongs to
     */
    CDynamicParameter(const codegen::fdl::CParameter& ParameterFDL,
                      CFullyQualifiedCommandID CommandID,
                      std::shared_ptr<codegen::proto::CDynamicMessageFactory> DMF,
                      CDynamicFeatureStub* Feature);

    PIMPL_DECLARE_PRIVATE(CDynamicParameter)
};

/**
 * @brief The CDynamicParameterList class is a simple wrapper around
 * @c QList<CDynamicParameter> that provides a few more convenience methods to
 * access the Parameter values in the list
 */
class SILA_CPP_EXPORT CDynamicParameterList : public QList<CDynamicParameter>
{
public:
    using QList::QList;

    /**
     * @brief Converting copy c'tor
     */
    explicit CDynamicParameterList(const QList<CDynamicParameter>& rhs);

    /**
     * @brief Get the identifiers of all @c CDynamicParameters in this list
     *
     * @return A list of all Parameter Identifiers
     */
    [[nodiscard]] QList<CFullyQualifiedCommandParameterID> identifiers() const;

    /**
     * @brief Returns the @c CDynamicParameter for the given @a Identifier
     *
     * @param Identifier The Identifier of the Parameter to get
     * @return A const reference to the @c CDynamicParameter
     *
     * @throws std::out_of_range if there is no @c CDynamicParameter with the
     * given name @a Identifier
     */
    [[nodiscard]] const CDynamicParameter& at(std::string_view Identifier) const;

    /**
     * @brief Returns the @c CDynamicParameter for the given @a Identifier
     *
     * @param Identifier The Identifier of the Parameter to get
     * @return A const reference to the @c CDynamicParameter
     *
     * @throws std::out_of_range if there is no @c CDynamicParameter with the
     * given name @a Identifier
     */
    [[nodiscard]] const CDynamicParameter& at(QStringView Identifier) const
    {
        return at(Identifier.toString().toStdString());
    }

    /**
     * @brief Returns the @c CDynamicParameter for the given @a Identifier
     *
     * @param Identifier The Fully Qualified Identifier of the Parameter to get
     * @return A const reference to the @c CDynamicParameter
     *
     * @throws std::out_of_range if there is no @c CDynamicParameter with the
     * given name @a Identifier
     */
    [[nodiscard]] const CDynamicParameter& at(
        const CFullyQualifiedCommandParameterID& Identifier) const;

    /**
     * @brief Returns the @c CDynamicParameter for the given @a Identifier
     *
     * @param Identifier The Identifier of the Parameter to get
     * @return A reference to the @c CDynamicParameter
     *
     * @throws std::out_of_range if there is no @c CDynamicParameter with the
     * given name @a Identifier
     */
    [[nodiscard]] CDynamicParameter& operator[](std::string_view Identifier);

    /**
     * @brief Returns the @c CDynamicParameter for the given @a Identifier
     *
     * @param Identifier The Identifier of the Parameter to get
     * @return A reference to the @c CDynamicParameter
     *
     * @throws std::out_of_range if there is no @c CDynamicParameter with the
     * given name @a Identifier
     */
    [[nodiscard]] CDynamicParameter& operator[](QStringView Identifier)
    {
        return operator[](Identifier.toString().toStdString());
    }

    /**
     * @brief Returns the @c CDynamicParameter for the given @a Identifier
     *
     * @param Identifier The Fully Qualified Identifier of the Parameter to get
     * @return A reference to the @c CDynamicParameter
     *
     * @throws std::out_of_range if there is no @c CDynamicParameter with the
     * given name @a Identifier
     */
    [[nodiscard]] CDynamicParameter& operator[](
        const CFullyQualifiedCommandParameterID& Identifier);

    using QList::at;
    using QList::operator[];
};

/**
 * @brief Overload for debugging @c CDynamicParameter
 */
SILA_CPP_EXPORT QDebug operator<<(QDebug dbg, const CDynamicParameter& rhs);

/**
 * @brief Overload for printing @c CDynamicParameters
 */
SILA_CPP_EXPORT std::ostream& operator<<(std::ostream& os,
                                         const CDynamicParameter& rhs);
}  // namespace SiLA2
#endif  // DYNAMICPARAMETER_H
