/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   ClientMetadata.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   22.02.2021
/// \brief  Declaration of the CClientMetadata class
//============================================================================
#ifndef CLIENTMETADATA_H
#define CLIENTMETADATA_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/client/DynamicValue.h>
#include <sila_cpp/common/FullyQualifiedMetadataID.h>
#include <sila_cpp/config.h>
#include <sila_cpp/global.h>

#include <QStringList>

#include <polymorphic_value.h>

#include <map>

//============================================================================
//                            FORWARD DECLARATIONS
//============================================================================
#if GRPC_VERSION >= GRPC_VERSION_CHECK(1, 32, 0)
namespace grpc
{
class Channel;
}  // namespace grpc
#else
namespace grpc_impl
{
class Channel;
}  // namespace grpc_impl
namespace grpc
{
typedef grpc_impl::Channel Channel;
}  // namespace grpc
#endif

namespace SiLA2
{
class CDynamicFeatureStub;
namespace codegen
{
    namespace fdl
    {
        class CMetadata;
    }  // namespace fdl
    namespace proto
    {
        class CDynamicMessageFactory;
    }  // namespace proto
}  // namespace codegen
}  // namespace SiLA2

namespace SiLA2
{
/**
 * @brief The CClientMetadata class represents additional information that a SiLA
 * Server expects to receive when executing a Command or reading or subscribing to
 * a Property.
 */
class SILA_CPP_EXPORT CClientMetadata : public CDynamicValue
{
    class PrivateImpl;
    using PrivateImplPtr = isocpp_p0201::polymorphic_value<PrivateImpl>;

public:
    // inherit special member functions
    using CDynamicValue::operator=;

    /**
     * @brief C'tor
     *
     * @param Identifier The Fully Qualified Metadata Identifier of this Metadata
     * @param MetadataFDL The parsed FDL description of the Metadata
     * @param Channel The gRPC channel to the Server
     * @param DMF A shared_ptr to the Dynamic Message Factory that should be
     * used to generate the dynamic Metadata/Response messages
     * @param Feature The SiLA Feature Stub that this Metadata belongs to
     */
    CClientMetadata(CFullyQualifiedMetadataID Identifier,
                    const codegen::fdl::CMetadata& MetadataFDL,
                    std::shared_ptr<grpc::Channel> Channel,
                    std::shared_ptr<codegen::proto::CDynamicMessageFactory> DMF,
                    CDynamicFeatureStub* Feature);

    /**
     * @brief Get the Fully Qualified Identifier of this Metadata
     *
     * @return The Metadata's Fully Qualified Identifier
     */
    [[nodiscard]] CFullyQualifiedMetadataID identifier() const;

    /**
     * @brief Get the Display Name of this Metadata
     *
     * @return The Metadata's Display Name
     */
    [[nodiscard]] QString displayName() const;

    /**
     * @brief Get the Description of this Metadata
     *
     * @return The Metadata's Description
     */
    [[nodiscard]] QString description() const;

    /**
     * @brief Get a list of all Fully Qualified Identifiers as QStrings of the
     * Features/Commands/Properties that are affected by this Metadata
     *
     * @return A list of all affected Features'/Commands'/Properties' Identifiers
     */
    [[nodiscard]] QStringList affectedCalls() const;

    /**
     * @brief Get a pointer to the Feature that this Metadata belongs to
     *
     * @note No ownership is transferred here!
     *
     * @return A pointer to the Feature of this Metadata
     */
    [[nodiscard]] CDynamicFeatureStub* feature() const;

    /**
     * @brief Convert this Metadata into its protobuf @c Message and serialize
     * this @c Message as a @c std::string
     *
     * @return The serialized protobuf @c Message of this Metadata
     */
    [[nodiscard]] std::string toSerializedMessage() const;

    /**
     * @brief Convert this Metadata into its corresponding protobuf @c Message
     *
     * @return A pointer to the protobuf @c Message for this Metadata
     */
    [[nodiscard]] google::protobuf::Message* toProtoMessagePtr() const override;

    /**
     * @brief Get a default constructed (i.e. empty) @c CDynamicStructure object
     * for this Value
     *
     * This object can be filled with the necessary Structure Element values
     * and then set as this Value by using @a setValue() or added to a List by
     * using @a addValue()
     *
     * @note This function should only be called when
     * <code>dataTypeType() == DataTypeType::STRUCTURE</code> or
     * <code>underlyingDataTypeType() == DataTypeType::STRUCTURE</code>.
     * Otherwise the returned @c CDynamicStructure will have no Elements (since
     * this Value has a different Data Type Type)
     *
     * @return An empty @c CDynamicStructure
     */
    [[nodiscard]] CDynamicStructure structure() const override;

    /**
     * @brief Returns the string representation of this Metadata that can be
     * displayed in a UI
     *
     * @return The Metadata's string representation
     */
    [[nodiscard]] QString prettyString() const override;

private:
    PIMPL_DECLARE_PRIVATE(CClientMetadata)
};

/**
 * @brief The CClientMetadataList class is a simple wrapper around
 * @c QList<CClientMetadata> that provides a few more convenience methods to
 * access the Metadata values in the list
 */
class SILA_CPP_EXPORT CClientMetadataList : public QList<CClientMetadata>
{
public:
    // inherit special member functions
    using QList::QList;
    using QList::operator[];

    /**
     * @brief Get the identifiers of all @c CClientMetadata in this list
     *
     * @return A list of all Metadata Identifiers
     */
    [[nodiscard]] QList<CFullyQualifiedMetadataID> identifiers() const;

    /**
     * @brief Returns the @c CClientMetadata for the given name @a Identifier
     *
     * @param Identifier The identifier of the Metadata to get
     * @return A reference to the @c CClientMetadata
     *
     * @throws std::out_of_range if there is no @c CClientMetadata with the
     * given identifier @a Identifier
     */
    [[nodiscard]] const CClientMetadata& at(
        const CFullyQualifiedMetadataID& Identifier) const;

    /**
     * @brief Returns the @c CClientMetadata for the given name @a Identifier
     *
     * @param Identifier The identifier of the Metadata to get
     * @return A reference to the @c CClientMetadata
     *
     * @throws std::out_of_range if there is no @c CClientMetadata with the
     * given identifier @a Identifier
     */
    [[nodiscard]] const CClientMetadata& at(std::string_view Identifier) const
    {
        return at(CFullyQualifiedMetadataID::fromString(Identifier.data()));
    }

    /**
     * @brief Returns the @c CClientMetadata for the given name @a Identifier
     *
     * @param Identifier The identifier of the Metadata to get
     * @return A reference to the @c CClientMetadata
     *
     * @throws std::out_of_range if there is no @c CClientMetadata with the
     * given identifier @a Identifier
     */
    [[nodiscard]] const CClientMetadata& at(QStringView Identifier) const
    {
        return at(CFullyQualifiedMetadataID::fromString(Identifier.toString()));
    }

    /**
     * @brief Returns the @c CClientMetadata for the given name @a Identifier
     *
     * @param Identifier The identifier of the Metadata to get
     * @return A reference to the @c CClientMetadata
     *
     * @throws std::out_of_range if there is no @c CClientMetadata with the
     * given identifier @a Identifier
     */
    [[nodiscard]] CClientMetadata& operator[](
        const CFullyQualifiedMetadataID& Identifier);

    /**
     * @brief Returns the @c CClientMetadata for the given name @a Identifier
     *
     * @param Identifier The identifier of the Metadata to get
     * @return A reference to the @c CClientMetadata
     *
     * @throws std::out_of_range if there is no @c CClientMetadata with the
     * given identifier @a Identifier
     */
    [[nodiscard]] CClientMetadata& operator[](std::string_view Identifier)
    {
        return operator[](
            CFullyQualifiedMetadataID::fromString(Identifier.data()));
    }

    /**
     * @brief Returns the @c CClientMetadata for the given name @a Identifier
     *
     * @param Identifier The identifier of the Metadata to get
     * @return A reference to the @c CClientMetadata
     *
     * @throws std::out_of_range if there is no @c CClientMetadata with the
     * given identifier @a Identifier
     */
    [[nodiscard]] CClientMetadata& operator[](QStringView Identifier)
    {
        return operator[](
            CFullyQualifiedMetadataID::fromString(Identifier.toString()));
    }

    /**
     * @brief Convert this List of Metadata into a @c std::multimap that is
     * suitable to be used in gRPC calls (this includes the conversion of the
     * Fully Qualified Metadata Identifier into a gRPC header name and the
     * serialization of the Metadata protobuf @c Message)
     *
     * @return The Metadata List as a @c std::multimap
     */
    [[nodiscard]] std::multimap<std::string, std::string> toMultiMap() const;
};

/**
 * @brief Overload for debugging @c CClientMetadata
 */
SILA_CPP_EXPORT QDebug operator<<(QDebug dbg, const CClientMetadata& rhs);

/**
 * @brief Overload for printing @c CClientMetadata
 */
SILA_CPP_EXPORT std::ostream& operator<<(std::ostream& os,
                                         const CClientMetadata& rhs);
}  // namespace SiLA2
#endif  // CLIENTMETADATA_H
