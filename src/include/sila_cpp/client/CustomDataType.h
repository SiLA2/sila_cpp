/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   CustomDataType.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   01.03.2021
/// \brief  Declaration of the CCustomDataType class
//============================================================================
#ifndef CUSTOMDATATYPE_H
#define CUSTOMDATATYPE_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/client/DynamicValue.h>
#include <sila_cpp/common/FullyQualifiedDataTypeID.h>
#include <sila_cpp/config.h>
#include <sila_cpp/global.h>

#include <polymorphic_value.h>

//============================================================================
//                            FORWARD DECLARATIONS
//============================================================================
#if GRPC_VERSION >= GRPC_VERSION_CHECK(1, 32, 0)
namespace grpc
{
class Channel;
}  // namespace grpc
#else
namespace grpc_impl
{
class Channel;
}  // namespace grpc_impl
namespace grpc
{
typedef grpc_impl::Channel Channel;
}  // namespace grpc
#endif

namespace SiLA2::codegen::fdl
{
class CDataTypeDefinition;
}  // namespace SiLA2::codegen::fdl

namespace SiLA2
{
/**
 * @brief The CCustomDataType class represents a value of a Custom Data Type that
 * cannot be known at compile time
 */
class SILA_CPP_EXPORT CCustomDataType : public CDynamicValue
{
    class PrivateImpl;
    using PrivateImplPtr = isocpp_p0201::polymorphic_value<PrivateImpl>;

public:
    // inherit special member functions
    using CDynamicValue::operator=;

    /**
     * @brief C'tor
     *
     * @param Identifier The Fully Qualified Data Type Identifier of this Custom
     * Data Type
     * @param DataTypeFDL The parsed FDL description of this Custom Data Type
     * @param Channel ignored
     * @param DMF A shared_ptr to the Dynamic Message Factory to use to create the
     * dynamic protobuf Message for this Custom Data Type
     * @param Feature The SiLA Feature Stub that this Custom Data Type belongs to
     */
    CCustomDataType(CFullyQualifiedDataTypeID Identifier,
                    const codegen::fdl::CDataTypeDefinition& DataTypeFDL,
                    const std::shared_ptr<grpc::Channel>& Channel,
                    std::shared_ptr<codegen::proto::CDynamicMessageFactory> DMF,
                    CDynamicFeatureStub* Feature);

    /**
     * @brief Get the Fully Qualified Identifier of this Data Type
     *
     * @return The Data Type's Fully Qualified Identifier
     */
    [[nodiscard]] CFullyQualifiedDataTypeID identifier() const;

    /**
     * @brief Get a pointer to the Feature that this Custom Data Type belongs to
     *
     * @note No ownership is transferred here!
     *
     * @return A pointer to the Feature of this Custom Data Type
     */
    [[nodiscard]] CDynamicFeatureStub* feature() const;

    /**
     * @brief Convert this Custom Data Type into its corresponding protobuf
     * @c Message
     *
     * @return A pointer to the protobuf @c Message for this Custom Data Type
     */
    [[nodiscard]] google::protobuf::Message* toProtoMessagePtr() const override;

    /**
     * @brief Get a default constructed (i.e. empty) @c CDynamicStructure object
     * for this Value
     *
     * This object can be filled with the necessary Structure Element values
     * and then set as this Value by using @a setValue() or added to a List by
     * using @a addValue()
     *
     * @note This function should only be called when
     * <code>dataTypeType() == DataTypeType::STRUCTURE</code> or
     * <code>underlyingDataTypeType() == DataTypeType::STRUCTURE</code>.
     * Otherwise the returned @c CDynamicStructure will have no Elements (since
     * this Value has a different Data Type Type)
     *
     * @return An empty @c CDynamicStructure
     */
    [[nodiscard]] CDynamicStructure structure() const override;

private:
    friend class CDynamicFeatureStub;

    /**
     * @internal Only to be used by @c CDynamicFeatureStub
     *
     * @brief Fills this custom data type with a default value
     */
    void fillWithDefaultValue();

    PIMPL_DECLARE_PRIVATE(CCustomDataType)
};

/**
 * @brief Overload for debugging @c CCustomDataType
 */
SILA_CPP_EXPORT QDebug operator<<(QDebug dbg, const CCustomDataType& rhs);

/**
 * @brief Overload for printing @c CCustomDataType
 */
SILA_CPP_EXPORT std::ostream& operator<<(std::ostream& os,
                                         const CCustomDataType& rhs);
}  // namespace SiLA2
#endif  // CUSTOMDATATYPE_H
