/*******************************************************************************
 ** This file is part of the sila_cpp project.
 ** Copyright 2021 SiLA 2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 *****************************************************************************/

//============================================================================
/// \file   test_logging.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   10.08.2021
/// \brief  Unit tests for the logging classes and functions
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/logging.h>

#include <catch2/catch.hpp>

//============================================================================
static const QList LOGGING_CATEGORY_NAMES = {"sila_cpp",
                                             "sila_cpp.client",
                                             "sila_cpp.codegen",
                                             "sila_cpp.common",
                                             "sila_cpp.discovery",
                                             "sila_cpp.framework.data_types",
                                             "sila_cpp.framework.errors",
                                             "sila_cpp.internal",
                                             "sila_cpp.server"};

//============================================================================
static const QList LOGGING_CATEGORIES = {
    sila_cpp,        sila_cpp_client,    sila_cpp_codegen,
    sila_cpp_common, sila_cpp_discovery, sila_cpp_data_types,
    sila_cpp_errors, sila_cpp_internal,  sila_cpp_server};

#define CLEAN_ENV()                                                              \
    std::cout << "\nclean env\n";                                                \
                                                                                 \
    qputenv("SILA_CPP_LOGGING_LEVEL", "");                                       \
    qputenv("SILA_CPP_CLIENT_LOGGING_LEVEL", "");                                \
    qputenv("SILA_CPP_CODEGEN_LOGGING_LEVEL", "");                               \
    qputenv("SILA_CPP_COMMON_LOGGING_LEVEL", "");                                \
    qputenv("SILA_CPP_DISCOVERY_LOGGING_LEVEL", "");                             \
    qputenv("SILA_CPP_FRAMEWORK_DATA_TYPES_LOGGING_LEVEL", "");                  \
    qputenv("SILA_CPP_FRAMEWORK_ERRORS_LOGGING_LEVEL", "");                      \
    qputenv("SILA_CPP_INTERNAL_LOGGING_LEVEL", "");                              \
    qputenv("SILA_CPP_SERVER_LOGGING_LEVEL", "");                                \
    SiLA2::logging::CLogManager::setLoggingLevel();                              \
    std::cout << "done\n";

#define DEBUG_CATEGORY_LOGGING_LEVELS(category)                                  \
    INFO(category().categoryName())                                              \
    INFO("isDebugEnabled " << std::boolalpha << category().isDebugEnabled())     \
    INFO("isInfoEnabled " << std::boolalpha << category().isInfoEnabled())       \
    INFO("isWarningEnabled " << std::boolalpha << category().isWarningEnabled()) \
    INFO("isCriticalEnabled " << std::boolalpha << category().isCriticalEnabled())

//============================================================================
SCENARIO("All default values", "[logging]")
{
    GIVEN("A clean environment")
    {
        CLEAN_ENV()

        WHEN("nothing is changed")
        {
            THEN("the logging level for all sila_cpp logging categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                    REQUIRE_FALSE(Category().isDebugEnabled());
                    REQUIRE(Category().isInfoEnabled());
                    REQUIRE(Category().isWarningEnabled());
                    REQUIRE(Category().isCriticalEnabled());
                }
            }
        }
    }
}

//============================================================================
SCENARIO("Changing SILA_CPP_LOGGING_LEVEL environment variable", "[logging]")
{
    GIVEN("A clean environment")
    {
        CLEAN_ENV()

        WHEN("SILA_CPP_LOGGING_LEVEL is set to DEBUG")
        {
            std::cout << '\n' << '\n';
            qputenv("SILA_CPP_LOGGING_LEVEL", "DEBUG");
            SiLA2::logging::CLogManager::setLoggingLevel();

            THEN("the logging level for all sila_cpp logging categories is DEBUG")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                    REQUIRE(Category().isDebugEnabled());
                    REQUIRE(Category().isInfoEnabled());
                    REQUIRE(Category().isWarningEnabled());
                    REQUIRE(Category().isCriticalEnabled());
                }
            }
        }
        AND_WHEN("SILA_CPP_LOGGING_LEVEL is set to INFO")
        {
            std::cout << '\n' << '\n';
            qputenv("SILA_CPP_LOGGING_LEVEL", "INFO");
            SiLA2::logging::CLogManager::setLoggingLevel();

            THEN("the logging level for all sila_cpp logging categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                    REQUIRE_FALSE(Category().isDebugEnabled());
                    REQUIRE(Category().isInfoEnabled());
                    REQUIRE(Category().isWarningEnabled());
                    REQUIRE(Category().isCriticalEnabled());
                }
            }
        }
        AND_WHEN("SILA_CPP_LOGGING_LEVEL is set to WARNING")
        {
            std::cout << '\n' << '\n';
            qputenv("SILA_CPP_LOGGING_LEVEL", "WARNING");
            SiLA2::logging::CLogManager::setLoggingLevel();

            THEN("the logging level for all sila_cpp logging categories is "
                 "WARNING")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                    REQUIRE_FALSE(Category().isDebugEnabled());
                    REQUIRE_FALSE(Category().isInfoEnabled());
                    REQUIRE(Category().isWarningEnabled());
                    REQUIRE(Category().isCriticalEnabled());
                }
            }
        }
        AND_WHEN("SILA_CPP_LOGGING_LEVEL is set to CRITICAL")
        {
            std::cout << '\n' << '\n';
            qputenv("SILA_CPP_LOGGING_LEVEL", "CRITICAL");
            SiLA2::logging::CLogManager::setLoggingLevel();

            THEN("the logging level for all sila_cpp logging categories is "
                 "CRITICAL")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                    REQUIRE_FALSE(Category().isDebugEnabled());
                    REQUIRE_FALSE(Category().isInfoEnabled());
                    REQUIRE_FALSE(Category().isWarningEnabled());
                    REQUIRE(Category().isCriticalEnabled());
                }
            }
        }
        AND_WHEN("SILA_CPP_LOGGING_LEVEL is set to FATAL")
        {
            std::cout << '\n' << '\n';
            qputenv("SILA_CPP_LOGGING_LEVEL", "FATAL");
            SiLA2::logging::CLogManager::setLoggingLevel();

            THEN("the logging level for all sila_cpp logging categories is FATAL")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                    REQUIRE_FALSE(Category().isDebugEnabled());
                    REQUIRE_FALSE(Category().isInfoEnabled());
                    REQUIRE_FALSE(Category().isWarningEnabled());
                    REQUIRE_FALSE(Category().isCriticalEnabled());
                }
            }
        }
    }
}

//============================================================================
SCENARIO("Modifying SILA_CPP_CLIENT_LOGGING_LEVEL environment variable",
         "[logging]")
{
    GIVEN("A clean environment")
    {
        CLEAN_ENV()

        WHEN("SILA_CPP_CLIENT_LOGGING_LEVEL is set to DEBUG")
        {
            std::cout << '\n' << '\n';
            qputenv("SILA_CPP_CLIENT_LOGGING_LEVEL", "DEBUG");
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.client");

            THEN("the logging level for the sila_cpp.client logging category is "
                 "DEBUG")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_client)

                REQUIRE(sila_cpp_client().isDebugEnabled());
                REQUIRE(sila_cpp_client().isInfoEnabled());
                REQUIRE(sila_cpp_client().isWarningEnabled());
                REQUIRE(sila_cpp_client().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.client") != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("SILA_CPP_CLIENT_LOGGING_LEVEL is set to INFO")
        {
            std::cout << '\n' << '\n';
            qputenv("SILA_CPP_CLIENT_LOGGING_LEVEL", "INFO");
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.client");

            THEN("the logging level for the sila_cpp.client logging category is "
                 "INFO")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_client)

                REQUIRE_FALSE(sila_cpp_client().isDebugEnabled());
                REQUIRE(sila_cpp_client().isInfoEnabled());
                REQUIRE(sila_cpp_client().isWarningEnabled());
                REQUIRE(sila_cpp_client().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.client") != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("SILA_CPP_CLIENT_LOGGING_LEVEL is set to WARNING")
        {
            std::cout << '\n' << '\n';
            qputenv("SILA_CPP_CLIENT_LOGGING_LEVEL", "WARNING");
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.client");

            THEN("the logging level for the sila_cpp.client logging category is "
                 "WARNING")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_client)

                REQUIRE_FALSE(sila_cpp_client().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_client().isInfoEnabled());
                REQUIRE(sila_cpp_client().isWarningEnabled());
                REQUIRE(sila_cpp_client().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.client") != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("SILA_CPP_CLIENT_LOGGING_LEVEL is set to CRITICAL")
        {
            std::cout << '\n' << '\n';
            qputenv("SILA_CPP_CLIENT_LOGGING_LEVEL", "CRITICAL");
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.client");

            THEN("the logging level for the sila_cpp.client logging category is "
                 "CRITICAL")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_client)

                REQUIRE_FALSE(sila_cpp_client().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_client().isInfoEnabled());
                REQUIRE_FALSE(sila_cpp_client().isWarningEnabled());
                REQUIRE(sila_cpp_client().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.client") != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("SILA_CPP_CLIENT_LOGGING_LEVEL is set to FATAL")
        {
            std::cout << '\n' << '\n';
            qputenv("SILA_CPP_CLIENT_LOGGING_LEVEL", "FATAL");
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.client");

            THEN("the logging level for the sila_cpp.client logging category is "
                 "FATAL")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_client)

                REQUIRE_FALSE(sila_cpp_client().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_client().isInfoEnabled());
                REQUIRE_FALSE(sila_cpp_client().isWarningEnabled());
                REQUIRE_FALSE(sila_cpp_client().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.client") != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
    }
}

//============================================================================
SCENARIO("Modifying SILA_CPP_CODEGEN_LOGGING_LEVEL environment variable",
         "[logging]")
{
    GIVEN("A clean environment")
    {
        CLEAN_ENV()

        WHEN("SILA_CPP_CODEGEN_LOGGING_LEVEL is set to DEBUG")
        {
            std::cout << '\n' << '\n';
            qputenv("SILA_CPP_CODEGEN_LOGGING_LEVEL", "DEBUG");
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.codegen");

            THEN("the logging level for the sila_cpp.codegen logging category is "
                 "DEBUG")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_codegen)

                REQUIRE(sila_cpp_codegen().isDebugEnabled());
                REQUIRE(sila_cpp_codegen().isInfoEnabled());
                REQUIRE(sila_cpp_codegen().isWarningEnabled());
                REQUIRE(sila_cpp_codegen().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.codegen")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("SILA_CPP_CODEGEN_LOGGING_LEVEL is set to INFO")
        {
            std::cout << '\n' << '\n';
            qputenv("SILA_CPP_CODEGEN_LOGGING_LEVEL", "INFO");
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.codegen");

            THEN("the logging level for the sila_cpp.codegen logging category is "
                 "INFO")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_codegen)

                REQUIRE_FALSE(sila_cpp_codegen().isDebugEnabled());
                REQUIRE(sila_cpp_codegen().isInfoEnabled());
                REQUIRE(sila_cpp_codegen().isWarningEnabled());
                REQUIRE(sila_cpp_codegen().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.codegen")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("SILA_CPP_CODEGEN_LOGGING_LEVEL is set to WARNING")
        {
            std::cout << '\n' << '\n';
            qputenv("SILA_CPP_CODEGEN_LOGGING_LEVEL", "WARNING");
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.codegen");

            THEN("the logging level for the sila_cpp.codegen logging category is "
                 "WARNING")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_codegen)

                REQUIRE_FALSE(sila_cpp_codegen().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_codegen().isInfoEnabled());
                REQUIRE(sila_cpp_codegen().isWarningEnabled());
                REQUIRE(sila_cpp_codegen().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.codegen")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("SILA_CPP_CODEGEN_LOGGING_LEVEL is set to CRITICAL")
        {
            std::cout << '\n' << '\n';
            qputenv("SILA_CPP_CODEGEN_LOGGING_LEVEL", "CRITICAL");
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.codegen");

            THEN("the logging level for the sila_cpp.codegen logging category is "
                 "CRITICAL")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_codegen)

                REQUIRE_FALSE(sila_cpp_codegen().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_codegen().isInfoEnabled());
                REQUIRE_FALSE(sila_cpp_codegen().isWarningEnabled());
                REQUIRE(sila_cpp_codegen().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.codegen")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("SILA_CPP_CODEGEN_LOGGING_LEVEL is set to FATAL")
        {
            std::cout << '\n' << '\n';
            qputenv("SILA_CPP_CODEGEN_LOGGING_LEVEL", "FATAL");
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.codegen");

            THEN("the logging level for the sila_cpp.codegen logging category is "
                 "FATAL")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_codegen)

                REQUIRE_FALSE(sila_cpp_codegen().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_codegen().isInfoEnabled());
                REQUIRE_FALSE(sila_cpp_codegen().isWarningEnabled());
                REQUIRE_FALSE(sila_cpp_codegen().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.codegen")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
    }
}

//============================================================================
SCENARIO("Modifying SILA_CPP_COMMON_LOGGING_LEVEL environment variable",
         "[logging]")
{
    GIVEN("A clean environment")
    {
        CLEAN_ENV()

        WHEN("SILA_CPP_COMMON_LOGGING_LEVEL is set to DEBUG")
        {
            std::cout << '\n' << '\n';
            qputenv("SILA_CPP_COMMON_LOGGING_LEVEL", "DEBUG");
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.common");

            THEN("the logging level for the sila_cpp.common logging category is "
                 "DEBUG")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_common)

                REQUIRE(sila_cpp_common().isDebugEnabled());
                REQUIRE(sila_cpp_common().isInfoEnabled());
                REQUIRE(sila_cpp_common().isWarningEnabled());
                REQUIRE(sila_cpp_common().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.common") != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("SILA_CPP_COMMON_LOGGING_LEVEL is set to INFO")
        {
            std::cout << '\n' << '\n';
            qputenv("SILA_CPP_COMMON_LOGGING_LEVEL", "INFO");
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.common");

            THEN("the logging level for the sila_cpp.common logging category is "
                 "INFO")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_common)

                REQUIRE_FALSE(sila_cpp_common().isDebugEnabled());
                REQUIRE(sila_cpp_common().isInfoEnabled());
                REQUIRE(sila_cpp_common().isWarningEnabled());
                REQUIRE(sila_cpp_common().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.common") != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("SILA_CPP_COMMON_LOGGING_LEVEL is set to WARNING")
        {
            std::cout << '\n' << '\n';
            qputenv("SILA_CPP_COMMON_LOGGING_LEVEL", "WARNING");
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.common");

            THEN("the logging level for the sila_cpp.common logging category is "
                 "WARNING")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_common)

                REQUIRE_FALSE(sila_cpp_common().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_common().isInfoEnabled());
                REQUIRE(sila_cpp_common().isWarningEnabled());
                REQUIRE(sila_cpp_common().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.common") != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("SILA_CPP_COMMON_LOGGING_LEVEL is set to CRITICAL")
        {
            std::cout << '\n' << '\n';
            qputenv("SILA_CPP_COMMON_LOGGING_LEVEL", "CRITICAL");
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.common");

            THEN("the logging level for the sila_cpp.common logging category is "
                 "CRITICAL")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_common)

                REQUIRE_FALSE(sila_cpp_common().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_common().isInfoEnabled());
                REQUIRE_FALSE(sila_cpp_common().isWarningEnabled());
                REQUIRE(sila_cpp_common().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.common") != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("SILA_CPP_COMMON_LOGGING_LEVEL is set to FATAL")
        {
            std::cout << '\n' << '\n';
            qputenv("SILA_CPP_COMMON_LOGGING_LEVEL", "FATAL");
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.common");

            THEN("the logging level for the sila_cpp.common logging category is "
                 "FATAL")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_common)

                REQUIRE_FALSE(sila_cpp_common().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_common().isInfoEnabled());
                REQUIRE_FALSE(sila_cpp_common().isWarningEnabled());
                REQUIRE_FALSE(sila_cpp_common().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.common") != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
    }
}

//============================================================================
SCENARIO("Modifying SILA_CPP_DISCOVERY_LOGGING_LEVEL environment variable",
         "[logging]")
{
    GIVEN("A clean environment")
    {
        CLEAN_ENV()

        WHEN("SILA_CPP_DISCOVERY_LOGGING_LEVEL is set to DEBUG")
        {
            std::cout << '\n' << '\n';
            qputenv("SILA_CPP_DISCOVERY_LOGGING_LEVEL", "DEBUG");
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.discovery");

            THEN("the logging level for the sila_cpp.discovery logging category "
                 "is DEBUG")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_discovery)

                REQUIRE(sila_cpp_discovery().isDebugEnabled());
                REQUIRE(sila_cpp_discovery().isInfoEnabled());
                REQUIRE(sila_cpp_discovery().isWarningEnabled());
                REQUIRE(sila_cpp_discovery().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.discovery")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("SILA_CPP_DISCOVERY_LOGGING_LEVEL is set to INFO")
        {
            std::cout << '\n' << '\n';
            qputenv("SILA_CPP_DISCOVERY_LOGGING_LEVEL", "INFO");
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.discovery");

            THEN("the logging level for the sila_cpp.discovery logging category "
                 "is INFO")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_discovery)

                REQUIRE_FALSE(sila_cpp_discovery().isDebugEnabled());
                REQUIRE(sila_cpp_discovery().isInfoEnabled());
                REQUIRE(sila_cpp_discovery().isWarningEnabled());
                REQUIRE(sila_cpp_discovery().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.discovery")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("SILA_CPP_DISCOVERY_LOGGING_LEVEL is set to WARNING")
        {
            std::cout << '\n' << '\n';
            qputenv("SILA_CPP_DISCOVERY_LOGGING_LEVEL", "WARNING");
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.discovery");

            THEN("the logging level for the sila_cpp.discovery logging category "
                 "is WARNING")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_discovery)

                REQUIRE_FALSE(sila_cpp_discovery().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_discovery().isInfoEnabled());
                REQUIRE(sila_cpp_discovery().isWarningEnabled());
                REQUIRE(sila_cpp_discovery().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.discovery")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("SILA_CPP_DISCOVERY_LOGGING_LEVEL is set to CRITICAL")
        {
            std::cout << '\n' << '\n';
            qputenv("SILA_CPP_DISCOVERY_LOGGING_LEVEL", "CRITICAL");
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.discovery");

            THEN("the logging level for the sila_cpp.discovery logging category "
                 "is CRITICAL")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_discovery)

                REQUIRE_FALSE(sila_cpp_discovery().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_discovery().isInfoEnabled());
                REQUIRE_FALSE(sila_cpp_discovery().isWarningEnabled());
                REQUIRE(sila_cpp_discovery().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.discovery")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("SILA_CPP_DISCOVERY_LOGGING_LEVEL is set to FATAL")
        {
            std::cout << '\n' << '\n';
            qputenv("SILA_CPP_DISCOVERY_LOGGING_LEVEL", "FATAL");
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.discovery");

            THEN("the logging level for the sila_cpp.discovery logging category "
                 "is FATAL")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_discovery)

                REQUIRE_FALSE(sila_cpp_discovery().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_discovery().isInfoEnabled());
                REQUIRE_FALSE(sila_cpp_discovery().isWarningEnabled());
                REQUIRE_FALSE(sila_cpp_discovery().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.discovery")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
    }
}

//============================================================================
SCENARIO("Modifying SILA_CPP_FRAMEWORK_DATA_TYPES_LOGGING_LEVEL environment "
         "variable",
         "[logging]")
{
    GIVEN("A clean environment")
    {
        CLEAN_ENV()

        WHEN("SILA_CPP_FRAMEWORK_DATA_TYPES_LOGGING_LEVEL is set to DEBUG")
        {
            std::cout << '\n' << '\n';
            qputenv("SILA_CPP_FRAMEWORK_DATA_TYPES_LOGGING_LEVEL", "DEBUG");
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.framework."
                                                         "data_types");

            THEN("the logging level for the sila_cpp.framework.data_types "
                 "logging category is DEBUG")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_data_types)

                REQUIRE(sila_cpp_data_types().isDebugEnabled());
                REQUIRE(sila_cpp_data_types().isInfoEnabled());
                REQUIRE(sila_cpp_data_types().isWarningEnabled());
                REQUIRE(sila_cpp_data_types().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.framework."
                                                          "data_types")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("SILA_CPP_FRAMEWORK_DATA_TYPES_LOGGING_LEVEL is set to INFO")
        {
            std::cout << '\n' << '\n';
            qputenv("SILA_CPP_FRAMEWORK_DATA_TYPES_LOGGING_LEVEL", "INFO");
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.framework."
                                                         "data_types");

            THEN("the logging level for the sila_cpp.framework.data_types "
                 "logging category is INFO")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_data_types)

                REQUIRE_FALSE(sila_cpp_data_types().isDebugEnabled());
                REQUIRE(sila_cpp_data_types().isInfoEnabled());
                REQUIRE(sila_cpp_data_types().isWarningEnabled());
                REQUIRE(sila_cpp_data_types().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.framework."
                                                          "data_types")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("SILA_CPP_FRAMEWORK_DATA_TYPES_LOGGING_LEVEL is set to WARNING")
        {
            std::cout << '\n' << '\n';
            qputenv("SILA_CPP_FRAMEWORK_DATA_TYPES_LOGGING_LEVEL", "WARNING");
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.framework."
                                                         "data_types");

            THEN("the logging level for the sila_cpp.framework.data_types "
                 "logging category is WARNING")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_data_types)

                REQUIRE_FALSE(sila_cpp_data_types().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_data_types().isInfoEnabled());
                REQUIRE(sila_cpp_data_types().isWarningEnabled());
                REQUIRE(sila_cpp_data_types().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.framework."
                                                          "data_types")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("SILA_CPP_FRAMEWORK_DATA_TYPES_LOGGING_LEVEL is set to CRITICAL")
        {
            std::cout << '\n' << '\n';
            qputenv("SILA_CPP_FRAMEWORK_DATA_TYPES_LOGGING_LEVEL", "CRITICAL");
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.framework."
                                                         "data_types");

            THEN("the logging level for the sila_cpp.framework.data_types "
                 "logging category is CRITICAL")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_data_types)

                REQUIRE_FALSE(sila_cpp_data_types().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_data_types().isInfoEnabled());
                REQUIRE_FALSE(sila_cpp_data_types().isWarningEnabled());
                REQUIRE(sila_cpp_data_types().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.framework."
                                                          "data_types")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("SILA_CPP_FRAMEWORK_DATA_TYPES_LOGGING_LEVEL is set to FATAL")
        {
            std::cout << '\n' << '\n';
            qputenv("SILA_CPP_FRAMEWORK_DATA_TYPES_LOGGING_LEVEL", "FATAL");
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.framework."
                                                         "data_types");

            THEN("the logging level for the sila_cpp.framework.data_types "
                 "logging category is FATAL")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_data_types)

                REQUIRE_FALSE(sila_cpp_data_types().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_data_types().isInfoEnabled());
                REQUIRE_FALSE(sila_cpp_data_types().isWarningEnabled());
                REQUIRE_FALSE(sila_cpp_data_types().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.framework."
                                                          "data_types")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
    }
}

//============================================================================
SCENARIO("Modifying SILA_CPP_FRAMEWORK_ERRORS_LOGGING_LEVEL environment variable",
         "[logging]")
{
    GIVEN("A clean environment")
    {
        CLEAN_ENV()

        WHEN("SILA_CPP_FRAMEWORK_ERRORS_LOGGING_LEVEL is set to DEBUG")
        {
            std::cout << '\n' << '\n';
            qputenv("SILA_CPP_FRAMEWORK_ERRORS_LOGGING_LEVEL", "DEBUG");
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.framework."
                                                         "errors");

            THEN("the logging level for the sila_cpp.framework.errors logging "
                 "category is DEBUG")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_errors)

                REQUIRE(sila_cpp_errors().isDebugEnabled());
                REQUIRE(sila_cpp_errors().isInfoEnabled());
                REQUIRE(sila_cpp_errors().isWarningEnabled());
                REQUIRE(sila_cpp_errors().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.framework."
                                                          "errors")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("SILA_CPP_FRAMEWORK_ERRORS_LOGGING_LEVEL is set to INFO")
        {
            std::cout << '\n' << '\n';
            qputenv("SILA_CPP_FRAMEWORK_ERRORS_LOGGING_LEVEL", "INFO");
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.framework."
                                                         "errors");

            THEN("the logging level for the sila_cpp.framework.errors logging "
                 "category is INFO")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_errors)

                REQUIRE_FALSE(sila_cpp_errors().isDebugEnabled());
                REQUIRE(sila_cpp_errors().isInfoEnabled());
                REQUIRE(sila_cpp_errors().isWarningEnabled());
                REQUIRE(sila_cpp_errors().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.framework."
                                                          "errors")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("SILA_CPP_FRAMEWORK_ERRORS_LOGGING_LEVEL is set to WARNING")
        {
            std::cout << '\n' << '\n';
            qputenv("SILA_CPP_FRAMEWORK_ERRORS_LOGGING_LEVEL", "WARNING");
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.framework."
                                                         "errors");

            THEN("the logging level for the sila_cpp.framework.errors logging "
                 "category is WARNING")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_errors)

                REQUIRE_FALSE(sila_cpp_errors().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_errors().isInfoEnabled());
                REQUIRE(sila_cpp_errors().isWarningEnabled());
                REQUIRE(sila_cpp_errors().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.framework."
                                                          "errors")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("SILA_CPP_FRAMEWORK_ERRORS_LOGGING_LEVEL is set to CRITICAL")
        {
            std::cout << '\n' << '\n';
            qputenv("SILA_CPP_FRAMEWORK_ERRORS_LOGGING_LEVEL", "CRITICAL");
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.framework."
                                                         "errors");

            THEN("the logging level for the sila_cpp.framework.errors logging "
                 "category is CRITICAL")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_errors)

                REQUIRE_FALSE(sila_cpp_errors().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_errors().isInfoEnabled());
                REQUIRE_FALSE(sila_cpp_errors().isWarningEnabled());
                REQUIRE(sila_cpp_errors().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.framework."
                                                          "errors")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("SILA_CPP_FRAMEWORK_ERRORS_LOGGING_LEVEL is set to FATAL")
        {
            std::cout << '\n' << '\n';
            qputenv("SILA_CPP_FRAMEWORK_ERRORS_LOGGING_LEVEL", "FATAL");
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.framework."
                                                         "errors");

            THEN("the logging level for the sila_cpp.framework.errors logging "
                 "category is FATAL")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_errors)

                REQUIRE_FALSE(sila_cpp_errors().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_errors().isInfoEnabled());
                REQUIRE_FALSE(sila_cpp_errors().isWarningEnabled());
                REQUIRE_FALSE(sila_cpp_errors().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.framework."
                                                          "errors")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
    }
}

//============================================================================
SCENARIO("Modifying SILA_CPP_INTERNAL_LOGGING_LEVEL environment variable",
         "[logging]")
{
    GIVEN("A clean environment")
    {
        CLEAN_ENV()

        WHEN("SILA_CPP_INTERNAL_LOGGING_LEVEL is set to DEBUG")
        {
            std::cout << '\n' << '\n';
            qputenv("SILA_CPP_INTERNAL_LOGGING_LEVEL", "DEBUG");
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.internal");

            THEN("the logging level for the sila_cpp.internal logging category "
                 "is DEBUG")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_internal)

                REQUIRE(sila_cpp_internal().isDebugEnabled());
                REQUIRE(sila_cpp_internal().isInfoEnabled());
                REQUIRE(sila_cpp_internal().isWarningEnabled());
                REQUIRE(sila_cpp_internal().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.internal")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("SILA_CPP_INTERNAL_LOGGING_LEVEL is set to INFO")
        {
            std::cout << '\n' << '\n';
            qputenv("SILA_CPP_INTERNAL_LOGGING_LEVEL", "INFO");
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.internal");

            THEN("the logging level for the sila_cpp.internal logging category "
                 "is INFO")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_internal)

                REQUIRE_FALSE(sila_cpp_internal().isDebugEnabled());
                REQUIRE(sila_cpp_internal().isInfoEnabled());
                REQUIRE(sila_cpp_internal().isWarningEnabled());
                REQUIRE(sila_cpp_internal().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.internal")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("SILA_CPP_INTERNAL_LOGGING_LEVEL is set to WARNING")
        {
            std::cout << '\n' << '\n';
            qputenv("SILA_CPP_INTERNAL_LOGGING_LEVEL", "WARNING");
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.internal");

            THEN("the logging level for the sila_cpp.internal logging category "
                 "is WARNING")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_internal)

                REQUIRE_FALSE(sila_cpp_internal().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_internal().isInfoEnabled());
                REQUIRE(sila_cpp_internal().isWarningEnabled());
                REQUIRE(sila_cpp_internal().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.internal")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("SILA_CPP_INTERNAL_LOGGING_LEVEL is set to CRITICAL")
        {
            std::cout << '\n' << '\n';
            qputenv("SILA_CPP_INTERNAL_LOGGING_LEVEL", "CRITICAL");
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.internal");

            THEN("the logging level for the sila_cpp.internal logging category "
                 "is CRITICAL")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_internal)

                REQUIRE_FALSE(sila_cpp_internal().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_internal().isInfoEnabled());
                REQUIRE_FALSE(sila_cpp_internal().isWarningEnabled());
                REQUIRE(sila_cpp_internal().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.internal")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("SILA_CPP_INTERNAL_LOGGING_LEVEL is set to FATAL")
        {
            std::cout << '\n' << '\n';
            qputenv("SILA_CPP_INTERNAL_LOGGING_LEVEL", "FATAL");
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.internal");

            THEN("the logging level for the sila_cpp.internal logging category "
                 "is FATAL")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_internal)

                REQUIRE_FALSE(sila_cpp_internal().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_internal().isInfoEnabled());
                REQUIRE_FALSE(sila_cpp_internal().isWarningEnabled());
                REQUIRE_FALSE(sila_cpp_internal().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.internal")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
    }
}

//============================================================================
SCENARIO("Modifying SILA_CPP_SERVER_LOGGING_LEVEL environment variable",
         "[logging]")
{
    GIVEN("A clean environment")
    {
        CLEAN_ENV()

        WHEN("SILA_CPP_SERVER_LOGGING_LEVEL is set to DEBUG")
        {
            std::cout << '\n' << '\n';
            qputenv("SILA_CPP_SERVER_LOGGING_LEVEL", "DEBUG");
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.server");

            THEN("the logging level for the sila_cpp.server logging category is "
                 "DEBUG")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_server)

                REQUIRE(sila_cpp_server().isDebugEnabled());
                REQUIRE(sila_cpp_server().isInfoEnabled());
                REQUIRE(sila_cpp_server().isWarningEnabled());
                REQUIRE(sila_cpp_server().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.server") != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("SILA_CPP_SERVER_LOGGING_LEVEL is set to INFO")
        {
            std::cout << '\n' << '\n';
            qputenv("SILA_CPP_SERVER_LOGGING_LEVEL", "INFO");
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.server");

            THEN("the logging level for the sila_cpp.server logging category is "
                 "INFO")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_server)

                REQUIRE_FALSE(sila_cpp_server().isDebugEnabled());
                REQUIRE(sila_cpp_server().isInfoEnabled());
                REQUIRE(sila_cpp_server().isWarningEnabled());
                REQUIRE(sila_cpp_server().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.server") != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("SILA_CPP_SERVER_LOGGING_LEVEL is set to WARNING")
        {
            std::cout << '\n' << '\n';
            qputenv("SILA_CPP_SERVER_LOGGING_LEVEL", "WARNING");
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.server");

            THEN("the logging level for the sila_cpp.server logging category is "
                 "WARNING")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_server)

                REQUIRE_FALSE(sila_cpp_server().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_server().isInfoEnabled());
                REQUIRE(sila_cpp_server().isWarningEnabled());
                REQUIRE(sila_cpp_server().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.server") != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("SILA_CPP_SERVER_LOGGING_LEVEL is set to CRITICAL")
        {
            std::cout << '\n' << '\n';
            qputenv("SILA_CPP_SERVER_LOGGING_LEVEL", "CRITICAL");
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.server");

            THEN("the logging level for the sila_cpp.server logging category is "
                 "CRITICAL")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_server)

                REQUIRE_FALSE(sila_cpp_server().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_server().isInfoEnabled());
                REQUIRE_FALSE(sila_cpp_server().isWarningEnabled());
                REQUIRE(sila_cpp_server().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.server") != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("SILA_CPP_SERVER_LOGGING_LEVEL is set to FATAL")
        {
            std::cout << '\n' << '\n';
            qputenv("SILA_CPP_SERVER_LOGGING_LEVEL", "FATAL");
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.server");

            THEN("the logging level for the sila_cpp.server logging category is "
                 "FATAL")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_server)

                REQUIRE_FALSE(sila_cpp_server().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_server().isInfoEnabled());
                REQUIRE_FALSE(sila_cpp_server().isWarningEnabled());
                REQUIRE_FALSE(sila_cpp_server().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.server") != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
    }
}

//============================================================================
SCENARIO("Changing all sila_cpp logging categories' level through "
         "CLogManager::setLoggingLevel",
         "[logging]")
{
    using LoggingLevel = SiLA2::logging::CLogManager::LoggingLevel;

    GIVEN("A clean environment")
    {
        CLEAN_ENV()

        WHEN("the logging level is set to DEBUG")
        {
            SiLA2::logging::CLogManager::setLoggingLevel(LoggingLevel::Debug);

            THEN("the logging level for all sila_cpp logging categories is DEBUG")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                    REQUIRE(Category().isDebugEnabled());
                    REQUIRE(Category().isInfoEnabled());
                    REQUIRE(Category().isWarningEnabled());
                    REQUIRE(Category().isCriticalEnabled());
                }
            }
        }
        AND_WHEN("the logging level is set to INFO")
        {
            SiLA2::logging::CLogManager::setLoggingLevel(LoggingLevel::Info);

            THEN("the logging level for all sila_cpp logging categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                    REQUIRE_FALSE(Category().isDebugEnabled());
                    REQUIRE(Category().isInfoEnabled());
                    REQUIRE(Category().isWarningEnabled());
                    REQUIRE(Category().isCriticalEnabled());
                }
            }
        }
        AND_WHEN("the logging level is set to WARNING")
        {
            SiLA2::logging::CLogManager::setLoggingLevel(LoggingLevel::Warning);

            THEN("the logging level for all sila_cpp logging categories is "
                 "WARNING")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                    REQUIRE_FALSE(Category().isDebugEnabled());
                    REQUIRE_FALSE(Category().isInfoEnabled());
                    REQUIRE(Category().isWarningEnabled());
                    REQUIRE(Category().isCriticalEnabled());
                }
            }
        }
        AND_WHEN("the logging level is set to CRITICAL")
        {
            SiLA2::logging::CLogManager::setLoggingLevel(LoggingLevel::Critical);

            THEN("the logging level for all sila_cpp logging categories is "
                 "CRITICAL")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                    REQUIRE_FALSE(Category().isDebugEnabled());
                    REQUIRE_FALSE(Category().isInfoEnabled());
                    REQUIRE_FALSE(Category().isWarningEnabled());
                    REQUIRE(Category().isCriticalEnabled());
                }
            }
        }
        AND_WHEN("the logging level is set to FATAL")
        {
            SiLA2::logging::CLogManager::setLoggingLevel(LoggingLevel::Fatal);

            THEN("the logging level for all sila_cpp logging categories is FATAL")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                    REQUIRE_FALSE(Category().isDebugEnabled());
                    REQUIRE_FALSE(Category().isInfoEnabled());
                    REQUIRE_FALSE(Category().isWarningEnabled());
                    REQUIRE_FALSE(Category().isCriticalEnabled());
                }
            }
        }
    }
}

//============================================================================
SCENARIO("Modifying sila_cpp.client category's logging level through "
         "CLogManager::setLoggingLevel",
         "[logging]")
{
    using LoggingLevel = SiLA2::logging::CLogManager::LoggingLevel;

    GIVEN("A clean environment")
    {
        CLEAN_ENV()

        WHEN("sila_cpp.client's logging level is set to DEBUG")
        {
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.client",
                                                         LoggingLevel::Debug);

            THEN("the logging level for the sila_cpp.client logging category is "
                 "DEBUG")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_client)

                REQUIRE(sila_cpp_client().isDebugEnabled());
                REQUIRE(sila_cpp_client().isInfoEnabled());
                REQUIRE(sila_cpp_client().isWarningEnabled());
                REQUIRE(sila_cpp_client().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.client") != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("sila_cpp.client's logging level is set to INFO")
        {
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.client",
                                                         LoggingLevel::Info);

            THEN("the logging level for the sila_cpp.client logging category is "
                 "INFO")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_client)

                REQUIRE_FALSE(sila_cpp_client().isDebugEnabled());
                REQUIRE(sila_cpp_client().isInfoEnabled());
                REQUIRE(sila_cpp_client().isWarningEnabled());
                REQUIRE(sila_cpp_client().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.client") != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("sila_cpp.client's logging level is set to WARNING")
        {
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.client",
                                                         LoggingLevel::Warning);

            THEN("the logging level for the sila_cpp.client logging category is "
                 "WARNING")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_client)

                REQUIRE_FALSE(sila_cpp_client().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_client().isInfoEnabled());
                REQUIRE(sila_cpp_client().isWarningEnabled());
                REQUIRE(sila_cpp_client().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.client") != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("sila_cpp.client's logging level is set to CRITICAL")
        {
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.client",
                                                         LoggingLevel::Critical);

            THEN("the logging level for the sila_cpp.client logging category is "
                 "CRITICAL")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_client)

                REQUIRE_FALSE(sila_cpp_client().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_client().isInfoEnabled());
                REQUIRE_FALSE(sila_cpp_client().isWarningEnabled());
                REQUIRE(sila_cpp_client().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.client") != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("sila_cpp.client's logging level is set to FATAL")
        {
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.client",
                                                         LoggingLevel::Fatal);

            THEN("the logging level for the sila_cpp.client logging category is "
                 "FATAL")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_client)

                REQUIRE_FALSE(sila_cpp_client().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_client().isInfoEnabled());
                REQUIRE_FALSE(sila_cpp_client().isWarningEnabled());
                REQUIRE_FALSE(sila_cpp_client().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.client") != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
    }
}

//============================================================================
SCENARIO("Modifying sila_cpp.codegen category's logging level through "
         "CLogManager::setLoggingLevel",
         "[logging]")
{
    using LoggingLevel = SiLA2::logging::CLogManager::LoggingLevel;

    GIVEN("A clean environment")
    {
        CLEAN_ENV()

        WHEN("sila_cpp.codegen's logging level is set to DEBUG")
        {
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.codegen",
                                                         LoggingLevel::Debug);

            THEN("the logging level for the sila_cpp.codegen logging category is "
                 "DEBUG")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_codegen)

                REQUIRE(sila_cpp_codegen().isDebugEnabled());
                REQUIRE(sila_cpp_codegen().isInfoEnabled());
                REQUIRE(sila_cpp_codegen().isWarningEnabled());
                REQUIRE(sila_cpp_codegen().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.codegen")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("sila_cpp.codegen's logging level is set to INFO")
        {
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.codegen",
                                                         LoggingLevel::Info);

            THEN("the logging level for the sila_cpp.codegen logging category is "
                 "INFO")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_codegen)

                REQUIRE_FALSE(sila_cpp_codegen().isDebugEnabled());
                REQUIRE(sila_cpp_codegen().isInfoEnabled());
                REQUIRE(sila_cpp_codegen().isWarningEnabled());
                REQUIRE(sila_cpp_codegen().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.codegen")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("sila_cpp.codegen's logging level is set to WARNING")
        {
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.codegen",
                                                         LoggingLevel::Warning);

            THEN("the logging level for the sila_cpp.codegen logging category is "
                 "WARNING")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_codegen)

                REQUIRE_FALSE(sila_cpp_codegen().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_codegen().isInfoEnabled());
                REQUIRE(sila_cpp_codegen().isWarningEnabled());
                REQUIRE(sila_cpp_codegen().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.codegen")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("sila_cpp.codegen's logging level is set to CRITICAL")
        {
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.codegen",
                                                         LoggingLevel::Critical);

            THEN("the logging level for the sila_cpp.codegen logging category is "
                 "CRITICAL")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_codegen)

                REQUIRE_FALSE(sila_cpp_codegen().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_codegen().isInfoEnabled());
                REQUIRE_FALSE(sila_cpp_codegen().isWarningEnabled());
                REQUIRE(sila_cpp_codegen().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.codegen")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("sila_cpp.codegen's logging level is set to FATAL")
        {
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.codegen",
                                                         LoggingLevel::Fatal);

            THEN("the logging level for the sila_cpp.codegen logging category is "
                 "FATAL")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_codegen)

                REQUIRE_FALSE(sila_cpp_codegen().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_codegen().isInfoEnabled());
                REQUIRE_FALSE(sila_cpp_codegen().isWarningEnabled());
                REQUIRE_FALSE(sila_cpp_codegen().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.codegen")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
    }
}

//============================================================================
SCENARIO("Modifying sila_cpp.common category's logging level through "
         "CLogManager::setLoggingLevel",
         "[logging]")
{
    using LoggingLevel = SiLA2::logging::CLogManager::LoggingLevel;

    GIVEN("A clean environment")
    {
        CLEAN_ENV()

        WHEN("sila_cpp.common's logging level is set to DEBUG")
        {
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.common",
                                                         LoggingLevel::Debug);

            THEN("the logging level for the sila_cpp.common logging category is "
                 "DEBUG")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_common)

                REQUIRE(sila_cpp_common().isDebugEnabled());
                REQUIRE(sila_cpp_common().isInfoEnabled());
                REQUIRE(sila_cpp_common().isWarningEnabled());
                REQUIRE(sila_cpp_common().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.common") != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("sila_cpp.common's logging level is set to INFO")
        {
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.common",
                                                         LoggingLevel::Info);

            THEN("the logging level for the sila_cpp.common logging category is "
                 "INFO")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_common)

                REQUIRE_FALSE(sila_cpp_common().isDebugEnabled());
                REQUIRE(sila_cpp_common().isInfoEnabled());
                REQUIRE(sila_cpp_common().isWarningEnabled());
                REQUIRE(sila_cpp_common().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.common") != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("sila_cpp.common's logging level is set to WARNING")
        {
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.common",
                                                         LoggingLevel::Warning);

            THEN("the logging level for the sila_cpp.common logging category is "
                 "WARNING")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_common)

                REQUIRE_FALSE(sila_cpp_common().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_common().isInfoEnabled());
                REQUIRE(sila_cpp_common().isWarningEnabled());
                REQUIRE(sila_cpp_common().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.common") != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("sila_cpp.common's logging level is set to CRITICAL")
        {
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.common",
                                                         LoggingLevel::Critical);

            THEN("the logging level for the sila_cpp.common logging category is "
                 "CRITICAL")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_common)

                REQUIRE_FALSE(sila_cpp_common().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_common().isInfoEnabled());
                REQUIRE_FALSE(sila_cpp_common().isWarningEnabled());
                REQUIRE(sila_cpp_common().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.common") != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("sila_cpp.common's logging level is set to FATAL")
        {
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.common",
                                                         LoggingLevel::Fatal);

            THEN("the logging level for the sila_cpp.common logging category is "
                 "FATAL")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_common)

                REQUIRE_FALSE(sila_cpp_common().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_common().isInfoEnabled());
                REQUIRE_FALSE(sila_cpp_common().isWarningEnabled());
                REQUIRE_FALSE(sila_cpp_common().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.common") != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
    }
}

//============================================================================
SCENARIO("Modifying sila_cpp.discovery category's logging level through "
         "CLogManager::setLoggingLevel",
         "[logging]")
{
    using LoggingLevel = SiLA2::logging::CLogManager::LoggingLevel;

    GIVEN("A clean environment")
    {
        CLEAN_ENV()

        WHEN("sila_cpp.discovery's logging level is set to DEBUG")
        {
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.discovery",
                                                         LoggingLevel::Debug);

            THEN("the logging level for the sila_cpp.discovery logging category "
                 "is DEBUG")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_discovery)

                REQUIRE(sila_cpp_discovery().isDebugEnabled());
                REQUIRE(sila_cpp_discovery().isInfoEnabled());
                REQUIRE(sila_cpp_discovery().isWarningEnabled());
                REQUIRE(sila_cpp_discovery().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.discovery")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("sila_cpp.discovery's logging level is set to INFO")
        {
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.discovery",
                                                         LoggingLevel::Info);

            THEN("the logging level for the sila_cpp.discovery logging category "
                 "is INFO")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_discovery)

                REQUIRE_FALSE(sila_cpp_discovery().isDebugEnabled());
                REQUIRE(sila_cpp_discovery().isInfoEnabled());
                REQUIRE(sila_cpp_discovery().isWarningEnabled());
                REQUIRE(sila_cpp_discovery().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.discovery")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("sila_cpp.discovery's logging level is set to WARNING")
        {
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.discovery",
                                                         LoggingLevel::Warning);

            THEN("the logging level for the sila_cpp.discovery logging category "
                 "is WARNING")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_discovery)

                REQUIRE_FALSE(sila_cpp_discovery().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_discovery().isInfoEnabled());
                REQUIRE(sila_cpp_discovery().isWarningEnabled());
                REQUIRE(sila_cpp_discovery().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.discovery")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("sila_cpp.discovery's logging level is set to CRITICAL")
        {
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.discovery",
                                                         LoggingLevel::Critical);

            THEN("the logging level for the sila_cpp.discovery logging category "
                 "is CRITICAL")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_discovery)

                REQUIRE_FALSE(sila_cpp_discovery().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_discovery().isInfoEnabled());
                REQUIRE_FALSE(sila_cpp_discovery().isWarningEnabled());
                REQUIRE(sila_cpp_discovery().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.discovery")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("sila_cpp.discovery's logging level is set to FATAL")
        {
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.discovery",
                                                         LoggingLevel::Fatal);

            THEN("the logging level for the sila_cpp.discovery logging category "
                 "is FATAL")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_discovery)

                REQUIRE_FALSE(sila_cpp_discovery().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_discovery().isInfoEnabled());
                REQUIRE_FALSE(sila_cpp_discovery().isWarningEnabled());
                REQUIRE_FALSE(sila_cpp_discovery().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.discovery")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
    }
}

//============================================================================
SCENARIO("Modifying sila_cpp.framework.data_types category's logging level  "
         "through CLogManager::setLoggingLevel",
         "[logging]")
{
    using LoggingLevel = SiLA2::logging::CLogManager::LoggingLevel;

    GIVEN("A clean environment")
    {
        CLEAN_ENV()

        WHEN("sila_cpp.framework.data_types' logging level is set to DEBUG")
        {
            SiLA2::logging::CLogManager::setLoggingLevel(
                "sila_cpp.framework.data_types", LoggingLevel::Debug);

            THEN("the logging level for the sila_cpp.framework.data_types "
                 "logging category is DEBUG")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_data_types)

                REQUIRE(sila_cpp_data_types().isDebugEnabled());
                REQUIRE(sila_cpp_data_types().isInfoEnabled());
                REQUIRE(sila_cpp_data_types().isWarningEnabled());
                REQUIRE(sila_cpp_data_types().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.framework."
                                                          "data_types")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("sila_cpp.framework.data_types' logging level is set to INFO")
        {
            SiLA2::logging::CLogManager::setLoggingLevel(
                "sila_cpp.framework.data_types", LoggingLevel::Info);

            THEN("the logging level for the sila_cpp.framework.data_types "
                 "logging category is INFO")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_data_types)

                REQUIRE_FALSE(sila_cpp_data_types().isDebugEnabled());
                REQUIRE(sila_cpp_data_types().isInfoEnabled());
                REQUIRE(sila_cpp_data_types().isWarningEnabled());
                REQUIRE(sila_cpp_data_types().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.framework."
                                                          "data_types")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("sila_cpp.framework.data_types' logging level is set to WARNING")
        {
            SiLA2::logging::CLogManager::setLoggingLevel(
                "sila_cpp.framework.data_types", LoggingLevel::Warning);

            THEN("the logging level for the sila_cpp.framework.data_types "
                 "logging category is WARNING")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_data_types)

                REQUIRE_FALSE(sila_cpp_data_types().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_data_types().isInfoEnabled());
                REQUIRE(sila_cpp_data_types().isWarningEnabled());
                REQUIRE(sila_cpp_data_types().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.framework."
                                                          "data_types")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("sila_cpp.framework.data_types' logging level is set to "
                 "CRITICAL")
        {
            SiLA2::logging::CLogManager::setLoggingLevel(
                "sila_cpp.framework.data_types", LoggingLevel::Critical);

            THEN("the logging level for the sila_cpp.framework.data_types "
                 "logging category is CRITICAL")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_data_types)

                REQUIRE_FALSE(sila_cpp_data_types().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_data_types().isInfoEnabled());
                REQUIRE_FALSE(sila_cpp_data_types().isWarningEnabled());
                REQUIRE(sila_cpp_data_types().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.framework."
                                                          "data_types")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("sila_cpp.framework.data_types' logging level is set to FATAL")
        {
            SiLA2::logging::CLogManager::setLoggingLevel(
                "sila_cpp.framework.data_types", LoggingLevel::Fatal);

            THEN("the logging level for the sila_cpp.framework.data_types "
                 "logging category is FATAL")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_data_types)

                REQUIRE_FALSE(sila_cpp_data_types().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_data_types().isInfoEnabled());
                REQUIRE_FALSE(sila_cpp_data_types().isWarningEnabled());
                REQUIRE_FALSE(sila_cpp_data_types().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.framework."
                                                          "data_types")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
    }
}

//============================================================================
SCENARIO("Modifying sila_cpp.framework.errors category's logging level through "
         "CLogManager::setLoggingLevel",
         "[logging]")
{
    using LoggingLevel = SiLA2::logging::CLogManager::LoggingLevel;

    GIVEN("A clean environment")
    {
        CLEAN_ENV()

        WHEN("sila_cpp.framework.errors' logging level is set to DEBUG")
        {
            SiLA2::logging::CLogManager::setLoggingLevel(
                "sila_cpp.framework.errors", LoggingLevel::Debug);

            THEN("the logging level for the sila_cpp.framework.errors logging "
                 "category is DEBUG")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_errors)

                REQUIRE(sila_cpp_errors().isDebugEnabled());
                REQUIRE(sila_cpp_errors().isInfoEnabled());
                REQUIRE(sila_cpp_errors().isWarningEnabled());
                REQUIRE(sila_cpp_errors().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.framework."
                                                          "errors")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("sila_cpp.framework.errors' logging level is set to INFO")
        {
            SiLA2::logging::CLogManager::setLoggingLevel(
                "sila_cpp.framework.errors", LoggingLevel::Info);

            THEN("the logging level for the sila_cpp.framework.errors logging "
                 "category is INFO")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_errors)

                REQUIRE_FALSE(sila_cpp_errors().isDebugEnabled());
                REQUIRE(sila_cpp_errors().isInfoEnabled());
                REQUIRE(sila_cpp_errors().isWarningEnabled());
                REQUIRE(sila_cpp_errors().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.framework."
                                                          "errors")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("sila_cpp.framework.errors' logging level is set to WARNING")
        {
            SiLA2::logging::CLogManager::setLoggingLevel(
                "sila_cpp.framework.errors", LoggingLevel::Warning);

            THEN("the logging level for the sila_cpp.framework.errors logging "
                 "category is WARNING")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_errors)

                REQUIRE_FALSE(sila_cpp_errors().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_errors().isInfoEnabled());
                REQUIRE(sila_cpp_errors().isWarningEnabled());
                REQUIRE(sila_cpp_errors().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.framework."
                                                          "errors")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("sila_cpp.framework.errors' logging level is set to CRITICAL")
        {
            SiLA2::logging::CLogManager::setLoggingLevel(
                "sila_cpp.framework.errors", LoggingLevel::Critical);

            THEN("the logging level for the sila_cpp.framework.errors logging "
                 "category is CRITICAL")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_errors)

                REQUIRE_FALSE(sila_cpp_errors().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_errors().isInfoEnabled());
                REQUIRE_FALSE(sila_cpp_errors().isWarningEnabled());
                REQUIRE(sila_cpp_errors().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.framework."
                                                          "errors")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("sila_cpp.framework.errors' logging level is set to FATAL")
        {
            SiLA2::logging::CLogManager::setLoggingLevel(
                "sila_cpp.framework.errors", LoggingLevel::Fatal);

            THEN("the logging level for the sila_cpp.framework.errors logging "
                 "category is FATAL")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_errors)

                REQUIRE_FALSE(sila_cpp_errors().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_errors().isInfoEnabled());
                REQUIRE_FALSE(sila_cpp_errors().isWarningEnabled());
                REQUIRE_FALSE(sila_cpp_errors().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.framework."
                                                          "errors")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
    }
}

//============================================================================
SCENARIO("Modifying sila_cpp.internal category's logging level through "
         "CLogManager::setLoggingLevel",
         "[logging]")
{
    using LoggingLevel = SiLA2::logging::CLogManager::LoggingLevel;

    GIVEN("A clean environment")
    {
        CLEAN_ENV()

        WHEN("sila_cpp.internal's logging level is set to DEBUG")
        {
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.internal",
                                                         LoggingLevel::Debug);

            THEN("the logging level for the sila_cpp.internal logging category "
                 "is DEBUG")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_internal)

                REQUIRE(sila_cpp_internal().isDebugEnabled());
                REQUIRE(sila_cpp_internal().isInfoEnabled());
                REQUIRE(sila_cpp_internal().isWarningEnabled());
                REQUIRE(sila_cpp_internal().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.internal")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("sila_cpp.internal's logging level is set to INFO")
        {
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.internal",
                                                         LoggingLevel::Info);

            THEN("the logging level for the sila_cpp.internal logging category "
                 "is INFO")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_internal)

                REQUIRE_FALSE(sila_cpp_internal().isDebugEnabled());
                REQUIRE(sila_cpp_internal().isInfoEnabled());
                REQUIRE(sila_cpp_internal().isWarningEnabled());
                REQUIRE(sila_cpp_internal().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.internal")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("sila_cpp.internal's logging level is set to WARNING")
        {
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.internal",
                                                         LoggingLevel::Warning);

            THEN("the logging level for the sila_cpp.internal logging category "
                 "is WARNING")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_internal)

                REQUIRE_FALSE(sila_cpp_internal().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_internal().isInfoEnabled());
                REQUIRE(sila_cpp_internal().isWarningEnabled());
                REQUIRE(sila_cpp_internal().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.internal")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("sila_cpp.internal's logging level is set to CRITICAL")
        {
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.internal",
                                                         LoggingLevel::Critical);

            THEN("the logging level for the sila_cpp.internal logging category "
                 "is CRITICAL")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_internal)

                REQUIRE_FALSE(sila_cpp_internal().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_internal().isInfoEnabled());
                REQUIRE_FALSE(sila_cpp_internal().isWarningEnabled());
                REQUIRE(sila_cpp_internal().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.internal")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("sila_cpp.internal's logging level is set to FATAL")
        {
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.internal",
                                                         LoggingLevel::Fatal);

            THEN("the logging level for the sila_cpp.internal logging category "
                 "is FATAL")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_internal)

                REQUIRE_FALSE(sila_cpp_internal().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_internal().isInfoEnabled());
                REQUIRE_FALSE(sila_cpp_internal().isWarningEnabled());
                REQUIRE_FALSE(sila_cpp_internal().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.internal")
                        != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
    }
}

//============================================================================
SCENARIO("Modifying sila_cpp.server category's logging level through "
         "CLogManager::setLoggingLevel",
         "[logging]")
{
    using LoggingLevel = SiLA2::logging::CLogManager::LoggingLevel;

    GIVEN("A clean environment")
    {
        CLEAN_ENV()

        WHEN("sila_cpp.server's logging level is set to DEBUG")
        {
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.server",
                                                         LoggingLevel::Debug);

            THEN("the logging level for the sila_cpp.server logging category is "
                 "DEBUG")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_server)

                REQUIRE(sila_cpp_server().isDebugEnabled());
                REQUIRE(sila_cpp_server().isInfoEnabled());
                REQUIRE(sila_cpp_server().isWarningEnabled());
                REQUIRE(sila_cpp_server().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.server") != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("sila_cpp.server's logging level is set to INFO")
        {
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.server",
                                                         LoggingLevel::Info);

            THEN("the logging level for the sila_cpp.server logging category is "
                 "INFO")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_server)

                REQUIRE_FALSE(sila_cpp_server().isDebugEnabled());
                REQUIRE(sila_cpp_server().isInfoEnabled());
                REQUIRE(sila_cpp_server().isWarningEnabled());
                REQUIRE(sila_cpp_server().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.server") != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("sila_cpp.server's logging level is set to WARNING")
        {
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.server",
                                                         LoggingLevel::Warning);

            THEN("the logging level for the sila_cpp.server logging category is "
                 "WARNING")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_server)

                REQUIRE_FALSE(sila_cpp_server().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_server().isInfoEnabled());
                REQUIRE(sila_cpp_server().isWarningEnabled());
                REQUIRE(sila_cpp_server().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.server") != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("sila_cpp.server's logging level is set to CRITICAL")
        {
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.server",
                                                         LoggingLevel::Critical);

            THEN("the logging level for the sila_cpp.server logging category is "
                 "CRITICAL")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_server)

                REQUIRE_FALSE(sila_cpp_server().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_server().isInfoEnabled());
                REQUIRE_FALSE(sila_cpp_server().isWarningEnabled());
                REQUIRE(sila_cpp_server().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.server") != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
        AND_WHEN("sila_cpp.server's logging level is set to FATAL")
        {
            SiLA2::logging::CLogManager::setLoggingLevel("sila_cpp.server",
                                                         LoggingLevel::Fatal);

            THEN("the logging level for the sila_cpp.server logging category is "
                 "FATAL")
            {
                DEBUG_CATEGORY_LOGGING_LEVELS(sila_cpp_server)

                REQUIRE_FALSE(sila_cpp_server().isDebugEnabled());
                REQUIRE_FALSE(sila_cpp_server().isInfoEnabled());
                REQUIRE_FALSE(sila_cpp_server().isWarningEnabled());
                REQUIRE_FALSE(sila_cpp_server().isCriticalEnabled());
            }
            AND_THEN("the logging level for all other sila_cpp logging "
                     "categories is INFO")
            {
                for (const auto& Category : LOGGING_CATEGORIES)
                {
                    if (strcmp(Category().categoryName(), "sila_cpp.server") != 0)
                    {
                        DEBUG_CATEGORY_LOGGING_LEVELS(Category)

                        REQUIRE_FALSE(Category().isDebugEnabled());
                        REQUIRE(Category().isInfoEnabled());
                        REQUIRE(Category().isWarningEnabled());
                        REQUIRE(Category().isCriticalEnabled());
                    }
                }
            }
        }
    }
}
