cmake_minimum_required(VERSION 3.13)

project(InteroperabilityTest VERSION 0.0.1 LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_FLAGS_DEBUG "-g -O1")
set(CMAKE_CXX_FLAGS_RELEASE "-O3")

# Find sila_cpp library
if(NOT TARGET sila_cpp)
    # if we're building standalone
    find_package(sila_cpp CONFIG REQUIRED)
else()
    # if we're building as subproject
    include(${SILA_CPP_SOURCE_DIR}/cmake/sila_cppConfig.cmake)
    set(PROTOBUF_IMPORT_DIRS ${SILA_CPP_SOURCE_DIR}/src/lib/sila_base/protobuf)
endif()

set(HELPERS
    DataTypeProvider/AllDatatypeStructure.h
    )

set(PROTOS
    meta/DataTypeProvider.proto
    )

add_library(${PROJECT_NAME}Features
    ${HELPERS}
    )

set_target_properties(${PROJECT_NAME}Features PROPERTIES DEBUG_POSTFIX "d")

target_protoc_generate_cpp(${PROJECT_NAME}Features ${PROTOS})


target_link_libraries(${PROJECT_NAME}Features
    sila2::sila_cpp
    )

# Server and Client
foreach(_target
    ${PROJECT_NAME}Server ${PROJECT_NAME}Client)
    # a lib containing the implementation
    add_library(${_target} ${_target}.h ${_target}.cpp)
    set_target_properties(${_target} PROPERTIES DEBUG_POSTFIX "d")
    target_link_libraries(${_target} ${PROJECT_NAME}Features)
    # an executable as a standalone command line application
    add_executable(${_target}-bin ${_target}-bin.cpp)
    target_link_libraries(${_target}-bin ${_target})
    set_target_properties(${_target}-bin PROPERTIES OUTPUT_NAME ${_target})
    # copy the libs to the directory of the executable so that it can find them
    add_custom_command(TARGET ${_target} POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy
        $<TARGET_FILE:sila2::sila_cpp>
        $<TARGET_FILE:sila2::sila_cpp_common>
        $<TARGET_FILE:sila2::sila_cpp_codegen>
        $<TARGET_FILE:QtZeroConf>
        $<TARGET_FILE_DIR:${_target}>
        )
endforeach()
# The server need the FDL files which are compiled into the binary using the resource file
target_sources(${PROJECT_NAME}Server PRIVATE ${PROJECT_NAME}.qrc)


# Unit test
# an object library that the other unit tests application can link to
add_library(${PROJECT_NAME} OBJECT
    test_globals.h
    test_DataTypeProvider.cpp
    )
target_link_libraries(${PROJECT_NAME}
    Catch2::Catch2
    ${PROJECT_NAME}Client
    )
# an executable as a standalone unit test application for the interoperability tests
add_executable(${PROJECT_NAME}-bin
    test_interoperability_main.cpp
    )
target_link_libraries(${PROJECT_NAME}-bin
    Catch2::Catch2
    ${PROJECT_NAME}
    ${PROJECT_NAME}Server
    )
set_target_properties(${PROJECT_NAME}-bin PROPERTIES OUTPUT_NAME ${PROJECT_NAME})
