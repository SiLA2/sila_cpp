/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   test_fdl.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   26.01.2021
/// \brief  Unit tests for the FDL serialization implementation
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/fdl/ConstraintAllowedTypes.h>
#include <sila_cpp/codegen/fdl/ConstraintContentType.h>
#include <sila_cpp/codegen/fdl/ConstraintElementCount.h>
#include <sila_cpp/codegen/fdl/ConstraintExclusive.h>
#include <sila_cpp/codegen/fdl/ConstraintFullyQualifiedID.h>
#include <sila_cpp/codegen/fdl/ConstraintInclusive.h>
#include <sila_cpp/codegen/fdl/ConstraintLength.h>
#include <sila_cpp/codegen/fdl/ConstraintPattern.h>
#include <sila_cpp/codegen/fdl/ConstraintSchema.h>
#include <sila_cpp/codegen/fdl/ConstraintSet.h>
#include <sila_cpp/codegen/fdl/ConstraintUnit.h>
#include <sila_cpp/codegen/fdl/DataTypeBasic.h>
#include <sila_cpp/codegen/fdl/DataTypeConstrained.h>
#include <sila_cpp/codegen/fdl/DataTypeIdentifier.h>
#include <sila_cpp/codegen/fdl/DataTypeList.h>
#include <sila_cpp/codegen/fdl/DataTypeStructure.h>
#include <sila_cpp/codegen/fdl/FDLSerializer.h>
#include <sila_cpp/codegen/fdl/Feature.h>
#include <sila_cpp/common/logging.h>
#include <sila_cpp/data_types.h>

#include <QFile>

#include <catch2/catch.hpp>

using namespace SiLA2::codegen::fdl;

//============================================================================
SCENARIO("Deserialize an empty Feature Definition", "[fdl][emtpy]")
{
    QLoggingCategory::setFilterRules("*.debug=true\n*.info=true");
    GIVEN("An empty Feature Definition read from a file")
    {
        auto FDLFile = QFile{":/fdl/Empty.sila.xml"};
        REQUIRE(FDLFile.open(QFile::ReadOnly | QFile::Text));
        const auto Switcher = GENERATE(1, 2);
        const auto FDLFileContent = [&Switcher, &FDLFile]() -> QString {
            if (Switcher == 1)
            {
                SUCCEED("Testing deserialization");
                return FDLFile.readAll();
            }
            else
            {
                SUCCEED("Testing serialization");
                // (since deserialization is guaranteed to work we can test if the
                // serialization works by deserializing the resulting FDL again
                // and checking it)
                CFeature TempFeature;
                REQUIRE(
                    CFDLSerializer::deserialize(TempFeature, FDLFile.readAll()));
                return CFDLSerializer::serialize(TempFeature);
            }
        }();

        WHEN("this Feature Definition is deserialized into a CFeature object")
        {
            CFeature Feature;
            REQUIRE(CFDLSerializer::deserialize(Feature, FDLFileContent));
            THEN("the deserialized CFeature object has the correct Identifier, "
                 "DisplayName, Description, Originator, Category, Ferature "
                 "Version and SiLA 2 Version")
            {
                REQUIRE(Feature.identifier() == "Empty");
                REQUIRE(Feature.displayName() == "Empty");
                REQUIRE(Feature.description() == "An empty Feature");
                REQUIRE(Feature.originator() == "org.silastandard");
                REQUIRE(Feature.category() == "test");
                REQUIRE(Feature.featureVersion() == "1.0");
                REQUIRE(Feature.sila2Version() == "1.0");
            }
            AND_THEN("this CFeature object is equal to itself")
            {
                REQUIRE(Feature == Feature);
            }

            AND_GIVEN("an inline Feature Description of the same Feature but "
                      "without DisplayName and Description")
            {
                const auto* const FDLContent =
                    R"(<?xml version="1.0" encoding="utf-8" ?>
                       <Feature SiLA2Version="1.0" FeatureVersion="1.0"
                                Originator="org.silastandard" Category="test">
                           <Identifier>Empty</Identifier>
                           <DisplayName/>
                           <Description>/>
                       </Feature>)";
                WHEN("this Feature Definition is deserialized into a CFeature "
                     "object")
                {
                    CFeature Feature2;
                    REQUIRE(CFDLSerializer::deserialize(Feature2, FDLContent));
                    THEN("this deserialized CFeature object is equal to the "
                         "previous deserialized CFeature object")
                    {
                        REQUIRE(Feature == Feature2);
                    }
                }
            }
        }
    }
}

//============================================================================
SCENARIO("Deserialize a Feature Definition with one Unobservable Command",
         "[fdl][command][unobs]")
{
    QLoggingCategory::setFilterRules("*.debug=true\n*.info=true");
    GIVEN("A Feature Definition read from a file")
    {
        auto FDLFile = QFile{":/fdl/UnobservableCommand.sila.xml"};
        REQUIRE(FDLFile.open(QFile::ReadOnly | QFile::Text));
        const auto Switcher = GENERATE(1, 2);
        const auto FDLFileContent = [&Switcher, &FDLFile]() -> QString {
            if (Switcher == 1)
            {
                SUCCEED("Testing deserialization");
                return FDLFile.readAll();
            }
            else
            {
                SUCCEED("Testing serialization");
                // (since deserialization is guaranteed to work we can test if the
                // serialization works by deserializing the resulting FDL again
                // and checking it)
                CFeature TempFeature;
                REQUIRE(
                    CFDLSerializer::deserialize(TempFeature, FDLFile.readAll()));
                return CFDLSerializer::serialize(TempFeature);
            }
        }();

        WHEN("this Feature Definition is deserialized into a CFeature object")
        {
            CFeature Feature;
            REQUIRE(CFDLSerializer::deserialize(Feature, FDLFileContent));
            THEN("the deserialized CFeature object has exactly one Command "
                 "object")
            {
                const auto Commands = Feature.commands();
                REQUIRE(Commands.size() == 1);

                const auto& Command = Commands.front();
                AND_THEN("the Command object has the correct Identifier, "
                         "DisplayName and Description")
                {
                    REQUIRE(Command.identifier() == "UnobservableCommand");
                    REQUIRE(Command.displayName() == "Unobservable Command");
                    REQUIRE(Command.description()
                            == "A simple Unobservable Command with no Parameters "
                               "and no Responses");
                }
                AND_THEN("the Command is Unobservable")
                {
                    REQUIRE_FALSE(Command.isObservable());
                }
                AND_THEN("the Command is equal to itself")
                {
                    REQUIRE(Command == Command);
                }
            }
            AND_THEN("this CFeature object is equal to itself")
            {
                REQUIRE(Feature == Feature);
            }
        }
    }
}

//============================================================================
SCENARIO("Deserialize a Feature Definition with multiple Unobservable Commands",
         "[fdl][command][unobs]")
{
    QLoggingCategory::setFilterRules("*.debug=true\n*.info=true");
    GIVEN("A Feature Definition read from a file")
    {
        auto FDLFile = QFile{":/fdl/UnobservableCommands.sila.xml"};
        REQUIRE(FDLFile.open(QFile::ReadOnly | QFile::Text));
        const auto Switcher = GENERATE(1, 2);
        const auto FDLFileContent = [&Switcher, &FDLFile]() -> QString {
            if (Switcher == 1)
            {
                SUCCEED("Testing deserialization");
                return FDLFile.readAll();
            }
            else
            {
                SUCCEED("Testing serialization");
                // (since deserialization is guaranteed to work we can test if the
                // serialization works by deserializing the resulting FDL again
                // and checking it)
                CFeature TempFeature;
                REQUIRE(
                    CFDLSerializer::deserialize(TempFeature, FDLFile.readAll()));
                return CFDLSerializer::serialize(TempFeature);
            }
        }();

        WHEN("this Feature Definition is deserialized into a CFeature object")
        {
            CFeature Feature;
            REQUIRE(CFDLSerializer::deserialize(Feature, FDLFileContent));
            THEN("the deserialized CFeature object has multiple Command objects")
            {
                const auto Commands = Feature.commands();
                REQUIRE(Commands.size() == 6);

                AND_THEN("all Commands are Unobservable")
                {
                    const auto& Command = GENERATE_REF(
                        from_range(std::begin(Commands), std::end(Commands)));
                    REQUIRE_FALSE(Command.isObservable());
                    AND_THEN("the Commands are equal to themselves")
                    {
                        REQUIRE(Command == Command);
                    }
                }

                AND_THEN("the first Command object has the correct Identifier, "
                         "DisplayName and Description")
                {
                    const auto& Command = Commands.at(0);
                    REQUIRE(Command.identifier() == "UnobservableCommand");
                    REQUIRE(Command.displayName() == "Unobservable Command");
                    REQUIRE(Command.description()
                            == "A simple Unobservable Command with no Parameters "
                               "and no Responses");
                    REQUIRE(Command.parameters().empty());
                    REQUIRE(Command.responses().empty());
                    REQUIRE(Command.errors().empty());
                }
                AND_THEN("the second Command object has the correct Identifier, "
                         "DisplayName, Description and Parameters")
                {
                    const auto& Command = Commands.at(1);
                    REQUIRE(Command.identifier()
                            == "UnobservableCommandWithParameter");
                    REQUIRE(Command.displayName()
                            == "Unobservable Command With Parameter");
                    REQUIRE(Command.description()
                            == "A simple Unobservable Command with one Parameter "
                               "and no Responses");

                    REQUIRE(Command.responses().empty());
                    REQUIRE(Command.parameters().size() == 1);
                    const auto Param1 = Command.parameters().at(0);
                    REQUIRE(Param1.identifier() == "Parameter1");
                    REQUIRE(Param1.dataType().type() == IDataType::Type::Basic);
                    REQUIRE(Param1.dataType().basic().identifier() == "String");
                    REQUIRE(Command.errors().empty());
                    AND_THEN("the Command Parameter is equal to itself")
                    {
                        REQUIRE(Param1 == Param1);
                    }
                }
                AND_THEN("the third Command object has the correct Identifier, "
                         "DisplayName, Description and Parameters")
                {
                    const auto& Command = Commands.at(2);
                    REQUIRE(Command.identifier()
                            == "UnobservableCommandWithParameters");
                    REQUIRE(Command.displayName()
                            == "Unobservable Command With Parameters");
                    REQUIRE(Command.description()
                            == "A simple Unobservable Command with multiple "
                               "Parameters and no Responses");

                    REQUIRE(Command.responses().empty());
                    REQUIRE(Command.parameters().size() == 2);
                    const auto Param1 = Command.parameters().at(0);
                    REQUIRE(Param1.identifier() == "Parameter1");
                    REQUIRE(Param1.dataType().type() == IDataType::Type::Basic);
                    REQUIRE(Param1.dataType().basic().identifier() == "String");
                    const auto Param2 = Commands.at(2).parameters().at(1);
                    REQUIRE(Param2.identifier() == "Parameter2");
                    REQUIRE(Param2.dataType().type() == IDataType::Type::Basic);
                    REQUIRE(Param2.dataType().basic().identifier() == "Integer");
                    AND_THEN("the Command Parameters are equal to themselves")
                    {
                        REQUIRE(Param1 == Param1);
                        REQUIRE(Param2 == Param2);
                    }
                    AND_THEN("the Command Parameters are not equal to each other")
                    {
                        REQUIRE(Param1 != Param2);
                    }
                }
                AND_THEN("the fourth Command object has the correct Identifier, "
                         "DisplayName, Description and Responses")
                {
                    const auto& Command = Commands.at(3);
                    REQUIRE(Command.identifier()
                            == "UnobservableCommandWithResponse");
                    REQUIRE(Command.displayName()
                            == "Unobservable Command With Response");
                    REQUIRE(Command.description()
                            == "A simple Unobservable Command with no Parameters "
                               "and one Response");

                    REQUIRE(Command.parameters().empty());
                    REQUIRE(Command.responses().size() == 1);
                    const auto Resp1 = Command.responses().at(0);
                    REQUIRE(Resp1.identifier() == "Response1");
                    REQUIRE(Resp1.dataType().type() == IDataType::Type::Basic);
                    REQUIRE(Resp1.dataType().basic().identifier() == "String");
                    AND_THEN("the Command Response is equal to itself")
                    {
                        REQUIRE(Resp1 == Resp1);
                    }
                }
                AND_THEN("the fifth Command object has the correct Identifier, "
                         "DisplayName, Description and Responses")
                {
                    const auto& Command = Commands.at(4);
                    REQUIRE(Command.identifier()
                            == "UnobservableCommandWithResponses");
                    REQUIRE(Command.displayName()
                            == "Unobservable Command With Responses");
                    REQUIRE(Command.description()
                            == "A simple Unobservable Command with no Parameters "
                               "and multiple Responses");

                    REQUIRE(Command.parameters().empty());
                    REQUIRE(Command.responses().size() == 2);
                    const auto Resp1 = Command.responses().at(0);
                    REQUIRE(Resp1.identifier() == "Response1");
                    REQUIRE(Resp1.dataType().type() == IDataType::Type::Basic);
                    REQUIRE(Resp1.dataType().basic().identifier() == "String");
                    const auto Resp2 = Command.responses().at(1);
                    REQUIRE(Resp2.identifier() == "Response2");
                    REQUIRE(Resp2.dataType().type() == IDataType::Type::Basic);
                    REQUIRE(Resp2.dataType().basic().identifier() == "Integer");
                    REQUIRE(Command.errors().empty());
                    AND_THEN("the Command Responses are equal to themselves")
                    {
                        REQUIRE(Resp1 == Resp1);
                        REQUIRE(Resp2 == Resp2);
                    }
                    AND_THEN("the Command Responses are not equal to each other")
                    {
                        REQUIRE(Resp1 != Resp2);
                    }
                }
                AND_THEN("the sixth Command object has the correct Identifier, "
                         "DisplayName, Description and Errors")
                {
                    const auto& Command = Commands.at(5);
                    REQUIRE(Command.identifier()
                            == "UnobservableCommandWithErrors");
                    REQUIRE(Command.displayName()
                            == "Unobservable Command With Errors");
                    REQUIRE(Command.description()
                            == "A simple Unobservable Command with Errors");

                    REQUIRE(Command.parameters().empty());
                    REQUIRE(Command.responses().empty());
                    REQUIRE(Command.errors().size() == 2);
                    REQUIRE(Command.error(0).identifier() == "Error");
                    REQUIRE(Command.error(1).identifier() == "Error2");
                }
            }
            AND_THEN("this CFeature object is equal to itself")
            {
                REQUIRE(Feature == Feature);
            }
        }
    }
}

//============================================================================
SCENARIO("Deserialize a Feature Definition with one Observable Command",
         "[fdl][command][unobs]")
{
    QLoggingCategory::setFilterRules("*.debug=true\n*.info=true");
    GIVEN("A Feature Definition read from a file")
    {
        auto FDLFile = QFile{":/fdl/ObservableCommand.sila.xml"};
        REQUIRE(FDLFile.open(QFile::ReadOnly | QFile::Text));
        const auto Switcher = GENERATE(1, 2);
        const auto FDLFileContent = [&Switcher, &FDLFile]() -> QString {
            if (Switcher == 1)
            {
                SUCCEED("Testing deserialization");
                return FDLFile.readAll();
            }
            else
            {
                SUCCEED("Testing serialization");
                // (since deserialization is guaranteed to work we can test if the
                // serialization works by deserializing the resulting FDL again
                // and checking it)
                CFeature TempFeature;
                REQUIRE(
                    CFDLSerializer::deserialize(TempFeature, FDLFile.readAll()));
                return CFDLSerializer::serialize(TempFeature);
            }
        }();

        WHEN("this Feature Definition is deserialized into a CFeature object")
        {
            CFeature Feature;
            REQUIRE(CFDLSerializer::deserialize(Feature, FDLFileContent));
            THEN("the deserialized CFeature object has exactly one Command "
                 "object")
            {
                const auto Commands = Feature.commands();
                REQUIRE(Commands.size() == 1);

                const auto& Command = Commands.front();
                AND_THEN("the Command object has the correct Identifier, "
                         "DisplayName and Description")
                {
                    REQUIRE(Command.identifier() == "ObservableCommand");
                    REQUIRE(Command.displayName() == "Observable Command");
                    REQUIRE(Command.description()
                            == "A simple Observable Command with no Parameters "
                               "and no Responses");
                }
                AND_THEN("the Command is Observable")
                {
                    REQUIRE(Command.isObservable());
                }
            }
            AND_THEN("this CFeature object is equal to itself")
            {
                REQUIRE(Feature == Feature);
            }
        }
    }
}

//============================================================================
SCENARIO("Deserialize a Feature Definition with multiple Observable Commands",
         "[fdl][command][unobs]")
{
    QLoggingCategory::setFilterRules("*.debug=true\n*.info=true");
    GIVEN("A Feature Definition read from a file")
    {
        auto FDLFile = QFile{":/fdl/ObservableCommands.sila.xml"};
        REQUIRE(FDLFile.open(QFile::ReadOnly | QFile::Text));
        const auto Switcher = GENERATE(1, 2);
        const auto FDLFileContent = [&Switcher, &FDLFile]() -> QString {
            if (Switcher == 1)
            {
                SUCCEED("Testing deserialization");
                return FDLFile.readAll();
            }
            else
            {
                SUCCEED("Testing serialization");
                // (since deserialization is guaranteed to work we can test if the
                // serialization works by deserializing the resulting FDL again
                // and checking it)
                CFeature TempFeature;
                REQUIRE(
                    CFDLSerializer::deserialize(TempFeature, FDLFile.readAll()));
                return CFDLSerializer::serialize(TempFeature);
            }
        }();

        WHEN("this Feature Definition is deserialized into a CFeature object")
        {
            CFeature Feature;
            REQUIRE(CFDLSerializer::deserialize(Feature, FDLFileContent));
            THEN("the deserialized CFeature object has multiple Command objects")
            {
                const auto Commands = Feature.commands();
                REQUIRE(Commands.size() == 7);

                AND_THEN("all Commands are Observable")
                {
                    const auto& Command = GENERATE_REF(
                        from_range(std::begin(Commands), std::end(Commands)));
                    REQUIRE(Command.isObservable());
                    AND_THEN("all Command are equal to themselves")
                    {
                        REQUIRE(Command == Command);
                    }
                }

                AND_THEN("the first Command object has the correct Identifier, "
                         "DisplayName and Description")
                {
                    const auto& Command = Commands.at(0);
                    REQUIRE(Command.identifier() == "ObservableCommand");
                    REQUIRE(Command.displayName() == "Observable Command");
                    REQUIRE(Command.description()
                            == "A simple Observable Command with no Parameters "
                               "and no Responses");
                    REQUIRE(Command.parameters().empty());
                    REQUIRE(Command.responses().empty());
                    REQUIRE(Command.errors().empty());
                }
                AND_THEN("the second Command object has the correct Identifier, "
                         "DisplayName, Description and Parameters")
                {
                    const auto& Command = Commands.at(1);
                    REQUIRE(Command.identifier()
                            == "ObservableCommandWithParameter");
                    REQUIRE(Command.displayName()
                            == "Observable Command With Parameter");
                    REQUIRE(Command.description()
                            == "A simple Observable Command with one Parameter "
                               "and no Responses");

                    REQUIRE(Command.responses().empty());
                    REQUIRE(Command.parameters().size() == 1);
                    const auto Param1 = Command.parameters().at(0);
                    REQUIRE(Param1.identifier() == "Parameter1");
                    REQUIRE(Param1.dataType().type() == IDataType::Type::Basic);
                    REQUIRE(Param1.dataType().basic().identifier() == "String");
                    REQUIRE(Command.errors().empty());
                    AND_THEN("the Command is equal to itself")
                    {
                        REQUIRE(Command == Command);
                    }
                    AND_THEN("the Command Parameter is equal to itself")
                    {
                        REQUIRE(Param1 == Param1);
                    }
                }
                AND_THEN("the third Command object has the correct Identifier, "
                         "DisplayName, Description and Parameters")
                {
                    const auto& Command = Commands.at(2);
                    REQUIRE(Command.identifier()
                            == "ObservableCommandWithParameters");
                    REQUIRE(Command.displayName()
                            == "Observable Command With Parameters");
                    REQUIRE(Command.description()
                            == "A simple Observable Command with multiple "
                               "Parameters and no Responses");

                    REQUIRE(Command.responses().empty());
                    REQUIRE(Command.parameters().size() == 2);
                    const auto Param1 = Command.parameters().at(0);
                    REQUIRE(Param1.identifier() == "Parameter1");
                    REQUIRE(Param1.dataType().type() == IDataType::Type::Basic);
                    REQUIRE(Param1.dataType().basic().identifier() == "String");
                    const auto Param2 = Commands.at(2).parameters().at(1);
                    REQUIRE(Param2.identifier() == "Parameter2");
                    REQUIRE(Param2.dataType().type() == IDataType::Type::Basic);
                    REQUIRE(Param2.dataType().basic().identifier() == "Integer");
                    REQUIRE(Command.errors().empty());
                    AND_THEN("the Command Parameters are equal to themselves")
                    {
                        REQUIRE(Param1 == Param1);
                        REQUIRE(Param2 == Param2);
                    }
                    AND_THEN("the Command Parameters are not equal to each other")
                    {
                        REQUIRE(Param1 != Param2);
                    }
                }
                AND_THEN("the fourth Command object has the correct Identifier, "
                         "DisplayName, Description and Responses")
                {
                    const auto& Command = Commands.at(3);
                    REQUIRE(Command.identifier()
                            == "ObservableCommandWithResponse");
                    REQUIRE(Command.displayName()
                            == "Observable Command With Response");
                    REQUIRE(Command.description()
                            == "A simple Observable Command with no Parameters "
                               "and one Response");

                    REQUIRE(Command.parameters().empty());
                    REQUIRE(Command.responses().size() == 1);
                    const auto Resp1 = Command.responses().at(0);
                    REQUIRE(Resp1.identifier() == "Response1");
                    REQUIRE(Resp1.dataType().type() == IDataType::Type::Basic);
                    REQUIRE(Resp1.dataType().basic().identifier() == "String");
                    REQUIRE(Command.errors().empty());
                    AND_THEN("the Command Response is equal to itself")
                    {
                        REQUIRE(Resp1 == Resp1);
                    }
                }
                AND_THEN("the fifth Command object has the correct Identifier, "
                         "DisplayName, Description and Responses")
                {
                    const auto& Command = Commands.at(4);
                    REQUIRE(Command.identifier()
                            == "ObservableCommandWithResponses");
                    REQUIRE(Command.displayName()
                            == "Observable Command With Responses");
                    REQUIRE(Command.description()
                            == "A simple Observable Command with no Parameters "
                               "and multiple Responses");

                    REQUIRE(Command.parameters().empty());
                    REQUIRE(Command.responses().size() == 2);
                    const auto Resp1 = Command.responses().at(0);
                    REQUIRE(Resp1.identifier() == "Response1");
                    REQUIRE(Resp1.dataType().type() == IDataType::Type::Basic);
                    REQUIRE(Resp1.dataType().basic().identifier() == "String");
                    const auto Resp2 = Command.responses().at(1);
                    REQUIRE(Resp2.identifier() == "Response2");
                    REQUIRE(Resp2.dataType().type() == IDataType::Type::Basic);
                    REQUIRE(Resp2.dataType().basic().identifier() == "Integer");
                    REQUIRE(Command.errors().empty());
                    AND_THEN("the Command Responses are equal to themselves")
                    {
                        REQUIRE(Resp1 == Resp1);
                        REQUIRE(Resp2 == Resp2);
                    }
                    AND_THEN("the Command Responses are not equal to each other")
                    {
                        REQUIRE(Resp1 != Resp2);
                    }
                }
                AND_THEN("the sixth Command object has the correct Identifier, "
                         "DisplayName, Description and Intermediate Responses")
                {
                    const auto& Command = Commands.at(5);
                    REQUIRE(Command.identifier()
                            == "ObservableCommandWithIntermediateResponses");
                    REQUIRE(Command.displayName()
                            == "Observable Command With Intermediate "
                               "Responses");
                    REQUIRE(Command.description()
                            == "A simple Observable Command with no Parameters, "
                               "no Responses and multiple Intermediate "
                               "Responses");

                    REQUIRE(Command.parameters().empty());
                    REQUIRE(Command.responses().empty());
                    REQUIRE(Command.intermediateResponses().size() == 2);
                    const auto Resp1 = Command.intermediateResponses().at(0);
                    REQUIRE(Resp1.identifier() == "Response1");
                    REQUIRE(Resp1.dataType().type() == IDataType::Type::Basic);
                    REQUIRE(Resp1.dataType().basic().identifier() == "String");
                    const auto Resp2 = Command.intermediateResponses().at(1);
                    REQUIRE(Resp2.identifier() == "Response2");
                    REQUIRE(Resp2.dataType().type() == IDataType::Type::Basic);
                    REQUIRE(Resp2.dataType().basic().identifier() == "Integer");
                    REQUIRE(Command.errors().empty());
                    AND_THEN("the Command Responses are equal to themselves")
                    {
                        REQUIRE(Resp1 == Resp1);
                        REQUIRE(Resp2 == Resp2);
                    }
                    AND_THEN("the Command Responses are not equal to each other")
                    {
                        REQUIRE(Resp1 != Resp2);
                    }
                }
                AND_THEN("the seventh Command object has the correct Identifier, "
                         "DisplayName, Description and Errors")
                {
                    const auto& Command = Commands.at(6);
                    REQUIRE(Command.identifier()
                            == "ObservableCommandWithErrors");
                    REQUIRE(Command.displayName()
                            == "Observable Command With Errors");
                    REQUIRE(Command.description()
                            == "A simple Observable Command with Errors");

                    REQUIRE(Command.parameters().empty());
                    REQUIRE(Command.responses().empty());
                    REQUIRE(Command.errors().size() == 2);
                    REQUIRE(Command.error(0).identifier() == "Error");
                    REQUIRE(Command.error(1).identifier() == "Error2");
                }
            }
            AND_THEN("this CFeature object is equal to itself")
            {
                REQUIRE(Feature == Feature);
            }
        }
    }
}

//============================================================================
SCENARIO("Deserialize a Feature Definition with a Data Type Definition",
         "[fdl][dtd]")
{
    QLoggingCategory::setFilterRules("*.debug=true\n*.info=true");
    GIVEN("A Feature Definition read from a file")
    {
        auto FDLFile = QFile{":/fdl/DataTypeDefinition.sila.xml"};
        REQUIRE(FDLFile.open(QFile::ReadOnly | QFile::Text));
        const auto Switcher = GENERATE(1, 2);
        const auto FDLFileContent = [&Switcher, &FDLFile]() -> QString {
            if (Switcher == 1)
            {
                SUCCEED("Testing deserialization");
                return FDLFile.readAll();
            }
            else
            {
                SUCCEED("Testing serialization");
                // (since deserialization is guaranteed to work we can test if the
                // serialization works by deserializing the resulting FDL again
                // and checking it)
                CFeature TempFeature;
                REQUIRE(
                    CFDLSerializer::deserialize(TempFeature, FDLFile.readAll()));
                return CFDLSerializer::serialize(TempFeature);
            }
        }();

        WHEN("this Feature Definition is deserialized into a CFeature object")
        {
            CFeature Feature;
            REQUIRE(CFDLSerializer::deserialize(Feature, FDLFileContent));
            THEN("the deserialized CFeature object has exactly one Command "
                 "object")
            {
                const auto Commands = Feature.commands();
                REQUIRE(Commands.size() == 1);

                const auto& Command = Commands.front();
                AND_THEN("the Command object has the correct Identifier, "
                         "DisplayName, Description and Parameter")
                {
                    REQUIRE(Command.identifier() == "CommandWithParameter");
                    REQUIRE(Command.displayName() == "Command With Parameter");
                    REQUIRE(Command.description()
                            == "A Command with one Parameter");

                    REQUIRE(Command.responses().empty());
                    REQUIRE(Command.parameters().size() == 1);
                    const auto Param1 = Command.parameters().at(0);
                    REQUIRE(Param1.identifier() == "Parameter1");
                    REQUIRE(Param1.dataType().type()
                            == IDataType::Type::Identifier);
                    REQUIRE(Param1.dataType().identifier().identifier()
                            == "BasicDataType");
                }
                AND_THEN("the Command is Unobservable")
                {
                    REQUIRE_FALSE(Command.isObservable());
                }
            }
            AND_THEN("the deserialized CFeature object has multiple "
                     "DataTypeDefinition object")
            {
                constexpr auto NumDataTypeDefinitions = 22;
                const auto DataTypeDefinitions = Feature.dataTypeDefinitions();
                REQUIRE(DataTypeDefinitions.size() == NumDataTypeDefinitions);

                static int i = 0;
                // we run through this twice: first for testing deserialization
                // and then for serialization, as well, so we have to reset `i`
                // when we come here the second time
                i %= NumDataTypeDefinitions;

                AND_THEN("the Definition object is a Basic Data Type "
                         "Definition")
                {
                    const auto& Definition = DataTypeDefinitions.at(i++);
                    REQUIRE(Definition.identifier() == "BasicDataType");
                    REQUIRE(Definition.displayName() == "Basic Data Type");
                    REQUIRE(Definition.description()
                            == "A Basic Data Type Definition");

                    REQUIRE(Definition.dataType().type()
                            == IDataType::Type::Basic);

                    REQUIRE_NOTHROW(Definition.dataType().basic());
                    REQUIRE_THROWS_AS(Definition.dataType().constrained(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().identifier(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().list(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().structure(),
                                      std::bad_cast);

                    REQUIRE(Definition.dataType().basic().identifier() == "Real");
                }
                AND_THEN("Definition object is a List Data Type Definition")
                {
                    const auto& Definition = DataTypeDefinitions.at(i++);
                    REQUIRE(Definition.identifier() == "ListDataType");
                    REQUIRE(Definition.displayName() == "List Data Type");
                    REQUIRE(Definition.description()
                            == "A List Data Type Definition");

                    REQUIRE(Definition.dataType().type()
                            == IDataType::Type::List);

                    REQUIRE_NOTHROW(Definition.dataType().list());
                    REQUIRE_THROWS_AS(Definition.dataType().basic(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().constrained(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().identifier(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().structure(),
                                      std::bad_cast);

                    const auto ListType = Definition.dataType().list().dataType();
                    REQUIRE(ListType.type() == IDataType::Type::Basic);
                    REQUIRE(ListType.basic().identifier() == "Time");
                }
                AND_THEN("the Definition object is a List Of Constrained Data "
                         "Type Definition")
                {
                    const auto& Definition = DataTypeDefinitions.at(i++);
                    REQUIRE(Definition.identifier()
                            == "ListOfConstrainedDataType");
                    REQUIRE(Definition.displayName()
                            == "List Of Constrained Data Type");
                    REQUIRE(Definition.description()
                            == "A List Of Constrained Data Type Definition");

                    REQUIRE(Definition.dataType().type()
                            == IDataType::Type::List);

                    REQUIRE_NOTHROW(Definition.dataType().list());
                    REQUIRE_THROWS_AS(Definition.dataType().basic(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().constrained(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().identifier(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().structure(),
                                      std::bad_cast);

                    const auto ListType = Definition.dataType().list().dataType();
                    REQUIRE(ListType.type() == IDataType::Type::Constrained);
                    const auto ConstrainedType =
                        ListType.constrained().dataType();
                    REQUIRE(ConstrainedType.type() == IDataType::Type::Basic);
                    REQUIRE(ConstrainedType.basic().identifier() == "String");
                }
                AND_THEN("the Definition object is a List Of Structure Data Type "
                         "Definition")
                {
                    const auto& Definition = DataTypeDefinitions.at(i++);
                    REQUIRE(Definition.identifier() == "ListOfStructureDataType");
                    REQUIRE(Definition.displayName()
                            == "List Of Structure Data Type");
                    REQUIRE(Definition.description()
                            == "A List Of Structure Data Type Definition");

                    REQUIRE(Definition.dataType().type()
                            == IDataType::Type::List);

                    REQUIRE_NOTHROW(Definition.dataType().list());
                    REQUIRE_THROWS_AS(Definition.dataType().basic(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().constrained(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().identifier(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().structure(),
                                      std::bad_cast);

                    const auto ListType = Definition.dataType().list().dataType();
                    REQUIRE(ListType.type() == IDataType::Type::Structure);

                    auto StructureElements = ListType.structure().elements();
                    REQUIRE(StructureElements.size() == 2);

                    const auto& X = StructureElements.at(0);
                    REQUIRE(X.identifier() == "X");
                    REQUIRE(X.displayName() == "X");
                    REQUIRE(X.description() == "X coordinate");
                    REQUIRE(X.dataType().type() == IDataType::Type::Basic);
                    REQUIRE(X.dataType().basic().identifier() == "Real");

                    const auto& Y = StructureElements.at(1);
                    REQUIRE(Y.identifier() == "Y");
                    REQUIRE(Y.displayName() == "Y");
                    REQUIRE(Y.description() == "Y coordinate");
                    REQUIRE(Y.dataType().type() == IDataType::Type::Basic);
                    REQUIRE(Y.dataType().basic().identifier() == "Real");
                }
                AND_THEN("the Definition object is a Structure Data Type "
                         "Definition")
                {
                    const auto& Definition = DataTypeDefinitions.at(i++);
                    REQUIRE(Definition.identifier() == "StructureDataType");
                    REQUIRE(Definition.displayName() == "Structure Data Type");
                    REQUIRE(Definition.description()
                            == "A Structure Data Type Definition");

                    REQUIRE(Definition.dataType().type()
                            == IDataType::Type::Structure);

                    REQUIRE_NOTHROW(Definition.dataType().structure());
                    REQUIRE_THROWS_AS(Definition.dataType().basic(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().constrained(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().identifier(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().list(),
                                      std::bad_cast);

                    auto StructureElements =
                        Definition.dataType().structure().elements();
                    REQUIRE(StructureElements.size() == 2);

                    const auto& X = StructureElements.at(0);
                    REQUIRE(X.identifier() == "X");
                    REQUIRE(X.displayName() == "X");
                    REQUIRE(X.description() == "X coordinate");
                    REQUIRE(X.dataType().type() == IDataType::Type::Basic);
                    REQUIRE(X.dataType().basic().identifier() == "Real");

                    const auto& Y = StructureElements.at(1);
                    REQUIRE(Y.identifier() == "Y");
                    REQUIRE(Y.displayName() == "Y");
                    REQUIRE(Y.description() == "Y coordinate");
                    REQUIRE(Y.dataType().type() == IDataType::Type::Basic);
                    REQUIRE(Y.dataType().basic().identifier() == "Real");
                }
                AND_THEN("the Definition object is a Structure Of Constrained "
                         "Data Type Definition")
                {
                    const auto& Definition = DataTypeDefinitions.at(i++);
                    REQUIRE(Definition.identifier()
                            == "StructureOfConstrainedDataType");
                    REQUIRE(Definition.displayName()
                            == "Structure Of Constrained Data Type");
                    REQUIRE(Definition.description()
                            == "A Structure Of Constrained Data Type Definition");

                    REQUIRE(Definition.dataType().type()
                            == IDataType::Type::Structure);

                    REQUIRE_NOTHROW(Definition.dataType().structure());
                    REQUIRE_THROWS_AS(Definition.dataType().basic(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().constrained(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().identifier(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().list(),
                                      std::bad_cast);

                    auto StructureElements =
                        Definition.dataType().structure().elements();
                    REQUIRE(StructureElements.size() == 2);

                    const auto& X = StructureElements.at(0);
                    REQUIRE(X.identifier() == "X");
                    REQUIRE(X.displayName() == "X");
                    REQUIRE(X.description() == "X coordinate");
                    REQUIRE(X.dataType().type() == IDataType::Type::Constrained);
                    REQUIRE(X.dataType().constrained().dataType().type()
                            == IDataType::Type::Basic);
                    REQUIRE(
                        X.dataType().constrained().dataType().basic().identifier()
                        == "Real");

                    const auto& Y = StructureElements.at(1);
                    REQUIRE(Y.identifier() == "Y");
                    REQUIRE(Y.displayName() == "Y");
                    REQUIRE(Y.description() == "Y coordinate");
                    REQUIRE(Y.dataType().type() == IDataType::Type::Constrained);
                    REQUIRE(Y.dataType().constrained().dataType().type()
                            == IDataType::Type::Basic);
                    REQUIRE(
                        Y.dataType().constrained().dataType().basic().identifier()
                        == "Real");
                }
                AND_THEN("the Definition object is a Structure Of List Data Type "
                         "Definition")
                {
                    const auto& Definition = DataTypeDefinitions.at(i++);
                    REQUIRE(Definition.identifier() == "StructureOfListDataType");
                    REQUIRE(Definition.displayName()
                            == "Structure Of List Data Type");
                    REQUIRE(Definition.description()
                            == "A Structure Of List Data Type Definition");

                    REQUIRE(Definition.dataType().type()
                            == IDataType::Type::Structure);

                    REQUIRE_NOTHROW(Definition.dataType().structure());
                    REQUIRE_THROWS_AS(Definition.dataType().basic(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().constrained(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().identifier(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().list(),
                                      std::bad_cast);

                    auto StructureElements =
                        Definition.dataType().structure().elements();
                    REQUIRE(StructureElements.size() == 2);

                    const auto& X = StructureElements.at(0);
                    REQUIRE(X.identifier() == "Xs");
                    REQUIRE(X.displayName() == "Xs");
                    REQUIRE(X.description() == "X coordinates");
                    REQUIRE(X.dataType().type() == IDataType::Type::List);
                    const auto XListType = X.dataType().list().dataType();
                    REQUIRE(XListType.type() == IDataType::Type::Basic);
                    REQUIRE(XListType.basic().identifier() == "Real");

                    const auto& Y = StructureElements.at(1);
                    REQUIRE(Y.identifier() == "Ys");
                    REQUIRE(Y.displayName() == "Ys");
                    REQUIRE(Y.description() == "Y coordinates");
                    REQUIRE(Y.dataType().type() == IDataType::Type::List);
                    const auto YListType = Y.dataType().list().dataType();
                    REQUIRE(YListType.type() == IDataType::Type::Basic);
                    REQUIRE(YListType.basic().identifier() == "Real");
                }
                AND_THEN("the Definition object is a Structure Of List Of "
                         "Constrained Data Type "
                         "Definition")
                {
                    const auto& Definition = DataTypeDefinitions.at(i++);
                    REQUIRE(Definition.identifier()
                            == "StructureOfListOfConstrainedDataType");
                    REQUIRE(Definition.displayName()
                            == "Structure Of List Of Constrained Data Type");
                    REQUIRE(Definition.description()
                            == "A Structure Of List Of Constrained Data Type "
                               "Definition");

                    REQUIRE(Definition.dataType().type()
                            == IDataType::Type::Structure);

                    REQUIRE_NOTHROW(Definition.dataType().structure());
                    REQUIRE_THROWS_AS(Definition.dataType().basic(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().constrained(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().identifier(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().list(),
                                      std::bad_cast);

                    auto StructureElements =
                        Definition.dataType().structure().elements();
                    REQUIRE(StructureElements.size() == 2);

                    const auto& X = StructureElements.at(0);
                    REQUIRE(X.identifier() == "Xs");
                    REQUIRE(X.displayName() == "Xs");
                    REQUIRE(X.description() == "X coordinates");
                    REQUIRE(X.dataType().type() == IDataType::Type::List);
                    const auto XListType = X.dataType().list().dataType();
                    REQUIRE(XListType.type() == IDataType::Type::Constrained);
                    const auto XConstrainedType =
                        XListType.constrained().dataType();
                    REQUIRE(XConstrainedType.type() == IDataType::Type::Basic);
                    REQUIRE(XConstrainedType.basic().identifier() == "Real");

                    const auto& Y = StructureElements.at(1);
                    REQUIRE(Y.identifier() == "Ys");
                    REQUIRE(Y.displayName() == "Ys");
                    REQUIRE(Y.description() == "Y coordinates");
                    REQUIRE(Y.dataType().type() == IDataType::Type::List);
                    const auto YListType = Y.dataType().list().dataType();
                    REQUIRE(YListType.type() == IDataType::Type::Constrained);
                    const auto YConstrainedType =
                        YListType.constrained().dataType();
                    REQUIRE(YConstrainedType.type() == IDataType::Type::Basic);
                    REQUIRE(YConstrainedType.basic().identifier() == "Real");
                }
                AND_THEN("the Definition object is a Structure Of Structure Data "
                         "Type Definition")
                {
                    const auto& Definition = DataTypeDefinitions.at(i++);
                    REQUIRE(Definition.identifier()
                            == "StructureOfStructureDataType");
                    REQUIRE(Definition.displayName()
                            == "Structure Of Structure Data Type");
                    REQUIRE(Definition.description()
                            == "A Structure Of Structure Data Type Definition");

                    REQUIRE(Definition.dataType().type()
                            == IDataType::Type::Structure);

                    REQUIRE_NOTHROW(Definition.dataType().structure());
                    REQUIRE_THROWS_AS(Definition.dataType().basic(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().constrained(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().identifier(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().list(),
                                      std::bad_cast);

                    auto StructureElements =
                        Definition.dataType().structure().elements();
                    REQUIRE(StructureElements.size() == 2);

                    const auto& A = StructureElements.at(0);
                    REQUIRE(A.identifier() == "A");
                    REQUIRE(A.displayName() == "A");
                    REQUIRE(A.description() == "Point A");
                    REQUIRE(A.dataType().type() == IDataType::Type::Structure);
                    auto AStructureElements = A.dataType().structure().elements();
                    REQUIRE(AStructureElements.size() == 2);
                    const auto& AX = AStructureElements.at(0);
                    REQUIRE(AX.identifier() == "X");
                    REQUIRE(AX.displayName() == "X");
                    REQUIRE(AX.description() == "X coordinate");
                    REQUIRE(AX.dataType().type() == IDataType::Type::Basic);
                    REQUIRE(AX.dataType().basic().identifier() == "Real");
                    const auto& AY = AStructureElements.at(1);
                    REQUIRE(AY.identifier() == "Y");
                    REQUIRE(AY.displayName() == "Y");
                    REQUIRE(AY.description() == "Y coordinate");
                    REQUIRE(AY.dataType().type() == IDataType::Type::Basic);
                    REQUIRE(AY.dataType().basic().identifier() == "Real");

                    const auto& B = StructureElements.at(1);
                    REQUIRE(B.identifier() == "B");
                    REQUIRE(B.displayName() == "B");
                    REQUIRE(B.description() == "Point B");
                    REQUIRE(B.dataType().type() == IDataType::Type::Structure);
                    auto BStructureElements = B.dataType().structure().elements();
                    REQUIRE(BStructureElements.size() == 2);
                    const auto& BX = BStructureElements.at(0);
                    REQUIRE(BX.identifier() == "X");
                    REQUIRE(BX.displayName() == "X");
                    REQUIRE(BX.description() == "X coordinate");
                    REQUIRE(BX.dataType().type() == IDataType::Type::Basic);
                    REQUIRE(BX.dataType().basic().identifier() == "Real");
                    const auto& BY = BStructureElements.at(1);
                    REQUIRE(BY.identifier() == "Y");
                    REQUIRE(BY.displayName() == "Y");
                    REQUIRE(BY.description() == "Y coordinate");
                    REQUIRE(BY.dataType().type() == IDataType::Type::Basic);
                    REQUIRE(BY.dataType().basic().identifier() == "Real");
                }
                AND_THEN("the Definition object is a Length Constraint Data Type "
                         "Definition")
                {
                    const auto& Definition = DataTypeDefinitions.at(i++);
                    REQUIRE(Definition.identifier()
                            == "LengthConstrainedDataType");
                    REQUIRE(Definition.displayName()
                            == "Length Constrained Data Type");
                    REQUIRE(Definition.description()
                            == "A Length Constrained Data Type Definition");

                    REQUIRE(Definition.dataType().type()
                            == IDataType::Type::Constrained);

                    REQUIRE_NOTHROW(Definition.dataType().constrained());
                    REQUIRE_THROWS_AS(Definition.dataType().basic(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().identifier(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().list(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().structure(),
                                      std::bad_cast);

                    const auto ConstrainedType =
                        Definition.dataType().constrained();

                    const auto DataType = ConstrainedType.dataType();
                    REQUIRE(DataType.type() == IDataType::Type::Basic);
                    REQUIRE(DataType.basic().identifier() == "String");

                    REQUIRE(ConstrainedType.constraints().size() == 1);
                    const auto Constraint = ConstrainedType.constraints().at(0);
                    REQUIRE(Constraint.type() == IConstraint::Type::Length);
                    REQUIRE(Constraint.length().value() == 20);
                }
                AND_THEN("the Definition object is a Min Max Length Constraint "
                         "Data Type Definition")
                {
                    const auto& Definition = DataTypeDefinitions.at(i++);
                    REQUIRE(Definition.identifier()
                            == "MinMaxLengthConstrainedDataType");
                    REQUIRE(Definition.displayName()
                            == "Min Max Length Constrained Data Type");
                    REQUIRE(Definition.description()
                            == "A Min Max Length Constrained Data Type "
                               "Definition");

                    REQUIRE(Definition.dataType().type()
                            == IDataType::Type::Constrained);

                    REQUIRE_NOTHROW(Definition.dataType().constrained());
                    REQUIRE_THROWS_AS(Definition.dataType().basic(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().identifier(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().list(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().structure(),
                                      std::bad_cast);

                    const auto ConstrainedType =
                        Definition.dataType().constrained();

                    const auto DataType = ConstrainedType.dataType();
                    REQUIRE(DataType.type() == IDataType::Type::Basic);
                    REQUIRE(DataType.basic().identifier() == "String");

                    REQUIRE(ConstrainedType.constraints().size() == 2);
                    const auto MinConstraint =
                        ConstrainedType.constraints().at(0);
                    REQUIRE(MinConstraint.type()
                            == IConstraint::Type::MinimalLength);
                    REQUIRE(MinConstraint.minimalLength().value() == 2);
                    const auto MaxConstraint =
                        ConstrainedType.constraints().at(1);
                    REQUIRE(MaxConstraint.type()
                            == IConstraint::Type::MaximalLength);
                    REQUIRE(MaxConstraint.maximalLength().value() == 6);
                }
                AND_THEN("the Definition object is a Element Count Constraint "
                         "Data Type Definition")
                {
                    const auto& Definition = DataTypeDefinitions.at(i++);
                    REQUIRE(Definition.identifier()
                            == "ElementCountConstrainedDataType");
                    REQUIRE(Definition.displayName()
                            == "Element Count Constrained Data Type");
                    REQUIRE(Definition.description()
                            == "An Element Count Constrained Data Type "
                               "Definition");

                    REQUIRE(Definition.dataType().type()
                            == IDataType::Type::Constrained);

                    REQUIRE_NOTHROW(Definition.dataType().constrained());
                    REQUIRE_THROWS_AS(Definition.dataType().basic(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().identifier(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().list(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().structure(),
                                      std::bad_cast);

                    const auto ConstrainedType =
                        Definition.dataType().constrained();

                    REQUIRE(ConstrainedType.dataType().type()
                            == IDataType::Type::List);
                    const auto ListType =
                        ConstrainedType.dataType().list().dataType();
                    REQUIRE(ListType.type() == IDataType::Type::Structure);

                    auto StructureElements = ListType.structure().elements();
                    REQUIRE(StructureElements.size() == 2);

                    const auto& X = StructureElements.at(0);
                    REQUIRE(X.identifier() == "X");
                    REQUIRE(X.displayName() == "X");
                    REQUIRE(X.description() == "X coordinate");
                    REQUIRE(X.dataType().type() == IDataType::Type::Basic);
                    REQUIRE(X.dataType().basic().identifier() == "Real");

                    const auto& Y = StructureElements.at(1);
                    REQUIRE(Y.identifier() == "Y");
                    REQUIRE(Y.displayName() == "Y");
                    REQUIRE(Y.description() == "Y coordinate");
                    REQUIRE(Y.dataType().type() == IDataType::Type::Basic);
                    REQUIRE(Y.dataType().basic().identifier() == "Real");

                    REQUIRE(ConstrainedType.constraints().size() == 1);
                    const auto Constraint = ConstrainedType.constraints().at(0);
                    REQUIRE(Constraint.type() == IConstraint::Type::ElementCount);
                    REQUIRE(Constraint.elementCount().value() == 5);
                }
                AND_THEN("the Definition object is a Min Max Element Count "
                         "Constraint Data Type Definition")
                {
                    const auto& Definition = DataTypeDefinitions.at(i++);
                    REQUIRE(Definition.identifier()
                            == "MinMaxElementCountConstrainedDataType");
                    REQUIRE(Definition.displayName()
                            == "Min Max Element Count Constrained Data Type");
                    REQUIRE(Definition.description()
                            == "A Min Max Element Count Constrained Data Type "
                               "Definition");

                    REQUIRE(Definition.dataType().type()
                            == IDataType::Type::Constrained);

                    REQUIRE_NOTHROW(Definition.dataType().constrained());
                    REQUIRE_THROWS_AS(Definition.dataType().basic(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().identifier(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().list(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().structure(),
                                      std::bad_cast);

                    const auto ConstrainedType =
                        Definition.dataType().constrained();

                    REQUIRE(ConstrainedType.dataType().type()
                            == IDataType::Type::List);
                    const auto ListType =
                        ConstrainedType.dataType().list().dataType();
                    REQUIRE(ListType.type() == IDataType::Type::Basic);
                    REQUIRE(ListType.basic().identifier() == "Integer");

                    REQUIRE(ConstrainedType.constraints().size() == 2);
                    const auto MinConstraint =
                        ConstrainedType.constraints().at(0);
                    REQUIRE(MinConstraint.type()
                            == IConstraint::Type::MinimalElementCount);
                    REQUIRE(MinConstraint.minimalElementCount().value() == 1);
                    const auto MaxConstraint =
                        ConstrainedType.constraints().at(1);
                    REQUIRE(MaxConstraint.type()
                            == IConstraint::Type::MaximalElementCount);
                    REQUIRE(MaxConstraint.maximalElementCount().value() == 10);
                }
                AND_THEN("the Definition object is a Min Max Exclusive Count "
                         "Constraint Data Type Definition")
                {
                    const auto& Definition = DataTypeDefinitions.at(i++);
                    REQUIRE(Definition.identifier()
                            == "MinMaxExclusiveConstrainedDataType");
                    REQUIRE(Definition.displayName()
                            == "Min Max Exclusive Constrained Data Type");
                    REQUIRE(Definition.description()
                            == "A Min Max Exclusive Constrained Data Type "
                               "Definition");

                    REQUIRE(Definition.dataType().type()
                            == IDataType::Type::Constrained);

                    REQUIRE_NOTHROW(Definition.dataType().constrained());
                    REQUIRE_THROWS_AS(Definition.dataType().basic(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().identifier(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().list(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().structure(),
                                      std::bad_cast);

                    const auto ConstrainedType =
                        Definition.dataType().constrained();

                    const auto DataType = ConstrainedType.dataType();
                    REQUIRE(DataType.type() == IDataType::Type::Basic);
                    REQUIRE(DataType.basic().identifier() == "Real");

                    REQUIRE(ConstrainedType.constraints().size() == 2);
                    const auto MinConstraint =
                        ConstrainedType.constraints().at(0);
                    REQUIRE(MinConstraint.type()
                            == IConstraint::Type::MinimalExclusive);
                    REQUIRE(MinConstraint.minimalExclusive().value() == "0.1");
                    const auto MaxConstraint =
                        ConstrainedType.constraints().at(1);
                    REQUIRE(MaxConstraint.type()
                            == IConstraint::Type::MaximalExclusive);
                    REQUIRE(MaxConstraint.maximalExclusive().value() == "1.0");
                }
                AND_THEN("the the Definition object is a Min Max Inclusive Count "
                         "Constraint Data Type Definition")
                {
                    const auto& Definition = DataTypeDefinitions.at(i++);
                    REQUIRE(Definition.identifier()
                            == "MinMaxInclusiveConstrainedDataType");
                    REQUIRE(Definition.displayName()
                            == "Min Max Inclusive Constrained Data Type");
                    REQUIRE(Definition.description()
                            == "A Min Max Inclusive Constrained Data Type "
                               "Definition");

                    REQUIRE(Definition.dataType().type()
                            == IDataType::Type::Constrained);

                    REQUIRE_NOTHROW(Definition.dataType().constrained());
                    REQUIRE_THROWS_AS(Definition.dataType().basic(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().identifier(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().list(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().structure(),
                                      std::bad_cast);

                    const auto ConstrainedType =
                        Definition.dataType().constrained();

                    const auto DataType = ConstrainedType.dataType();
                    REQUIRE(DataType.type() == IDataType::Type::Basic);
                    REQUIRE(DataType.basic().identifier() == "Date");

                    REQUIRE(ConstrainedType.constraints().size() == 2);
                    const auto MinConstraint =
                        ConstrainedType.constraints().at(0);
                    REQUIRE(MinConstraint.type()
                            == IConstraint::Type::MinimalInclusive);
                    REQUIRE(MinConstraint.minimalInclusive().value()
                            == "01.01.1970");
                    const auto MaxConstraint =
                        ConstrainedType.constraints().at(1);
                    REQUIRE(MaxConstraint.type()
                            == IConstraint::Type::MaximalInclusive);
                    REQUIRE(MaxConstraint.maximalInclusive().value()
                            == "31.12.2100");
                }
                AND_THEN("the Definition object is a Fully Qualified Identifier "
                         "Constraint Data Type Definition")
                {
                    const auto& Definition = DataTypeDefinitions.at(i++);
                    REQUIRE(Definition.identifier()
                            == "FullyQualifiedIdentifierConstrainedDataType");
                    REQUIRE(Definition.displayName()
                            == "Fully Qualified Identifier Constrained Data "
                               "Type");
                    REQUIRE(Definition.description()
                            == "A Fully Qualified Identifier Constrained Data "
                               "Type Definition");

                    REQUIRE(Definition.dataType().type()
                            == IDataType::Type::Constrained);

                    REQUIRE_NOTHROW(Definition.dataType().constrained());
                    REQUIRE_THROWS_AS(Definition.dataType().basic(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().identifier(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().list(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().structure(),
                                      std::bad_cast);

                    const auto ConstrainedType =
                        Definition.dataType().constrained();

                    const auto DataType = ConstrainedType.dataType();
                    REQUIRE(DataType.type() == IDataType::Type::Basic);
                    REQUIRE(DataType.basic().identifier() == "String");

                    REQUIRE(ConstrainedType.constraints().size() == 1);
                    const auto Constraint = ConstrainedType.constraints().at(0);
                    REQUIRE(Constraint.type()
                            == IConstraint::Type::FullyQualifiedIdentifier);
                    REQUIRE(Constraint.fullyQualifiedIdentifier().value()
                            == "CommandIdentifier");
                }
                AND_THEN("the Definition object is a Pattern Constraint Data "
                         "Type Definition")
                {
                    const auto& Definition = DataTypeDefinitions.at(i++);
                    REQUIRE(Definition.identifier()
                            == "PatternConstrainedDataType");
                    REQUIRE(Definition.displayName()
                            == "Pattern Constrained Data Type");
                    REQUIRE(Definition.description()
                            == "A Pattern Constrained Data Type Definition");

                    REQUIRE(Definition.dataType().type()
                            == IDataType::Type::Constrained);

                    REQUIRE_NOTHROW(Definition.dataType().constrained());
                    REQUIRE_THROWS_AS(Definition.dataType().basic(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().identifier(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().list(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().structure(),
                                      std::bad_cast);

                    const auto ConstrainedType =
                        Definition.dataType().constrained();

                    const auto DataType = ConstrainedType.dataType();
                    REQUIRE(DataType.type() == IDataType::Type::Basic);
                    REQUIRE(DataType.basic().identifier() == "String");

                    REQUIRE(ConstrainedType.constraints().size() == 1);
                    const auto Constraint = ConstrainedType.constraints().at(0);
                    REQUIRE(Constraint.type() == IConstraint::Type::Pattern);
                    REQUIRE(Constraint.pattern().value() == "[a-z]*");
                }
                AND_THEN("the Definition object is a Set Constraint Data Type "
                         "Definition")
                {
                    const auto& Definition = DataTypeDefinitions.at(i++);
                    REQUIRE(Definition.identifier() == "SetConstrainedDataType");
                    REQUIRE(Definition.displayName()
                            == "Set Constrained Data Type");
                    REQUIRE(Definition.description()
                            == "A Set Constrained Data Type Definition");

                    REQUIRE(Definition.dataType().type()
                            == IDataType::Type::Constrained);

                    REQUIRE_NOTHROW(Definition.dataType().constrained());
                    REQUIRE_THROWS_AS(Definition.dataType().basic(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().identifier(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().list(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().structure(),
                                      std::bad_cast);

                    const auto ConstrainedType =
                        Definition.dataType().constrained();

                    const auto DataType = ConstrainedType.dataType();
                    REQUIRE(DataType.type() == IDataType::Type::Basic);
                    REQUIRE(DataType.basic().identifier() == "Integer");

                    REQUIRE(ConstrainedType.constraints().size() == 1);
                    const auto Constraint = ConstrainedType.constraints().at(0);
                    REQUIRE(Constraint.type() == IConstraint::Type::Set);
                    const auto Set = Constraint.set();
                    REQUIRE(Set.values().size() == 7);
                    for (int j = 0; j < Set.values().size(); ++j)
                    {
                        REQUIRE(Set.value(j).toInt() == std::pow(2, j));
                    }
                }
                AND_THEN("the Definition object is an Allowed Types Constraint "
                         "Data Type Definition")
                {
                    const auto& Definition = DataTypeDefinitions.at(i++);
                    REQUIRE(Definition.identifier()
                            == "AllowedTypesConstrainedDataType");
                    REQUIRE(Definition.displayName()
                            == "Allowed Types Constrained Data Type");
                    REQUIRE(Definition.description()
                            == "An Allowed Types Constrained Data Type "
                               "Definition");

                    REQUIRE(Definition.dataType().type()
                            == IDataType::Type::Constrained);

                    REQUIRE_NOTHROW(Definition.dataType().constrained());
                    REQUIRE_THROWS_AS(Definition.dataType().basic(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().identifier(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().list(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().structure(),
                                      std::bad_cast);

                    const auto ConstrainedType =
                        Definition.dataType().constrained();

                    const auto DataType = ConstrainedType.dataType();
                    REQUIRE(DataType.type() == IDataType::Type::Basic);
                    REQUIRE(DataType.basic().identifier() == "Any");

                    REQUIRE(ConstrainedType.constraints().size() == 1);
                    const auto Constraint = ConstrainedType.constraints().at(0);
                    REQUIRE(Constraint.type() == IConstraint::Type::AllowedTypes);
                    const auto AllowedTypes = Constraint.allowedTypes();
                    REQUIRE(AllowedTypes.types().size() == 3);

                    const auto& AllowedType1 = AllowedTypes.type(0);
                    REQUIRE(AllowedType1.type() == IDataType::Type::Basic);
                    REQUIRE(AllowedType1.basic().identifier() == "Integer");
                    const auto& AllowedType2 = AllowedTypes.type(1);
                    REQUIRE(AllowedType2.type() == IDataType::Type::Basic);
                    REQUIRE(AllowedType2.basic().identifier() == "Real");
                    const auto& AllowedType3 = AllowedTypes.type(2);
                    REQUIRE(AllowedType3.type() == IDataType::Type::Basic);
                    REQUIRE(AllowedType3.basic().identifier() == "Boolean");
                }
                AND_THEN("the Definition object is a Content Type Constraint "
                         "Data Type Definition")
                {
                    const auto& Definition = DataTypeDefinitions.at(i++);
                    REQUIRE(Definition.identifier()
                            == "ContentTypeConstrainedDataType");
                    REQUIRE(Definition.displayName()
                            == "Content Type Constrained Data Type");
                    REQUIRE(Definition.description()
                            == "A Content Type Constrained Data Type Definition");

                    REQUIRE(Definition.dataType().type()
                            == IDataType::Type::Constrained);

                    REQUIRE_NOTHROW(Definition.dataType().constrained());
                    REQUIRE_THROWS_AS(Definition.dataType().basic(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().identifier(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().list(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().structure(),
                                      std::bad_cast);

                    const auto ConstrainedType =
                        Definition.dataType().constrained();

                    const auto DataType = ConstrainedType.dataType();
                    REQUIRE(DataType.type() == IDataType::Type::Basic);
                    REQUIRE(DataType.basic().identifier() == "String");

                    REQUIRE(ConstrainedType.constraints().size() == 1);
                    const auto Constraint = ConstrainedType.constraints().at(0);
                    REQUIRE(Constraint.type() == IConstraint::Type::ContentType);
                    const auto ContentType = Constraint.contentType();

                    REQUIRE(ContentType.contentType() == "application");
                    REQUIRE(ContentType.subType() == "xml");
                    REQUIRE(ContentType.parameters().size() == 2);
                    REQUIRE(ContentType.parameter(0).attribute() == "Dummy");
                    REQUIRE(ContentType.parameter(0).value() == "Whatever");
                    REQUIRE(ContentType.parameter(1).attribute() == "Dummy2");
                    REQUIRE(ContentType.parameter(1).value() == "Whatever2");
                }
                AND_THEN("the Definition object is a Schema Constraint Data Type "
                         "Definition")
                {
                    const auto& Definition = DataTypeDefinitions.at(i++);
                    REQUIRE(Definition.identifier()
                            == "SchemaConstrainedDataType");
                    REQUIRE(Definition.displayName()
                            == "Schema Constrained Data Type");
                    REQUIRE(Definition.description()
                            == "A Schema Constrained Data Type Definition");

                    REQUIRE(Definition.dataType().type()
                            == IDataType::Type::Constrained);

                    REQUIRE_NOTHROW(Definition.dataType().constrained());
                    REQUIRE_THROWS_AS(Definition.dataType().basic(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().identifier(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().list(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().structure(),
                                      std::bad_cast);

                    const auto ConstrainedType =
                        Definition.dataType().constrained();

                    const auto DataType = ConstrainedType.dataType();
                    REQUIRE(DataType.type() == IDataType::Type::Basic);
                    REQUIRE(DataType.basic().identifier() == "Binary");

                    REQUIRE(ConstrainedType.constraints().size() == 2);

                    const auto SchemaConstraint =
                        ConstrainedType.constraints().at(0);
                    REQUIRE(SchemaConstraint.type() == IConstraint::Type::Schema);
                    const auto Schema = SchemaConstraint.schema();
                    REQUIRE(Schema.schemaType()
                            == CConstraintSchema::SchemaType::Xml);
                    REQUIRE(Schema.url()
                            == "https://gitlab.com/SiLA2/sila_base/raw/master/"
                               "schema/FeatureDefinition.xsd");

                    const auto ContentTypeConstraint =
                        ConstrainedType.constraints().at(1);
                    REQUIRE(ContentTypeConstraint.type()
                            == IConstraint::Type::ContentType);
                    const auto ContentType = ContentTypeConstraint.contentType();
                    REQUIRE(ContentType.contentType() == "application");
                    REQUIRE(ContentType.subType() == "xml");
                    REQUIRE(ContentType.parameters().isEmpty());
                }
                AND_THEN("the Definition object is a Unit Constraint Data Type "
                         "Definition")
                {
                    const auto& Definition = DataTypeDefinitions.at(i++);
                    REQUIRE(Definition.identifier() == "UnitConstrainedDataType");
                    REQUIRE(Definition.displayName()
                            == "Unit Constrained Data Type");
                    REQUIRE(Definition.description()
                            == "A Unit Constrained Data Type Definition");

                    REQUIRE(Definition.dataType().type()
                            == IDataType::Type::Constrained);

                    REQUIRE_NOTHROW(Definition.dataType().constrained());
                    REQUIRE_THROWS_AS(Definition.dataType().basic(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().identifier(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().list(),
                                      std::bad_cast);
                    REQUIRE_THROWS_AS(Definition.dataType().structure(),
                                      std::bad_cast);

                    const auto ConstrainedType =
                        Definition.dataType().constrained();

                    const auto DataType = ConstrainedType.dataType();
                    REQUIRE(DataType.type() == IDataType::Type::Basic);
                    REQUIRE(DataType.basic().identifier() == "Real");

                    REQUIRE(ConstrainedType.constraints().size() == 1);

                    const auto UnitConstraint =
                        ConstrainedType.constraints().at(0);
                    REQUIRE(UnitConstraint.type() == IConstraint::Type::Unit);
                    const auto Unit = UnitConstraint.unit();
                    REQUIRE(Unit.label() == "N");
                    REQUIRE(Unit.conversionFactor() == 1);
                    REQUIRE(Unit.conversionOffset() == 0);

                    REQUIRE(Unit.siBaseUnit().size() == 3);
                    const auto UnitComponentKilogram = Unit.siBaseUnit().at(0);
                    REQUIRE(UnitComponentKilogram.siUnit()
                            == CUnitComponent::SIUnit::Kilogram);
                    REQUIRE(UnitComponentKilogram.exponent() == 1);
                    const auto UnitComponentMeter = Unit.siBaseUnit().at(1);
                    REQUIRE(UnitComponentMeter.siUnit()
                            == CUnitComponent::SIUnit::Meter);
                    REQUIRE(UnitComponentMeter.exponent() == 1);
                    const auto UnitComponentSecond = Unit.siBaseUnit().at(2);
                    REQUIRE(UnitComponentSecond.siUnit()
                            == CUnitComponent::SIUnit::Second);
                    REQUIRE(UnitComponentSecond.exponent() == -2);
                }
            }
            AND_THEN("this CFeature object is equal to itself")
            {
                REQUIRE(Feature == Feature);
            }
        }
    }
}

//============================================================================
SCENARIO("Deserialize a Feature Definition with Properties", "[fdl][property]")
{
    QLoggingCategory::setFilterRules("*.debug=true\n*.info=true");
    GIVEN("A Feature Definition read from a file")
    {
        auto FDLFile = QFile{":/fdl/Property.sila.xml"};
        REQUIRE(FDLFile.open(QFile::ReadOnly | QFile::Text));
        const auto Switcher = GENERATE(1, 2);
        const auto FDLFileContent = [&Switcher, &FDLFile]() -> QString {
            if (Switcher == 1)
            {
                SUCCEED("Testing deserialization");
                return FDLFile.readAll();
            }
            else
            {
                SUCCEED("Testing serialization");
                // (since deserialization is guaranteed to work we can test if the
                // serialization works by deserializing the resulting FDL again
                // and checking it)
                CFeature TempFeature;
                REQUIRE(
                    CFDLSerializer::deserialize(TempFeature, FDLFile.readAll()));
                return CFDLSerializer::serialize(TempFeature);
            }
        }();

        WHEN("this Feature Definition is deserialized into a CFeature object")
        {
            CFeature Feature;
            REQUIRE(CFDLSerializer::deserialize(Feature, FDLFileContent));
            THEN("the deserialized CFeature object has two Property objects")
            {
                const auto Properties = Feature.properties();
                REQUIRE(Properties.size() == 4);

                AND_THEN("the first Property is an Unobservable Property")
                {
                    const auto& Unobservable = Properties.at(0);
                    REQUIRE_FALSE(Unobservable.isObservable());
                    AND_THEN("the Property has the correct Identifier, "
                             "DisplayName and Description")
                    {
                        REQUIRE(Unobservable.identifier()
                                == "UnobservableProperty");
                        REQUIRE(Unobservable.displayName()
                                == "Unobservable Property");
                        REQUIRE(Unobservable.description()
                                == "A simple Unobservable Property");
                        REQUIRE(Unobservable.dataType().type()
                                == IDataType::Type::Basic);
                        REQUIRE(Unobservable.dataType().basic().identifier()
                                == "String");
                        REQUIRE(Unobservable.errors().empty());
                    }
                    AND_THEN("the first Property is equal to itself")
                    {
                        REQUIRE(Unobservable == Unobservable);
                    }
                }
                AND_THEN("the second Property is an Observable Property")
                {
                    const auto& Observable = Properties.at(1);
                    REQUIRE(Observable.isObservable());
                    AND_THEN("the Property has the correct Identifier, "
                             "DisplayName and Description")
                    {
                        REQUIRE(Observable.identifier() == "ObservableProperty");
                        REQUIRE(Observable.displayName()
                                == "Observable Property");
                        REQUIRE(Observable.description()
                                == "A simple Observable Property");
                        REQUIRE(Observable.dataType().type()
                                == IDataType::Type::Basic);
                        REQUIRE(Observable.dataType().basic().identifier()
                                == "String");
                        REQUIRE(Observable.errors().empty());
                    }
                    AND_THEN("the second Property is equal to itself")
                    {
                        REQUIRE(Observable == Observable);
                    }
                }
                AND_THEN("the third Property is an Unobservable Property with "
                         "Errors")
                {
                    const auto& Unobservable = Properties.at(2);
                    REQUIRE_FALSE(Unobservable.isObservable());
                    AND_THEN("the Property has the correct Identifier, "
                             "DisplayName, Description and Errors")
                    {
                        REQUIRE(Unobservable.identifier()
                                == "UnobservablePropertyWithErrors");
                        REQUIRE(Unobservable.displayName()
                                == "Unobservable Property With Errors");
                        REQUIRE(Unobservable.description()
                                == "A simple Unobservable Property With Errors");
                        REQUIRE(Unobservable.dataType().type()
                                == IDataType::Type::Basic);
                        REQUIRE(Unobservable.dataType().basic().identifier()
                                == "String");
                        REQUIRE(Unobservable.errors().size() == 2);
                        REQUIRE(Unobservable.error(0).identifier() == "Error");
                        REQUIRE(Unobservable.error(1).identifier() == "Error2");
                    }
                    AND_THEN("the third Property is equal to itself")
                    {
                        REQUIRE(Unobservable == Unobservable);
                    }
                }
                AND_THEN("the fourth Property is an Observable Property with "
                         "Errors")
                {
                    const auto& Observable = Properties.at(3);
                    REQUIRE(Observable.isObservable());
                    AND_THEN("the Property has the correct Identifier, "
                             "DisplayName and Description")
                    {
                        REQUIRE(Observable.identifier()
                                == "ObservablePropertyWithErrors");
                        REQUIRE(Observable.displayName()
                                == "Observable Property With Errors");
                        REQUIRE(Observable.description()
                                == "A simple Observable Property With Errors");
                        REQUIRE(Observable.dataType().type()
                                == IDataType::Type::Basic);
                        REQUIRE(Observable.dataType().basic().identifier()
                                == "String");
                        REQUIRE(Observable.errors().size() == 2);
                        REQUIRE(Observable.error(0).identifier() == "Error");
                        REQUIRE(Observable.error(1).identifier() == "Error2");
                    }
                    AND_THEN("the fourth Property is equal to itself")
                    {
                        REQUIRE(Observable == Observable);
                    }
                }
            }
            AND_THEN("this CFeature object is equal to itself")
            {
                REQUIRE(Feature == Feature);
            }
        }
    }
}

//============================================================================
SCENARIO("Deserialize a Feature Definition with a Defined Execution Error",
         "[fdl][error]")
{
    QLoggingCategory::setFilterRules("*.debug=true\n*.info=true");
    GIVEN("A Feature Definition read from a file")
    {
        auto FDLFile = QFile{":/fdl/DefinedExecutionError.sila.xml"};
        REQUIRE(FDLFile.open(QFile::ReadOnly | QFile::Text));
        const auto Switcher = GENERATE(1, 2);
        const auto FDLFileContent = [&Switcher, &FDLFile]() -> QString {
            if (Switcher == 1)
            {
                SUCCEED("Testing deserialization");
                return FDLFile.readAll();
            }
            else
            {
                SUCCEED("Testing serialization");
                // (since deserialization is guaranteed to work we can test if the
                // serialization works by deserializing the resulting FDL again
                // and checking it)
                CFeature TempFeature;
                REQUIRE(
                    CFDLSerializer::deserialize(TempFeature, FDLFile.readAll()));
                return CFDLSerializer::serialize(TempFeature);
            }
        }();

        WHEN("this Feature Definition is deserialized into a CFeature object")
        {
            CFeature Feature;
            REQUIRE(CFDLSerializer::deserialize(Feature, FDLFileContent));
            THEN("the deserialized CFeature object has exactly one Error object")
            {
                const auto Errors = Feature.errors();
                REQUIRE(Errors.size() == 1);

                const auto& Error = Errors.front();
                AND_THEN("the Error has the correct Identifier, DisplayName and "
                         "Description")
                {
                    REQUIRE(Error.identifier() == "SimpleError");
                    REQUIRE(Error.displayName() == "Simple Error");
                    REQUIRE(Error.description() == "A simple Error");
                }
                AND_THEN("the Error is equal to itself")
                {
                    REQUIRE(Error == Error);
                }
            }
            AND_THEN("this CFeature object is equal to itself")
            {
                REQUIRE(Feature == Feature);
            }
        }
    }
}

//============================================================================
SCENARIO("Deserialize a Feature Definition with Metadata", "[fdl][metadata]")
{
    QLoggingCategory::setFilterRules("*.debug=true\n*.info=true");
    GIVEN("A Feature Definition read from a file")
    {
        auto FDLFile = QFile{":/fdl/Metadata.sila.xml"};
        REQUIRE(FDLFile.open(QFile::ReadOnly | QFile::Text));
        const auto Switcher = GENERATE(1, 2);
        const auto FDLFileContent = [&Switcher, &FDLFile]() -> QString {
            if (Switcher == 1)
            {
                SUCCEED("Testing deserialization");
                return FDLFile.readAll();
            }
            else
            {
                SUCCEED("Testing serialization");
                // (since deserialization is guaranteed to work we can test if the
                // serialization works by deserializing the resulting FDL again
                // and checking it)
                CFeature TempFeature;
                REQUIRE(
                    CFDLSerializer::deserialize(TempFeature, FDLFile.readAll()));
                return CFDLSerializer::serialize(TempFeature);
            }
        }();

        WHEN("this Feature Definition is deserialized into a CFeature object")
        {
            CFeature Feature;
            REQUIRE(CFDLSerializer::deserialize(Feature, FDLFileContent));
            THEN("the deserialized CFeature object has exactly two Metadata "
                 "objects")
            {
                const auto Metadata = Feature.metadata();
                REQUIRE(Metadata.size() == 2);

                AND_THEN("the first Metadata has the correct Identifier, "
                         "DisplayName, Description and Data Type")
                {
                    REQUIRE(Metadata.front().identifier() == "SimpleMetadata");
                    REQUIRE(Metadata.front().displayName() == "Simple Metadata");
                    REQUIRE(Metadata.front().description()
                            == "A simple Metadata");
                    const auto DataType = Metadata.front().dataType();
                    REQUIRE(DataType.type() == IDataType::Type::Basic);
                    REQUIRE(DataType.basic().identifier() == "Timestamp");
                }
                AND_THEN("the first Metadata is equal to itself")
                {
                    REQUIRE(Metadata.front() == Metadata.front());
                }
                AND_THEN("the second Metadata has the correct Identifier, "
                         "DisplayName, Description and Data Type as well as "
                         "Errors")
                {
                    REQUIRE(Metadata.back().identifier() == "MetadataWithErrors");
                    REQUIRE(Metadata.back().displayName()
                            == "Metadata With Errors");
                    REQUIRE(Metadata.back().description()
                            == "A Metadata with Errors");
                    const auto DataType = Metadata.back().dataType();
                    REQUIRE(DataType.type() == IDataType::Type::Basic);
                    REQUIRE(DataType.basic().identifier() == "Time");

                    REQUIRE(Metadata.back().errors().size() == 2);
                    REQUIRE(Metadata.back().error(0).identifier() == "Error");
                    REQUIRE(Metadata.back().error(1).identifier() == "Error2");
                }
                AND_THEN("the first Metadata is equal to itself")
                {
                    REQUIRE(Metadata.back() == Metadata.back());
                }
            }
            AND_THEN("this CFeature object is equal to itself")
            {
                REQUIRE(Feature == Feature);
            }
        }
    }
}
