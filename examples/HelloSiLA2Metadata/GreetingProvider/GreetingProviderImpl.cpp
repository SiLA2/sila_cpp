/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   GreetingProviderImpl.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   07.01.2020
/// \brief  Implementation of the CGreetingProviderImpl class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include "GreetingProviderImpl.h"

#include <sila_cpp/common/logging.h>
#include <sila_cpp/framework/data_types/SiLAString.h>
#include <sila_cpp/framework/error_handling/FrameworkError.h>
#include <sila_cpp/framework/error_handling/ValidationError.h>

using namespace std;
using namespace sila2::org::silastandard::examples::greetingprovider::v1;

//============================================================================
CGreetingProviderImpl::CGreetingProviderImpl(SiLA2::CSiLAServer* parent)
    : CSiLAFeature{parent},
      m_SayHelloCommand{this},
      m_StartYearProperty{this, 2022, &CGreetingProviderImpl::getStartYear},
      // the following Commands/Properties are affected by this metadata
      m_AffectedByTestDataProperty{this,
                                   {{SiLA2::CFullyQualifiedCommandID{
                                        fullyQualifiedIdentifier(), "SayHello"}
                                         .toString()},
                                    {SiLA2::CFullyQualifiedPropertyID{
                                        fullyQualifiedIdentifier(), "StartYear"}
                                         .toString()}}}
{
    m_SayHelloCommand.setExecutor(this, &CGreetingProviderImpl::SayHello);
}

SayHello_Responses CGreetingProviderImpl::SayHello(SayHelloWrapper* Command)
{
    const auto Request = Command->parameters();
    qDebug() << "Request contains:" << Request;
    if (!Request.has_name())
    {
        static const SiLA2::CFullyQualifiedCommandParameterID ParamID{
            {fullyQualifiedIdentifier(), "SayHello"}, "Name"};
        throw SiLA2::CValidationError{ParamID, "SayHello command was called "
                                               "without a Name! Specify a Name "
                                               "with at least one character."};
    }

    using FrameworkErrorType = SiLA2::CFrameworkError::FrameworkErrorType;
    if (Request.name().value() == "error")
    {
        qWarning()
            << "SayHello was called with 'error' as Name. A "
               "COMMAND_EXECUTION_NOT_ACCEPTED Framework Error will be thrown.";

        throw SiLA2::CFrameworkError{
            FrameworkErrorType::CommandExecutionNotAccepted};
    }

    try
    {
        const auto Metadata = Command->metadata().value<Metadata_TestData>(
            {fullyQualifiedIdentifier(), "TestData"});
        qDebug() << "Received Metadata" << Metadata;

        auto Response = SayHello_Responses{};
        Response.set_allocated_greeting(
            SiLA2::CString{"Hello SiLA 2 " + Request.name().value() + ' '
                           + Metadata.testdata().value() + "!"}
                .toProtoMessagePtr());
        return Response;
    }
    catch ([[maybe_unused]] const std::out_of_range& err)
    {
        qWarning() << "Didn't receive required 'TestData' metadata";
        throw SiLA2::CFrameworkError{FrameworkErrorType ::InvalidMetadata};
    }
}

//============================================================================
SiLA2::CFullyQualifiedFeatureID CGreetingProviderImpl::fullyQualifiedIdentifier()
    const
{
    static const SiLA2::CFullyQualifiedFeatureID FeatureID{
        "org.silastandard", "examples", "GreetingProvider", "v1"};
    return FeatureID;
}

//============================================================================
SiLA2::CInteger CGreetingProviderImpl::getStartYear(
    const SiLA2::CMetadataContainer& Metadata) const
{
    qDebug() << "Received Metadata" << Metadata;
    return m_StartYearProperty.value();
}
