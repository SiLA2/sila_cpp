# Hello SiLA 2 example with metadata
This example is functionally equivalent to the [HelloSiLA2] example but both Features contain a Metadatum called `TestData`.

Refer to the dedicated [wiki page](https://gitlab.com/SiLA2/sila_cpp/-/wikis/Tutorial/SiLA-Server#metadata) for a detailed explanation of how metadata can be used in sila_cpp.

[HelloSiLA2]: https://gitlab.com/SiLA2/sila_cpp/-/tree/master/examples/HelloSiLA2
