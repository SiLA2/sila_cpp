FROM fmeinicke/grpc-docker:latest

RUN apt-get update \
  && apt-get install -y \
    ssh \
    build-essential \
    gcc \
    g++ \
    gdb \
    clang \
    cmake \
    rsync \
    tar \
    python3-pip \
    git \
    ninja-build \
    qtbase5-dev \
    libssl-dev \
    libavahi-client-dev \
    libavahi-common-dev \
    curl \
  && apt-get clean \
  && pip3 install conan 'jinja2<3.1' gcovr==5.0

# git config --global --add safe.directory /tmp/sila_cpp

RUN ( \
    echo 'LogLevel DEBUG2'; \
    echo 'PermitRootLogin yes'; \
    echo 'PasswordAuthentication yes'; \
    echo 'Subsystem sftp /usr/lib/openssh/sftp-server'; \
  ) > /etc/ssh/sshd_config_test_clion \
  && mkdir /run/sshd

RUN useradd -m user -u 6969 \
  && yes password | passwd user

RUN usermod -s /bin/bash user

CMD ["/usr/sbin/sshd", "-D", "-e", "-f", "/etc/ssh/sshd_config_test_clion"]

## workaround for permission errors from https://www.jetbrains.com/help/clion/2022.2/clion-toolchains-in-docker.html#sample-dockerfile
#ARG UID=1000
#RUN useradd -m -u ${UID} -s /bin/bash builder
#USER builder